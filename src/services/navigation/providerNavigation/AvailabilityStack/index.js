import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import Availability from '../../../../screens/providerFlow/Availability/index';
import updateAvailability from '../../../../screens/providerFlow/updateAvailability/index';
const Stack = createStackNavigator();
export default function notificationStack() {
  return (
    <Stack.Navigator initialRouteName="Availability" headerMode="none">
      <Stack.Screen name="Availability" component={Availability} />
      <Stack.Screen name="updateAvailability" component={updateAvailability} />
    </Stack.Navigator>
  );
}
