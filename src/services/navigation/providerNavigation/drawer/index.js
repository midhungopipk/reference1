import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Platform,
  AsyncStorage,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from '../../../utilities/colors/index';
import bottomTab from '../bottomTab/index';
import {height, width} from 'react-native-dimension';
import styles from './styles';
import termsAndCondition from '../../../../screens/providerFlow/termsAndCondition/index';
import About from '../../../../screens/providerFlow/About/index';
import privacyPolicy from '../../../../screens/providerFlow/privacyPolicy/index';
import settingsStack from '../settingsStack/index';
import AvailabilityStack from '../AvailabilityStack/index';
import history from '../../../../screens/providerFlow/history/index';
import EditProfile from '../../../../screens/providerFlow/EditProfile/index';
import EarningsStack from '../EarningsStack/index';
import Reviews from '../../../../screens/providerFlow/Reviews/index';
import workLocation from '../../../../screens/providerFlow/workLocation/index';
import workLocationService from '../../../../screens/providerFlow/updateWorkLocation/index';
import AddSubService from '../../../../screens/providerFlow/addSubService/index';
import servicesStack from '../servicesStack/index';
import services from '../../../../screens/providerFlow/services/index';
import servicesType from '../../../../screens/providerFlow/servicesType';
import yourPrices from '../../../../screens/providerFlow/yourPrices/index';
import profile from '../../../../screens/providerFlow/profile/index';
import bookingDetailsStack from '../bookingDetailsStack/index';
import chat from '../../../../screens/providerFlow/chat/index';

import {routes} from '../../../../services';
import {getData} from '../../../FireBase';
import {Icon} from 'react-native-elements';
import {totalSize} from 'react-native-dimension';

const Drawer = createDrawerNavigator();
const CustomDrawerComponent = (props) => {
  const [state, setstate] = useState({});
  useEffect(() => {
    const getResult = async () => {
      let id = await AsyncStorage.getItem('user');
      await getData('users', id).then(async (data) => {
        console.log('drawer data: ', data);
        setstate(data);
      });
    };
    getResult();
  }, []);
  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.avatarContainer}>
        {state.image ? (
          state.image == '' ? (
            <View
              style={{
                width: totalSize(10),
                height: totalSize(10),
                borderRadius: totalSize(10) / 2,
                backgroundColor: colors.snow,
                borderWidth: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon type={'font-awesome'} name={'user'} size={totalSize(6)} />
            </View>
          ) : (
            <Image
              source={
                state.image
                  ? state.image == ''
                    ? require('../../../../assets/images/image1.png')
                    : {uri: state.image}
                  : require('../../../../assets/images/image1.png')
              }
              style={styles.avatarImage}
            />
          )
        ) : (
          <View
            style={{
              width: totalSize(10),
              height: totalSize(10),
              borderRadius: totalSize(10) / 2,
              backgroundColor: colors.snow,
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon type={'font-awesome'} name={'user'} size={totalSize(6)} />
          </View>
        )}
        <Text style={styles.avatarName}>
          {state.firstName ? state.firstName + ' ' + state.lastName : ''}
        </Text>
      </View>
      {/* <DrawerItemList {...props} /> */}
      <TouchableOpacity
        onPress={() => props.navigation.navigate('AvailabilityStack')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Availability
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('EarningsStack')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Earnings
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('history')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Service History
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('settingsStack')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          settings
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('About')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>About</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('termsAndCondition')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Terms and Condition
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('privacyPolicy')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Privacy Policy
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={async () => {
          await AsyncStorage.removeItem('user');
          props.navigation.navigate(routes.authHome);
        }}
        style={[
          styles.drawerRow,
          {
            backgroundColor: colors.steel,
            marginTop: Platform.OS == 'ios' ? height(7) : height(14),
          },
        ]}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>Logout</Text>
      </TouchableOpacity>
    </DrawerContentScrollView>
  );
};
export default function MyDrawer(props) {
  return (
    <Drawer.Navigator
      hideStatusBar={true}
      statusBarAnimation={'fade'}
      initialRouteName={'bottomTab'}
      drawerStyle={{backgroundColor: colors.snow, width: width(70)}}
      drawerContent={(props) => <CustomDrawerComponent {...props} />}>
      <Drawer.Screen name="bottomTab" component={bottomTab} />
      <Drawer.Screen name="termsAndCondition" component={termsAndCondition} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="privacyPolicy" component={privacyPolicy} />
      <Drawer.Screen name="settingsStack" component={settingsStack} />
      <Drawer.Screen name="EditProfile" component={EditProfile} />
      <Drawer.Screen name="history" component={history} />
      <Drawer.Screen name="AvailabilityStack" component={AvailabilityStack} />
      <Drawer.Screen name="EarningsStack" component={EarningsStack} />
      <Drawer.Screen name="Reviews" component={Reviews} />
      <Drawer.Screen name="profile" component={profile} />
      <Drawer.Screen name="services" component={services} />
      <Drawer.Screen name="servicesType" component={servicesType} />
      <Drawer.Screen name="yourPrices" component={yourPrices} />
      <Drawer.Screen
        name="workLocationService"
        component={workLocationService}
      />
      <Drawer.Screen name="addSubService" component={AddSubService} />
      <Drawer.Screen name="workLocation" component={workLocation} />
      {/* <Drawer.Screen name="servicesStack" component={servicesStack} /> */}

      <Drawer.Screen
        name="bookingDetailsStack"
        component={bookingDetailsStack}
      />
      <Drawer.Screen name="chat" component={chat} />
    </Drawer.Navigator>
  );
}
