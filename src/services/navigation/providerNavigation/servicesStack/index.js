import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import services from '../../../../screens/providerFlow/services/index';
import servicesType from '../../../../screens/providerFlow/servicesType';
import yourPrices from '../../../../screens/providerFlow/yourPrices/index';
import profile from '../../../../screens/providerFlow/profile/index';

const Stack = createStackNavigator();
export default function notificationStack() {
  return (
    <Stack.Navigator initialRouteName="profile" headerMode="none">
      {/* <Stack.Navigator initialRouteName="services" headerMode="none"> */}
      <Stack.Screen name="profile" component={profile} />
      <Stack.Screen name="services" component={services} />
      <Stack.Screen name="servicesType" component={servicesType} />
      <Stack.Screen name="yourPrices" component={yourPrices} />
    </Stack.Navigator>
  );
}
