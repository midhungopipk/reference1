import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {Component} from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import home from '../../../../screens/providerFlow/home/index';
import profile from '../../../../screens/providerFlow/profile/index';
import notification from '../../../../screens/providerFlow/notification/index';
import Calender from '../../../../screens/providerFlow/Calender/index';
import servicesStack from '../servicesStack/index';
import {width, height} from 'react-native-dimension';
import {colors} from '../../../utilities/colors/index';

const Tab = createBottomTabNavigator();

export default function BottomTab() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: colors.silver,
        inactiveTintColor: colors.snow,
        showLabel: false,
        tabStyle: {
          padding: width(1),
          backgroundColor: colors.appColor1,
        },
      }}>
      <Tab.Screen
        name="home"
        component={home}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialIcons name={'dashboard'} size={width(6)} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Calender"
        component={Calender}
        options={{
          tabBarIcon: ({color}) => (
            <Ionicons name={'calendar'} size={width(6)} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="notification"
        component={notification}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome name={'bell'} size={width(6)} color={color} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="servicesStack"
        component={servicesStack}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialIcons name={'person'} size={width(7)} color={color} />
          ),
        }}
      /> */}
      <Tab.Screen
        name="profile"
        component={profile}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialIcons name={'person'} size={width(7)} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
