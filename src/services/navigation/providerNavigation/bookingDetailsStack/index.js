import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import bookingDetails from '../../../../screens/providerFlow/bookingDetails/index';
import Directions from '../../../../screens/providerFlow/Directions/index';
import startService from '../../../../screens/providerFlow/startService/index';
import serviceInProgress from '../../../../screens/providerFlow/serviceInProgress/index';
import rating from '../../../../screens/providerFlow/rating/index';
const Stack = createStackNavigator();
export default function notificationStack() {
  return (
    <Stack.Navigator initialRouteName="bookingDetails" headerMode="none">
      <Stack.Screen name="bookingDetails" component={bookingDetails} />
      <Stack.Screen name="Directions" component={Directions} />
      <Stack.Screen name="startService" component={startService} />
      <Stack.Screen name="serviceInProgress" component={serviceInProgress} />
      <Stack.Screen name="rating" component={rating} />
    </Stack.Navigator>
  );
}
