import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import Earnings from '../../../../screens/providerFlow/Earnings/index';
import WithdrawEarnings from '../../../../screens/providerFlow/WithdrawEarnings/index';
import selectBank from '../../../../screens/providerFlow/selectBank/index';
import addBank from '../../../../screens/providerFlow/addBank/index';
import confirmWithdrawl from '../../../../screens/providerFlow/confirmWithdrawl/index';
const Stack = createStackNavigator();
export default function notificationStack() {
  return (
    <Stack.Navigator initialRouteName="Earnings" headerMode="none">
      <Stack.Screen name="Earnings" component={Earnings} />
      <Stack.Screen name="WithdrawEarnings" component={WithdrawEarnings} />
      <Stack.Screen name="selectBank" component={selectBank} />
      <Stack.Screen name="addBank" component={addBank} />
      <Stack.Screen name="confirmWithdrawl" component={confirmWithdrawl} />
    </Stack.Navigator>
  );
}
