import React, {Component, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {header, routes, headers} from '../../../constants';
// import * as Auth from '../../../screens/authFlow';
import BottomTab from '../bottomTab';
import * as Home from '../../../../screens/mainFlow';

const HomeStack = createStackNavigator();

const HomeNavigation = () => {
  return (
    <HomeStack.Navigator
      screenOptions={headers.screenOptions}
      // initialRouteName={routes.homeLanding}
    >
      <HomeStack.Screen
        name={routes.homeLanding}
        component={BottomTab}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.categories}
        component={Home.Categories}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.subCategories}
        component={Home.SubCategories}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.filters}
        component={Home.Filters}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.productListing}
        component={Home.ProductListing}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.filterProducts}
        component={Home.FilterProducts}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.productDetail}
        component={Home.ProductDetail}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.covidTest}
        component={Home.CovidTest}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.bloodTest}
        component={Home.BloodTest}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.selectTimeNDate}
        component={Home.SelectTimeNDate}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.mapTest}
        component={Home.MapTest}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.bookingSummary}
        component={Home.BookingSummary}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.swabTest}
        component={Home.SwabTest}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.vipTestMapHome}
        component={Home.MapVipTestHome}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.downloadReport}
        component={Home.DownloadReport}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.addAddress}
        component={Home.AddAddress}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.quotationRequest}
        component={Home.QuotationRequest}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.quotationRequestDelivery}
        component={Home.QuotationRequestDelivery}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.quotationRequestPayMethod}
        component={Home.QuotationRequestPayMethod}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.uploadPrescription}
        component={Home.UploadPrescription}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.pharmacies}
        component={Home.Pharmacies}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.pharmacyDetail}
        component={Home.PharmacyDetail}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.pharmacyProducts}
        component={Home.PharmacyProducts}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.pharmacyPage}
        component={Home.PharmacyPage}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.about}
        component={Home.About}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.wishList}
        component={Home.WishList}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.prescriptions}
        component={Home.Prescriptions}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.prescriptionDetail}
        component={Home.PrescriptionDetail}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.quotationsProfile}
        component={Home.QuotationsProfile}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.comparisonList}
        component={Home.ComparisonList}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.myOrders}
        component={Home.MyOrders}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.orderDetail}
        component={Home.OrderDetail}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.trackOrder}
        component={Home.TrackOrder}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.trackOrderDetail}
        component={Home.TrackOrderDetail}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.checkOut}
        component={Home.CheckOut}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.checkOutChangeAddress}
        component={Home.CheckoutChangeAddress}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.orderConfirm}
        component={Home.OrderConfirm}
        options={{
          headerShown: false,
          gestureEnabled: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.editProfile}
        component={Home.EditProfile}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.myPoints}
        component={Home.MyPoints}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.viewProducts}
        component={Home.ViewProducts}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <HomeStack.Screen
        name={routes.discountProducts}
        component={Home.DiscountProducts}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
    </HomeStack.Navigator>
  );
};

export default HomeNavigation;
