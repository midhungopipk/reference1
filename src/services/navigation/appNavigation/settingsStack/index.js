import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import settings from '../../../../screens/mainFlow/settings/index';
import accountSettings from '../../../../screens/mainFlow/accountSettings/index';
import NotificationSettings from '../../../../screens/mainFlow/NotificationSettings/index';
import paymentMethod from '../../../../screens/mainFlow/paymentMethod/index';
import addPaymentMethods from '../../../../screens/mainFlow/addPaymentMethods/index';
const Stack = createStackNavigator();
export default function notificationStack() {
  return (
    <Stack.Navigator initialRouteName="settings" headerMode="none">
      <Stack.Screen name="settings" component={settings} />
      <Stack.Screen name="accountSettings" component={accountSettings} />
      <Stack.Screen name="NotificationSettings" component={NotificationSettings} />
      <Stack.Screen name="paymentMethod" component={paymentMethod} />
      <Stack.Screen name="addPaymentMethods" component={addPaymentMethods} />
    </Stack.Navigator>
  );
}
