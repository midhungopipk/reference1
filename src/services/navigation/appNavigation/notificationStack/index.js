import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import notification from '../../../../screens/mainFlow/notification/index';


const Stack = createStackNavigator();
export default function notificationStack() {
    return (
        <Stack.Navigator initialRouteName="notification" headerMode="none">
            <Stack.Screen name="notification" component={notification} />
            
        </Stack.Navigator>
    );
}