import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Platform,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
// import { connect, useDispatch, useSelector } from 'react-redux';

import React, {useEffect, useState} from 'react';
import {colors} from '../../../utilities/colors/index';
// import BottomTab from '../BottomTab'

import bottomTab from '../bottomTab/index';
import history from '../../../../screens/mainFlow/history';
import settingsStack from '../../appNavigation/settingsStack/index';
import About from '../../../../screens/mainFlow/About';
import termsAndCondition from '../../../../screens/mainFlow/termsAndCondition/index';
import privacyPolicy from '../../../../screens/mainFlow/privacyPolicy/index';
import {height, width} from 'react-native-dimension';
import filter from '../../../../screens/mainFlow/filter/index';
import servicesType from '../../../../screens/mainFlow/servicesType/index';
import EditProfile from '../../../../screens/mainFlow/EditProfile/index';
import bookingDetailsStack from '../bookingDetailsStack/index';
import chat from '../../../../screens/mainFlow/chat/index';
import search from '../../../../screens/mainFlow/search/index';
import providerStack from '../providerStack/index';
import styles from './styles';
import {routes} from '../../../../services';
import {getData} from '../../../FireBase';
import {Icon} from 'react-native-elements';
import {totalSize} from 'react-native-dimension';

const Drawer = createDrawerNavigator();
const CustomDrawerComponent = (props) => {
  const [state, setstate] = useState({});
  useEffect(() => {
    const getResult = async () => {
      let id = await AsyncStorage.getItem('user');
      await getData('users', id).then(async (data) => {
        console.log('drawer data: ', data);
        setstate(data);
      });
    };
    getResult();
  }, []);

  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.avatarContainer}>
        {state.image ? (
          state.image == '' ? (
            <View
              style={{
                width: totalSize(10),
                height: totalSize(10),
                borderRadius: totalSize(10) / 2,
                backgroundColor: colors.snow,
                borderWidth: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon type={'font-awesome'} name={'user'} size={totalSize(6)} />
            </View>
          ) : (
            <Image
              source={
                state.image
                  ? state.image == ''
                    ? require('../../../../assets/images/image1.png')
                    : {uri: state.image}
                  : require('../../../../assets/images/image1.png')
              }
              style={styles.avatarImage}
            />
          )
        ) : (
          <View
            style={{
              width: totalSize(10),
              height: totalSize(10),
              borderRadius: totalSize(10) / 2,
              backgroundColor: colors.snow,
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon type={'font-awesome'} name={'user'} size={totalSize(6)} />
          </View>
        )}
        <Text style={styles.avatarName}>
          {state.firstName ? state.firstName + ' ' + state.lastName : ''}
        </Text>
      </View>
      {/* <DrawerItemList {...props} /> */}
      <TouchableOpacity
        onPress={() => props.navigation.navigate('history')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>History</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('settingsStack')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          settings
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('About')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>About</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('termsAndCondition')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Terms and Condition
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('privacyPolicy')}
        style={styles.drawerRow}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>
          Privacy Policy
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={async () => {
          await AsyncStorage.removeItem('user');
          props.navigation.navigate(routes.authHome);
        }}
        style={[
          styles.drawerRow,
          {
            backgroundColor: colors.steel,
            marginTop: Platform.OS == 'ios' ? height(14) : height(28),
          },
        ]}>
        <Text style={[styles.rowText, {color: colors.appColor1}]}>Logout</Text>
      </TouchableOpacity>
    </DrawerContentScrollView>
  );
};
export default function MyDrawer(props) {
  return (
    <Drawer.Navigator
      hideStatusBar={true}
      statusBarAnimation={'fade'}
      initialRouteName={'bottomTab'}
      drawerStyle={{backgroundColor: colors.snow, width: width(70)}}
      drawerContent={(props) => <CustomDrawerComponent {...props} />}>
      <Drawer.Screen name="bottomTab" component={bottomTab} />
      <Drawer.Screen name="history" component={history} />
      <Drawer.Screen name="settingsStack" component={settingsStack} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="termsAndCondition" component={termsAndCondition} />
      <Drawer.Screen name="privacyPolicy" component={privacyPolicy} />
      <Drawer.Screen name="filter" component={filter} />
      <Drawer.Screen name="servicesType" component={servicesType} />
      <Drawer.Screen name="EditProfile" component={EditProfile} />
      <Drawer.Screen
        name="bookingDetailsStack"
        component={bookingDetailsStack}
      />
      <Drawer.Screen name="chat" component={chat} />
      <Drawer.Screen name="providerStack" component={providerStack} />
      <Drawer.Screen name="search" component={search} />
    </Drawer.Navigator>
  );
}
