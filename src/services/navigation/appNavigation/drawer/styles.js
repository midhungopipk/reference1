import { StyleSheet } from 'react-native';
import {colors} from '../../../utilities/colors/index';
import { width, height, totalSize } from 'react-native-dimension';

const styles = StyleSheet.create({

    //CustomDrawer Styles
    ////////////////////
    avatarContainer: {
        height: height(28),
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatarName: {
        color: colors.black,
        fontSize: width(4.5),
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: height(2)
    },
    avatarImage: {
        borderRadius: width(20),
        height: height(17),
        width: width(34),
        resizeMode: 'cover',
        alignSelf: 'center'
    },
    drawerRow: {
        width: width(65),
        borderTopRightRadius: width(30),
        borderBottomRightRadius: width(30),
        height: height(7),
        paddingStart: width(8),
        justifyContent: 'center',
    },
    rowText: {
        color: colors.snow,
    }

});
export default styles;