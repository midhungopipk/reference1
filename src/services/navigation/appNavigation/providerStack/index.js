import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import ProviderProfile from '../../../../screens/mainFlow/ProviderProfile/index';
import BookNow from '../../../../screens/mainFlow/BookNow/index';
import dateSelection from '../../../../screens/mainFlow/dateSelection/index';
import requestSent from '../../../../screens/mainFlow/requestSent/index';

const Stack = createStackNavigator();
export default function providerStack() {
  return (
    <Stack.Navigator initialRouteName="ProviderProfile" headerMode="none">
      <Stack.Screen name="ProviderProfile" component={ProviderProfile} />
      <Stack.Screen name="BookNow" component={BookNow} />
      <Stack.Screen name="dateSelection" component={dateSelection} />
      <Stack.Screen name="requestSent" component={requestSent} />
    </Stack.Navigator>
  );
}
