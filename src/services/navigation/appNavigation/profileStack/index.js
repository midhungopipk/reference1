import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import profile from '../../../../screens/mainFlow/profile/index';


const Stack = createStackNavigator();
export default function notificationStack() {
    return (
        <Stack.Navigator initialRouteName="profile" headerMode="none">
            <Stack.Screen name="profile" component={profile} />
            
        </Stack.Navigator>
    );
}