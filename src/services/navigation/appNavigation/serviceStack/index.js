import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import services from '../../../../screens/mainFlow/services/index';

const Stack = createStackNavigator();
export default function servicesStack() {
  return (
    <Stack.Navigator initialRouteName="services" headerMode="none">
      <Stack.Screen name="services" component={services} />
    </Stack.Navigator>
  );
}
