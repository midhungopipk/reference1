import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {useEffect, useState} from 'react';
import {width, height, totalSize} from 'react-native-dimension';
import {colors} from '../../../utilities/colors/index';
import * as Home from '../../../../screens/mainFlow';
import {CustomIcon, CustomIconCart, SmallText} from '../../../../components';
import {appImages} from '../../../utilities';
import {store} from '../../../../Redux/configureStore';
import {Translate} from '../../../helpingMethods';
import {useSelector} from 'react-redux';

const Tab = createBottomTabNavigator();

export default function BottomTab() {
  const [cartValue, setCartValue] = useState(0);
  const cart = useSelector(state => state.cart);
  const reduxCartValue = cart.data.data;
  useEffect(() => {
    // if (store.getState().cart.data && store.getState().cart.data.data) {
    console.log('navigation hook is listening');
    // console.log('cart: ', cart);
    // console.log('reduxCartValue: ', reduxCartValue);
    // }
    // }, [cart]);
  }, [store.getState().cart.data]);

  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: colors.activeBottomIcon,
        inactiveTintColor: colors.inActiveBottomIcon,
        // activeTintColor: colors.gradient1,
        // inactiveTintColor: colors.frost,
        showLabel: false,
        tabStyle: {
          padding: width(1),
          backgroundColor: colors.snow,
        },
      }}>
      <Tab.Screen
        name={'Home'}
        component={Home.Home}
        options={{
          // tabBarLabel: ({color}) => (
          //   <SmallText style={{color}}>{Translate('Home')}</SmallText>
          // ),
          tabBarIcon: ({color}) => (
            <CustomIcon
              icon={appImages.navHome}
              size={width(7)}
              color={color}
            />
            // <Entypo name={'tools'} size={width(6)} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Categories"
        component={Home.Categories}
        options={{
          // tabBarLabel: ({color}) => (
          //   <SmallText style={{color}}>{Translate('Categories')}</SmallText>
          // ),
          tabBarIcon: ({color}) => (
            <CustomIcon
              icon={appImages.navCategory}
              size={width(7)}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Services"
        component={Home.DiscountProducts}
        options={{
          // tabBarLabel: ({color}) => (
          //   <SmallText style={{color}}>{Translate('Services')}</SmallText>
          // ),
          tabBarIcon: ({color}) => (
            <CustomIcon
              icon={appImages.navService}
              size={totalSize(7.5)}
              // color={'trasnparent'}
              style={{marginBottom: height(4)}}
            />
          ),
          // tabBarLabel: () => {
          //   color: 'transparent';
          // },
        }}
      />
      <Tab.Screen
        name="Account"
        component={Home.Account}
        options={{
          // tabBarLabel: ({color}) => (
          //   <SmallText style={{color}}>{Translate('Account')}</SmallText>
          // ),
          tabBarIcon: ({color}) => (
            <CustomIcon
              icon={appImages.navUser}
              size={width(7)}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Cart"
        component={Home.Cart}
        options={{
          tabBarIcon: ({color}) => (
            <CustomIconCart
              icon={appImages.navCart}
              size={width(7)}
              color={color}
              quantity={
                store.getState().cart.data && store.getState().cart.data.data
                  ? store.getState().cart.data.data
                  : 0
              }
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
