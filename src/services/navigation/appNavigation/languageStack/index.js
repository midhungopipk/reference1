import React, {Component, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {header, routes, headers} from '../../../constants';
import {LanguageSelection} from '../../../../screens/authFlow';

// import * as Auth from '../../../screens/authFlow';
// import * as Auth from '../../../screens/mainFlow';

const LanguageStack = createStackNavigator();

const LanguageNavigation = () => {
  return (
    <LanguageStack.Navigator
      screenOptions={headers.screenOptions}
      // initialRouteName={routes.welcome}
    >
      <LanguageStack.Screen
        name={routes.languageSelection}
        component={LanguageSelection}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      {/* <LanguageStack.Screen
        name={routes.walkthrough}
        component={WalkThrough}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      /> */}
    </LanguageStack.Navigator>
  );
};

export default LanguageNavigation;
