import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import BookingDetails from '../../../../screens/mainFlow/BookingDetails/index';
import trackServiceProvider from '../../../../screens/mainFlow/trackServiceProvider/index';
import ServiceInProgress from '../../../../screens/mainFlow/ServiceInProgress/index';
import RatingScreen from '../../../../screens/mainFlow/RatingScreen/index';
const Stack = createStackNavigator();
export default function BookingDetailsStack() {
  return (
    <Stack.Navigator initialRouteName="BookingDetails" headerMode="none">
      <Stack.Screen name="BookingDetails" component={BookingDetails} />
      <Stack.Screen
        name="trackServiceProvider"
        component={trackServiceProvider}
      />
      <Stack.Screen name="ServiceInProgress" component={ServiceInProgress} />
      <Stack.Screen name="RatingScreen" component={RatingScreen} />
    </Stack.Navigator>
  );
}
