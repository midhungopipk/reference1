import React, {useEffect, useState} from 'react';
import {Platform, PermissionsAndroid} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LanguageNavigation from './appNavigation/languageStack';
import AuthNavigation from './authNavigation';
import Splash from '../../screens/authFlow/splash';
import HomeNavigation from '../navigation/appNavigation/homeStack/index';
import AsyncStorage from '@react-native-community/async-storage';
import {storageConst, routes} from '../constants';
import {
  fetchUser,
  fetchCategoryId,
  fetchToken,
  fetchLanguage,
} from '../../../src/Redux/Actions/index';
import Geolocation from 'react-native-geolocation-service';
import {store} from '../../../src/Redux/configureStore';
import messaging from '@react-native-firebase/messaging';
import {useSelector, useDispatch} from 'react-redux';
import {navigationRef, navigate} from './RootNavigation';

const MainStack = createStackNavigator();

export function Navigation() {
  const dispatch = useDispatch();
  const Token = useSelector(state => state.Token);
  const {token} = Token;
  const Language = useSelector(state => state.Language);
  const {language} = Language;
  const [loading, setLoading] = useState(true);

  // const [language, setLanguage] = useState(false);

  useEffect(async () => {
    // await AsyncStorage.removeItem(storageConst.firstTime);
    checkTokens();
    requestLocationPermission();
    notificationSettings();
    // setTimeout(() => {
    //   setLoading(false);
    // }, 300);
  }, []);

  const checkTokens = async () => {
    await AsyncStorage.getItem(storageConst.userId).then(async userId => {
      if (userId) {
        await AsyncStorage.getItem(storageConst.userData).then(
          async userData => {
            if (userData) {
              let user = JSON.parse(userData);
              store.dispatch(
                fetchUser({
                  user,
                }),
              );

              await AsyncStorage.getItem(storageConst.language).then(
                async language => {
                  if (language == null || language == undefined) {
                    await AsyncStorage.setItem(storageConst.language, 'en');
                  } else {
                    setTimeout(() => {
                      let token = {
                        token: true,
                      };
                      dispatch(fetchToken(token));
                      setLoading(false);
                      // this.setState({loading: false});
                    }, 300);
                  }
                },
              );
            }
          },
        );
      } else {
        await AsyncStorage.getItem(storageConst.firstTime).then(async value => {
          if (value == null || value == undefined) {
            setTimeout(() => {
              setLoading(false);
            }, 300);
          } else {
            setTimeout(() => {
              setLoading(false);
              let language = {
                language: true,
              };
              dispatch(fetchLanguage(language));
            }, 300);
          }
        });
      }
    });
  };
  const requestLocationPermission = async () => {
    // if (Platform.OS == 'ios') Geolocation.requestAuthorization('always');
    try {
      if (Platform.OS == 'ios') {
        Geolocation.requestAuthorization('always');
      } else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Dwaae',
            message: 'Dwaae wants to access to your location ',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the location');
          // alert('You can use the location');
        } else {
          console.log('location permission denied');
          alert('Location permission denied');
        }
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const notificationSettings = async () => {
    requestUserPermission();
    await messaging().onMessage(async remoteMessage => {
      console.log('this is from onMessage method: ', remoteMessage);
    });
    await messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log(remoteMessage);
      console.log('Message handled in the splash!', remoteMessage);
    });
    await messaging().onNotificationOpenedApp(async remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        // remoteMessage.notification,
        remoteMessage,
      );
      // navigation.navigate(remoteMessage.data.type);
      await AsyncStorage.getItem(storageConst.userId).then(async userId => {
        if (userId) {
          let token = {
            token: true,
          };
          dispatch(fetchToken(token));
          setTimeout(() => {
            let token = {
              token: true,
            };
            dispatch(fetchToken(token));
            if (remoteMessage.data.type == 'P') {
              navigate(routes.productDetail, {
                productId: remoteMessage.data.type_data,
              });
            } else if (remoteMessage.data.type == 'V') {
              navigate(routes.pharmacyDetail, {
                item: remoteMessage.data.type_data,
              });
            } else if (remoteMessage.data.type == 'C') {
              let categoryId = {
                categoryId: remoteMessage.data.type_data,
              };
              dispatch(fetchCategoryId(categoryId));
              navigate(routes.productListing);
            }
          }, 1000);
        } else {
          await AsyncStorage.getItem(storageConst.guestId).then(async value => {
            // let token = {
            //   token: true,
            // };
            // dispatch(fetchToken(token));
            let user_id = '0';
            if (value) {
              user_id = value;
            }
            let user = {
              api_key: '',
              birthday: '0',
              company: '',
              company_id: '0',
              ec_location_detail: '',
              email: '',
              emirate_id_number: '',
              fax: '',
              firstname: 'Guest',
              insurance_number: '',
              is_root: 'N',
              janrain_identifier: '',
              lang_code: 'en',
              last_login: '0',
              last_passwords: '',
              lastname: 'User',
              message: 'You have been successfully logged in.',
              password_change_timestamp: '1',
              phone: '',
              purchase_timestamp_from: '0',
              purchase_timestamp_to: '0',
              referer: '',
              responsible_email: '',
              status: 'A',
              success: true,
              tax_exempt: 'N',
              timestamp: '1622368819',
              total_products: 0,
              url: '',
              user_id: user_id,
              user_login: '0',
              user_type: 'C',
            };
            await store.dispatch(
              fetchUser({
                user: user,
              }),
            );
            await AsyncStorage.setItem(storageConst.guest, '1');
            await AsyncStorage.setItem(
              storageConst.userData,
              JSON.stringify(user),
            ).then(async () => {
              await AsyncStorage.setItem(storageConst.userId, '0').then(
                async () => {
                  let token = {
                    token: true,
                  };
                  dispatch(fetchToken(token));

                  setTimeout(() => {
                    if (remoteMessage.data.type == 'P') {
                      navigate(routes.productDetail, {
                        productId: remoteMessage.data.type_data,
                      });
                    } else if (remoteMessage.data.type == 'V') {
                      navigate(routes.pharmacyDetail, {
                        item: remoteMessage.data.type_data,
                      });
                    } else if (remoteMessage.data.type == 'C') {
                      let categoryId = {
                        categoryId: remoteMessage.data.type_data,
                      };
                      dispatch(fetchCategoryId(categoryId));
                      navigate(routes.productListing);
                    }
                  }, 1500);
                },
              );
            });
          });
        }
      });
    });

    // Check whether an initial notification is available
    await messaging()
      .getInitialNotification()
      .then(async remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
            remoteMessage.data,
          );

          await AsyncStorage.getItem(storageConst.userId).then(async userId => {
            if (userId) {
              setTimeout(() => {
                if (remoteMessage.data.type == 'P') {
                  navigate(routes.productDetail, {
                    productId: remoteMessage.data.type_data,
                  });
                } else if (remoteMessage.data.type == 'V') {
                  navigate(routes.pharmacyDetail, {
                    item: remoteMessage.data.type_data,
                  });
                } else if (remoteMessage.data.type == 'C') {
                  let categoryId = {
                    categoryId: remoteMessage.data.type_data,
                  };
                  dispatch(fetchCategoryId(categoryId));
                  navigate(routes.productListing);
                }
              }, 2000);
            } else {
              await AsyncStorage.getItem(storageConst.guestId).then(
                async value => {
                  let user_id = '0';
                  if (value) {
                    user_id = value;
                  }
                  let user = {
                    api_key: '',
                    birthday: '0',
                    company: '',
                    company_id: '0',
                    ec_location_detail: '',
                    email: '',
                    emirate_id_number: '',
                    fax: '',
                    firstname: 'Guest',
                    insurance_number: '',
                    is_root: 'N',
                    janrain_identifier: '',
                    lang_code: 'en',
                    last_login: '0',
                    last_passwords: '',
                    lastname: 'User',
                    message: 'You have been successfully logged in.',
                    password_change_timestamp: '1',
                    phone: '',
                    purchase_timestamp_from: '0',
                    purchase_timestamp_to: '0',
                    referer: '',
                    responsible_email: '',
                    status: 'A',
                    success: true,
                    tax_exempt: 'N',
                    timestamp: '1622368819',
                    total_products: 0,
                    url: '',
                    user_id: user_id,
                    user_login: '0',
                    user_type: 'C',
                  };
                  await store.dispatch(
                    fetchUser({
                      user: user,
                    }),
                  );
                  await AsyncStorage.setItem(storageConst.guest, '1');
                  await AsyncStorage.setItem(
                    storageConst.userData,
                    JSON.stringify(user),
                  ).then(async () => {
                    await AsyncStorage.setItem(storageConst.userId, '0').then(
                      async () => {
                        await AsyncStorage.setItem(
                          storageConst.userId,
                          '0',
                        ).then(async () => {
                          let token = {
                            token: true,
                          };
                          dispatch(fetchToken(token));
                          setTimeout(() => {
                            if (remoteMessage.data.type == 'P') {
                              navigate(routes.productDetail, {
                                productId: remoteMessage.data.type_data,
                              });
                            } else if (remoteMessage.data.type == 'V') {
                              navigate(routes.pharmacyDetail, {
                                item: remoteMessage.data.type_data,
                              });
                            } else if (remoteMessage.data.type == 'C') {
                              let categoryId = {
                                categoryId: remoteMessage.data.type_data,
                              };
                              dispatch(fetchCategoryId(categoryId));
                              navigate(routes.productListing);
                            }
                          }, 2000);
                        });
                      },
                    );
                  });
                },
              );
            }
          });
        }
      });
  };
  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      getFcmToken();
    }
  };
  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log('fcm token:', fcmToken);
      // this.setState({fcm: fcmToken});
    } else {
    }
  };

  return loading ? (
    <Splash />
  ) : (
    <NavigationContainer ref={navigationRef}>
      <MainStack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName={routes.auth}>
        {!token ? (
          !language ? (
            <MainStack.Screen
              name={routes.languageSelection}
              component={LanguageNavigation}
            />
          ) : (
            <MainStack.Screen name={routes.auth} component={AuthNavigation} />
          )
        ) : (
          <MainStack.Screen
            screenOptions={{headerShown: false}}
            name={routes.home}
            component={HomeNavigation}
            options={{
              gestureEnabled: false,
            }}
          />
        )}
      </MainStack.Navigator>
    </NavigationContainer>
  );
}
