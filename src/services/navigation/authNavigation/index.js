import React, {Component, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {header, routes, headers} from '../../constants';
import * as Auth from '../../../screens/authFlow';
// import * as Auth from '../../../screens/mainFlow';

const AuthStack = createStackNavigator();

const AuthNavigation = () => {
  return (
    <AuthStack.Navigator
      screenOptions={headers.screenOptions}
      // initialRouteName={routes.welcome}
    >
      {/* <AuthStack.Screen
        name={routes.splash}
        component={Auth.Splash}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      /> */}
      {/* <AuthStack.Screen
        name={routes.languageSelection}
        component={Auth.LanguageSelection}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      /> */}
      <AuthStack.Screen
        name={routes.signin}
        component={Auth.Login}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <AuthStack.Screen
        name={routes.signup}
        component={Auth.SignUp}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <AuthStack.Screen
        name={routes.signupComplete}
        component={Auth.SignUpComplete}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <AuthStack.Screen
        name={routes.otpLogin}
        component={Auth.OTPLogin}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
      <AuthStack.Screen
        name={routes.otpCode}
        component={Auth.OTPCode}
        options={{
          headerShown: false,
          //title: 'Sign In'
        }}
      />
    </AuthStack.Navigator>
  );
};

export default AuthNavigation;
