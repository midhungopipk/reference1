import {apiUrl, headers, homeHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  fetchLoggedInUser,
  startFetch,
  endFetchWithError,
} from '../redux/actions';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';

const state = store.getState();
const userState = state.user;
const signedInUsers = state.signedInUsers;

export const GetUserData = async (user_id, callback) => {
  const url = `${apiUrl}/sapi.php?_d=ProfilesApi/${user_id}`;
  axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        callback(response.data);
      }
    })
    .catch(error => {
      callback(false);
      console.log('update error', error);
    });
};
export const updateUserProfile = async (user_id, data, callback) => {
  let dataStringified = JSON.stringify(data);
  console.log(dataStringified);
  const url = `${apiUrl}/sapi.php?_d=ProfilesApi/${user_id}`;
  axios({
    method: 'put',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        callback(response.data);
      }
    })
    .catch(error => {
      callback(false);
      console.log('update error', error);
    });
};
export const UploadProfilePicture = async (params, onSuccess, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';
  const url = `${apiUrl}/sapi.php?_d=SaveProfileImage&lang_code=${language}`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const GetCountryStateData = async (user_id, callback) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';
  const url = `${apiUrl}/sapi.php?_d=AddressesApi&user_id=${user_id}&create_new_address=Y&lang_code=${language}&currency_code=AED`;
  axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        callback(response.data);
      }
    })
    .catch(error => {
      callback(false);
      console.log('update error', error);
    });
};
export const AddNewAddress = async (data, setResponse, loading) => {
  const url = `${apiUrl}/sapi.php?_d=AddressesApi`;
  let dataStringified = JSON.stringify(data);
  console.log('data ---- : ', dataStringified);
  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const UpdateExistingAddress = async (
  user_id,
  data,
  setResponse,
  loading,
) => {
  const url = `${apiUrl}/sapi.php?_d=AddressesApi/${user_id}`;
  let dataStringified = JSON.stringify(data);
  console.log('data ---- : ', dataStringified);
  await axios({
    method: 'put',
    url: url,
    data: dataStringified,
    headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      // store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getUserAddresses = async (user_id, setResponse, loading) => {
  const url = `${apiUrl}/sapi.php?_d=AddressesApi&user_id=${user_id}`;
  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const createUser = async (user, callback) => {
  let dataStringified = JSON.stringify(user);

  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=SignupApi&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(function (response) {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(error => {
      if (error.response && error.response.data) {
        Toast.show(error.response.data.error);
      }
      callback(false);
    });
};
export const loginUser = async (credentials, callback) => {
  let dataStringified = JSON.stringify(credentials);

  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=LoginApi&lang_code=${language}&currency_code=AED`;
  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {
      ...headers,
      'Content-Type': 'application/json',
    },
  })
    .then(async response => {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(error => {
      if (error.response && error.response.data) {
        Toast.show(error.response.data.error);
      }
      callback(false);
    });
};
export const forgotPassword = async (credentials, callback) => {
  let dataStringified = JSON.stringify(credentials);

  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=LoginApi&lang_code=${language}&currency_code=AED`;
  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {
      ...headers,
      'Content-Type': 'application/json',
    },
  })
    .then(async response => {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(error => {
      if (error.response && error.response.data) {
        Toast.show(error.response.data.error);
      }
      callback(false);
    });
};
export const sendOTP = async (number, callback) => {
  // let dataStringified = JSON.stringify(credentials);

  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=OtpLoginApi&phone_no=${number}&lang_code=${language}&currency_code=AED`;
  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async response => {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(error => {
      if (error.response && error.response.data) {
        Toast.show(error.response.data.error);
      }
      callback(false);
    });
};
export const verifyOTP = async (credentials, callback) => {
  let dataStringified = JSON.stringify(credentials);

  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=OtpLoginApi`;
  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {
      ...headers,
      'Content-Type': 'application/json',
    },
  })
    .then(async response => {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(error => {
      if (error.response && error.response.data) {
        Toast.show(error.response.data.error);
      }
      callback(false);
    });
};
export const updateUserWithRaw = async (id, data, callback) => {
  let userData = await AsyncStorage.getItem('userData');
  userData = JSON.parse(userData);

  console.log(userData.token);

  const url = `${apiUrl}/users/${id}`;

  axios({
    method: 'put',
    url: url,
    data: JSON.stringify(data),
    headers: {
      ...headers,
      'X-Auth-Token': userData.token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        callback(response.data);
        // const signedInUsers = state.signedInUsers;
        // let signedInUsersList = [...state.signedInUsers.signedInUsers];

        // signedInUsers.signedInUsers.forEach((element, index) => {
        //   if (element.id == response.data.id) {
        //     signedInUsersList[index] = response.data;
        //   }
        // });

        // await store.dispatch(
        //   fetchLoggedInUser([...signedInUsers.signedInUsers]),
        // );
        // await AsyncStorage.setItem(
        //   'signedInUsers',
        //   JSON.stringify([...signedInUsers.signedInUsers]),
        // );
        // if (response.data.token) {
        //   store.dispatch(
        //     fetchUser({
        //       user: response.data,
        //       token: parseJwt(response.data.token),
        //     }),
        //   );
        // }
      }
    })
    .catch(error => {
      callback(false);
      console.log('update error', error);
    });
};

/*Logout: Recive a Credentials model request, navigation and nthe destiny route*/
export const logoutUser = (credentials, navigation, nextRoute) => {};

/* RequestResetPassword : Recive a email and make a request for reset password*/
export const requestResetPassword = async (email, navigation, nextRoute) => {
  const url = `${apiUrl}/password_resets`;

  const data = {
    email: email,
  };

  await axios({
    method: 'POST',
    url,
    data,
    headers: {...headers, 'Content-type': 'application/json'},
  })
    .then(function (response) {})
    .catch(function (error) {
      console.log(error.response);
    });
};

export const deleteAddress = async (id, callback) => {
  console.log('........id..del..', id);
  const url = `${apiUrl}/sapi.php?_d=AddressesApi/${id}`;
  axios({
    method: 'delete',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      console.log(response);
      if (response.status == 200) {
        callback(response.data);
      }
    })
    .catch(error => {
      callback(false);
      console.log('update errordddd', JSON.stringify(error));
    });
};

/* ResetPassword : After the request accepted you can send the new password*/
export const resetPassword = async (passwords, navigation, nextRoute) => {
  const id = userState.user.id;
  const url = `${apiUrl}/password_resets/${id}`;
  let isSuccess = false;

  const data = {
    'user[password]': passwords.new_password,
    'user[password_confirmation]': passwords.password_confirmation,
  };

  await axios({
    method: 'PUT',
    url,
    data,
    headers: {...headers, 'Content-type': 'application/json'},
  })
    .then(function (response) {
      if (response.status === '200') {
        isSuccess = true;
      }
      isSuccess = false;
    })
    .catch(function (response) {
      isSuccess = false;
    });

  return isSuccess;
};
