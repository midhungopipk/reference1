import {apiUrl, headers, homeHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
// import {
//   fetchUser,
//   fetchLoggedInUser,
//   startFetch,
//   endFetchWithError,
// } from '../redux/actions';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';

const state = store.getState();

// Create/Update Emirates ID
// POST : {{url}}/sapi.php?_d=EmiratesCardsApi

// curl --location -g --request POST '{{url}}/sapi.php?_d=EmiratesCardsApi' \
// --form 'front_side=@"/Users/rajatagarwal/Pictures/Dummy product images/p-5.jpg"' \
// --form 'back_side=@"/Users/rajatagarwal/Pictures/Dummy product images/plain_sky_blue_tshirt.jpg"' \
// --form 'params="{
//     \"emirate_profile_id\": 0,
//     \"user_id\": 1,
//     \"card_name\": \"Test card\"
// }"'

// Response:
// {
//   "emirate_profile_id": 102,
//   "emirate_card": {
//     "emirate_profile_id": "102",
//     "user_id": "1",
//     "create_time": "1622914453",
//     "emirate_profile_name": "",
//     "front_side_path": "efront_side/60bbb595b56e0/ahkylsxpzujf.jpg",
//     "back_side_path": "efront_side/60bbb595b5bd1/ighsaukovuv.jpg",
//     "is_default": "N",
//     "status": "A"
//   },
//   "message": "",
//   "success": true,
//   "action": ""
// }

export const UploadEmiratesCard = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=EmiratesCardsApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const UploadInsuranceCard = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=InsuranceCardsApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const UploadPrescriptionApi = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};

/* GetMedia: Recieve all Media of Library */
export const getAllMedia = async callback => {
  const url = `${apiUrl}/media`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      Accept: 'application/json; version=2',
      //   'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      // console.log('media response:', response);
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      callback(false, error.response);
    });
};

/* UploadMedia: Upload new Media  */
export const uploadMedia = async (params, onSuccess) => {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  // console.log('Media API triggered: ', params);
  var data = new FormData();
  data.append('media[object]', {
    uri: params.path,
    type: params.type,
    name: params.fileName,
  });
  // data.append('media[object]', params.path);
  data.append('media[name]', params.fileName);
  console.log('Media API, Send formdata: ', data);
  var config = {
    method: 'post',
    url: `${apiUrl}/media/upload`,
    headers: {
      ...headers,
      Accept: 'application/json; version=3',
      'X-Auth-Token': user.token,
      // 'Content-Type': 'application/json',
    },
    data: data,
  };

  axios(config)
    .then(function (response) {
      if (response.data) {
        console.log('media uploaded: ', response.data);
        Toast.show('Media uploaded !');
        onSuccess(response.data);
      }
      // console.log('See uploading response', response.data);
    })
    .catch(function (error) {
      console.log(error);
      // Toast.show('Something went wrong while uploading image', {
      //   duration: Toast.durations.SHORT,
      //   backgroundColor: 'red',
      //   textColor: '#FFF',
      // });
      onSuccess(false);
    });
};

/* GetMedia: Recieve all Media of Library */
export const getMediaTypes = async callback => {
  const url = `${apiUrl}/media/types`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      Accept: 'application/json; version=3',
      //   'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      Toast.show('Some thing went wrong', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      callback(false, error.response);
    });
};

/* GetMedia: Recieve all Media of Library */
export const getMediaByTags = async callback => {
  const url = `${apiUrl}/media/tags`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      Accept: 'application/json; version=3',
      //   'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      // console.log('media response:', response);
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      Toast.show('Some thing went wrong while fetching media tags', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      callback(false, error.response);
    });
};

/* GetMedia: Recieve all Media of Library */
export const getMediaPlaceholders = async callback => {
  const url = `${apiUrl}/media/placeholders`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      Accept: 'application/json; version=3',
      //   'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      // console.log('media response:', response);
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      callback(false, error.response);
      Toast.show('Something went wrong while fetching placeholders', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

/* GetMediaById: Recieve details of Specific Media */
export const getMediaById = async (mediaId, callback) => {
  const url = `${apiUrl}/media/${mediaId}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      Accept: 'application/json; version=2',
      //   'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      if (response.data) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      callback(false, error.response);
    });
};

/* Delete Team Member */
export const deleteTeamMember = async id => {
  const url = `${apiUrl}/team/members/${id}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then(function (response) {
      // if (response.data) {
      //   teamResponse(response.data);
      // }
    })
    .catch(function (error) {});
};

export const serviceGetTags = async () => {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  var url = apiUrl + 'tags?type=Medium';
  return new Promise(function (resolve, reject) {
    fetch(url, {
      method: 'GET',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        Accept: 'application/json; version=2',
        //   'Content-Type': 'application/json',
      },
    })
      .then(response => {
        response.json().then(res => {
          if (response.status == '200') resolve(res);
          else reject(res);
        });
      })
      .catch(error => {
        reject(error);
      });
  });
};

export const getTags = async setData => {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  const url = `${apiUrl}tags`;

  // ********************************

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=3',
    },
  })
    .then(function (response) {
      if (response.data) {
        setData(response.data);
      }
    })
    .catch(function (error) {
      setData(false);
      // console.log('See error messag', error);
      Toast.show('Something went wrong while fetching tags', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

export const serviceSearch = async (
  name,
  type,
  tags,
  page,
  tag_id,
  placeholder_id,
) => {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  var url = apiUrl + 'media/search';
  let formdata = new FormData();
  formdata.append('name', name);
  formdata.append('type', type);
  formdata.append('tags', tags);
  formdata.append('tag_id', tag_id);
  formdata.append('placeholder_id', placeholder_id);
  formdata.append('per_page', 25);
  formdata.append('page', page);

  return new Promise(function (resolve, reject) {
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        Accept: 'application/json; version=2',
        //   'Content-Type': 'application/json',
      },
      body: formdata,
    })
      .then(response => {
        response.json().then(res => {
          // console.log('search media:', res);
          if (response.status == '200') resolve(res);
          else reject(res);
        });
      })
      .catch(error => {
        reject(error);
      });
  });
};

export const addContact = async (params, loading, onSuccess) => {
  const url = `${apiUrl}/contacts`;
  const formdata = generateFormData(params, 'contact');

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=2',
    },
  })
    .then(async function (response) {
      loading(false);

      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('See add error', error);
      Toast.show(error.response.data.errors[0].message, {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      loading(false);
    });
};

// export const mediaSearch = async (
//   setData,
//   name,
//   type,
//   tags = '',
//   page,
//   tag_id = '',
//   placeholder_id = '',
// ) => {
//   let user = await AsyncStorage.getItem('userData');
//   user = JSON.parse(user);
//   const url = `${apiUrl}media/search?per_page=${25}&page=${page}&name=${name}&placeholder_id=${placeholder_id}&type=${type}&tag_id=${tag_id}&tags=${tags}`;
//   // console.log('See url of pai', url);

//   // ********************************

//   await axios({
//     method: 'post',
//     url: url,
//     headers: {
//       ...headers,
//       'X-Auth-Token': user.token,
//       'Content-Type': 'multipart/form-data',
//       Accept: 'application/json; version=3',
//     },
//   })
//     .then(function(response) {
//       if (response.data) {
//         setData(response.data);
//       }
//     })
//     .catch(function(error) {
//       setData(false);
//       // console.log('See error messag', error);
//       Toast.show('Something went wrong', {
//         duration: Toast.durations.SHORT,
//         backgroundColor: 'red',
//         textColor: '#FFF',
//       });
//     });
// };

export const mediaSearch = async (
  setData,
  name,
  type,
  page,
  tag_id = [],
  placeholder_id = '',
) => {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  var data = JSON.stringify({
    per_page: 25,
    placeholder_id: placeholder_id,
    type: type,
    tag_id: tag_id,
    page: page,
    name: name,
  });

  var config = {
    method: 'post',
    url: 'https://prod.recruitsuite.co/api/media/search',
    headers: {
      Authorization:
        'StackedSportsAuthKey key=7b64dc29-ee30-4bb4-90b4-af2e877b6452',
      Accept: 'application/json; version=3',
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Cookie:
        'ahoy_visitor=10ada9a2-342a-4449-991f-3563c4f9a55e; ahoy_visit=2e3be3ed-ca73-4355-b13d-08e55a461fc6; _memcache-recruitsuite_session=4aaeef2741574e6e5314752a443ed246',
    },
    data: data,
  };

  axios(config)
    .then(function (response) {
      if (response.data) {
        setData(response.data);
      }
    })
    .catch(function (error) {
      console.log('Error response', error);
      Toast.show('Something went wrong', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

// export const serviceGetTags = async callback => {
//   const url = `${apiUrl}/media`;
//   let user = await AsyncStorage.getItem('userData');
//   user = JSON.parse(user);

//   await axios({
//     method: 'get',
//     url: url,
//     headers: {
//       ...headers,
//       'X-Auth-Token': user.token,
//       Accept: 'application/json; version=2',
//       //   'Content-Type': 'application/json',
//     },
//   })
//     .then(function(response) {
//       console.log('media response:', response);
//       if (response.data) {
//         callback(response.data);
//       }
//     })
//     .catch(function(error) {
//       callback(false, error.response);
//     });
// };
