import {apiUrl, headers, homeHeaders} from './config';
import axios from 'axios';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';
import RNFetchBlob from 'react-native-fetch-blob';

const state = store.getState();

// Download prescription file
// GET : https://www.dwaae.com/test/sapi.php?_d=DownloadFile&type=prescription&id=252&file_name=7_706bad47-47bc-4e13-a63e-2b4457a4950c_1024x1024.jpg
// to get the precription file..

export const UploadEmiratesCard = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=EmiratesCardsApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
    // headers: {
    //   ...homeHeaders,
    //   'Content-Type': 'multipart/form-data',
    //   Accept: '*/*',
    // },
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};

export const DeleteEmiratesCard = async (cardId, onSuccess) => {
  const url = `${apiUrl}/sapi.php?_d=EmiratesCardsApi/${cardId}`;
  await axios({
    method: 'delete',
    url: url,
    headers: headers,
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const UploadInsuranceCard = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=InsuranceCardsApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const DeleteInsuranceCard = async (cardId, onSuccess) => {
  const url = `${apiUrl}/sapi.php?_d=InsuranceCardsApi/${cardId}`;
  await axios({
    method: 'delete',
    url: url,
    headers: headers,
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const UploadPrescriptionApi = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const UploadQuotation = async (params, onSuccess, loading) => {
  const url = `${apiUrl}/sapi.php?_d=RfqAPi`;
  await axios({
    method: 'post',
    url: url,
    data: params,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      onSuccess(false);
      loading(false);
    });
};
export const getPrescriptionList = async (user_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi&user_id=${user_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getQuotationList = async (user_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  // const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi&user_id=${user_id}&lang_code=${language}&currency_code=AED`;
  const url = `${apiUrl}/sapi.php?_d=RfqAPi&user_id=${user_id}&page=1&items_per_page=10&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getPrescriptionDetail = async (
  user_id,
  prescription_id,
  setResponse,
  loading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi&req_id=${prescription_id}&user_id=${user_id}&lang_code=${language}&currency_code=AED`;
  // const url = `${apiUrl}/sapi.php?_d=UploadPrescriptionApi&user_id=${user_id}&lang_code=${language}&currency_code=AED`;

  // {{url}}/sapi.php?_d=UploadPrescriptionApi&req_id=252&user_id={{user_id}}&lang_code={{lang_code}}&currency_code={{currency_code}}

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getQuotationDetail = async (
  user_id,
  quotation_id,
  setResponse,
  loading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=RfqAPi&rfq_id=${quotation_id}&user_id=${user_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      // console.log('response: ', response.data);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const downloadPrescription = async (
  prescription_id,
  file_name,
  setResponse,
  loading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=DownloadFile&type=prescription&id=${prescription_id}&file_name=${file_name}`;

  // let ext = getExtention(image_URL);
  // ext = '.' + ext[0];

  let date = new Date(); //To add the time suffix in filename
  let downloadDir = RNFetchBlob.fs.dirs.DownloadDir;
  RNFetchBlob.config({
    // add this option that makes response data to be stored as a file,
    // this is much more performant.
    fileCache: true,
    addAndroidDownloads: {
      //Related to the Android only
      useDownloadManager: true,
      notification: true,
      path:
        downloadDir +
        '/file_' +
        Math.floor(date.getTime() + date.getSeconds() / 2),
      // + ext,
      description: 'file',
    },
  })
    .fetch('GET', url, {
      Authorization:
        'Basic YXBpQGR3YWFlLmNvbTpjNnBSS1p1dFBUMzUwZjBUWTVmaHJNNzdLNlc3dGEwOQ==',
      // 'Basic YXBpQGR3YWFlLmNvbTpLN2Y5OGY3ODRiUFVTNjQ4ODdSTkM3U3pLcTQ0MTY0MA==',
      Cookie: 'sid_customer_4f80f=1119ec0a3bf8df03a1fbeaccc8376894-C',
    })
    .then(res => {
      // the temp file path
      console.log('The file saved to ', res.path());
      Toast.show(`File has been downloaded at ${res.path()}`);
    });
};

export const downloadVideo = image_URL => {
  //Main function to download the image
  let date = new Date(); //To add the time suffix in filename
  //Image URL which we want to download
  // let image_URL =
  //   'https://raw.githubusercontent.com/AboutReact/sampleresource/master/gift.png';
  //Getting the extention of the file
  let ext = getExtention(image_URL);
  ext = '.' + ext[0];

  //Get config and fs from RNFetchBlob
  //config: To pass the downloading related options
  //fs: To get the directory path in which we want our image to download
  const {config, fs} = RNFetchBlob;
  let PictureDir = fs.dirs.PictureDir;
  let options = {
    fileCache: true,
    addAndroidDownloads: {
      //Related to the Android only
      useDownloadManager: true,
      notification: true,
      path:
        PictureDir +
        '/video_' +
        Math.floor(date.getTime() + date.getSeconds() / 2) +
        ext,
      description: 'Video',
    },
  };
  Toast.show('Downloading Started...', {
    duration: Toast.durations.SHORT,
    backgroundColor: '#000',
    textColor: '#FFF',
  });
  config(options)
    .fetch('GET', image_URL)
    .then(res => {
      //Showing alert after successful downloading
      console.log('res -> ', JSON.stringify(res));
      // alert('Image Downloaded Successfully.');
      Toast.show('File Downloaded Successfully.', {
        duration: Toast.durations.SHORT,
        backgroundColor: '#000',
        textColor: '#FFF',
      });
      return true;
    });
};

export const getExtention = filename => {
  //To get the file extension
  return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
};
