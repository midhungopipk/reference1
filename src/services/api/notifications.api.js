import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  fetchLoggedInUser,
  startFetch,
  endFetchWithError,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();
const userState = state.user;
const signedInUsers = state.signedInUsers;

export async function registerDevice(type, token, setResponse, userData) {
  // console.log('\n\n\n\n\n\n\n\n\n user data', userData);

  let data = JSON.stringify({
    device: {
      id: token,
      type: type,
    },
  });
  const url = `${apiUrl}users/${userData.id}/devices`;
  console.log('data:', data);
  await axios({
    method: 'post',
    url: url,
    data: data,
    headers: {
      ...authHeaders,
      'X-Auth-Token': userData.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      console.log('register device token : ', response);
      if (response.status == 201) {
        setResponse(response.data);
      }
      // return response
    })
    .catch(function (response) {
      console.log('resgister device error: ', response);
      // alert(response.message)
    });
}

export async function registerPushNotification(
  type,
  token,
  setResponse,
  userData,
) {
  console.log('\n\n\n\n\n\n\n\n\n', userData);
  let formdata = new FormData();
  formdata.append('push[identity]', userData.id);
  formdata.append('push[type]', type);
  formdata.append('push[address]', token);
  const url = `${apiUrl}/push_notifications/register`;

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...authHeaders,
      'X-Auth-Token': userData.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      console.log(response);
      if (response.status == 201) {
        setResponse(response.data);
      }
      // return response
    })
    .catch(function (response) {
      console.log(response);
      // alert(response.message)
    });
}
