import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import * as RNLocalize from 'react-native-localize';

const state = store.getState();

/* GetBoards: Recive boards model request */
export const getBoards = async (setBoardsResponse, setLoading) => {
  const url = `${apiUrl}/filters`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* GetBoards: Recive boards model request */
export const getAthletes = async (
  paramter,
  setAthletesResponse,
  setLoading,
) => {
  const url = `${apiUrl}/athletes?page=${paramter.page}&per_page=${paramter.ppage}&sort_column=${paramter.sort}&sort_dir=${paramter.sortdir}&search=${paramter.search}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      // console.log('athletes response:', response);
      if (response.status == 200) {
        setAthletesResponse(response.data);
      }
    })
    .catch(function (error) {
      // console.log('athletes error:', error);
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

export const serviceCreateMessage = async msgInfo => {
  let formdata = new FormData();
  formdata.append('message[body]', msgInfo.body);
  formdata.append('message[contact_ids]', msgInfo.athletes);
  formdata.append('message[filter_ids]', msgInfo.filters);
  formdata.append('message[send_at]', msgInfo.sendat);
  //if (msgInfo.media_id != '')
  formdata.append(
    'message[media_placeholder_id]',
    msgInfo.media_placeholder_id,
  );
  formdata.append('message[media_id]', msgInfo.media_id);
  formdata.append('message[send_when_most_active]', msgInfo.mostactive);
  formdata.append('message[platform]', msgInfo.platform);

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  return new Promise(function (resolve, reject) {
    fetch(apiUrl + 'messages', {
      method: 'POST',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json; version=5',
      },

      body: formdata,
    })
      .then(response => {
        response.json().then(res => {
          console.log('create message response:', res);
          resolve(res);
          // // console.warn(res);
          // if(response.status == '200')
          //     resolve(res);
          // else
          //     reject(res);
        });
      })
      .catch(error => {
        console.log('errror in create:', error);
        reject(error);
      });
  });
};

export const serviceUpdateMessage = async (id, msgInfo) => {
  let formdata = new FormData();
  if (msgInfo.body) formdata.append('message[body]', msgInfo.body);
  if (msgInfo.contacts)
    formdata.append('message[contact_ids]', msgInfo.contacts);
  if (msgInfo.filters) formdata.append('message[filter_ids]', msgInfo.filters);
  if (msgInfo.sendat) {
    formdata.append('message[send_at]', msgInfo.sendat);
  }
  //if (msgInfo.media_id != '')
  // formdata.append('message[media_placeholder_id]', msgInfo.media_id);
  // formdata.append('message[media_id]', msgInfo.media_id);
  // formdata.append('message[send_when_most_active]', msgInfo.mostactive);
  // formdata.append('message[platform]', msgInfo.platform);

  if (msgInfo.status) {
    formdata.append('message[status]', msgInfo.status);
  }

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  const url = `${apiUrl}/messages/${id}`;

  return new Promise(function (resolve, reject) {
    fetch(url, {
      method: 'put',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        // 'Content-Type': 'multipart/form-data',
        Accept: 'application/json; version=5',
      },

      body: formdata,
    })
      .then(response => {
        response.json().then(res => {
          console.log('update message response:', res);
          resolve(res);
          // // console.warn(res);
          // if(response.status == '200')
          //     resolve(res);
          // else
          //     reject(res);
        });
      })
      .catch(error => {
        console.log('errror in update:', error.response);
        reject(error);
      });
  });
};

/* GetTaskQueues: Recive all queued tasks request */
export const getTaskQueues = async (setResponse, setLoading, date) => {
  let zone = encodeURI(RNLocalize.getTimeZone());
  // const url = `${apiUrl}task_queues?date=${date}&timezone=${zone}`;
  const url = `${apiUrl}task_queues?date=${date}&timezone=${zone}`;
  console.log('url: ', url);
  // const url = `${apiUrl}task_queues?date=${date}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      console.log('task queue response:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log('task queue error:', error);

      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* GetTaskQueues: Recive all queued tasks request */
export const getTasksPerDay = async (setResponse, setLoading) => {
  const url = `${apiUrl}task_queues/counts`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log('task queue error:', error);

      setLoading();
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* GetMessages: Recive all messages request */
export const getMessages = async (setResponse, setLoading) => {
  const url = `${apiUrl}/messages?include_all=false`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=5',
    },
  })
    .then(async function (response) {
      // console.log('messages response:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* GetDrafts: Recive all draft messages request */
export const getDrafts = async (setResponse, setLoading) => {
  const url = `${apiUrl}/messages?only_drafts=true`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=5',
    },
  })
    .then(async function (response) {
      // console.log('messages response:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* GetMessage: Recive message by ID request */
export const getMessageById = async (id, setResponse, setLoading) => {
  const url = `${apiUrl}/messages/${id}?only_sendable=true`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=5',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* CancelMessage: Cancel message by ID request */
export const cancelMessage = async (id, setResponse, setLoading) => {
  const url = `${apiUrl}/messages/${id}/cancel`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'put',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=5',
    },
  })
    .then(async function (response) {
      console.log('Cancel media res:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* DeleteMessage: Delete messages by ID request */
export const deleteMessage = async (id, setResponse, setLoading) => {
  const url = `${apiUrl}/messages/${id}&message[status]=archived`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'delete',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=5',
    },
  })
    .then(async function (response) {
      // console.log('Delete media res:', response);
      setResponse(response);
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

export async function serviceUpdateRecipientStatus(
  message_id,
  athlete_id,
  status,
  note,
) {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  let formdata = new FormData();
  formdata.append('status', status);
  if (note != null) {
    formdata.append('response', note);
  }

  return new Promise(function (resolve, reject) {
    fetch(
      apiUrl + 'messages/' + message_id + '/update_recipient/' + athlete_id,
      {
        method: 'PUT',
        headers: {
          ...headers,
          'X-Auth-Token': user.token,
          // 'Content-Type': 'multipart/form-data',
          Accept: 'application/json; version=5',
        },

        body: formdata,
      },
    )
      .then(response => {
        console.log('update recipient response :', response);
        if (response.status == '200') resolve(response);
        else reject(response);
        // response.json().then(res => {
        //   console.log('update recipient response :', res);
        //   if (response.status == '200') resolve(res);
        //   else reject(res);
        // });
      })
      .catch(error => {
        console.log('update recipient error :', error);

        reject(error);
      });
  });
}

export async function serviceCompleteMessage(messageId) {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  let formData = new FormData();
  formData.append('message[status]', 'Completed');

  return new Promise(function (resolve, reject) {
    fetch(apiUrl + 'messages/' + messageId, {
      method: 'PUT',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        Accept: 'application/json; version=5',
      },
      body: formData,
    })
      .then(response => {
        response.json().then(res => {
          if (response.status == '200') resolve(res);
          else reject(res);
        });
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function serviceCancelMessage(id) {
  return new Promise(function (resolve, reject) {
    fetch(Config.BASE_URL + 'messages/' + id + '/cancel', {
      method: 'PUT',
      headers: {
        Authorization: Config.Authorization,
        Accept: Config.AcceptV4,
        'X-Auth-Token': Config.AuthToken,
      },
    })
      .then(response => {
        response.json().then(res => {
          if (response.status == '200') resolve(res);
          else reject(res);
        });
      })
      .catch(error => {
        reject(error);
      });
  });
}
