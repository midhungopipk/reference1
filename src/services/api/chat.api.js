import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
  fetchContact,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();

/* GetChats: Recieve All Chats */
export const getChats = async onSuccess => {
  const url = `${apiUrl}/messages/inbox`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=3',
    },
  })
    .then(async function (response) {
      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('update error', error);
      onSuccess(false);
    });
};
