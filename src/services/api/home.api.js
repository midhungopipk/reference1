import {apiUrl, headers, homeHeaders, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
} from '../redux/actions';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';

const state = store.getState();

export const getHomeData = async (userID, setBoardsResponse, setLoading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=HomepageApi&lat=344343&lng=45333&user_id=${userID}&lang_code=${language}&currency_code=USD`;
  // let user = await AsyncStorage.getItem('userData');
  // user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getViewAllData = async (
  userID,
  gridId,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=HomepageProductsApi&id=${gridId}&lat=344343&lng=45333&user_id=${userID}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
    });
};
export const getMainCategories = async (setResponse, setLoading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=CategoryListApi&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getProductListingSubCategories = async (
  userId,
  categoryId,
  page,
  selectedSort,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  // const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=popularity&sort_order=desc&user_id=${userId}&lang_code=${language}&currency_code=USD`;
  const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=${selectedSort.sort_by}&sort_order=${selectedSort.sort_order}&user_id=${userId}&lang_code=${language}&currency_code=USD`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getFilters = async (
  userId,
  category_id,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=FiltersApi&category_id=${category_id}&user_id=${userId}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getProductListingParentCategories = async (
  userId,
  categoryId,
  page,
  selectedSort,
  setBoardsResponse,
  setLoading,
) => {
  //categoryID: 265
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  // const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=price&sort_order=desc&user_id=${userId}&lang_code=${language}&currency_code=AED`;
  const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=${selectedSort.sort_by}&sort_order=${selectedSort.sort_order}&user_id=${userId}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getProductListingParentCategoriesFilters = async (
  hashes,
  userId,
  categoryId,
  page,
  selectedSort,
  setBoardsResponse,
  setLoading,
) => {
  //categoryID: 265
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=${selectedSort.sort_by}&sort_order=${selectedSort.sort_order}&user_id=${userId}&lang_code=${language}&currency_code=AED&${hashes}`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //store.dispatch(endFetchTeamWithError());
      //loading(false);
    });
};
export const getFilterProducts = async (
  userID,
  categoryId,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=FiltersApi&category_id=${categoryId}&user_id=${userID}&lang_code=${language}&currency_code=AED`;

  // let user = await AsyncStorage.getItem('userData');
  // user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getProductDetail = async (
  productId,
  userID,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=ProductsApi&product_id=${productId}&lat=344343&lng=45333&lang_code=${language}&currency_code=AED&user_id=${userID}`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

// GET : {{url}}/sapi.php?_d=SearchApi&keyword=test&user_id={{user_id}}&lang_code={{lang_code}}&currency_code={{currency_code}}
export const homeSearchBar = async (
  keyword,
  userID,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=SearchApi&keyword=${keyword}&user_id=${userID}&lang_code=${language}&currency_code=AED`;
  // let user = await AsyncStorage.getItem('userData');
  // user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

//Sample for using promise in future
export async function serviceCompleteMessage(messageId) {
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  let formData = new FormData();
  formData.append('message[status]', 'Completed');

  return new Promise(function (resolve, reject) {
    fetch(apiUrl + 'messages/' + messageId, {
      method: 'PUT',
      headers: {
        ...headers,
        'X-Auth-Token': user.token,
        Accept: 'application/json; version=5',
      },
      body: formData,
    })
      .then(response => {
        response.json().then(res => {
          if (response.status == '200') resolve(res);
          else reject(res);
        });
      })
      .catch(error => {
        reject(error);
      });
  });
}
