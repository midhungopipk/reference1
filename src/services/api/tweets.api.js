import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();

/* GetTweets: Recive all tweets request */
export const getTweets = async (setResponse, setLoading) => {
  const url = `${apiUrl}/tweets`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  // let h = {
  //   ...headers,
  //   'X-Auth-Token': user.token,
  //   'Content-Type': 'multipart/form-data',
  //   Accept: 'application/json; version=1',
  // };
  // console.log('h: ', h);
  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      // console.log('tweets response:', response);
      if (response.status == 200) {
        console.log('tweets response:', response.data);
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
    });
};

/* GetTweet: Recive tweet by ID request */
export const getTweetById = async (id, setResponse, setLoading) => {
  const url = `${apiUrl}/tweets/${id}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      console.log('Tweet by id rsponse:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

/* UpdateTweet: Update tweet by ID request */
export const updateTweetById = async (
  id,
  status,
  message,
  setResponse,
  setLoading,
) => {
  const url = `${apiUrl}/tweets/${id}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'put',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
    data: {
      published_tweet: {
        status: status,
        body: message,
      },
    },
  })
    .then(async function (response) {
      console.log('Tweet update rsponse:', response);
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log('update tweet error:', error);
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};

export const createTweet = async (setCreateTweetResponse, data) => {
  let formdata = new FormData();
  // formdata.append('message[body]', msgInfo.body);

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  const url = `${apiUrl}/tweets`;
  // console.log('tweet id in createTweet api:', id);
  console.log('tweet data in createTweet api:', data);
  await axios({
    method: 'POST',
    url: url,
    data: data,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(response => {
      // response.json().then(res => {
      console.log('create tweet response:', response);
      if (response.data) {
        setCreateTweetResponse(response.data);
      }
      // resolve(res);
      // // console.warn(res);
      // if(response.status == '200')
      //     resolve(res);
      // else
      //     reject(res);
      // });
    })
    .catch(error => {
      console.log('errror in create tweet:', error);
      // reject(error);
    });
};

export const updateTweet = async (
  id,
  data,
  setCreateTweetResponse,
  setLoading,
) => {
  let formdata = new FormData();
  // formdata.append('message[body]', msgInfo.body);

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  const url = `${apiUrl}/tweets/${id}`;
  console.log('tweet data in updateTweet api:', data);
  console.log('tweet id in updateTweet api:', id);

  await axios({
    method: 'PUT',
    url: url,
    data: data,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(response => {
      // response.json().then(res => {
      console.log('update tweet response:', response);
      if (response.data) setCreateTweetResponse(response.data);
      // resolve(res);
      // // console.warn(res);
      // if(response.status == '200')
      //     resolve(res);
      // else
      //     reject(res);
      // });
    })
    .catch(error => {
      setLoading(false);
      console.log('errror in update tweet:', error.response);
      // reject(error);
    });
};
