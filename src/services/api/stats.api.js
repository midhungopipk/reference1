import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
  fetchContact,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();

/* GetContact: Get Details of Specific Contact */
export const getStats = async (onSuccess, loading, startDate, endDate) => {
  const url = `${apiUrl}/stats?start_date=${startDate}&end_date=${endDate}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      console.log('Stats responnse:', response);
      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('Stats error:', error);
      loading && loading(false);
    });
};
