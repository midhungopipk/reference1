import jwt_decode from 'jwt-decode';

export const generateFormData = (obj, endPointClass) => {
  let formdata = new FormData();
  for (let key in obj) {
    formdata.append(`${endPointClass}[${key}]`, obj[key]);
  }
  return formdata;
};

export const parseJwt = token => {
  /* other logic for the token */
  return jwt_decode(token);
};
