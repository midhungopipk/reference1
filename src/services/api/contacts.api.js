import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
  fetchContact,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();

/* GetContacts: Recieve All Contacts */
export const getContacts = async (loading, pageNumber, onSuccess) => {
  const url = `${apiUrl}/contacts?page=${pageNumber}&per_page=100&sort_column=first_name`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=2',
    },
  })
    .then(async function (response) {
      await store.dispatch(fetchContact(response.data));
      loading && loading(false);

      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('update error', error.response);
      loading && loading(false);
    });
};

/* GetContacts: Recieve All Contacts */
export const searchContacts = async (onSuccess, searchText) => {
  const url = `${apiUrl}/contacts?per_page=100&sort_column=first_name&search=${searchText}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=2',
    },
  })
    .then(async function (response) {
      // await store.dispatch(fetchContact(response.data));

      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      onSuccess(false);
      Toast.show('Something went wrong while searching', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      console.log('update error', error);
    });
};

/* GetContact: Get Details of Specific Contact */
export const getContactDetail = async (contactId, loading, onSuccess) => {
  const url = `${apiUrl}/contacts/${contactId}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=2',
    },
  })
    .then(async function (response) {
      loading(false);

      if (response.data && onSuccess) {
        // teamResponse(response.data);
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('See error', error);
      //   store.dispatch(endFetchTeamWithError());
      loading && loading(false);
    });
};

/* AddContact: Add new Contact */
export const addContact = async (params, loading, onSuccess) => {
  const url = `${apiUrl}/contacts`;
  const formdata = generateFormData(params, 'contact');
  console.log('See formdata', formdata);
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=2',
    },
  })
    .then(async function (response) {
      loading(false);

      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      const errorMessage = 'Something went wrong!';
      Toast.show(errorMessage, {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      loading(false);
    });
};

/* AddContact: Edit Existing Contact */
export const editContact = async (id, params, loading, onSuccess) => {
  console.log(params.team_tags);
  const url = `${apiUrl}contacts/${id}`;
  let formdata = new FormData();

  if (params.first_name)
    formdata.append(`contact[first_name]`, params.first_name);
  if (params.last_name) formdata.append(`contact[last_name]`, params.last_name);
  if (params.nick_name) formdata.append(`contact[nick_name]`, params.nick_name);
  if (params.phone) formdata.append(`contact[phone]`, params.phone);
  if (params.email) formdata.append(`contact[email]`, params.email);
  if (params.twitter_handle)
    formdata.append(`contact[twitter_handle]`, params.twitter_handle);
  if (params.graduation_year)
    formdata.append(`contact[graduation_year]`, params.graduation_year);
  if (params.high_school)
    formdata.append(`contact[high_school]`, params.high_school);
  if (params.state) formdata.append(`contact[state]`, params.state);
  if (params.position_tags)
    formdata.append(`contact[position_tags]`, params.position_tags);
  if (params.status_id) formdata.append(`contact[status_id]`, params.status_id);
  if (params.rank_id) formdata.append(`contact[rank_id]`, params.rank_id);
  if (params.team_tags) formdata.append(`contact[team_tags]`, params.team_tags);
  if (params.recruiting_coach_id)
    formdata.append(`contact[recruiting_coach_id]`, params.recruiting_coach_id);
  if (params.position_coach_id)
    formdata.append(`contact[position_coach_id]`, params.position_coach_id);

  // console.log('See form data in edit', formdata);
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'put',
    url: url,
    data: formdata,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=3',
    },
  })
    .then(async function (response) {
      loading(false);

      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      loading(false);
      console.log('See error messages', error.response);
      const errorMessage = 'Something went wrong';
      Toast.show(errorMessage, {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

/* ArchiveContact: Archive Existing Contact */
export const archiveContact = async (id, onSuccess) => {
  const url = `${apiUrl}contacts/${id}`;

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'DELETE',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=3',
    },
  })
    .then(async function (response) {
      onSuccess(true, id);
    })
    .catch(function (error) {
      console.log('See error', error);
      onSuccess(false);
      Toast.show('User not deleted Successfully', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

/* GetGrads: Recieve All Grad years */
export const getGradYears = async onSuccess => {
  const url = `${apiUrl}/team/grad_years`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('update error', error.response);
      onSuccess(false);
    });
};

/* GetPositions: Recieve All Positions */
export const getPositions = async onSuccess => {
  const url = `${apiUrl}/team/positions`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in postiion api', error.response);
      onSuccess(false);
    });
};

/* GetStatuses: Recieve All Statuses */
export const getStatuses = async onSuccess => {
  const url = `${apiUrl}/team/statuses`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in status api', error.response);
      onSuccess(false);
    });
};

/* GetRanks: Recieve Ranks */
export const getRanks = async onSuccess => {
  const url = `${apiUrl}/team/ranks`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in status api', error.response);
      onSuccess(false);
    });
};

/* GetCoaches: Recieve List of Coaches */
export const getTeamMembers = async onSuccess => {
  const url = `${apiUrl}/team/members`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in coaches api', error.response);
      onSuccess(false);
    });
};

/* GetCoaches: Recieve List of Coaches */
export const getRelationshipTypes = async onSuccess => {
  const url = `${apiUrl}/team/people_types`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in relationship types api', error.response);
      onSuccess(false);
    });
};

/* GetTags: Recieve List of Tags */
export const getTags = async onSuccess => {
  const url = `${apiUrl}/tags`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=3',
    },
  })
    .then(async function (response) {
      if (response.data && onSuccess) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      console.log('error in tags api', error.response);
      onSuccess(false);
    });
};

/* AddRelationship: Add new Relationship of contact */
export const addRelationship = async (contactId, params, onSuccess) => {
  console.log('See contact id', contactId);
  const url = `${apiUrl}/contacts/${contactId}/people`;
  const formdata = generateFormData(params, 'person');
  console.log('See formdata', formdata);
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      onSuccess(false);
      console.log('See add opponent error', error.response);
      Toast.show(error.response.data.week[0], {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};
