import ImagePicker from 'react-native-image-picker';
import ImageCropPicker from 'react-native-image-crop-picker';

export function image_picker(handler) {
  const options = {
    title: 'Select Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  ImagePicker.showImagePicker(response => {
    if (response.didCancel) {
      console.log('user cancel the image');
    } else if (response.error) {
      console.log('Error: ', response.error);
    } else if (response.customButton) {
      console.log('user tapped custom: ', response.customButton);
    } else {
      //  source = {uri: response.uri};
      onChange(response, 'image', handler);
    }
  });
}

export function open_image_library(handler) {
  ImageCropPicker.openPicker({
    mediaType: 'any',
    compressImageQuality: 0.8,
    compressVideoPreset: 'Passthrough',
  }).then(response => {
    console.log('See response :', response);
    handler(response);
  });
}

export function selectMultipleMediaFiles(handler) {
  ImageCropPicker.openPicker({
    mediaType: 'any',
    multiple: true,
    compressVideoPreset: 'Passthrough',
  }).then(response => {
    handler(response);
  });
}

const onChange = (response, identifier, handler) => {
  if (identifier == 'image') {
    handler(response);
  } else {
    return;
  }
};
