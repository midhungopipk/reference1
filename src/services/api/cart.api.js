import {apiUrl, headers, homeHeaders, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {fetchUser} from '../redux/actions';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';

const state = store.getState();

export const addToCart = async (cartData, setResponse, setLoading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';
  const url = `${apiUrl}/sapi.php?_d=AddToCartApi&lang_code=${language}&currency_code=AED`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const addToCartTest = async (cartData, setResponse) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=AddToCartApi&lang_code=${language}&currency_code=AED`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const deleteProductFromCart = async (cartData, setResponse) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=AddToCartApi/1&lang_code=${language}&currency_code=AE`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'put',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getCart = async (user_id, setBoardsResponse, setLoading) => {
  const url = `${apiUrl}/sapi.php?_d=AddToCartApi&user_id=${user_id}`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const deleteCart = async (cartData, setResponse, setLoading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=AddToCartApi&lang_code=${language}&currency_code=AED`;
  let dataStringified = JSON.stringify(cartData);

  // let user = await AsyncStorage.getItem('userData');
  // user = JSON.parse(user);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const checkoutCart = async (user_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=CheckoutApi&user_id=${user_id}&payment_id=&shipping_ids=&profile_id=&currency_code=AED&lang_code=${language}&gc_code=caff10`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const PlaceOrderCheckout = async (cartData, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=CheckoutApi&currency_code=AED&lang_code=${language}`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const capturePaytabPayment = async (cartData, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=PaytabCapture`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getWishList = async (user_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=WishlistApi&user_id=${user_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const addToWishList = async (cartData, setResponse) => {
  const url = `${apiUrl}/sapi.php?_d=WishlistApi`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const deleteProductFromWishList = async (item_id, setResponse) => {
  const url = `${apiUrl}/sapi.php?_d=WishlistApi/${item_id}`;

  await axios({
    method: 'delete',
    url: url,
    headers: headers,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const deleteProductWithProductIdList = async (wishData, setResponse) => {
  const url = `${apiUrl}/sapi.php?_d=WishlistApi/1`;
  let dataStringified = JSON.stringify(wishData);
  await axios({
    method: 'put',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const deleteWishList = async (params, setResponse, loading) => {
  const url = `${apiUrl}/sapi.php?_d=WishlistApi/1`;
  let dataStringified = JSON.stringify(params);
  await axios({
    method: 'put',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const addToComparisonList = async (cartData, setResponse) => {
  const url = `${apiUrl}/sapi.php?_d=WishlistApi`;
  let dataStringified = JSON.stringify(cartData);

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getOrderList = async (user_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=OrderListApi&user_id=${user_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const getOrderDetail = async (order_id, setResponse, loading) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=OrderDetailApi&order_id=${order_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
export const cancelOrder = async (
  order_id,
  orderData,
  setResponse,
  loading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';
  let dataStringified = JSON.stringify(orderData);

  const url = `${apiUrl}/sapi.php?_d=OrderDetailApi&order_id=${order_id}&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'post',
    url: url,
    data: dataStringified,
    headers: {...homeHeaders, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log(error);
      //   store.dispatch(endFetchTeamWithError());
      loading(false);
    });
};
