import {apiUrl, headers, homeHeaders, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
} from '../redux/actions';
import {store} from '../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {storageConst} from '../constants';

const state = store.getState();

export const getAllNearestVendors = async (
  user_id,
  setResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=VendorsApi&lat=344343&lng=45333&user_id=${user_id}&flavour=1&lang_code=${language}&currency_code=AED&company=&sort_by=company&sort_order=asc&page=1&items_per_page=10`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getVendorDetail = async (
  user_id,
  company_id,
  setResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=VendorApi&user_id=${user_id}&company_id=${company_id}&selected_category=25&feature_hash=&sort_by=&sort_order=&page=&items_per_page=&lat=344343&lng=45333&lang_code=${language}&currency_code=AED`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
export const getProductListingSubCategories = async (
  userId,
  categoryId,
  page,
  setBoardsResponse,
  setLoading,
) => {
  let language = await AsyncStorage.getItem(storageConst.language);
  if (language == null || language == undefined) language = 'en';

  const url = `${apiUrl}/sapi.php?_d=ProductListingApi&category_id=${categoryId}&page=${page}&items_per_page=10&sort_by=popularity&sort_order=desc&user_id=${userId}&lang_code=${language}&currency_code=USD`;

  await axios({
    method: 'get',
    url: url,
    headers: homeHeaders,
    // headers: {...headers, 'Content-Type': 'application/json'},
  })
    .then(async function (response) {
      if (response.status == 200) {
        setBoardsResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
