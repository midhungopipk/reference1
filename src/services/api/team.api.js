import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';
import {
  fetchUser,
  startFetchTeam,
  endFetchTeamWithError,
  fetchTeam,
} from '../redux/actions';
import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const state = store.getState();

/* GetTeamMembers: Recive team members model request */
export const getTeamMembers = async (page, onResponse) => {
  const url = `${apiUrl}/team/members?per_page=20&page=${page}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await store.dispatch(startFetchTeam());

  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(function (response) {
      // await store.dispatch(fetchTeam(response.data));
      // loading(false);

      if (response.data) {
        onResponse && onResponse(response.data);
      }
    })
    .catch(function (error) {
      console.log('Team get error', error);
      onResponse && onResponse(false);
      Toast.show('Something went wrong', {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
      store.dispatch(endFetchTeamWithError());
    });
};

/* Invite: Invite new team member to staff */
export const inviteMember = async (user, callback) => {
  const formdata = generateFormData(user, 'user');
  const url = `${apiUrl}/team/members`;
  let userData = await AsyncStorage.getItem('userData');
  userData = JSON.parse(userData);
  let team = [...state.team.team];

  // await store.dispatch(startFetchTeam());

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...authHeaders,
      'X-Auth-Token': userData.token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then(async response => {
      callback(response.data);
    })
    .catch(async error => {
      callback(false);
      Toast.show('Something went wrong', {
        duration: Toast.durations.LONG,
        textColor: '#FFF',
        backgroundColor: 'red',
      });
      // await store.dispatch(endFetchTeamWithError());
      // loading(false);
    });
};
// GET CONTACTS FOR TWEETS
export const getMembers = async (setResponse, setLoading) => {
  const url = `${apiUrl}/team/members`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
/*GET SNIPPETS */
export const getSnippets = async (setResponse, setLoading) => {
  const url = `${apiUrl}/team/snippets`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  await axios({
    method: 'get',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.status == 200) {
        setResponse(response.data);
      }
    })
    .catch(function (error) {
      setLoading(false);
      //   store.dispatch(endFetchTeamWithError());
      //   loading(false);
    });
};
/* Invite: Invite new team member to staff */
export const inviteMember1 = async (user, loading) => {
  const formdata = generateFormData(user, 'user');
  const url = `${apiUrl}/team/members`;
  let userData = await AsyncStorage.getItem('userData');
  userData = JSON.parse(userData);
  let team = [...state.team.team];

  await store.dispatch(startFetchTeam());

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...authHeaders,
      'X-Auth-Token': userData.token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then(async response => {
      let found = team.find(item => item.id == response.data.id);
      if (!found) {
        team.push(response.data);
        await store.dispatch(fetchTeam([...team]));
        loading(false);
        Toast.show('Invite sent successfully', {
          duration: Toast.durations.LONG,
          textColor: '#FFF',
        });
      } else {
        loading(false);
      }
    })
    .catch(async error => {
      await store.dispatch(endFetchTeamWithError());
      loading(false);
      Toast.show('Something went wrong', {
        duration: Toast.durations.LONG,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};

/* Delete Team Member */
export const deleteTeamMember = async (id, setResponse) => {
  const url = `${apiUrl}/team/members/${id}`;
  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);
  let team = [...state.team.team];

  await axios({
    method: 'DELETE',
    url: url,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then(async function (response) {
      // team = team.filter(item => item.id != id);
      setResponse(response, id);
      // await store.dispatch(fetchTeam(team));
    })
    .catch(function (error) {
      Toast.show(error.response.data.error, {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};
