import {apiUrl, headers, authHeaders} from './config';
import {generateFormData, parseJwt} from './utilities';
import axios from 'axios';

import {store} from '../../Redux';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

/* AddOpponent: Add new Opponent */
export const addOpponent = async (contactId, params, onSuccess) => {
  const url = `${apiUrl}/contacts/${contactId}/opponents`;
  const formdata = generateFormData(params, 'opponents');

  let user = await AsyncStorage.getItem('userData');
  user = JSON.parse(user);

  await axios({
    method: 'post',
    url: url,
    data: formdata,
    headers: {
      ...headers,
      'X-Auth-Token': user.token,
      'Content-Type': 'application/json',
      Accept: 'application/json; version=1',
    },
  })
    .then(async function (response) {
      if (response.data) {
        onSuccess(response.data);
      }
    })
    .catch(function (error) {
      onSuccess(false);
      // console.log('See add opponent error', error.response);
      Toast.show(error.response.data.week[0], {
        duration: Toast.durations.SHORT,
        backgroundColor: 'red',
        textColor: '#FFF',
      });
    });
};
