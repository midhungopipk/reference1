// FETCHING_FILTER_DATA
import {FETCHING_LANGUAGE} from '../constants';

const initialState = {
  language: false,
};

export const LanguageReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_LANGUAGE:
      return {
        ...state,
        ...action.data,
        // data: action.data,
      };

    default:
      return state;
  }
};
