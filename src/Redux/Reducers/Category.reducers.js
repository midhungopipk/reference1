import {FETCHING_DATA_CATEGORY_ID_SUCCESS} from '../constants';

const initialState = {
  categoryId: '',
  subCategoryId: '',
  productId: '',
  lang_code: 'en',
};

export const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_DATA_CATEGORY_ID_SUCCESS:
      return {
        ...state,
        ...action.data,
        // categoryIdNew: action.data,
      };

    default:
      return state;
  }
};
