import {
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE,
  UPDATE_TWITTER_CONECTION,
  UPDATE_TWITTER_PROFILE,
  SET_USER,
} from '../constants';

// const initialState = {
//   user: {
//     api_key: '',
//     birthday: '',
//     company: '',
//     company_id: '',
//     ec_location_detail: '',
//     email: '',
//     emirate_id_number: '',
//     fax: '',
//     firstname: '',
//     insurance_number: '',
//     last_passwords: '',
//     lastname: '',
//     phone: '',
//     purchase_timestamp_from: '',
//     purchase_timestamp_to: '',
//     referer: '',
//     responsible_email: '',
//     total_products: '',
//     url: '',
//     user_id: '',
//     user_login: '',
//     user_type: '',
//   },
//   is_root: '',
//   janrain_identifier: '',
//   lang_code: '',
//   last_login: '',
//   message: '',
//   password_change_timestamp: '',
//   status: '',
//   success: true,
//   tax_exempt: '',
//   timestamp: '',
//   usergroups: [],
//   isFeching: false,
//   error: false,
//   signedInUsers: [],
// };

const initialState = {user: {}};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_DATA:
      return {
        ...state,
        user: {},
        isFeching: true,
      };
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        ...action.data,
        isFeching: false,
      };
    case SET_USER:
      return {
        ...state,
        user: action.data,
      };
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFeching: false,
        error: true,
      };
    case UPDATE_TWITTER_CONECTION:
      return {
        ...state,
        linkedTwitter: action.data,
      };
    case UPDATE_TWITTER_PROFILE:
      return {
        ...state,
        user: {
          ...state.user,
          twitter_profile: {
            name: action.data.user.name,
            nickname: action.data.user.nickname,
          },
          photo_url: action.data.user.picture,
        },
        token: action.data.token,
      };

    default:
      return state;
  }
};

// const initialState = {
//   user: {
//     id: '',
//     first_name: 'Coach Ashley',
//     last_name: '',
//     email: 'no@mail.com',
//     phone: '999999',
//     name: '',
//     sms_number: 0,
//     last_login_at: '',
//     team: {
//       id: 0,
//       name: 'Team',
//     },
//     twitter_profile: {
//       nickname: '',
//       name: '',
//       picture:
//         'https://www.RecruitSuite.co/assets/no-avatar-9201633b2d1e5f06137c6a0f2b2a9af8f7a0b7c9636265ecf03bf90cec433f66.png',
//     },
//     photo_url: 'https://placeimg.com/300/300/people',
//     url: '',
//     user_id: 0,
//   },
//   token: '',
//   linkedTwitter: false,
//   isFeching: false,
//   error: false,
//   signedInUsers: [],

//   api_key: "", birthday: "0", company: "", company_id: "0",
//    ec_location_detail: "",
//     email: "testecommerceagain@gmail.com",
//      emirate_id_number: "", fax: "",
//       firstname: "Rajat", insurance_number: "", is_root: "N",
//        janrain_identifier: "",
//         lang_code: "en",
//     last_login: "1616588634",
//     last_passwords: "", lastname: "Agarwal",
//      message: "You have been successfully logged in.",
//       password_change_timestamp: "1",
//        phone: "+971-56-856-2620",
//     purchase_timestamp_from: "0", purchase_timestamp_to: "0",
//     referer: "", responsible_email: "",
//     status: "A",
//     success: true,
//     tax_exempt: "N", timestamp: "1616575144", total_products: "2",
//     url: "", user_id: "250",
//     user_login: "user_250",
//      user_type: "C", usergroups: []
// };
