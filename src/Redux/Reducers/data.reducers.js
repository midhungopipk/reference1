import {
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE,
  FETCHING_DATA_PEOPLE_SUCCESS,
  FETCHING_DATA_HOME_SUCCESS,
} from '../constants';

const initialState = {
  data: [],
  isFeching: false,
  error: false,
};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFeching: true,
        categoryId: '',
      };
    // case FETCHING_DATA_CATEGORY_ID_SUCCESS:
    //   return {
    //     ...state,
    //     data: action.data,
    //     isFeching: false,
    //     categoryId: action.data,
    //   };
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        // data: action.data,
        ...action.data,
        action,
        isFeching: false,
      };
    case FETCHING_DATA_HOME_SUCCESS:
      return {
        ...state,
        data: action.data,
        isFeching: false,
      };
    case FETCHING_DATA_PEOPLE_SUCCESS:
      return {
        ...state,
        dataPeople: action.dataPeople,
        isFeching: false,
      };
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFeching: false,
        error: true,
      };
    default:
      return state;
  }
};
