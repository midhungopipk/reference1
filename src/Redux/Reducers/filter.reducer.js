// FETCHING_FILTER_DATA
import {FETCHING_FILTER_DATA} from '../constants';

const initialState = {};

export const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_FILTER_DATA:
      return {
        ...state,
        ...action.data,
        // data: action.data,
      };

    default:
      return state;
  }
};
