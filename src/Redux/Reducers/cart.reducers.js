import {FETCHING_CART_DATA, FETCHING_CART_CHECKOUT_DATA} from '../constants';

const initialState = {data: []};

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CART_DATA:
      return {
        ...state,
        ...action.data,
        data: action.data,
      };

    case FETCHING_CART_CHECKOUT_DATA:
      return {
        ...state,
        ...action.data,
        data: action.data,
      };

    default:
      return state;
  }
};
