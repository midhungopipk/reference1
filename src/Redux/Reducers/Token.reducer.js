// FETCHING_FILTER_DATA
import {FETCHING_TOKEN} from '../constants';

const initialState = {
  token: false,
};

export const TokenReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_TOKEN:
      return {
        ...state,
        ...action.data,
        // data: action.data,
      };

    default:
      return state;
  }
};
