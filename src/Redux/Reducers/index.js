import Auth from './Auth';
import Navigation from './Navigation';
import {dataReducer} from './data.reducers';
import {userReducer} from './user.reducers';
import {categoryReducer} from './Category.reducers';
import {cartReducer} from './cart.reducers';
import {filterReducer} from './filter.reducer';
import {TokenReducer} from './Token.reducer';
import {LanguageReducer} from './LanguageSelection.reducers';

import {combineReducers} from 'redux';

export default combineReducers({
  data: dataReducer,
  user: userReducer,
  category: categoryReducer,
  cart: cartReducer,
  filter: filterReducer,
  Token: TokenReducer,
  Language: LanguageReducer,
  Auth: Auth,
  Navigation: Navigation,
});
