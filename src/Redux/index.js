import {createStore, applyMiddleware} from 'redux';
import Reducers from './Reducers';
import ReduxThunk from 'redux-thunk';

const store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));
export default store;

// export const store = configureStore();

// import {createStore, applyMiddleware} from 'redux';
// import ReduxThunk from 'redux-thunk';
// import Reducers from './Reducers/index';
// const store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));
// export default store;
