import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {setScreenState} from '../../../Redux/Actions/Navigation';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  SmallText,
  MyPointsBanner,
  RegularText,
  SmallTitle,
  MediumTitle,
  TinyTitle,
  TinierTitle,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {ImageBackground} from 'react-native';

export default function MyPoints({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([
    {img: appImages.bloodTest},
    {img: appImages.swabTest},
    {img: appImages.vaccine},
  ]);
  const [confirmedTests, setConfirmedTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
    },
  ]);
  const [historyTests, setHistoryTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
  ]);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <MyPointsBanner
            source={appImages.pointsHeader}
            imageStyle={styles.bannerImage}
            onBackPress={() => navigation.pop()}
          />
          <View style={{height: height(100)}}></View>
          <View
            style={{
              position: 'absolute',
              alignSelf: 'center',
              top: Platform.OS == 'ios' ? height(23) : height(31),
            }}>
            <FlatList
              data={categoryData1}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              renderItem={({index, item}) => (
                <ImageBackground
                  source={appImages.pointsCard}
                  style={[styles.cardBgImage, {elevation: 1}]}
                  imageStyle={[
                    {
                      borderRadius: totalSize(2),
                    },
                  ]}>
                  <View style={styles.insideCardContainer}>
                    <View>
                      {Platform.OS == 'ios' ? (
                        <TinierTitle>Dwaae</TinierTitle>
                      ) : (
                        <TinyTitle>Dwaae</TinyTitle>
                      )}
                      {Platform.OS == 'ios' ? (
                        <SmallText style={appStyles.textGray}>
                          5 AED Gift Coupen
                        </SmallText>
                      ) : (
                        <RegularText style={appStyles.textGray}>
                          5 AED Gift Coupen
                        </RegularText>
                      )}
                    </View>
                  </View>
                </ImageBackground>
              )}
            />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
