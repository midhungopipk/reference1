import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
  ActivityIndicator,
  I18nManager,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {CommonActions} from '@react-navigation/native';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  SearchBarHome,
  Spacer,
  ImageCategoriesWOText,
  HomeCategories,
  LogoMainImage,
  ButtonPrescription,
  SmallTitle,
  TinyTitle,
  SmallText,
  SliderHomeBanner,
  RegularText,
  CustomIcon,
  LoadingCard,
  HomeItemCard,
  TouchableCustomIcon,
  MediumText,
  VersionModal,
} from '../../../components';
import {
  appImages,
  routes,
  sizes,
  storageConst,
  appStyles,
  fontFamily,
} from '../../../services';
import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {
  getHomeData,
  homeSearchBar,
  getProductListingParentCategories,
} from '../../../services/api/home.api';
import {addToCart, getCart, getWishList} from '../../../services/api/cart.api';
import {FAB} from 'react-native-paper';
/* Redux */
import {
  fetchHomeData,
  fetchUser,
  fetchCategoryId,
  fetchCartData,
  fetchFilterData,
  fetchToken,
} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToCartMethod,
  AddToWishListMethod,
  Translate,
  RemoveProductFromWishListMethod,
  RemoveProductFromWLMethod,
} from '../../../services/helpingMethods';
import messaging from '@react-native-firebase/messaging';
import FastImage from 'react-native-fast-image';
import {Linking} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {ScrollView} from 'react-native';
// import {WalkthroughElement, startWalkthrough} from 'react-native-walkthrough';
// import profileWalkthrough from '../guides/profileWalkthrough';

export default function Home({navigation, route}) {
  let listViewRef;
  const [loading, setLoading] = useState(true);
  const [cartLoading, setCartLoading] = useState(false);
  const [dataLoading, setDataLoading] = useState(true);
  const [wishLoading, setwishLoading] = useState(true);
  const [categoryChangeLoading, setCategoryChangeLoading] = useState(false);
  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [searchBool, setSearchBool] = useState(false);
  const [searchProducts, setSearchProducts] = useState([]);
  const [mainBanner, setMainBanner] = useState([]);
  const [ProductsData, setProductsData] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [changeableTitle, setChangeableTitle] = useState('');
  const [response, setResponse] = useState([]);
  const [categoryData, setCategoryData] = useState([]);
  const [productMedicationData, setProductMedicationData] = useState([]);
  const [productTopSellerData, setProductTopSellerData] = useState([]);
  const [productNewestData, setProductNewestData] = useState([]);
  const [bannerData, setBannerData] = useState([]);
  const [wishListData, setWishListData] = useState([]);
  const [liveSearchData, setLiveSearchData] = useState([]);
  const [categoryIndex, setCategoryIndex] = useState(0);
  const [liveLoading, setLiveLoading] = useState(false);
  const [LTR, setLTR] = useState(true);
  const [liveSearchBool, setLiveSearchBool] = useState(false);
  const [versionVisible, setVersionVisible] = useState(false);
  const [showCross, setShowCross] = useState(false);
  const [selectedSort, setSelectedSort] = useState({
    sort_by: 'popularity',
    sort_order: 'desc',
  });
  const dispatch = useDispatch();
  const flatListRef = React.useRef();

  // const tokenn = useSelector(state => state.Token.token);
  // console.log('tokenn: ', tokenn);

  useEffect(async () => {
    console.log('current version::::', sizes.current_version);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }

    await messaging()
      .subscribeToTopic('HomedevoApp')
      .then(() => console.log('Subscribed to topic!'));
    setLoading(true);
    getHomeData(user.user_id, getHomeResponse, setLoading);
    console.log('user: ', user.user_id);
    getCart(user.user_id, onGetCartResponse, setLoading);
    getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      let language = await AsyncStorage.getItem(storageConst.language);

      if (language == 'ar') {
        setLTR(false);
      }
      if (user.user_id) {
        setLoading(true);
        getHomeData(user.user_id, getHomeResponse, setLoading);
        console.log('user: ', user.user_id);
        getCart(user.user_id, onGetCartResponse, setLoading);
        getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
      }
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    setTimeout(() => {
      computeLikes();
    }, 2000);
  }, [wishListData]);

  //Wish List API response
  const onGetWishlistResponse = async res => {
    console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) {
        setWishListData(res.products);
      }
      setwishLoading(false);
    }
    setwishLoading(false);
  };

  //Wish list computing likes
  const computeLikes = async () => {
    if (wishListData.length) {
      if (ProductsData.length) {
        let completeData = ProductsData;

        for (let i = 0; i < completeData.length; i++) {
          if (completeData[i].type == 'products') {
            for (let j = 0; j < completeData[i].data.length; j++) {
              for (let k = 0; k < wishListData.length; k++) {
                if (
                  wishListData[k].product_id ==
                  completeData[i].data[j].product_id
                ) {
                  console.log(
                    'products matched',
                    completeData[i].data[j].product,
                  );
                  completeData[i].data[j].liked = true;
                }
              }
            }
          }
        }

        for (let i = 0; i < completeData.length; i++) {
          if (
            completeData[i].type == 'products' &&
            completeData[i].title == 'Medications'
          ) {
            setProductMedicationData(completeData[i].data);
            // setProductMedicationData(prev => [...prev]);
            break;
          }
        }
        // setTimeout(() => {
        //   setProductsData(completeData);
        //   setProductsData(prev => [...prev]);
        // }, 200);
        setProductsData(completeData);
        setProductsData(prev => [...prev]);
        // setTimeout(() => {
        //   console.log(ProductsData);
        // }, 200);
        // setProductsData(prev => [...prev]);

        // console.log('complete data after computing likes: ', completeData);
        // console.log('comparison wish list: ', wishListData);
      }
    }
  };

  //Get cart API response
  const onGetCartResponse = async res => {
    console.log('Cart API: ', res);
    if (res) {
      // if (res.cart && res.cart.cart_products) {
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      console.log('from store: ', store.getState().cart.data.data);
      //Kind of force Update to show cart value
      navigation.navigate('Home');
      // }
    }
  };

  //Get Home API response
  const getHomeResponse = async res => {
    console.log('home API: ', res);
    // setLoading(false);
    // let language = await AsyncStorage.getItem(storageConst.language);
    if (res) {
      // Sending Data to redux store
      await store.dispatch(
        fetchHomeData({
          data: res,
        }),
      );

      if (res.api_version && sizes.current_version) {
        if (res.api_version != sizes.current_version) {
          console.log(res.api_version);
          setVersionVisible(true);
        } else {
          setVersionVisible(false);
        }
      }

      for (let i = 0; i < res.layout.length; i++) {
        if (res.layout[i].type == 'products') {
          let likeList = res.layout[i].data;
          likeList.forEach(element => {
            element.liked = false;
          });
          // console.log('likes updated', likeList);
          res.layout[i].data = likeList;
        }
      }

      setResponse(res);
      setProductsData(res.layout);
      for (let i = 0; i < res.layout.length; i++) {
        if (res.layout[i].type == 'banner') {
          if (res.layout[i].data.length) {
            let bannerImages = [];
            res.layout[i].data.forEach(element => {
              bannerImages.push(element.image_path);
            });
            setBannerData(res.layout[i].data);
            setMainBanner(bannerImages);
          }
          continue;
        }
        if (res.layout[i].type == 'categories') {
          // if (language == 'ar') {
          //   setCategoryData(res.layout[i].data.reverse());
          // } else {
          //   setCategoryData(res.layout[i].data);
          // }
          setCategoryData(res.layout[i].data);
          getProductListingParentCategories(
            user.user_id,
            res.layout[i].data[0].category_id,
            0,
            selectedSort,
            onGetChangeCategoryResponse,
            setCategoryChangeLoading,
          );
          continue;
        }
      }
      setLoading(false);
    }
    setLoading(false);
    // setTimeout(() => {
    //   computeLikes();
    // }, 2000);
  };

  //Get cart API response
  const getCartResponse = async res => {
    console.log('Cart API: ', res);
    setCartLoading(false);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        if (user.user_id == '0') {
          let updatedUser = user;
          user.user_id = res.guest_id;
          console.log('Previous user: ', user);
          console.log('Updated user: ', updatedUser);
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
        await store.dispatch(
          fetchCartData({
            data: res.total_products,
          }),
        );
        navigation.navigate('Home');
      }
    }
    // setLoading(false);
  };

  //Get Wish List API response
  const getWishResponse = async res => {
    console.log('Wishlist API: ', res);
    if (res) {
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  //Heading Container Component
  const HeadingContainer = props => {
    return (
      <View>
        {LTR ? (
          <View
            style={[
              styles.titleRowContainer,
              // {transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]},
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyTitle style={props.style}>{props.title}</TinyTitle>
            ) : (
              <SmallTitle style={[props.style, {fontWeight: 'bold'}]}>
                {props.title}
              </SmallTitle>
            )}

            {props.viewAll ? (
              <TouchableOpacity
                style={styles.viewAllButton}
                onPress={() => {
                  console.log('grid id: ', props.gridId);
                  navigation.navigate(props.route, {id: props.gridId});
                }}>
                {Platform.OS == 'ios' ? (
                  <RegularText>{Translate('Show All') + ' >'}</RegularText>
                ) : (
                  <MediumText>{Translate('Show All')}</MediumText>
                )}
              </TouchableOpacity>
            ) : (
              <View style={styles.simpleRow}>
                <TouchableCustomIcon
                  icon={appImages.back}
                  onPress={() => handleCategoryChange(false)}
                  // onPress={UpButtonHandler}
                  size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
                />

                {/* <WalkthroughElement id="change-categories"> */}
                <TouchableCustomIcon
                  onPress={() => handleCategoryChange(true)}
                  // onPress={DownButtonHandler}
                  icon={appImages.next}
                  size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
                  style={[{marginLeft: totalSize(1.5)}]}
                />
                {/* </WalkthroughElement> */}
              </View>
            )}
          </View>
        ) : (
          <View
            style={[
              styles.titleRowContainer,
              // {transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]},
            ]}>
            {props.viewAll ? (
              <TouchableOpacity
                style={styles.viewAllButton}
                onPress={() => {
                  navigation.navigate(props.route, {id: props.gridId});
                }}>
                {Platform.OS == 'ios' ? (
                  <RegularText>{Translate('Show All') + ' >'}</RegularText>
                ) : (
                  <MediumText>{Translate('Show All')}</MediumText>
                )}
              </TouchableOpacity>
            ) : (
              <View style={[styles.simpleRow, {transform: [{scaleX: -1}]}]}>
                <TouchableCustomIcon
                  icon={appImages.back}
                  onPress={() => handleCategoryChange(false)}
                  // onPress={UpButtonHandler}
                  size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
                />

                {/* <WalkthroughElement id="change-categories"> */}
                <TouchableCustomIcon
                  onPress={() => handleCategoryChange(true)}
                  // onPress={DownButtonHandler}
                  icon={appImages.next}
                  size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
                  style={[{marginLeft: totalSize(1.5)}]}
                />
                {/* </WalkthroughElement> */}
              </View>
            )}

            {Platform.OS == 'ios' ? (
              <TinyTitle style={props.style}>{props.title}</TinyTitle>
            ) : (
              <SmallTitle style={[props.style, {fontWeight: 'bold'}]}>
                {props.title}
              </SmallTitle>
            )}
          </View>
        )}
      </View>
    );
  };

  const UpButtonHandler = () => {
    //OnCLick of Up button we scrolled the list to top
    listViewRef.scrollToOffset({offset: 0, animated: true});
  };

  const DownButtonHandler = () => {
    //OnCLick of down button we scrolled the list to bottom
    listViewRef.scrollToEnd({animated: true});
  };

  //Search Bar Text Handler Component
  const handleSearchBarText = text => {
    //categoryData, productMedicationData, productTopSellerData, productNewestData
    setDataLoading(true);
    if (text != '') {
      homeSearchBar(text, user.user_id, onGetSearchResponse, dataLoading);
    } else {
      removeFilter();
    }
  };

  //Live Search Bar Text Handler Component
  const handleLiveSearchText = text => {
    //categoryData, productMedicationData, productTopSellerData, productNewestData
    setLiveLoading(true);
    if (text != '') {
      homeSearchBar(text, user.user_id, onGetLiveSearchResponse, dataLoading);
    } else {
      removeFilter();
    }
  };

  //Get Live Search Text API Response
  const onGetLiveSearchResponse = async res => {
    console.log('Search API: ', res);
    // setLoading(false);
    if (res.success) {
      if (res.products.length) {
        setLiveSearchData(res.products);
      }

      // setResponse(res);
      setLiveLoading(false);
    }
    setLiveLoading(false);
  };

  //Get Search Text API Response
  const onGetSearchResponse = async res => {
    console.log('Search API: ', res);
    // setLoading(false);
    if (res.success) {
      if (res.products.length) {
        res.products.forEach(element => {
          element.liked = false;
        });

        let newProductMedicationData = res.products;
        for (let i = 0; i < newProductMedicationData.length; i++) {
          for (let j = 0; j < wishListData.length; j++) {
            if (
              newProductMedicationData[i].product_id ==
              wishListData[j].product_id
            ) {
              newProductMedicationData[i].liked = true;
            }
          }
        }
        setSearchProducts(newProductMedicationData);
      }

      // setResponse(res);
      setDataLoading(false);
    }
    setDataLoading(false);
  };

  //Handle Banner Press
  const handleBannerPress = async index => {
    if (bannerData[index].type == 'P') {
      navigation.navigate(routes.productDetail, {
        productId: bannerData[index].location_id,
      });
    } else if (bannerData[index].type == 'C') {
      // await store.dispatch(
      //   fetchCategoryId({
      //     categoryId: bannerData[index].location_id,
      //   }),
      // );
      let categoryId = {
        categoryId: bannerData[index].location_id,
      };
      dispatch(fetchCategoryId(categoryId));
      await store.dispatch(
        fetchFilterData({
          data: {},
        }),
      );
      navigation.navigate(routes.productListing);
    }
  };

  //Handle Category Change Category Press
  const handleCategoryChange = async side => {
    let tempIndex = categoryIndex;
    if (side) {
      if (tempIndex < categoryData.length - 1) {
        tempIndex += 1;
      } else {
        tempIndex = 0;
      }
    } else {
      if (tempIndex > 0) {
        tempIndex -= 1;
      } else {
        tempIndex = categoryData.length - 1;
      }
    }
    setCategoryIndex(tempIndex);
    setCategoryChangeLoading(true);
    getProductListingParentCategories(
      user.user_id,
      categoryData[tempIndex].category_id,
      0,
      selectedSort,
      onGetChangeCategoryResponse,
      setCategoryChangeLoading,
    );
    console.log('side: ', side);
  };

  //Change Category API Response
  const onGetChangeCategoryResponse = async res => {
    console.log('Change category API: ', res);
    if (res.success) {
      setProductMedicationData(res.products);
      setChangeableTitle(res.category_name);
      setCategoryChangeLoading(false);
    }
    setCategoryChangeLoading(false);
  };

  //Search Filter Function
  const SearchFilterFunction = text => {
    //categoryData, productMedicationData, productTopSellerData, productNewestData
    if (text != '') {
      //setCategoryData
      const CategoryFilterData = categoryData.filter(function (item) {
        const itemData = item.category
          ? item.category.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setCategoryData(CategoryFilterData);

      //setProductMedicationData
      const productMedication = productMedicationData.filter(function (item) {
        const itemData = item.product
          ? item.product.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setProductMedicationData(productMedication);

      //setProductTopSellerData
      const productTopSeller = productTopSellerData.filter(function (item) {
        const itemData = item.product
          ? item.product.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setProductTopSellerData(productTopSeller);

      //setProductNewestData
      const productNewest = productNewestData.filter(function (item) {
        const itemData = item.product
          ? item.product.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setProductNewestData(productNewest);
    } else {
      removeFilter();
    }
  };

  //Remove Filter When Text Is Empty
  const removeFilter = () => {
    let res = response;
    for (let i = 0; i < res.layout.length; i++) {
      if (res.layout[i].type == 'banner') {
        if (res.layout[i].data.length) {
          let bannerImages = [];
          res.layout[i].data.forEach(element => {
            bannerImages.push(element.image_path);
          });
          bannerImages.splice(bannerImages.length - 1, 1);
          // console.log(bannerImages);
          setMainBanner(bannerImages);
        }

        continue;
      }
      if (res.layout[i].type == 'categories') {
        setCategoryData(res.layout[i].data);
        continue;
      }
      if (
        res.layout[i].type == 'products' &&
        res.layout[i].title == 'Medications'
      ) {
        setProductMedicationData(res.layout[i].data);
        continue;
      }
      if (
        res.layout[i].type == 'products' &&
        res.layout[i].title == 'Top Sellers'
      ) {
        setProductTopSellerData(res.layout[i].data);
        continue;
      }
      if (
        res.layout[i].type == 'products' &&
        res.layout[i].title == 'Newest products'
      ) {
        setProductNewestData(res.layout[i].data);
        continue;
      }
    }
  };

  //Empty Place Holder Text
  const EmptyPlaceholderText = ({text}) => {
    return <RegularText style={{alignSelf: 'center'}}></RegularText>;
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />

      {loading ? (
        <LoadingCard />
      ) : (
        // <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={[styles.row, {paddingLeft: width(5)}]}>
            <View style={{marginTop: height(4)}}>
              <LogoMainImage
                logo={appImages.logoFull}
                size={Platform.OS == 'ios' ? totalSize(10) : totalSize(12)}
                style={{width: width(35), height: height(12)}}
              />
            </View>
            <Image source={appImages.homeVectorBg} style={styles.vectorBg} />
          </View>

          <KeyboardAwareScrollView>
            {/* <WalkthroughElement id="search-products"> */}
            <SearchBarHome
              placeholder={Translate('Search Products')}
              customIconLeft={appImages.find}
              onChangeText={text => {
                setSearchText(text);
                if (text == '') {
                  setSearchBool(false);
                  setLiveLoading(false);
                }

                if (text.length > 2) {
                  setLiveSearchBool(true);
                  handleLiveSearchText(text);
                  setShowCross(true);
                } else {
                  setShowCross(false);
                }
              }}
              ShowCross={showCross}
              onCrossPress={() => {
                setSearchText('');
                setSearchBool(false);
                setLiveLoading(false);
                setShowCross(false);
              }}
              onPress={() => {
                if (searchText != '') {
                  setSearchBool(true);
                  setLiveSearchBool(false);
                  handleSearchBarText(searchText);
                }
              }}
              onSubmitEditing={() => {
                if (searchText != '') {
                  setSearchBool(true);
                  setLiveSearchBool(false);
                  handleSearchBarText(searchText);
                }
              }}
              value={searchText}
              // customIconRight={searchText == '' ? null : appImages.crossIcon}
              customIconRight={appImages.UploadPresc}
              iconSizeRight={Platform.OS == 'ios' ? height(5.5) : height(7.5)}
              onRightIconPress={() =>
                navigation.navigate(routes.uploadPrescription)
              }
              LTR={LTR}
            />

            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin
              }
            />
            {liveSearchBool && searchText.length > 2 ? (
              <View style={[styles.searchContainerAbsolute]}>
                {liveLoading ? (
                  <ActivityIndicator />
                ) : liveSearchData.length ? (
                  <FlatList
                    data={liveSearchData}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(contact, index) => String(index)}
                    renderItem={({index, item}) => (
                      <View
                        style={[
                          styles.row,
                          {
                            marginVertical: height(1),
                            borderBottomWidth: 1,
                            borderColor: colors.borderLightColor,
                          },
                        ]}>
                        <TouchableOpacity
                          onPress={() => {
                            navigation.navigate(routes.productDetail, {
                              productId: item.product_id,
                            });
                            setLiveSearchBool(false);
                          }}>
                          <MediumText>{item.product}</MediumText>
                        </TouchableOpacity>
                        <TouchableCustomIcon
                          icon={appImages.searchBarIcon}
                          size={totalSize(2)}
                        />
                      </View>
                    )}
                  />
                ) : (
                  <RegularText>No data found</RegularText>
                )}
              </View>
            ) : (
              <>
                <Spacer height={sizes.smallMargin} />

                {searchBool ? (
                  dataLoading ? (
                    <ActivityIndicator
                      size={'large'}
                      style={{marginTop: sizes.baseMargin}}
                    />
                  ) : searchProducts.length ? (
                    <>
                      <FlatList
                        data={searchProducts}
                        extraData={searchProducts}
                        contentContainerStyle={{
                          alignItems: 'center',
                        }}
                        numColumns={2}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(contact, index) => String(index)}
                        renderItem={({index, item}) => (
                          <HomeItemCard
                            LTR={LTR ? true : false}
                            item={item}
                            animation={'fadeIn'}
                            onPress={() =>
                              navigation.navigate(routes.productDetail, {
                                productId: item.product_id,
                              })
                            }
                            addToCartPress={async () => {
                              // if (user.user_id == '0') {
                              //   navigation.navigate(routes.signin);
                              // } else {
                              if (!cartLoading) {
                                setCartLoading(true);
                                await AddToCartMethod({
                                  item,
                                  user,
                                  getCartResponse,
                                });
                              }
                              // }
                            }}
                            onWishPress={async () => {
                              let newList = searchProducts;
                              if (!newList[index].liked) {
                                await AddToWishListMethod({
                                  item,
                                  user,
                                  getWishResponse,
                                });
                                newList[index].liked = true;
                                setSearchProducts(newList);
                                setSearchProducts(prev => [...prev]);
                              } else {
                                await RemoveProductFromWLMethod({
                                  user_id: user.user_id,
                                  product_id: item.product_id,
                                  getWishResponse,
                                });
                                newList[index].liked = false;
                                setSearchProducts(newList);
                                setSearchProducts(prev => [...prev]);
                              }
                            }}
                          />
                        )}
                      />
                      <Spacer height={sizes.doubleBaseMargin} />
                    </>
                  ) : (
                    <RegularText
                      style={{
                        alignSelf: 'center',
                        marginTop: sizes.baseMargin,
                      }}>
                      No data found
                    </RegularText>
                  )
                ) : (
                  <>
                    <View style={styles.categoryMainContainer}>
                      {categoryData.length ? (
                        <FlatList
                          ref={flatListRef}
                          data={categoryData}
                          extraData={categoryData}
                          horizontal
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(contact, index) => String(index)}
                          renderItem={({index, item}) => (
                            <HomeCategories
                              source={
                                item.image_url && item.image_url != ''
                                  ? {uri: item.image_url}
                                  : null
                              }
                              onPress={async () => {
                                let categoryId = {
                                  categoryId: item.category_id,
                                };
                                dispatch(fetchCategoryId(categoryId));
                                await store.dispatch(
                                  fetchFilterData({
                                    data: {},
                                  }),
                                );
                                navigation.navigate(routes.productListing);
                              }}
                              item={item}
                            />
                          )}
                        />
                      ) : (
                        <EmptyPlaceholderText />
                      )}
                    </View>
                    <Spacer
                      height={
                        Platform.OS == 'ios'
                          ? sizes.baseMargin
                          : sizes.baseMargin
                      }
                    />
                    <SliderHomeBanner
                      images={
                        mainBanner.length
                          ? mainBanner
                          : [appImages.placeHolderProductDetail]
                      }
                      onPress={index => {
                        handleBannerPress(index);
                      }}
                    />
                    {/* <Spacer height={sizes.baseMargin} /> */}
                    <View style={styles.parentColorContainer}>
                      <View style={[styles.categoryMainContainer]}>
                        <Spacer height={sizes.baseMargin} />
                        <HeadingContainer title={changeableTitle} />
                        <Spacer height={sizes.smallMargin} />

                        <FlatList
                          data={productMedicationData}
                          extraData={productMedicationData}
                          removeClippedSubviews={true}
                          horizontal
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(contact, index) => String(index)}
                          renderItem={({index, item}) => (
                            <HomeItemCard
                              LTR={LTR ? true : false}
                              item={item}
                              onPress={() =>
                                navigation.navigate(routes.productDetail, {
                                  productId: item.product_id,
                                })
                              }
                              addToCartPress={async () => {
                                // if (user.user_id == '0') {
                                //   navigation.navigate(routes.signin);
                                // } else {
                                if (!cartLoading) {
                                  setCartLoading(true);
                                  await AddToCartMethod({
                                    item,
                                    user,
                                    getCartResponse,
                                  });
                                }
                              }}
                              onWishPress={async () => {
                                let newList = productMedicationData;
                                if (!newList[index].liked) {
                                  await AddToWishListMethod({
                                    item,
                                    user,
                                    getWishResponse,
                                  });
                                  newList[index].liked = true;
                                  setProductMedicationData(newList);
                                  setProductMedicationData(prev => [...prev]);
                                } else {
                                  if (newList[index].liked) {
                                    await RemoveProductFromWLMethod({
                                      user_id: user.user_id,
                                      product_id: item.product_id,
                                      getWishResponse,
                                    });
                                    newList[index].liked = false;
                                    setProductMedicationData(newList);
                                    setProductMedicationData(prev => [...prev]);
                                  }
                                }
                              }}
                            />
                          )}
                          ref={ref => {
                            listViewRef = ref;
                          }}
                        />

                        <Spacer height={sizes.baseMargin} />
                      </View>
                    </View>

                    {ProductsData &&
                      ProductsData.length &&
                      ProductsData.map((itemm, indexx) => {
                        return itemm.type == 'products' &&
                          itemm.title != 'Medications' ? (
                          <>
                            <View style={styles.categoryMainContainer}>
                              <Spacer height={sizes.baseMargin} />
                              <HeadingContainer
                                title={itemm.title}
                                gridId={itemm.id ? itemm.id : '2'}
                                // gridId={'3'}
                                viewAll
                                route={routes.viewProducts}
                              />
                              <Spacer height={sizes.smallMargin} />
                              <FlatList
                                data={itemm.data}
                                extraData={itemm.data}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(contact, index) => String(index)}
                                renderItem={({index, item}) => (
                                  <HomeItemCard
                                    LTR={LTR ? true : false}
                                    item={item}
                                    onPress={() =>
                                      navigation.navigate(
                                        routes.productDetail,
                                        {
                                          productId: item.product_id,
                                        },
                                      )
                                    }
                                    addToCartPress={async () => {
                                      console.log('here');
                                      if (!cartLoading) {
                                        setCartLoading(true);
                                        await AddToCartMethod({
                                          item,
                                          user,
                                          getCartResponse,
                                        });
                                      }
                                    }}
                                    onWishPress={async () => {
                                      let allData = ProductsData;
                                      let newList = ProductsData[indexx].data;
                                      if (!newList[index].liked) {
                                        await AddToWishListMethod({
                                          item,
                                          user,
                                          getWishResponse,
                                        });
                                        newList[index].liked = true;
                                        allData[indexx].data = newList;
                                        setProductsData(allData);
                                        setProductsData(prev => [...prev]);
                                      } else {
                                        await RemoveProductFromWLMethod({
                                          user_id: user.user_id,
                                          product_id: item.product_id,
                                          getWishResponse,
                                        });
                                        newList[index].liked = false;
                                        allData[indexx].data = newList;
                                        setProductsData(allData);
                                        setProductsData(prev => [...prev]);
                                      }
                                    }}
                                  />
                                )}
                              />
                              <Spacer height={sizes.baseMargin} />
                            </View>
                            <Spacer
                              height={
                                Platform.OS == 'ios'
                                  ? sizes.TinyMargin
                                  : sizes.baseMargin
                              }
                            />
                          </>
                        ) : null;
                      })}
                  </>
                )}
              </>
            )}
          </KeyboardAwareScrollView>
          {/* <WalkthroughElement id="ask-pharmacy"> */}
          <TouchableOpacity
            onPress={() => {
              if (LTR) {
                Linking.openURL(
                  'whatsapp://send?text=' +
                    'Hello HomeDevo team' +
                    '&phone=+971506333105',
                )
                  .then(data => {
                    console.log('WhatsApp Opened successfully ' + data); //<---Success
                  })
                  .catch(() => {
                    // alert('Make sure WhatsApp installed on your device'); //<---Error
                    Linking.openURL(
                      `sms:&addresses=+971506333105&body=Hello HomeDevo team`,
                    );
                  });
              } else {
                Linking.openURL(
                  'whatsapp://send?text=' +
                    'HomeDevo مرحبا فريق' +
                    '&phone=+971506333105',
                )
                  .then(data => {
                    console.log('WhatsApp Opened successfully ' + data); //<---Success
                  })
                  .catch(() => {
                    // alert('Make sure WhatsApp installed on your device'); //<---Error
                    Linking.openURL(
                      `sms:&addresses=+971506333105&body=HomeDevo مرحبا فريق`,
                    );
                  });
              }
            }}
            style={styles.askBg}>
            <FastImage
              source={appImages.askBg}
              style={styles.askImage}
              resizeMode={'contain'}
            />

            <RegularText
              style={[
                appStyles.textWhite,
                {
                  position: 'absolute',
                  top: height(3),
                },
              ]}>
              {Translate('Ask Support')}
            </RegularText>
          </TouchableOpacity>
          {/* <FAB
            style={styles.fab}
            label={Translate('Ask Pharmacist')}
            small
            icon="plus"
            onPress={() => console.log('Pressed')}
            animated={true}
          /> */}
          {categoryChangeLoading ? (
            <View style={styles.loaderBg}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
          {cartLoading ? (
            <View style={styles.loaderBg}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
        </View>
      )}
      <VersionModal
        isVisible={versionVisible}
        onPress={() => {
          Platform.OS == 'ios'
            ? Linking.openURL(
                'https://apps.apple.com/pk/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328',
              )
            : Linking.openURL(
                'https://play.google.com/store/apps/details?id=com.dwaae.smartapp',
              );
        }}
      />
    </Fragment>
  );
}
