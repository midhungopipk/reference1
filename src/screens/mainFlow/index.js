import Home from './home';
import Categories from './categories';
import SubCategories from './subCategories';
import Filters from './filter';
import ProductListing from './productListing';
import FilterProducts from './filterProducts';
import ProductDetail from './productDetail';
import Services from './services';
import CovidTest from './covidTest';
import BloodTest from './bloodTest';
import SelectTimeNDate from './selectTimeNDate';
import MapTest from './mapTest';
import BookingSummary from './bookingSummary';
import SwabTest from './swabTest';
import MapVipTestHome from './mapVipTestHome';
import DownloadReport from './downloadReport';
import AddAddress from './addAddress';
import QuotationRequest from './quotationRequest';
import QuotationRequestDelivery from './quotationRequestDelivery';
import QuotationRequestPayMethod from './quotationRequestPayMethod';
import UploadPrescription from './uploadPrescription';
import Pharmacies from './pharmacies';
import PharmacyDetail from './pharmacyDetail';
import PharmacyProducts from './pharmacyProducts';
import PharmacyPage from './pharmacyPage';
import Account from './account';
import About from './about';
import WishList from './wishList';
import Prescriptions from './prescriptions';
import PrescriptionDetail from './prescriptionDetail';
import QuotationsProfile from './quotationsProfile';
import ComparisonList from './comparisonList';
import MyOrders from './myOrders';
import OrderDetail from './orderDetail';
import TrackOrder from './trackOrder';
import TrackOrderDetail from './trackOrderDetail';
import Cart from './cart';
import CheckOut from './checkOut';
import CheckoutChangeAddress from './checkoutChangeAddress';
import OrderConfirm from './orderConfirm';
import EditProfile from './editProfile';
import MyPoints from './myPoints';
import ViewProducts from './viewProducts';
import DiscountProducts from './discountProducts';
import PaymentScreen from './payement';

export {
  Home,
  Categories,
  SubCategories,
  Filters,
  ProductListing,
  FilterProducts,
  ProductDetail,
  Services,
  CovidTest,
  BloodTest,
  SelectTimeNDate,
  MapTest,
  BookingSummary,
  SwabTest,
  MapVipTestHome,
  DownloadReport,
  AddAddress,
  QuotationRequest,
  QuotationRequestDelivery,
  QuotationRequestPayMethod,
  UploadPrescription,
  Pharmacies,
  PharmacyDetail,
  PharmacyProducts,
  PharmacyPage,
  Account,
  About,
  WishList,
  Prescriptions,
  PrescriptionDetail,
  QuotationsProfile,
  ComparisonList,
  MyOrders,
  OrderDetail,
  TrackOrder,
  TrackOrderDetail,
  Cart,
  CheckOut,
  CheckoutChangeAddress,
  OrderConfirm,
  EditProfile,
  MyPoints,
  ViewProducts,
  PaymentScreen,
  DiscountProducts,
};
