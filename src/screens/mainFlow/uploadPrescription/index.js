import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Switch} from 'react-native-elements';
import {Icon} from 'react-native-elements';
import Toast from 'react-native-simple-toast';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TextInputColored,
  RegularText,
  CustomIcon,
  TinyText,
  MediumText,
  UploadEmiratesIdModal,
  UploadInsuranceCardModal,
  TouchableCustomIcon,
  LoadingCard,
  CameraDocumentModal,
} from '../../../components';
// import CameraModal from '../../../components/CameraModal';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import {launchImageLibrary} from 'react-native-image-picker';
import {store} from '../../../Redux/configureStore';
import {useInputValue} from '../../../services/hooks/useInputValue';
import {CheckBox} from 'react-native-elements';
import {ActivityIndicator} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import {fetchToken, fetchLanguage} from '../../../Redux/Actions';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';

/* Api Calls */
import {getUserAddresses} from '../../../services/api/user.api';
import {
  UploadEmiratesCard,
  UploadInsuranceCard,
  UploadPrescriptionApi,
  UploadQuotation,
  DeleteEmiratesCard,
  DeleteInsuranceCard,
} from '../../../services/api/prescription.api';
import AsyncStorage from '@react-native-community/async-storage';

export default function UploadPrescription({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [useEmiratesInsurance, setUseEmiratesInsurance] = useState(false);
  const [cameraModalVisible, setCameraModalVisible] = useState(false);
  const [emiratesloading, setEmiratesloading] = useState(false);
  const [insuranceloading, setInsuranceloading] = useState(false);
  const productName = useInputValue('');
  const productQuantity = useInputValue('');
  const productUnit = useInputValue('');
  const [selectedAddress, setSelectedAddress] = useState({});
  const patientName = useInputValue('');
  const emiratesName = useInputValue('');
  const insuranceName = useInputValue('');
  const comments = useInputValue('');
  const user = store.getState().user.user.user;
  const [switchPatient, setSwitchPatient] = useState(false);
  const [switchInsurance, setSwitchInsurance] = useState(false);
  const [uploadIdVisible, setUploadIdVisible] = useState(false);
  const [insuranceModalVisible, setInsuranceModalVisible] = useState(false);
  const [test1, setTest1] = useState(true);
  const [test2, setTest2] = useState(false);
  const [PrescriptionAvatar, setPrescriptionAvatar] = useState(null);
  const [avatarSource1, setAvatarSource1] = useState(null);
  const [avatarSource2, setAvatarSource2] = useState(null);
  const [emiratesUploadResponse, setEmiratesUploadResponse] = useState(null);
  const [insuranceUploadResponse, setInsuranceUploadResponse] = useState(null);
  const [avatarInsurance1, setAvatarInsurance1] = useState(null);
  const [avatarInsurance2, setAvatarInsurance2] = useState(null);
  const [callbackE1, setCallbackE1] = useState(null);
  const [callbackE2, setCallbackE2] = useState(null);
  const [callbackI1, setCallbackI1] = useState(null);
  const [callbackI2, setCallbackI2] = useState(null);
  const [callbackPrescription, setCallbackPrescription] = useState([]);
  // const [callbackPrescription, setCallbackPrescription] = useState(null);
  const [addresses, setAddresses] = useState(null);
  const [addressesTemp, setAddressesTemp] = useState(null);
  const [profileId, setProfileId] = useState(null);
  const dispatch = useDispatch();
  const [guest, setGuest] = useState(false);
  const [LTR, setLTR] = useState(true);

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getUserAddresses(user.user_id, onGetResponse, setLoading);
    await AsyncStorage.getItem(storageConst.guest).then(async value => {
      if (value == '1') {
        setGuest(true);
        showAlert();
      } else {
        setGuest(false);
      }
    });
    setLoading(false);
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUserAddresses(user.user_id, onGetResponse, setLoading);
    });
    return unsubscribe;
  }, [navigation]);

  //Sign In Alert for Guest
  const showAlert = async () => {
    Alert.alert(
      Translate('Sign In'),
      Translate('Sign In as a user to complete order'),
      [
        {
          text: Translate('Cancel'),
          onPress: () => {
            console.log('Cancel Pressed');
          },
          style: 'cancel',
        },
        {
          text: Translate('Sign In'),
          onPress: async() => {
            // let language = {
            //   language: true,
            // };
            // dispatch(fetchLanguage(language));
            // let token = {
            //   token: false,
            // };
            // dispatch(fetchToken(token));
            setLoading(true);
            await AsyncStorage.removeItem(storageConst.userData).then(
              async () => {
                await AsyncStorage.removeItem(storageConst.userId).then(
                  async () => {
                    await AsyncStorage.removeItem(storageConst.guest).then(
                      async () => {
                        setLoading(false);
                        // Toast.show('You have been Logged out!');
                        let language = {
                          language: true,
                        };
                        dispatch(fetchLanguage(language));
                        let token = {
                          token: false,
                        };
                        dispatch(fetchToken(token));
                      },
                    );
                  },
                );
              },
            );
          },
        },
      ],
    );
  };

  //Get User Address
  const onGetResponse = async res => {
    console.log('Get user Address API: ', res);
    if (res) {
      if (res.profiles && res.profiles.length) {
        res.profiles.forEach(element => {
          element.selected = false;
        });
        setAddressesTemp(res.profiles);
        setAddresses(res.profiles);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Handle Select Address
  const handleSelectAddress = index => {
    let newData = addressesTemp;
    newData.forEach(element => {
      element.selected = false;
    });
    newData[index].selected = true;
    setAddresses(newData);
    setAddresses(prev => [...prev]);
    setProfileId(newData[index].profile_id);
  };

  //Handle Emirates Card Attached
  const onEmiratesAttached = () => {
    if (emiratesName.value == '' && callbackE1 == null && callbackE2 == null) {
      Toast.show('Enter Emirates ID name, and fron back pictures first');
    } else if (emiratesName.value == '') {
      Toast.show('Emirates ID name is missing');
    } else if (callbackE1 == null) {
      Toast.show('Emirates id front side missing');
    } else if (callbackE2 == null) {
      Toast.show('Emirates id back side missing');
    } else {
      setEmiratesloading(true);
      var data = new FormData();
      let paramData = JSON.stringify({
        emirate_profile_id: 0,
        user_id: user.user_id,
        card_name: emiratesName.value,
      });
      data.append('front_side', {
        uri:
          Platform.OS == 'ios'
            ? callbackE1.uri.replace('file://', '')
            : callbackE1.uri,
        type: callbackE1.type,
        name: callbackE1.fileName,
      });
      data.append('back_side', {
        uri:
          Platform.OS == 'ios'
            ? callbackE2.uri.replace('file://', '')
            : callbackE2.uri,
        type: callbackE2.type,
        name: callbackE2.fileName,
      });
      data.append('params', paramData);
      console.log('param data: ', data);
      UploadEmiratesCard(data, onEmiratesUploadingResponse, setEmiratesloading);
    }
  };

  //Emirates Upload Response
  const onEmiratesUploadingResponse = async res => {
    console.log('response of upload emirates api : ', res);
    setEmiratesloading(false);
    if (res.success) {
      setEmiratesUploadResponse(res);
      setUploadIdVisible(false);
      Toast.show('Successfully uploaded Emirates card');
    } else {
      Toast.show(res.message);
    }
  };

  //Handle Insurance Attached
  const onInsuranceAttached = () => {
    if (switchInsurance) {
      if (callbackI1 == null) {
        Toast.show('Upload Insurance card front side first');
      } else if (callbackI2 == null) {
        Toast.show('Upload Insurance card back side first');
      } else {
        setInsuranceloading(true);
        var data = new FormData();
        let paramData = JSON.stringify({
          insurance_profile_id: 0,
          user_id: user.user_id,
          card_name: 'Test card',
        });
        data.append('front_side', {
          uri:
            Platform.OS == 'ios'
              ? callbackI1.uri.replace('file://', '')
              : callbackI1.uri,
          type: callbackI1.type,
          name: callbackI1.fileName,
        });
        data.append('back_side', {
          uri:
            Platform.OS == 'ios'
              ? callbackI2.uri.replace('file://', '')
              : callbackI2.uri,
          type: callbackI2.type,
          name: callbackI2.fileName,
        });
        data.append('params', paramData);
        UploadInsuranceCard(
          data,
          onInsuranceUploadingResponse,
          setInsuranceloading,
        );
      }
    } else {
      console.log('No need to upload insurance card');
    }
  };

  //Insurance Uploading API Rsponse
  const onInsuranceUploadingResponse = async res => {
    console.log('response of upload insurance api : ', res);
    setInsuranceloading(false);
    if (res.success) {
      setInsuranceUploadResponse(res);
      setInsuranceModalVisible(false);
      Toast.show('Successfully uploaded Insurance card');
    } else {
      Toast.show(res.message);
    }
  };

  //Delete Emirates API Response
  const onDeleteEmiratesResponse = res => {
    setEmiratesloading(false);
    if (res.success) {
      setEmiratesUploadResponse(null);
      emiratesName.onChange('');
      setAvatarSource1(null);
      setAvatarSource2(null);
      console.log('card deleted successfully');
    } else {
      Toast.show(res.message);
    }
  };

  //Delete Insurance Response
  const onDeleteInsuranceResponse = res => {
    setInsuranceloading(false);
    if (res.success) {
      setInsuranceUploadResponse(null);
      setSwitchInsurance(false);
      setUseEmiratesInsurance(false);
      insuranceName.onChange('');
      setAvatarInsurance1(null);
      setAvatarInsurance2(null);
      console.log('card deleted successfully');
    } else {
      Toast.show(res.message);
    }
  };

  //Handle Custom Insurance Attached
  const onCustomInsuranceAttached = (callback1, callback2, name) => {
    setInsuranceloading(true);
    var data = new FormData();
    let paramData = JSON.stringify({
      insurance_profile_id: 0,
      user_id: user.user_id,
      card_name: name,
    });
    data.append('front_side', {
      uri:
        Platform.OS == 'ios'
          ? callback1.uri.replace('file://', '')
          : callback1.uri,
      type: callback1.type,
      name: callback1.fileName,
    });
    data.append('back_side', {
      uri:
        Platform.OS == 'ios'
          ? callback2.uri.replace('file://', '')
          : callback2.uri,
      type: callback2.type,
      name: callback2.fileName,
    });
    data.append('params', paramData);
    UploadInsuranceCard(
      data,
      onInsuranceUploadingResponse,
      setInsuranceloading,
    );
  };

  //Handle Replicate Insurance
  const replicateInsurance = () => {
    if (emiratesUploadResponse != null) {
      setCallbackI1(callbackE1);
      setCallbackI2(callbackE2);
      setAvatarInsurance1(callbackE1.uri);
      setAvatarInsurance2(callbackE2.uri);
      insuranceName.onChange(emiratesName.value);
      onCustomInsuranceAttached(callbackE1, callbackE2, emiratesName.value);
    } else {
      Toast.show('Upload Emirates ID first.');
      setTimeout(() => {
        setUseEmiratesInsurance(false);
      }, 1500);
    }
  };

  //Handle Flush Insurance Replica
  const flushInsuranceReplica = () => {
    setInsuranceUploadResponse(null);
    setCallbackI1(null);
    setCallbackI2(null);
    setAvatarInsurance1(null);
    setAvatarInsurance2(null);
    insuranceName.onChange('');
    setInsuranceloading(false);
  };

  //Handle Check Camera Permission
  // const checkCameraPermission = async () => {
  //   try {
  //     if (Platform.OS == 'android') {
  //       const granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.CAMERA,
  //         {
  //           title: 'Camera Permission Required',
  //           message: 'Application needs access to your Camera to take picture',
  //         },
  //       );
  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         SelectCamera();
  //         console.log('Camera Permission Granted.');
  //       } else {
  //         // If permission denied then show alert
  //         Alert.alert('Error', 'Camera Permission Not Granted');
  //       }
  //     } else {
  //       SelectCamera();
  //     }
  //   } catch (err) {
  //     // To handle permission related exception
  //     console.log('++++' + err);
  //   }
  // };

  //Handle Select Camera
  // const SelectCamera = () => {
  //   const options = {
  //     title: 'selectPhoto',
  //     storageOptions: {
  //       skipBackup: true,
  //       path: 'images',
  //     },
  //   };

  //   launchCamera(options, callback => {
  //     if (callback.didCancel) {
  //       console.log('User cancelled image picker');
  //     } else if (callback.error) {
  //       console.log('ImagePicker Error: ', callback.error);
  //     } else if (callback.customButton) {
  //       console.log('User tapped custom button: ', callback.customButton);
  //     } else {
  //       // setAvatarSource(callback.uri);
  //       // setModalVisible(false);
  //       setPrescriptionAvatar(callback.uri);
  //       // setCallbackPrescription(callback);
  //       // callback.kind = 'image';
  //       // setCallbackPrescription.push(callback);
  //       callback.kind = 'image';
  //       let listPrescription = callbackPrescription;
  //       listPrescription.push(callback);
  //       setCallbackPrescription(listPrescription);
  //       setCallbackPrescription(prev => [...prev]);
  //     }
  //   });
  // };

  //Select Presciption Image
  const SelectPrescriptionImage = () => {
    const options = {
      title: 'selectPhoto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, callback => {
      if (callback.didCancel) {
        console.log('User cancelled image picker');
      } else if (callback.error) {
        console.log('ImagePicker Error: ', callback.error);
      } else if (callback.customButton) {
        console.log('User tapped custom button: ', callback.customButton);
      } else {
        console.log('image response : ', callback);
        // if (side == 'front') {
        setPrescriptionAvatar(callback.uri);
        // setCallbackPrescription(callback);
        callback.kind = 'image';
        let listPrescription = callbackPrescription;
        listPrescription.push(callback);
        setCallbackPrescription(listPrescription);
        setCallbackPrescription(prev => [...prev]);

        // } else {
        //   setAvatarSource2(callback.uri);
        //   setCallbackE2(callback);
        // }
        // this.ImageUpload(callback);
      }
    });
  };

  //Select Image
  const SelectImage = side => {
    const options = {
      title: 'selectPhoto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, callback => {
      if (callback.didCancel) {
        console.log('User cancelled image picker');
      } else if (callback.error) {
        console.log('ImagePicker Error: ', callback.error);
      } else if (callback.customButton) {
        console.log('User tapped custom button: ', callback.customButton);
      } else {
        console.log('image response : ', callback);
        if (side == 'front') {
          setAvatarSource1(callback.uri);
          setCallbackE1(callback);
        } else {
          setAvatarSource2(callback.uri);
          setCallbackE2(callback);
        }
        // this.ImageUpload(callback);
      }
    });
  };

  //Select Image Insurance
  const SelectImageInsurance = side => {
    const options = {
      title: 'selectPhoto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, callback => {
      if (callback.didCancel) {
        console.log('User cancelled image picker');
      } else if (callback.error) {
        console.log('ImagePicker Error: ', callback.error);
      } else if (callback.customButton) {
        console.log('User tapped custom button: ', callback.customButton);
      } else {
        console.log('image response : ', callback);
        if (side == 'front') {
          setAvatarInsurance1(callback.uri);
          setCallbackI1(callback);
        } else {
          setAvatarInsurance2(callback.uri);
          setCallbackI2(callback);
        }
        // this.ImageUpload(callback);
      }
    });
  };

  const EmiratesCard = ({id, headerText, onPress}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={styles.row}>
          {Platform.OS == 'ios' ? (
            <RegularText
              style={{
                fontFamily: fontFamily.appTextBold,
                marginLeft: width(3),
              }}>
              {headerText}
            </RegularText>
          ) : (
            <RegularText
              style={{
                fontFamily: fontFamily.appTextBold,
                marginLeft: width(3),
              }}>
              {headerText}
            </RegularText>
          )}
          {emiratesUploadResponse == null && (
            <TouchableOpacity
              onPress={onPress}
              style={[
                styles.addProduct,
                {
                  width: null,
                  paddingHorizontal: width(2),
                },
              ]}>
              {Platform.OS == 'ios' ? (
                <TinyText
                  style={[appStyles.whiteText, {marginRight: width(1)}]}>
                  {Translate('Add Emirates ID')}
                </TinyText>
              ) : (
                <SmallText
                  style={[appStyles.whiteText, {marginRight: width(1)}]}>
                  {Translate('Add Emirates ID')}
                </SmallText>
              )}
              <CustomIcon icon={appImages.plusCircle} size={totalSize(1.7)} />
            </TouchableOpacity>
          )}
        </View>
        {emiratesloading && (
          <ActivityIndicator color={'black'} style={{alignSelf: 'center'}} />
        )}
        {emiratesUploadResponse != null && (
          <View
            style={[
              styles.onlyRow,
              {marginLeft: width(4), marginTop: sizes.smallMargin},
            ]}>
            <TouchableCustomIcon
              icon={appImages.downloadPrescription}
              size={totalSize(4)}
              onPress={() => setUploadIdVisible(true)}
            />
            <SmallText style={[{marginLeft: width(2)}]}>
              {emiratesName.value}
            </SmallText>
            <TouchableCustomIcon
              icon={appImages.redCross}
              size={totalSize(2)}
              style={{marginLeft: width(2)}}
              onPress={async () => {
                Alert.alert('DWAAE', Translate('Delete this card?'), [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'Delete',
                    onPress: async () => {
                      setEmiratesloading(true);
                      DeleteEmiratesCard(
                        emiratesUploadResponse.emirate_profile_id,
                        onDeleteEmiratesResponse,
                      );
                    },
                  },
                ]);
              }}
            />
          </View>
        )}
      </View>
    );
  };

  const InsuranceCard = ({onPress}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={styles.row}>
          <View style={styles.onlyRow}>
            {Platform.OS == 'ios' ? (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                  marginLeft: width(3),
                  marginRight: width(5),
                }}>
                {Translate('Insurance Card')}
              </RegularText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                  marginLeft: width(3),
                  marginRight: width(5),
                }}>
                {Translate('Insurance Card')}
              </RegularText>
            )}

            <Switch
              value={switchInsurance}
              onValueChange={val => {
                setSwitchInsurance(val);
                if (!val) {
                  flushInsuranceReplica();
                  setUseEmiratesInsurance(false);
                }
              }}
            />
          </View>
          {switchInsurance &&
            insuranceUploadResponse == null &&
            !useEmiratesInsurance && (
              <TouchableOpacity
                onPress={onPress}
                style={[
                  styles.addProduct,
                  {
                    // width: id ? width(27) : width(30)}
                    width: null,
                    paddingHorizontal: width(2),
                  },
                ]}>
                {Platform.OS == 'ios' ? (
                  <TinyText
                    style={[appStyles.whiteText, {marginRight: width(1)}]}>
                    {Translate('Add Insurance Card')}
                  </TinyText>
                ) : (
                  <SmallText
                    style={[appStyles.whiteText, {marginRight: width(1)}]}>
                    {Translate('Add Insurance Card')}
                  </SmallText>
                )}
                <CustomIcon icon={appImages.plusCircle} size={totalSize(1.7)} />
              </TouchableOpacity>
            )}
        </View>
        {switchInsurance && (
          <View>
            <CheckBox
              title={Translate('Use Insurance Card Linked to Emirates ID')}
              checked={useEmiratesInsurance}
              onPress={() => {
                //Checking with flase condition because it will be true in this press
                if (!useEmiratesInsurance) replicateInsurance();
                if (useEmiratesInsurance && insuranceUploadResponse != null)
                  flushInsuranceReplica();
                setUseEmiratesInsurance(!useEmiratesInsurance);
              }}
              containerStyle={{
                backgroundColor: 'transparent',
                borderWidth: 0,
                marginBottom: 0,
              }}
              textStyle={{fontSize: totalSize(1.3), color: colors.gradient1}}
            />
            <SmallText style={{marginLeft: width(14), color: colors.textGrey2}}>
              {Translate(
                'Dwaee will use your Emirates ID number to get your insurance policy details',
              )}
            </SmallText>
          </View>
        )}
        {insuranceloading && (
          <ActivityIndicator
            color={'black'}
            style={{alignSelf: 'center', marginTop: height(2)}}
          />
        )}

        {insuranceUploadResponse != null && (
          <View
            style={[
              styles.onlyRow,
              {marginLeft: width(4), marginTop: sizes.smallMargin},
            ]}>
            <TouchableCustomIcon
              icon={appImages.downloadPrescription}
              size={totalSize(4)}
              onPress={() => setInsuranceModalVisible(true)}
            />
            <SmallText style={[{marginLeft: width(2)}]}>
              {insuranceName.value}
            </SmallText>
            <TouchableCustomIcon
              icon={appImages.redCross}
              size={totalSize(2)}
              style={{marginLeft: width(2)}}
              onPress={async () => {
                Alert.alert('DWAAE', 'Delete this card?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'Delete',
                    onPress: async () => {
                      setInsuranceloading(true);
                      DeleteInsuranceCard(
                        insuranceUploadResponse.insurance_profile_id,
                        onDeleteInsuranceResponse,
                      );
                    },
                  },
                ]);
              }}
            />
          </View>
        )}
      </View>
    );
  };

  const AddressCard = ({
    home,
    text1,
    text2,
    text3,
    text4,
    onPress,
    selected,
  }) => {
    return (
      <View
        style={[
          styles.productCard,
          styles.row,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <TouchableOpacity
          style={[appStyles.center, styles.onlyRow, {flex: 0.3}]}
          onPress={onPress}>
          <TouchableOpacity
            style={[styles.radioButton, {marginRight: width(3)}]}
            onPress={onPress}>
            <View
              style={[
                styles.innerRadioButton,
                {
                  backgroundColor: selected
                    ? colors.statusBarColor
                    : 'transparent',
                },
              ]}
            />
          </TouchableOpacity>
          <View style={appStyles.center}>
            <CustomIcon
              icon={home ? appImages.homeIconTest : appImages.suitcase}
              size={totalSize(2.5)}
            />
            <TinyText
              style={{
                marginTop: height(1),
                fontFamily: fontFamily.appTextBold,
              }}>
              {home ? 'Home' : 'Work'}
            </TinyText>
          </View>
        </TouchableOpacity>
        <View style={{flex: 0.6, paddingHorizontal: width(4)}}>
          {Platform.OS == 'ios' ? (
            <SmallText style={{fontFamily: fontFamily.appTextLight}}>
              {text1}
            </SmallText>
          ) : (
            <RegularText style={{fontFamily: fontFamily.appTextLight}}>
              {text1}
            </RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText style={{fontFamily: fontFamily.appTextLight}}>
              {text2}
            </SmallText>
          ) : (
            <RegularText style={{fontFamily: fontFamily.appTextLight}}>
              {text2}
            </RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText style={{fontFamily: fontFamily.appTextLight}}>
              {text3}
            </SmallText>
          ) : (
            <RegularText style={{fontFamily: fontFamily.appTextLight}}>
              {text3}
            </RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText style={{fontFamily: fontFamily.appTextLight}}>
              {text4}
            </SmallText>
          ) : (
            <RegularText style={{fontFamily: fontFamily.appTextLight}}>
              {text4}
            </RegularText>
          )}
        </View>
        <View style={[{flex: 0.1}]}>
          <Icon
            name={'dots-three-vertical'}
            type={'entypo'}
            size={Platform.OS == 'ios' ? totalSize(1.5) : totalSize(1.8)}
            style={{marginBottom: height(5)}}
          />
        </View>
      </View>
    );
  };

  //Add New Address
  const AddressCardNew = ({home, onPress, item}) => {
    return (
      <View
        style={[
          styles.addressCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={[styles.row, {flex: 1, paddingHorizontal: width(4)}]}>
          <View style={styles.onlyRow}>
            {/* <View style={appStyles.center}> */}
            <TouchableOpacity style={[styles.radioButton]} onPress={onPress}>
              <View
                style={[
                  styles.innerRadioButton,
                  {
                    backgroundColor: item.selected
                      ? colors.statusBarColor
                      : 'transparent',
                  },
                ]}
              />
            </TouchableOpacity>
            {/* </View> */}
            <TouchableOpacity
              style={[appStyles.center, styles.onlyRow, {marginLeft: width(2)}]}
              onPress={onPress}>
              <View style={appStyles.center}>
                <View
                  style={{
                    width: Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                    height: Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                    borderRadius:
                      Platform.OS == 'ios'
                        ? totalSize(6) / 2
                        : totalSize(7) / 2,
                    backgroundColor: colors.greyBg,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    borderColor: colors.borderLightColor,
                  }}>
                  <CustomIcon
                    icon={
                      item.profile_name == 'my_home'
                        ? appImages.building
                        : appImages.suitcase
                    }
                    // icon={appImages.building}
                    size={Platform.OS == 'ios' ? totalSize(3) : totalSize(4)}
                  />
                </View>
                <TinyText
                  style={{
                    marginTop: height(1),
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  {item.profile_name == 'my_home'
                    ? Translate('Home')
                    : Translate('Work')}
                  {/* {home ? Translate('Home') : Translate('Work')} */}
                </TinyText>
              </View>
            </TouchableOpacity>

            <View
              style={{
                paddingHorizontal: width(3),
                width: width(44),
              }}>
              {Platform.OS == 'ios' ? (
                <SmallText>{item.s_address}</SmallText>
              ) : (
                <RegularText>{item.s_address}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{item.s_city + ', ' + item.s_country}</SmallText>
              ) : (
                <RegularText>{item.s_city + ', ' + item.s_country}</RegularText>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  };

  //Handle Submit Order
  const handleSubmitORder = async () => {
    //Create form data here then hit API of submit order
    // let insuranceType = 'N';
    // if (switchInsurance) {
    //   if (useEmiratesInsurance) {
    //     insuranceType = 'E';
    //   } else {
    //     insuranceType = 'S';
    //   }
    // }

    //
    // await AsyncStorage.getItem(storageConst.guestId)
    if (user.user_id == '0') {
      navigation.navigate(routes.signin);
    } else {
      // if (callbackPrescription.length == 0) {
      //   Toast.show('Upload Prescription first');
      // } else if (patientName.value == '') {
      //   Toast.show('Enter Patient name first');
      // } else if (emiratesUploadResponse == null) {
      //   Toast.show('Upload emirates id first');
      // } else if (insuranceUploadResponse == null && insuranceType != 'N') {
      //   Toast.show('Upload insurance card first');
      // }
      if (profileId == null) {
        Toast.show('Add address first');
      } else if (
        (productName = '' || productQuantity == '' || productUnit == '')
      ) {
        Toast.show('Fill complete product data first');
      } else {
        setLoading(true);
        var data = new FormData();

        // %%%%%%%%%%%%%%%%%%% NEW APPROACH %%%%%%%%%%%%%%%%%%%%%%%
        let language = await AsyncStorage.getItem(storageConst.language);
        if (language == null || language == undefined) language = 'en';
        let paramData = {};
        paramData = JSON.stringify({
          //***************************************
          //***************************************
          user_id: user.user_id,
          lang_code: language,
          currency_code: 'AED',
          product: [
            {
              product_name: productName,
              quantity: productQuantity,
              quantity_unit: productUnit,
              description: comments,
            },
          ],
          delivery_location: {
            address: 'test',
            address_2: 'test',
            country: 'AE',
            state: 'abu-dhabi',
            city: 'ts',
            zipcode: '201301',
          },
          buyer_details: {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            phone: user.phone,
            fax: '344334',
            company: 'test',
            url: 'ccx',
          },
          payment_type: 'C',
          //***************************************
          //***************************************
        });

        data.append('params', paramData);

        console.log('form data: ', data);
        // UploadPrescriptionApi(data, onPrescriptionResponse, setLoading);
        UploadQuotation(data, onPrescriptionResponse, setLoading);
        // %%%%%%%%%%%%%%%%%%% END NEW APPROACH %%%%%%%%%%%%%%%%%%%%%%%
      }
    }
  };

  //Handle Prescription Response
  const onPrescriptionResponse = res => {
    console.log('response of upload prescription api : ', res);
    if (res.success) {
      setCallbackE1(null);
      setCallbackE2(null);
      setCallbackI1(null);
      setCallbackI2(null);
      setCallbackPrescription([]);
      setPrescriptionAvatar(null);
      setAvatarInsurance1(null);
      setAvatarInsurance2(null);
      setAvatarSource1(null);
      setAvatarSource2(null);
      setSwitchInsurance(false);
      setEmiratesUploadResponse(null);
      setInsuranceUploadResponse(null);
      Toast.show(res.message);
      setLoading(false);
      navigation.navigate('Home');
    } else {
      Toast.show(res.message);
      setLoading(false);
    }
    setLoading(false);
  };

  //Select Document
  const selectDocument = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.pdf,
          DocumentPicker.types.doc,
          DocumentPicker.types.docx,
        ],
        // type: [DocumentPicker.types.pdf],
      });
      setPrescriptionAvatar(null);
      // FileViewer.open(res.uri);
      res.kind = 'doc';
      let listPrescription = callbackPrescription;
      listPrescription.push(res);
      setCallbackPrescription(listPrescription);
      setCallbackPrescription(prev => [...prev]);
      // openDoc(res);
      console.log('Doc response: ', res);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Toast.show(Translate('Please select something'));
      } else {
        Toast.show(err);
        throw err;
      }
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />

          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              <View style={[styles.productCard, appStyles.shadowSmall]}>
                {/* <View style={styles.row}>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      Upload Item
                    </RegularText>
                  ) : (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      Upload Item
                    </RegularText>
                  )}
                  <TouchableOpacity
                    onPress={() => {
                      if (guest) {
                        showAlert();
                      } else {
                        setCameraModalVisible(true);
                      }
                    }}
                    style={[
                      styles.addProduct,
                      {
                        width: null,
                        paddingHorizontal: width(2),
                      },
                    ]}>
                    {Platform.OS == 'ios' ? (
                      <TinyText
                        style={[appStyles.whiteText, {marginRight: width(1)}]}>
                        Upload
                      </TinyText>
                    ) : (
                      <SmallText
                        style={[appStyles.whiteText, {marginRight: width(1)}]}>
                        Upload
                      </SmallText>
                    )}
                    <CustomIcon
                      icon={appImages.prescription}
                      size={totalSize(1.7)}
                    />
                  </TouchableOpacity>
                </View> */}

                {/* {callbackPrescription != null && ( */}
                {/* {callbackPrescription.length && */}
                {/* {callbackPrescription.map((item, index) => {
                  return (
                    <View
                      style={[
                        styles.onlyRow,
                        {marginLeft: width(4), marginTop: sizes.smallMargin},
                      ]}>
                      <TouchableCustomIcon
                        icon={appImages.downloadPrescription}
                        size={totalSize(4)}
                        onPress={selectDocument}
                      />
                      <TinyText style={[{marginLeft: width(2)}]}>
                        {item.fileName}
                      </TinyText>
                      <TouchableCustomIcon
                        icon={appImages.redCross}
                        size={totalSize(2)}
                        style={{marginLeft: width(2)}}
                        onPress={() => {
                          // setCallbackPrescription(null);
                          // setPrescriptionAvatar(null);
                          let newList = callbackPrescription;
                          newList.splice(index, 1);
                          setCallbackPrescription(newList);
                          setCallbackPrescription(prev => [...prev]);
                        }}
                      />
                    </View>
                  );
                })} */}
                <>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {/* {Translate('Patient Name & Prescription')} */}
                      {Translate('Product Name')}
                    </RegularText>
                  ) : (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {Translate('Product Name')}
                      {/* {Translate('Patient Name & Prescription')} */}
                    </RegularText>
                  )}
                  <Spacer height={sizes.baseMargin} />
                  <TextInputColored
                    containerStyle={[
                      styles.inputfieldStyle,
                      {
                        marginHorizontal: width(3),
                        textAlign: LTR ? 'left' : 'right',
                      },
                    ]}
                    fieldStyle={[styles.fieldStyle]}
                    // placeholder={Translate('Enter Patient Name')}
                    placeholder={'Dewalt Hight Performance Jigsaw'}
                    value={productName.value}
                    onChangeText={productName.onChange}
                  />
                  <Spacer height={sizes.baseMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {/* {Translate('Patient Name & Prescription')} */}
                      {Translate('Quantity')}
                    </RegularText>
                  ) : (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {Translate('Quantity')}
                      {/* {Translate('Patient Name & Prescription')} */}
                    </RegularText>
                  )}
                  <Spacer height={sizes.baseMargin} />
                  <TextInputColored
                    containerStyle={[
                      styles.inputfieldStyle,
                      {marginHorizontal: width(3)},
                    ]}
                    fieldStyle={[
                      styles.fieldStyle,
                      {
                        textAlign: LTR ? 'left' : 'right',
                      },
                    ]}
                    // placeholder={Translate('Enter Patient Name')}
                    placeholder={'200'}
                    value={productQuantity.value}
                    onChangeText={productQuantity.onChange}
                  />
                  <Spacer height={sizes.baseMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {/* {Translate('Patient Name & Prescription')} */}
                      {Translate('Unit')}
                    </RegularText>
                  ) : (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(3),
                      }}>
                      {Translate('Unit')}
                      {/* {Translate('Patient Name & Prescription')} */}
                    </RegularText>
                  )}
                  <Spacer height={sizes.baseMargin} />
                  <TextInputColored
                    containerStyle={[
                      styles.inputfieldStyle,
                      {
                        marginHorizontal: width(3),
                        textAlign: LTR ? 'left' : 'right',
                      },
                    ]}
                    fieldStyle={[styles.fieldStyle]}
                    // placeholder={Translate('Enter Patient Name')}
                    placeholder={Translate('Pieces')}
                    value={productUnit.value}
                    onChangeText={productUnit.onChange}
                  />
                </>
              </View>

              <Spacer height={sizes.baseMargin} />
              <View
                style={[appStyles.shadowSmall, {backgroundColor: colors.snow}]}>
                <Spacer height={sizes.baseMargin} />

                {Platform.OS == 'ios' ? (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      marginLeft: width(3),
                    }}>
                    {Translate('Comments')}
                  </RegularText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      marginLeft: width(3),
                    }}>
                    {Translate('Comments')}
                  </RegularText>
                )}
                <Spacer height={sizes.baseMargin} />
                <TextInputColored
                  containerStyle={[
                    styles.inputfieldStyle,
                    {marginHorizontal: width(3)},
                  ]}
                  fieldStyle={[
                    styles.fieldStyle,
                    {
                      height: height(14),
                      textAlignVertical: 'top',
                      textAlign: LTR ? 'left' : 'right',
                    },
                  ]}
                  multiline
                  // placeholder={Translate(
                  //   'Please type your delivery notes here',
                  // )}
                  placeholder={Translate('Comments about the quotation')}
                  value={comments.value}
                  onChangeText={comments.onChange}
                />

                <Spacer height={sizes.baseMargin} />
              </View>
              <Spacer height={sizes.baseMargin} />
              <View
                style={[appStyles.shadowSmall, {backgroundColor: colors.snow}]}>
                <Spacer height={sizes.baseMargin} />
                <View style={styles.row}>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(4),
                      }}>
                      {Translate('My Addresses')}
                    </RegularText>
                  ) : (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginLeft: width(4),
                      }}>
                      {Translate('My Addresses')}
                    </RegularText>
                  )}
                  <TouchableOpacity
                    onPress={() => {
                      if (guest) {
                        showAlert();
                      } else {
                        navigation.navigate(routes.addAddress);
                      }
                    }}
                    style={[
                      styles.addProduct,
                      {
                        // width: width(22) hntgbnt ngtb scwas kmuf6brd sxwascedvgktjrtcjfrj
                        width: null,
                        paddingHorizontal: width(2),
                      },
                    ]}>
                    {Platform.OS == 'ios' ? (
                      <TinyText
                        style={[appStyles.whiteText, {marginRight: width(1)}]}>
                        {Translate('Add Address')}
                      </TinyText>
                    ) : (
                      <SmallText
                        style={[appStyles.whiteText, {marginRight: width(1)}]}>
                        {Translate('Add Address')}
                      </SmallText>
                    )}
                    <CustomIcon
                      icon={appImages.plusCircle}
                      size={totalSize(1.7)}
                    />
                  </TouchableOpacity>
                </View>

                <FlatList
                  nestedScrollEnabled={true}
                  data={addresses}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({index, item}) => (
                    <AddressCardNew
                      item={item}
                      home
                      onPress={() => {
                        console.log('address item: ', item);
                        setSelectedAddress(item);
                        handleSelectAddress(index);
                      }}
                    />
                  )}
                />
                <Spacer height={sizes.baseMargin} />
              </View>
              <Spacer height={sizes.baseMargin} />

              <ButtonColored
                isLoading={loading}
                // text={Translate('SUBMIT ORDER')}
                text={Translate('REQUEST QUOTATION')}
                onPress={() => {
                  if (guest) {
                    showAlert();
                  } else {
                    handleSubmitORder();
                    // Toast.show('Under development');
                  }
                }}
                //   onPress={() =>
                //     navigation.navigate(routes.quotationRequestDelivery)
                //   }
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(92),
                  alignSelf: 'center',
                  borderRadius: sizes.loginRadius,
                }}
              />
              <Spacer height={sizes.baseMargin} />
            </View>
            <UploadEmiratesIdModal
              isVisible={uploadIdVisible}
              frontImage={
                avatarSource1 != null
                  ? {
                      uri: avatarSource1,
                    }
                  : appImages.camera
              }
              backImage={
                avatarSource2 != null
                  ? {
                      uri: avatarSource2,
                    }
                  : appImages.camera
              }
              loading={emiratesloading}
              onChangeText={emiratesName.onChange}
              textValue={emiratesName.value}
              toggleModal={() => setUploadIdVisible(false)}
              onPressFront={() => SelectImage('front')}
              onPressBack={() => SelectImage('back')}
              onPressButton={() => onEmiratesAttached()}
            />
            <UploadInsuranceCardModal
              isVisible={insuranceModalVisible}
              frontImage={
                avatarInsurance1 != null
                  ? {
                      uri: avatarInsurance1,
                    }
                  : appImages.camera
              }
              backImage={
                avatarInsurance2 != null
                  ? {
                      uri: avatarInsurance2,
                    }
                  : appImages.camera
              }
              loading={insuranceloading}
              toggleModal={() => setInsuranceModalVisible(false)}
              onChangeText={insuranceName.onChange}
              textValue={insuranceName.value}
              onPressFront={() => SelectImageInsurance('front')}
              onPressBack={() => SelectImageInsurance('back')}
              onPressButton={() => onInsuranceAttached()}
            />
            {/* <CameraDocumentModal
              isVisible={cameraModalVisible}
              onClose={() => setCameraModalVisible(false)}
              imageFromCamera={() => {
                setCameraModalVisible(false);
                setTimeout(() => {
                  checkCameraPermission();
                }, 700);
                // SelectCamera();
              }}
              imageFromGallery={() => {
                setCameraModalVisible(false);
                setTimeout(() => {
                  SelectPrescriptionImage();
                }, 700);
              }}
              FromDocuments={() => {
                setCameraModalVisible(false);
                setTimeout(() => {
                  selectDocument();
                }, 700);
              }}
            /> */}
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
