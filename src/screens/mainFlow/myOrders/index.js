import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  TouchableCustomIcon,
  MediumText,
  LineHorizontal,
  LoadingCard,
  BackIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native';
/* Api Calls */
import {getOrderList} from '../../../services/api/cart.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchCategoryId, fetchCartData} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToCartMethod,
  RemoveProductFromWishListMethod,
  Translate,
} from '../../../services/helpingMethods';

export default function MyOrders({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const [uploadIdVisible, setUploadIdVisible] = useState(false);

  const [accountListTemp, setAccountListTemp] = useState();
  const [accountList, setAccountList] = useState([
    // {
    //   title: 'Prescription #258963',
    //   date: '06/29/2020, 18:12',
    //   status: 'Shipped',
    //   collapsed: false,
    //   items: 2,
    //   text1: 'Candes Eclat Fresh Lightening Face Tonic 200 mL',
    //   text2: 'DW-NAD-P01',
    //   price: 'AED 69.00',
    // },
  ]);

  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;

  useEffect(() => {
    setLoading(true);
    getOrderList(user.user_id, onGetResponse, setLoading);
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      if (user.user_id) {
        setLoading(true);
        getOrderList(user.user_id, onGetResponse, setLoading);
      }
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  //Get Orders API response
  const onGetResponse = async res => {
    console.log('Get orders API: ', res);
    if (res) {
      if (res.orders && res.orders.length) {
        res.orders.forEach(element => {
          element.collapsed = true;
        });
        setAccountListTemp(res.orders);
        setAccountList(res.orders);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Handle Collapse
  const HandleCollapse = index => {
    let newData = accountListTemp;
    newData.forEach(element => {
      element.collapsed = true;
    });
    newData[index].collapsed = false;
    setAccountList(newData);
    setAccountList(prev => [...prev]);

    // tempList[index].collapsed = !accountList[index].collapsed;
    // setAccountList(tempList);
  };

  //Search Filter Function
  const SearchFilterFunction = text => {
    //alert(accountListTemp);
    //categoryData, productMedicationData, productTopSellerData, productNewestData
    if (text != ''  && accountListTemp != undefined ) {
      const productMedication = accountListTemp.filter(function (item) {
        const itemData = item.order_id
          ? item.order_id.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setAccountList(productMedication);
      setAccountList(prev => [...prev]);
    } else {
      if(accountListTemp != undefined){
      removeFilter();
      }
    }
  };

  //Remove Filter
  const removeFilter = () => {
    setAccountList(accountListTemp);
  };

  //Product Card Component
  const ProductCard = ({item, onCollapsablePress}) => {
    return (
      <View>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <TouchableOpacity
            onPress={() => navigation.navigate(routes.orderDetail, {item})}
            style={[styles.row, {paddingHorizontal: width(3)}]}>
            <View style={[styles.onlyRow]}>
              <TouchableCustomIcon
                icon={item.collapsed ? appImages.addIcon : appImages.removeIcon}
                size={totalSize(3)}
                style={{marginRight: width(5)}}
                onPress={onCollapsablePress}
              />
              <View>
                {Platform.OS == 'ios' ? (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextMedium,
                    }}>
                    {Translate('Order') + ` #${item.order_id}`}
                  </RegularText>
                ) : (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextMedium,
                    }}>
                    {Translate('Order') + ` #${item.order_id}`}
                  </MediumText>
                )}
                <Spacer height={sizes.TinyMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      color: colors.textGrey2,
                    }}>
                    {item.timestamp}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      color: colors.textGrey2,
                    }}>
                    {item.timestamp}
                  </RegularText>
                )}
                <Spacer height={sizes.TinyMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      color: item.status_color
                        ? item.status_color
                        : colors.redLight,
                      // item.status == 'Shipped' || item.status == 'Open'
                      //   ? colors.statusBarColor
                      //   : colors.redLight,
                    }}>
                    {Translate('Status') + ': ' + item.status_description}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      color: item.status_color
                        ? item.status_color
                        : colors.redLight,
                    }}>
                    {Translate('Status') + ': ' + item.status_description}
                  </RegularText>
                )}
              </View>
            </View>
            <View style={[styles.onlyRow]}>
              <TouchableCustomIcon
                onPress={() => navigation.navigate(routes.orderDetail, {item})}
                icon={appImages.arrowRight}
                size={totalSize(4)}
              />
            </View>
          </TouchableOpacity>

          {item.collapsed
            ? null
            : item.products && item.products.length
            ? item.products.map((item, index) => {
                return (
                  <>
                    <Spacer height={sizes.smallMargin} />
                    <LineHorizontal />
                    <View style={{paddingHorizontal: width(3)}}>
                      <Spacer height={sizes.smallMargin} />
                      {Platform.OS == 'ios' ? (
                        <RegularText
                          style={{
                            fontFamily: fontFamily.appTextMedium,
                          }}>
                          {item.product}
                        </RegularText>
                      ) : (
                        <MediumText
                          style={{
                            fontFamily: fontFamily.appTextMedium,
                          }}>
                          {item.product}
                        </MediumText>
                      )}
                      <Spacer height={sizes.TinyMargin} />
                      {Platform.OS == 'ios' ? (
                        <SmallText style={appStyles.textGray}>
                          {item.product_code}
                        </SmallText>
                      ) : (
                        <RegularText style={appStyles.textGray}>
                          {item.product_code}
                        </RegularText>
                      )}
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <RegularText>{item.format_subtotal}</RegularText>
                        ) : (
                          <MediumText>{item.format_subtotal}</MediumText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText style={appStyles.blueText}>
                            {item.amount + ' ' + Translate('Items')}
                          </SmallText>
                        ) : (
                          <RegularText style={appStyles.blueText}>
                            {item.amount + ' ' + Translate('Items')}
                          </RegularText>
                        )}
                      </View>
                    </View>
                    <Spacer height={sizes.smallMargin} />
                    <LineHorizontal />
                  </>
                );
              })
            : null}
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          {/* <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                styles.row,
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                },
              ]}>
              <View>
                <MediumText
                  style={[
                    appStyles.whiteText,
                    {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                  ]}>
                  {Translate('My Orders')}
                </MediumText>
                <SmallText
                  style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                  {accountList.length} {Translate('Orders')}
                </SmallText>
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputfieldStyle}
                  placeholder={Translate('Search from your orders')}
                  placeholderTextColor={colors.snow}
                  onChangeText={SearchFilterFunction}
                />
                <Icon
                  name={'search'}
                  color={colors.snow}
                  size={totalSize(2.5)}
                />
              </View>
            </View>
          </LinearGradient> */}

          <View
            style={[
              appStyles.shadowSmall,
              styles.row,
              {
                // flex: 1,
                backgroundColor: colors.snow,
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <BackIcon onPress={() => navigation.pop()} color={colors.black} />
            <View>
              <MediumText style={[{fontFamily: fontFamily.appTextBold}]}>
                {Translate('My Orders')}
              </MediumText>
              <SmallText>
                {accountList.length} {Translate('Orders')}
              </SmallText>
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputfieldStyle}
                placeholder={Translate('Search from your orders')}
                onChangeText={SearchFilterFunction}
              />
              <Icon name={'search'} size={totalSize(2.5)} />
            </View>
          </View>

          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              {accountList.length ? (
                <FlatList
                  data={accountList}
                  extraData={accountList}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}
                  numColumns={1}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({index, item}) => (
                    <ProductCard
                      key={index}
                      item={item}
                      onCollapsablePress={() => HandleCollapse(index)}
                      onPress={() =>
                        navigation.navigate(routes.orderDetail, {item})
                      }
                    />
                  )}
                />
              ) : (
                <RegularText
                  style={{alignSelf: 'center', marginTop: height(5)}}>
                  {Translate('No orders found')}
                </RegularText>
              )}
              <Spacer height={sizes.doubleBaseMargin} />
              <ButtonColored
                onPress={() => navigation.navigate('Home')}
                text={Translate('CONTINUE SHOPPING')}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(92),
                  alignSelf: 'center',
                  borderRadius: sizes.loginRadius,
                }}
              />
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
