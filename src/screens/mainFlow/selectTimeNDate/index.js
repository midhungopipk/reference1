import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  TinyTitle,
  HeaderSimple,
  SmallText,
  TinierTitle,
  DropdownPickerModal,
  ButtonColored,
} from '../../../components';
import {routes, sizes} from '../../../services';

export default function SelectTimeNDate({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [openDate, setOpenDate] = useState(false);
  const [openMonth, setOpenMonth] = useState(false);
  const [openYear, setOpenYear] = useState(false);
  const [openTimeSlot, setOpenTimeSlot] = useState(false);
  const [valueDay, setValueDay] = useState('02');
  const [valueMonth, setValueMonth] = useState('06');
  const [valueYear, setValueYear] = useState('2021');
  const [valueTimeSlot, setValueTimeSlot] = useState('08:00 AM - 12:00 PM');
  const [itemsDate, setItemsDate] = useState([
    {label: '01', value: '01'},
    {label: '02', value: '02'},
    {label: '03', value: '03'},
    {label: '04', value: '04'},
    {label: '05', value: '05'},
    {label: '06', value: '06'},
    {label: '07', value: '07'},
    {label: '08', value: '08'},
    {label: '09', value: '09'},
    {label: '10', value: '10'},
    {label: '11', value: '11'},
    {label: '12', value: '12'},
    {label: '13', value: '13'},
    {label: '14', value: '14'},
    {label: '15', value: '15'},
    {label: '16', value: '16'},
    {label: '17', value: '17'},
    {label: '18', value: '18'},
    {label: '19', value: '19'},
    {label: '20', value: '20'},
    {label: '21', value: '21'},
    {label: '22', value: '22'},
    {label: '23', value: '23'},
    {label: '24', value: '24'},
    {label: '25', value: '25'},
    {label: '26', value: '26'},
    {label: '27', value: '27'},
    {label: '28', value: '28'},
    {label: '29', value: '29'},
    {label: '30', value: '30'},
    {label: '31', value: '31'},
  ]);
  const [itemsMonth, setItemsMonth] = useState([
    {label: '01', value: '01'},
    {label: '02', value: '02'},
    {label: '03', value: '03'},
    {label: '04', value: '04'},
    {label: '05', value: '05'},
    {label: '06', value: '06'},
    {label: '07', value: '07'},
    {label: '08', value: '08'},
    {label: '09', value: '09'},
    {label: '10', value: '10'},
    {label: '11', value: '11'},
    {label: '12', value: '12'},
  ]);
  const [itemsYear, setItemsYear] = useState([
    {label: '2021', value: '2021'},
    {label: '2022', value: '2022'},
  ]);
  const [itemsTimeSlot, setItemsTimeSlot] = useState([
    {label: '08:00 AM - 12:00 PM', value: '08:00 AM - 12:00 PM'},
    {label: '12:00 PM - 03:00 PM', value: '12:00 PM - 03:00 PM'},
    {label: '03:00 PM - 06:00 PM', value: '03:00 PM - 06:00 PM'},
  ]);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        {/* <KeyboardAwareScrollView> */}
        <View>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />

          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>
              Choose Date and Time
            </TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>
              Choose Date and Time
            </TinyTitle>
          )}
          <Spacer
            height={
              Platform.OS == 'ios'
                ? sizes.baseMargin * 1.5
                : sizes.doubleBaseMargin
            }
          />
          <SmallText style={{marginLeft: width(4), color: colors.textGrey}}>
            Select Test Date
          </SmallText>
          <Spacer height={sizes.smallMargin} />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <DropdownPickerModal
              open={openDate}
              value={valueDay}
              items={itemsDate}
              setOpen={setOpenDate}
              setValue={setValueDay}
              setItems={setItemsDate}
              maxHeight={20}
              containerStyle={{
                marginLeft: width(4),
                width: width(18),
              }}
              zIndex={3000}
              zIndexInverse={1000}
            />
            <DropdownPickerModal
              open={openMonth}
              value={valueMonth}
              items={itemsMonth}
              setOpen={setOpenMonth}
              setValue={setValueMonth}
              setItems={setItemsMonth}
              containerStyle={{
                marginLeft: width(4),
                width: width(18),
              }}
              zIndex={2000}
              zIndexInverse={2000}
            />
            <DropdownPickerModal
              open={openYear}
              value={valueYear}
              items={itemsYear}
              setOpen={setOpenYear}
              setValue={setValueYear}
              setItems={setItemsYear}
              containerStyle={{
                marginLeft: width(4),
                width: width(22),
              }}
              zIndex={3000}
              zIndexInverse={1000}
            />
          </View>

          <Spacer height={sizes.baseMargin * 1.5} />
          <SmallText style={{marginLeft: width(4), color: colors.textGrey}}>
            Preferable Test Time (You will get notified with your test time)
          </SmallText>
          <Spacer height={sizes.smallMargin} />
          <DropdownPickerModal
            open={openTimeSlot}
            value={valueTimeSlot}
            items={itemsTimeSlot}
            setOpen={setOpenTimeSlot}
            setValue={setValueTimeSlot}
            setItems={setItemsTimeSlot}
            containerStyle={{
              marginLeft: width(4),
              width: width(50),
            }}
            disableBorderRadius={true}
            zIndex={1000}
            zIndexInverse={3000}
          />

          <Spacer height={sizes.doubleBaseMargin * 3} />
          {/* </KeyboardAwareScrollView> */}
        </View>

        <View
          style={{width: width(100), position: 'absolute', bottom: height(4)}}>
          <ButtonColored
            text={'CONTINUE'}
            onPress={() => navigation.navigate(routes.mapTest)}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              marginBottom: sizes.doubleBaseMargin,
              alignSelf: 'flex-end',
              borderRadius: 5,
            }}
          />
        </View>
        <Spacer height={sizes.baseMargin} />
      </SafeAreaView>
    </Fragment>
  );
}
