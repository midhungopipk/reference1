import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
  Linking,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  TinyTitle,
  BackIcon,
  SmallTitle,
  TinyText,
  HeaderSimple,
  FilterProductsModal,
  LargeText,
  LineHorizontal,
  CustomIcon,
  SmallText,
  VersionModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';
import {store} from '../../../Redux/configureStore';
import {getHomeData} from '../../../services/api/home.api';

export default function Services({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([
    // {img: appImages.s1, name: Translate('Covid-19 Test')},
    {img: appImages.s2, name: Translate('Upload Prescription')},
    // {img: appImages.s3, name: Translate('Quotation Request')},
    {img: appImages.s4, name: Translate('24/7 Pharmacies')},
  ]);

  const [versionVisible, setVersionVisible] = useState(false);
  const user = store.getState().user.user.user;

  useEffect(async () => {
    setLoading(true);
    getHomeData(user.user_id, getVersionHomeResponse, setLoading);
  }, [user]);

  const getVersionHomeResponse = async res => {
    if (res) {
      if (res.api_version && sizes.current_version) {
        if (res.api_version != sizes.current_version) {
          console.log(res.api_version);
          setVersionVisible(true);
        } else {
          setVersionVisible(false);
        }
      }
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={colors.statusBarBlueColor}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.activeBottomIcon}]}>
        <KeyboardAwareScrollView>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          <View style={[styles.onlyRow, {width: width(100)}]}>
            {/* <BackIcon color={colors.snow} /> */}
            <LargeText
              style={[
                appStyles.whiteText,
                {marginLeft: width(7), marginVertical: height(2)},
              ]}>
              {Translate('Services')}
            </LargeText>
          </View>

          <LineHorizontal
            style={{
              marginHorizontal: width(6),
              backgroundColor: colors.borderLightColor,
            }}
          />

          <Spacer height={sizes.doubleBaseMargin} />

          <View style={[styles.mainViewContainer, appStyles.center]}>
            <CustomIcon icon={appImages.healthIcon} size={totalSize(25)} />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <TinyTitle style={styles.oneLinerText}>
                {Translate('A Better Access To Health Care Services')}!
              </TinyTitle>
            ) : (
              <SmallTitle style={styles.oneLinerText}>
                {Translate('A Better Access To Health Care Services')}!
              </SmallTitle>
            )}
            <Spacer height={sizes.doubleBaseMargin} />
            <FlatList
              data={categoryData1}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              numColumns={2}
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <TouchableOpacity
                  style={styles.servicesListContainer}
                  onPress={
                    () =>
                      index == 0
                        ? navigation.navigate(routes.uploadPrescription)
                        : navigation.navigate(routes.pharmacies)
                    // ? navigation.navigate(routes.pharmacies)
                    // : index == 2
                    // ? navigation.navigate(routes.quotationRequest)
                    // : navigation.navigate(routes.pharmacies)
                    // index == 0
                    //   ? navigation.navigate(routes.covidTest)
                    //   : index == 1
                    //   ? navigation.navigate(routes.uploadPrescription)
                    //   : index == 2
                    //   ? navigation.navigate(routes.quotationRequest)
                    //   : navigation.navigate(routes.pharmacies)
                  }>
                  <CustomIcon icon={item.img} />
                  {Platform.OS == 'ios' ? (
                    <TinyText style={{fontFamily: fontFamily.appTextMedium}}>
                      {item.name}
                    </TinyText>
                  ) : (
                    <SmallText
                      style={{
                        fontFamily: fontFamily.appTextBold,
                        marginRight: width(2),
                      }}>
                      {item.name}
                    </SmallText>
                  )}
                </TouchableOpacity>
              )}
            />

            <Spacer height={sizes.baseMargin} />
          </View>

          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />
          <FilterProductsModal
            isVisible={isVisible}
            toggleModal={() => setIsVisible(false)}
          />
        </KeyboardAwareScrollView>
      </SafeAreaView>
      <VersionModal
        isVisible={versionVisible}
        onPress={() => {
          Platform.OS == 'ios'
            ? Linking.openURL(
                'https://apps.apple.com/pk/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328',
              )
            : Linking.openURL(
                'https://play.google.com/store/apps/details?id=com.dwaae.smartapp',
              );
        }}
      />
    </Fragment>
  );
}
