import React, {Fragment, useState, useEffect, Component} from 'react';
import {StatusBar, SafeAreaView, View, BackHandler} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {HeaderSimple} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {WebView} from 'react-native-webview';

export default function About({navigation, route}) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container]}>
        <KeyboardAwareScrollView>
          <HeaderSimple onBackPress={() => navigation.pop()} />
          <View style={{width: width(100), height: height(100)}}>
            <WebView
              source={{
                uri: 'https://homedevo.com/about-us/',
              }}
            />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
