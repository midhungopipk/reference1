import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  ImageCategoriesTextSimple,
  LogoMain,
  FilterPageBanner,
  SmallText,
  HomeItemCard,
  Wrapper,
  TinierTitle,
  TouchableCustomIcon,
  TinyText,
  LoadingCard,
  FilterProductsModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {getFilterProducts} from '../../../services/api/home.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {storageConst} from '../../../services';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';

export default function FilterProducts({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([
    {img: appImages.filterProduct1},
    {img: appImages.filterProduct2},
    {img: appImages.filterProduct3},
    {img: appImages.filterProduct4},
    {img: appImages.filterProduct5},
  ]);
  const [cRSelected, setCRSelected] = useState(1);
  const [categoryRoundData, setCategoryRoundData] = useState([
    {selected: true, img: appImages.round1, name: 'Pain Relief'},
    {selected: false, img: appImages.round2, name: 'Stomach & Bowel'},
    {selected: false, img: appImages.round3, name: 'Skin Treatment'},
    {selected: false, img: appImages.round1, name: 'Pain Relief'},
  ]);
  const [listData, setListData] = useState([
    {
      chart: appImages.chart,
      like: appImages.like,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product1,
    },
    {
      chart: appImages.chartFill,
      like: appImages.likeFill,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product2,
    },
    {
      chart: appImages.chart,
      like: appImages.like,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product1,
    },
    {
      chart: appImages.chartFill,
      like: appImages.like,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product2,
    },
    {
      chart: appImages.chart,
      like: appImages.likeFill,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product1,
    },
    {
      chart: appImages.chart,
      like: appImages.like,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product2,
    },
  ]);
  const [LTR, setLTR] = useState(true);
  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [response, setResponse] = useState(null);
  const [categoryName, setCategoryName] = useState('');
  const [categoryId, setCategoryId] = useState(
    store.getState().category.categoryId,
  );

  useEffect(async () => {
    setLoading(true);
    // getFilterProducts(user.user_id, categoryId, onGetResponse, setLoading);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
  }, [user, categoryId]);

  const onGetResponse = async res => {
    console.log('Filter API: ', res);
    // setLoading(false);
    if (res) {
      setResponse(res);
      setCategoryName(res.category_name);
      setListData(res.products);
      setLoading(false);
    }
    setLoading(false);
  };

  const HandleListPress = index => {
    const temp = [
      {selected: false, img: appImages.round1, name: 'Pain Relief'},
      {selected: false, img: appImages.round2, name: 'Stomach & Bowel'},
      {selected: false, img: appImages.round3, name: 'Skin Treatment'},
      {selected: false, img: appImages.round1, name: 'Pain Relief'},
    ];

    temp[index].selected = true;

    setCategoryRoundData(temp);
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.baseMargin * 1.3} />
          <View
            style={[
              styles.mainViewContainer,
              appStyles.spaceBetween,
              {width: width(100), flexDirection: 'row'},
            ]}>
            <FilterPageBanner
              animation={'fadeInRight'}
              source={appImages.cBanner2}
              imageStyle={styles.bigBanner}
              text={'Filter'}
              subText={'You can filter from our wide range of quality products'}
              onBackPress={() => navigation.pop()}
              // resizeMode={'cover'}
            />
          </View>

          <View
            style={[
              styles.mainViewContainer,
              {backgroundColor: colors.bgLight},
            ]}>
            <Spacer height={2} />
            <FlatList
              data={categoryData1}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <ImageCategoriesTextSimple
                  // key={index}
                  source={item.img}
                  animation={'fadeInLeft'}
                  imageStyle={styles.smallBannerList}
                  resizeMode={'stretch'}
                />
              )}
            />

            <Spacer height={sizes.baseMargin} />
            <Wrapper
              animation={'fadeIn'}
              style={[styles.filterContainer, appStyles.shadowSmall]}>
              <FlatList
                data={categoryRoundData}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                // horizontal
                numColumns={4}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <TouchableOpacity
                    key={index}
                    style={styles.roundCategoryMainContainer}
                    // onPress={HandleListPress(index)}>
                    onPress={() => HandleListPress(index)}>
                    <View
                      style={[
                        styles.roundCategoryListContainer,
                        {
                          backgroundColor: item.selected
                            ? colors.statusBarColor
                            : 'transparent',
                        },
                      ]}>
                      <ImageCategoriesTextSimple
                        source={item.img}
                        animation={'fadeInLeft'}
                        imageStyle={styles.roundCategoryList}
                        resizeMode={'contain'}
                        onPress={() => HandleListPress(index)}
                      />
                    </View>
                    <SmallText
                      style={{
                        marginTop: height(1),
                        color: item.selected
                          ? colors.statusBarColor
                          : colors.textGrey,
                      }}>
                      {item.name}
                    </SmallText>
                  </TouchableOpacity>
                )}
              />
            </Wrapper>
            <Spacer height={sizes.baseMargin} />
            <Wrapper
              animation={'fadeIn'}
              style={[styles.filterContainer, appStyles.shadowSmall]}>
              <View>
                <TinierTitle
                  style={{
                    fontFamily:
                      Platform.OS == 'ios'
                        ? fontFamily.appTextMedium
                        : fontFamily.appTextBold,
                  }}>
                  Skin Care
                </TinierTitle>
                {Platform.OS == 'ios' ? (
                  <TinyText style={{color: colors.textGrey1}}>
                    1279 items found
                  </TinyText>
                ) : (
                  <SmallText style={{color: colors.textGrey1}}>
                    1279 items found
                  </SmallText>
                )}
              </View>
              <View style={styles.onlyRow}>
                <TouchableOpacity
                  onPress={() => setIsVisible(true)}
                  style={[
                    styles.onlyRow,
                    {borderRightWidth: 1, borderColor: colors.borderColor},
                  ]}>
                  <SmallText>Filter</SmallText>
                  <TouchableCustomIcon
                    onPress={() => setIsVisible(true)}
                    icon={appImages.filter}
                    size={Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)}
                    style={{marginLeft: width(2), marginRight: width(5)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setIsVisible(true)}
                  style={styles.onlyRow}>
                  <SmallText style={{marginLeft: width(5)}}>Sort</SmallText>
                  <TouchableCustomIcon
                    onPress={() => setIsVisible(true)}
                    icon={appImages.sort}
                    size={Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)}
                    style={{marginLeft: width(2)}}
                  />
                </TouchableOpacity>
              </View>
            </Wrapper>
            <Spacer height={sizes.TinyMargin} />
            <FlatList
              data={listData}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              numColumns={2}
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <HomeItemCard
                  item={item}
                  LTR={LTR}
                  style={{
                    borderColor: colors.borderLightColor,
                    borderRadius: 5,
                    width: width(43),
                    marginHorizontal: width(1),
                  }}
                  onPress={() => navigation.navigate(routes.productDetail)}
                />
              )}
            />
          </View>

          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />
          <FilterProductsModal
            isVisible={isVisible}
            toggleModal={() => setIsVisible(false)}
          />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
