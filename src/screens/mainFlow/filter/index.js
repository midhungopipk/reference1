import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  RegularText,
  LoadingCard,
  TinyTitle,
  TouchableCustomIcon,
  CheckBoxPrimary,
  ButtonColored,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {useSelector, useDispatch} from 'react-redux';
/* Api Calls */
import {getFilters} from '../../../services/api/home.api';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {fetchUser, fetchFilterData} from '../../../Redux/Actions/index';

export default function Filters({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [tempCategoryData, setTempCategoryData] = useState([
    // {name: 'Seller'},
  ]);
  const [categoryData, setCategoryData] = useState([
    // {name: 'Seller'},
  ]);
  const [categoryData1, setCategoryData1] = useState([
    // {img: appImages.c9},
  ]);
  //   const [multiSliderValue, setMultiSliderValue] = useState([3, 7]);
  const [multiSliderValue, setMultiSliderValue] = useState([0, 0]);
  const [lowerLimit, setLowerLimit] = useState(0);
  const [upperLimit, setUpperLimit] = useState(0);
  const [response, setResponse] = useState([]);
  const user = store.getState().user.user.user;
  // const [categoryId, setCategoryId] = useState(
  //   store.getState().category.categoryId,
  // );
  const ID = useSelector(state => state.category);
  const {categoryId} = ID;
  console.log('id redux: ', categoryId);

  useEffect(() => {
    setLoading(true);
    getFilters(user, categoryId, getFilterResponse, setLoading);
  }, [user]);

  //Get Filter API Response
  const getFilterResponse = async res => {
    console.log('Filters API: ', res);
    if (res.success) {
      for (let i = 0; i < res.filters.length; i++) {
        res.filters[i].selected = false;
        for (let j = 0; j < res.filters[i].variants.length; j++) {
          res.filters[i].variants[j].selected = false;
        }
        if (
          res.filters[i].description == 'Price' &&
          !res.filters[i].variants.length
        ) {
          if (res.filters[i].range) {
            res.filters[i].selectedRange = {
              min: res.filters[i].range.min,
              max: res.filters[i].range.max,
            };
          }
          console.log(
            'selected range initialized: ',
            res.filters[i].selectedRange,
          );
        }
      }
      setTempCategoryData(res.filters);
      if (res.filters.length) res.filters[0].selected = true;
      setCategoryData(res.filters);
      if (res.filters && res.filters.length && res.filters[0].variants)
        setCategoryData1(res.filters[0].variants);
      setLoading(false);
    }
    setLoading(false);
  };

  //Handle Reset Filters
  const resetFilters = async () => {
    let tempCategoryDataNew = tempCategoryData;
    for (let i = 0; i < tempCategoryDataNew.length; i++) {
      tempCategoryDataNew[i].selected = false;
      for (let j = 0; j < tempCategoryDataNew[i].variants.length; j++) {
        tempCategoryDataNew[i].variants[j].selected = false;
      }
      if (
        tempCategoryDataNew[i].description == 'Price' &&
        !tempCategoryDataNew[i].variants.length
      ) {
        tempCategoryDataNew[i].selectedRange = {
          min: tempCategoryDataNew[i].range.min,
          max: tempCategoryDataNew[i].range.max,
        };
        console.log(
          'selected range initialized: ',
          tempCategoryDataNew[i].selectedRange,
        );
      }
    }
    // setTempCategoryData(res.filters);
    if (tempCategoryDataNew.length) tempCategoryDataNew[0].selected = true;
    setCategoryData(tempCategoryDataNew);
    if (
      tempCategoryDataNew &&
      tempCategoryDataNew.length &&
      tempCategoryDataNew[0].variants
    )
      setCategoryData1(tempCategoryDataNew[0].variants);
    setCategoryData(prev => [...prev]);
  };

  //Handle Apply Filter
  const handleFilterApply = async () => {
    await store.dispatch(
      fetchFilterData({
        data: categoryData,
      }),
    );
    navigation.pop();
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.appBgColor1}]}>
          <View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.smallMargin
                  : sizes.doubleBaseMargin
              }
            />
            <View style={[styles.onlyRow, {paddingLeft: width(4)}]}>
              <TouchableCustomIcon
                icon={appImages.backArrow}
                onPress={() => navigation.pop()}
                size={totalSize(2)}
              />
              <TinyTitle style={{marginLeft: width(4)}}>Filters</TinyTitle>
            </View>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
              }
            />
            <View
              style={[
                styles.mainViewContainer,
                {width: width(100), flexDirection: 'row'},
              ]}>
              {categoryData && categoryData.length ? (
                <View
                  style={{
                    flex: 3,
                  }}>
                  <FlatList
                    data={categoryData}
                    extraData={categoryData}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(contact, index) => String(index)}
                    renderItem={({index, item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          if (item.variants.length) {
                            setCategoryData1(item.variants);
                            setCategoryData1(prev => [...prev]);
                          } else {
                            if (item.description == 'Price') {
                              console.log(item.range.min, item.range.max);
                              setLowerLimit(item.range.min);
                              setUpperLimit(item.range.max);
                              setMultiSliderValue([
                                item.range.min,
                                item.range.max,
                              ]);
                              setCategoryData1(item);
                            } else {
                              setCategoryData1([]);
                            }
                          }
                          let temp = tempCategoryData;
                          temp.forEach(element => {
                            element.selected = false;
                          });
                          temp[index].selected = true;
                          setCategoryData(temp);
                          setCategoryData(prev => [...prev]);
                        }}
                        style={[
                          styles.filterHeading,
                          {
                            backgroundColor: item.selected
                              ? colors.greenLighter
                              : colors.snow,
                          },
                        ]}>
                        <RegularText
                          style={[
                            appStyles.textBold,
                            {
                              color: item.selected
                                ? colors.statusBarColor
                                : colors.black,
                            },
                          ]}>
                          {item.description}
                        </RegularText>
                      </TouchableOpacity>
                    )}
                  />
                </View>
              ) : null}
              <View
                style={{
                  flex: 7,
                  backgroundColor: colors.bgLight,
                }}>
                <View>
                  {categoryData1 && categoryData1.length ? (
                    <FlatList
                      data={categoryData1}
                      contentContainerStyle={{justifyContent: 'space-between'}}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={(contact, index) => String(index)}
                      renderItem={({index, item}) => (
                        <View
                          style={{
                            paddingVertical: height(1),
                            paddingLeft: width(4),
                            // paddingRight: width(2),
                            borderWidth: 1,
                            borderColor: colors.borderLightColor,
                            flexDirection: 'row',
                          }}>
                          <CheckBoxPrimary
                            checked={item.selected}
                            onPress={() => {
                              let temp = categoryData1;
                              temp[index].selected = !temp[index].selected;
                              setCategoryData1(temp);
                              setCategoryData1(prev => [...prev]);
                              let mainTemp = categoryData;
                              for (let i = 0; i < mainTemp.length; i++) {
                                if (mainTemp[i].selected) {
                                  mainTemp[i].variants = temp;
                                  setCategoryData(mainTemp);
                                  break;
                                }
                              }
                            }}
                          />
                          <View style={{paddingRight: width(6)}}>
                            {Platform.OS == 'ios' ? (
                              <SmallText>{item.variant}</SmallText>
                            ) : (
                              <RegularText>{item.variant}</RegularText>
                            )}
                          </View>
                        </View>
                      )}
                    />
                  ) : categoryData1 && categoryData1.description == 'Price' ? (
                    <View style={{alignItems: 'center'}}>
                      <Spacer height={sizes.baseMargin * 2} />
                      <MultiSlider
                        values={[multiSliderValue[0], multiSliderValue[1]]}
                        sliderLength={width(60)}
                        enableLabel
                        min={lowerLimit}
                        max={upperLimit}
                        onValuesChangeFinish={values => {
                          setMultiSliderValue([values[0], values[1]]);
                          console.log(multiSliderValue);

                          let temp = categoryData1;
                          console.log('before range: ', temp);
                          temp.selectedRange = {
                            min: values[0],
                            max: values[1],
                          };
                          console.log('after range: ', temp);

                          setCategoryData1(temp);
                          // setCategoryData1(prev => [...prev]);
                          let mainTemp = categoryData;
                          for (let i = 0; i < mainTemp.length; i++) {
                            if (mainTemp[i].selected) {
                              mainTemp[i] = temp;
                              setCategoryData(mainTemp);
                              break;
                            }
                          }
                        }}
                      />
                    </View>
                  ) : null}
                </View>
              </View>
            </View>

            <Spacer height={sizes.baseMargin} />

            <View style={[styles.row]}>
              <ButtonColored
                text={'RESET ALL'}
                onPress={resetFilters}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(40),
                  alignSelf: 'center',
                  borderRadius: 5,
                  backgroundColor: colors.snow,
                  borderWidth: 1,
                  borderColor: colors.statusBarColor,
                }}
                textStyle={{color: colors.black}}
              />
              <ButtonColored
                text={'APPLY'}
                // onPress={() => navigation.pop()}
                onPress={handleFilterApply}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(40),
                  alignSelf: 'center',
                  borderRadius: 5,
                }}
              />
            </View>
            <Spacer height={sizes.baseMargin} />
          </View>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
