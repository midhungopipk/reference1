import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {setScreenState} from '../../../Redux/Actions/Navigation';
import {Icon} from 'react-native-elements';

import {
  Spacer,
  TinyTitle,
  HeaderSimple,
  SmallText,
  TinierTitle,
  DropdownPickerModal,
  MediumText,
  TextInputColored,
  ButtonColored,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function BloodTest({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([
    {img: appImages.bloodTest},
    {img: appImages.swabTest},
    {img: appImages.vaccine},
  ]);
  const [confirmedTests, setConfirmedTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
    },
  ]);
  const [historyTests, setHistoryTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
  ]);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState('2');
  const [items, setItems] = useState([
    {label: '01', value: '1'},
    {label: '02', value: '2'},
    {label: '03', value: '3'},
    // {label: '04', value: '4'},
    // {label: '05', value: '5'},
  ]);

  const PersonCard = props => {
    return (
      <View style={[styles.personContainer]}>
        <MediumText style={{color: colors.gradient1}}>
          Person # {props.number}
        </MediumText>
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Full Name (as Emirates ID / Passport)
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Full Name (as Emirates ID / Passport)
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Emirates ID Number / Passport Number
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Emirates ID Number / Passport Number
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Mobile Number
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Mobile Number
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />

          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>
              How many persons ?
            </TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>
              How many persons ?
            </TinyTitle>
          )}
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          <DropdownPickerModal
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            containerStyle={{
              marginLeft: width(4),
              width: width(20),
            }}
          />
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          <PersonCard number={1} />
          <PersonCard number={2} />
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          <ButtonColored
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              borderRadius: 5,
            }}
            text={'CONTINUE'}
            onPress={() => navigation.navigate(routes.selectTimeNDate)}
          />
          <Spacer height={sizes.doubleBaseMargin} />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
