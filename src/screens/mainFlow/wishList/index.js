import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  WishListCard,
  Wrapper,
  TinierTitle,
  HeaderSimple,
  TinyText,
  RegularText,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
} from '../../../services';
/* Api Calls */
import {getWishList, deleteWishList} from '../../../services/api/cart.api';
import AsyncStorage from '@react-native-community/async-storage';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchUser, fetchCartData} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToCartMethod,
  RemoveProductFromWishListMethod,
} from '../../../services/helpingMethods';

export default function WishList({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [listData, setListData] = useState([]);
  const user = store.getState().user.user.user;
  const [LTR, setLTR] = useState(true);
  const [cartLoading, setCartLoading] = useState(false);

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getWishList(user.user_id, onGetResponse, setLoading);
  }, [user]);

  //Get Wish List API Response
  const onGetResponse = async res => {
    console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) setListData(res.products);
      else {
        setListData([]);
        setListData(prev => [...prev]);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Get Cart API Response
  const getCartResponse = async res => {
    console.log('Cart API: ', res);
    setCartLoading(false);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        if (user.user_id == '0') {
          let updatedUser = user;
          user.user_id = res.guest_id;
          console.log('Previous user: ', user);
          console.log('Updated user: ', updatedUser);
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
    }
  };

  //Get Wish List API Response
  const getWishResponse = async res => {
    console.log('Wish list remove API: ', res);
    if (res) {
      Toast.show(res.message);

      // if (res.products && res.products.length) {
      //   setListData(res.products);
      //   setListData(prev => [...prev]);
      // } else {
      //   setListData([]);
      //   setListData(prev => [...prev]);
      // }
      getWishList(user.user_id, onGetResponse, setLoading);
    }
  };

  //Handle Clear Wish List Press
  const handleClearWishList = async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == null || language == undefined) language = 'en';
    let params = {
      user_id: user.user_id,
      lang_code: language,
      currency_code: 'AED',
    };
    deleteWishList(params, onDeleteListResponse, setLoading);
  };

  //Get Delete Wish List API Response
  const onDeleteListResponse = async res => {
    console.log('Wish list remove API: ', res);
    if (res.success) {
      setListData([]);
      setListData(prev => [...prev]);
      Toast.show(res.message);
    }
    setLoading(false);
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          <KeyboardAwareScrollView>
            <HeaderSimple
              showSearch
              onBackPress={() => navigation.pop()}
              onPress={() => navigation.navigate('Cart')}
              quantity={
                store.getState().cart.data && store.getState().cart.data.data
                  ? store.getState().cart.data.data
                  : 0
              }
            />

            <View
              style={[
                styles.mainViewContainer,
                {backgroundColor: colors.bgLight},
              ]}>
              <Wrapper
                animation={'fadeIn'}
                style={[styles.filterContainer, appStyles.shadowSmall]}>
                <View>
                  <TinierTitle
                    style={{
                      fontFamily:
                        Platform.OS == 'ios'
                          ? fontFamily.appTextBold
                          : fontFamily.appTextBold,
                    }}>
                    My Wish List
                  </TinierTitle>
                  {Platform.OS == 'ios' ? (
                    <TinyText
                      style={{
                        color: colors.textGrey1,
                        fontFamily: fontFamily.appTextDisplayRegular,
                      }}>
                      {listData.length} items in wish list
                    </TinyText>
                  ) : (
                    <SmallText
                      style={{
                        color: colors.textGrey1,
                        fontFamily: fontFamily.appTextDisplayRegular,
                      }}>
                      {listData.length} items in wish list
                    </SmallText>
                  )}
                </View>
                <TouchableOpacity onPress={handleClearWishList}>
                  <RegularText
                    style={{
                      fontFamily:
                        Platform.OS == 'ios'
                          ? fontFamily.appTextMedium
                          : fontFamily.appTextBold,
                      color: colors.gradient1,
                    }}>
                    CLEAR ALL
                  </RegularText>
                </TouchableOpacity>
              </Wrapper>
              <Spacer height={sizes.smallMargin} />
              <FlatList
                data={listData}
                // contentContainerStyle={{
                //   alignItems: 'center',
                // }}
                numColumns={2}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <WishListCard
                    item={item}
                    LTR={LTR}
                    style={{
                      borderColor: colors.borderLightColor,
                      borderRadius: 5,
                      width: width(43),
                      marginHorizontal: width(1),
                      marginLeft: index % 2 == 0 ? width(4) : width(2),
                    }}
                    onPress={() =>
                      navigation.navigate(routes.productDetail, {
                        productId: item.product_id,
                      })
                    }
                    addToCartPress={async () => {
                      if (!cartLoading) {
                        setCartLoading(true);
                        await AddToCartMethod({
                          item,
                          user,
                          getCartResponse,
                        });
                      }
                    }}
                    deleteItemPress={async () => {
                      await RemoveProductFromWishListMethod({
                        item_id: item.item_id,
                        getWishResponse,
                      });
                    }}
                  />
                )}
              />
            </View>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin
              }
            />
          </KeyboardAwareScrollView>
          {cartLoading ? (
            <View
              style={{
                position: 'absolute',
                bottom: height(30),
                borderRadius: 5,
                width: totalSize(5),
                height: totalSize(5),
                backgroundColor: 'rgba(255,255,255,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
        </SafeAreaView>
      )}
    </Fragment>
  );
}
