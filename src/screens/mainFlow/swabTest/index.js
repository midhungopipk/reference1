import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  TinyTitle,
  HeaderSimple,
  CustomIcon,
  SmallText,
  TinierTitle,
  DropdownPickerModal,
  MediumText,
  TextInputColored,
  ButtonColored,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function SwabTest({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);

  const [open, setOpen] = useState(false);
  const [test, setTest] = useState(1);
  const [value, setValue] = useState('2');
  const [items, setItems] = useState([
    {label: '01', value: '1'},
    {label: '02', value: '2'},
    {label: '03', value: '3'},
    {label: '04', value: '4'},
    {label: '05', value: '5'},
  ]);

  const PersonCard = props => {
    return (
      <View style={[styles.personContainer]}>
        <MediumText style={{color: colors.gradient1}}>
          Person # {props.number}
        </MediumText>
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Full Name (as Emirates ID / Passport)
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Full Name (as Emirates ID / Passport)
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Emirates ID Number / Passport Number
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Emirates ID Number / Passport Number
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
        <Spacer height={sizes.smallMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText style={{fontFamily: fontFamily.appTextBold}}>
            Mobile Number
          </SmallText>
        ) : (
          <MediumText style={{fontFamily: fontFamily.appTextBold}}>
            Mobile Number
          </MediumText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
        />
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />

          <Spacer
            height={
              Platform.OS == 'ios'
                ? sizes.baseMargin * 1.5
                : sizes.doubleBaseMargin
            }
          />
          <View style={[styles.onlyRow, {justifyContent: 'space-around'}]}>
            <TouchableOpacity style={styles.onlyRow} onPress={() => setTest(1)}>
              <TouchableOpacity
                style={[styles.radioButton, {marginRight: width(2)}]}
                onPress={() => setTest(1)}>
                <View
                  style={[
                    styles.innerRadioButton,
                    {
                      backgroundColor:
                        test == 1 ? colors.statusBarColor : 'transparent',
                    },
                  ]}
                />
              </TouchableOpacity>
              <CustomIcon
                icon={appImages.testClinic}
                size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
              />
              <SmallText style={{marginLeft: width(2)}}>Lab Test</SmallText>
            </TouchableOpacity>
            <TouchableOpacity style={styles.onlyRow} onPress={() => setTest(2)}>
              <TouchableOpacity
                style={[styles.radioButton, {marginRight: width(1)}]}
                onPress={() => setTest(2)}>
                <View
                  style={[
                    styles.innerRadioButton,
                    {
                      backgroundColor:
                        test == 2 ? colors.statusBarColor : 'transparent',
                    },
                  ]}
                />
              </TouchableOpacity>
              <CustomIcon
                icon={appImages.testHome}
                size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
              />
              <SmallText style={{marginLeft: width(1)}}>
                VIP Test (At your home)
              </SmallText>
            </TouchableOpacity>
          </View>
          <Spacer
            height={
              Platform.OS == 'ios'
                ? sizes.baseMargin * 1.2
                : sizes.baseMargin * 1.3
            }
          />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>
              How many persons ?
            </TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>
              How many persons ?
            </TinyTitle>
          )}
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          <DropdownPickerModal
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            containerStyle={{
              marginLeft: width(4),
              width: width(20),
            }}
          />
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          <PersonCard number={1} />
          <PersonCard number={2} />
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          <ButtonColored
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              borderRadius: 5,
            }}
            text={'CONTINUE'}
            onPress={() => navigation.navigate(routes.vipTestMapHome)}
          />
          <Spacer height={sizes.doubleBaseMargin} />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
