import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  Image,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TextInputColored,
  RegularText,
  ImageProfile,
  LoadingCard,
  CameraModal,
  CustomIcon,
  TinyText,
} from '../../../components';
// import CameraModal from '../../../components/CameraModal';
import {
  appImages,
  fontFamily,
  routes,
  sizes,
  Translate,
  appStyles,
} from '../../../services';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
/* Api Calls */
import {
  updateUserProfile,
  UploadProfilePicture,
  GetUserData,
  getUserAddresses,
  deleteAddress,
} from '../../../services/api/user.api';
import {fetchUser} from '../../../Redux/Actions/index';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchCartCheckoutData} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {useInputValue} from '../../../services/hooks/useInputValue';
import AsyncStorage from '@react-native-community/async-storage';
import {storageConst} from '../../../services/constants';
import FastImage from 'react-native-fast-image';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function EditProfile({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [profileImageloading, setProfileImageloading] = useState(false);
  const [dataLoading, setDataLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [avatarSource, setAvatarSource] = useState(null);
  const [callbackE1, setCallbackE1] = useState(null);
  const [profileUploadResponse, setProfileUploadResponse] = useState(null);

  const [userData, setUserData] = useState(null);

  const [address, setAddress] = useState([]);

  const FirstName = useInputValue('');
  const LastName = useInputValue('');
  const MobileNumber = useInputValue('');
  const Email = useInputValue('');
  // const Password = useInputValue('');
  // const RPassword = useInputValue('');
  const FirstNameErr = useInputValue('');
  const LastNameErr = useInputValue('');
  const MobileNumberErr = useInputValue('');
  const EmailErr = useInputValue('');
  // const PasswordErr = useInputValue('');
  // const RPasswordErr = useInputValue('');

  // const user = useSelector(state => state.user.user);
  const [LTR, setLTR] = useState(true);
  const user = store.getState().user.user.user;

  useEffect(async () => {
    // setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') setLTR(false);
    GetUserData(user.user_id, onGetUserDataResponse);
    getUserAddresses(user.user_id, onGetAddress);
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      console.log('called navigation');
      GetUserData(user.user_id, onGetUserDataResponse);
      getUserAddresses(user.user_id, onGetAddress);
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(async () => {
    console.log('avatar source called');
    setAvatarSource(avatarSource);
  }, [avatarSource]);

  const onGetAddress = async res => {
    console.log('.......sss.........cxxx..', res.profiles);
    setAddress(res.profiles);
  };

  const onGetUserDataResponse = async res => {
    console.log('User Data api response called in beginning: ', res);
    if (res.success) {
      console.log('in avatar source: ', res.profile.image_profile);
      FirstName.onChange(res.profile.firstname);
      LastName.onChange(res.profile.lastname);
      MobileNumber.onChange(res.profile.phone);
      Email.onChange(res.profile.email);
      setAvatarSource(res.profile.image_profile);
      setDataLoading(false);
    } else {
      // await AsyncStorage.getItem(storageConst.userData).then(async data => {
      //   if (data) {
      //     FirstName.onChange(res.profile.firstname);
      //     LastName.onChange(res.profile.lastname);
      //     MobileNumber.onChange(res.profile.phone);
      //     Email.onChange(res.profile.email);
      //     // if (res.profile.image_profile && res.profile.image_profile != '') {
      //     setAvatarSource(res.profile.image_profile);
      //     // }
      //   }
      // });
      setDataLoading(false);
    }
    setLoading(false);
  };

  const handleUpdateProfile = async () => {
    setDataLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == null || language == undefined) language = 'en';

    let data = {
      user_id: user.user_id,
      profile_data: {
        email: Email.value,
        password: '',
        firstname: FirstName.value,
        lastname: LastName.value,
        phone: MobileNumber.value,
      },
      lang_code: language,
      currency_code: 'AED',
    };
    console.log('data  :', data);
    updateUserProfile(user.user_id, data, onGetResponse, setDataLoading);
  };

  const delAddress = id => {
    deleteAddress(id, onResponse, setDataLoading);
  };

  const onResponse = async res => {
    if (res.success) {
      getUserAddresses(user.user_id, onGetAddress);
    } else {
      setDataLoading(false);
    }
  };

  const onGetResponse = async res => {
    console.log('Update Profile response: ', res);
    if (res.success) {
      setDataLoading(false);
      Toast.show('Profile updated successfully');
    } else {
      setDataLoading(false);
    }
    setLoading(false);
  };

  const onImageAttached = imageObj => {
    setProfileImageloading(true);
    var data = new FormData();
    let paramData = JSON.stringify({
      user_id: user.user_id,
    });
    if (imageObj != null) {
      data.append('image', {
        uri:
          Platform.OS == 'ios'
            ? imageObj.uri.replace('file://', '')
            : imageObj.uri,
        type: imageObj.type,
        name: imageObj.fileName,
      });
      data.append('params', paramData);
      console.log('param data: ', data);
      UploadProfilePicture(data, onImageUploadResponse, setProfileImageloading);
    }
  };
  const onImageUploadResponse = async res => {
    console.log('response of upload profile picture api : ', res);
    setProfileImageloading(false);
    if (res.success) {
      Toast.show('Successfully uploaded Your profile picture');
    } else {
      Toast.show(res.message);
    }
  };

  const SelectCamera = () => {
    const options = {
      title: 'selectPhoto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchCamera(options, callback => {
      if (callback.didCancel) {
        console.log('User cancelled image picker');
      } else if (callback.error) {
        console.log('ImagePicker Error: ', callback.error);
      } else if (callback.customButton) {
        console.log('User tapped custom button: ', callback.customButton);
      } else {
        setAvatarSource(callback.uri);
        setModalVisible(false);
        if (callback.uri) {
          // setTimeout(() => {
          onImageAttached(callback);
          // }, 1000);
        }

        // this.setState({
        //   avatarSource: callback.uri,
        //   show: true,
        //   modalVisible: false,
        // });
        // this.ImageUpload(callback);
      }
    });
  };

  const SelectImage = () => {
    const options = {
      title: 'selectPhoto',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, callback => {
      if (callback.didCancel) {
        console.log('User cancelled image picker');
      } else if (callback.error) {
        console.log('ImagePicker Error: ', callback.error);
      } else if (callback.customButton) {
        console.log('User tapped custom button: ', callback.customButton);
      } else {
        console.log('image response : ', callback);
        setAvatarSource(callback.uri);
        setModalVisible(false);
        // onImageAttached();
        // setTimeout(() => {
        onImageAttached(callback);
        // }, 1000);
      }
    });
  };

  const ProductCard = ({home, onPress, item}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: width(85),
          justifyContent: 'space-between',
        }}>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={[styles.row, {flex: 1, paddingHorizontal: width(4)}]}>
            <View style={styles.onlyRow}>
              <TouchableOpacity
                style={[appStyles.center, styles.onlyRow]}
                onPress={onPress}>
                <View style={appStyles.center}>
                  <View
                    style={{
                      width: Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                      height:
                        Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                      borderRadius:
                        Platform.OS == 'ios'
                          ? totalSize(6) / 2
                          : totalSize(7) / 2,
                      backgroundColor: colors.greyBg,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: 0.5,
                      borderColor: colors.borderLightColor,
                    }}>
                    <CustomIcon
                      icon={
                        item.profile_name == 'my_home'
                          ? appImages.building
                          : appImages.suitcase
                      }
                      size={Platform.OS == 'ios' ? totalSize(3) : totalSize(4)}
                    />
                  </View>
                  <TinyText
                    style={{
                      marginTop: height(1),
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {item.profile_name == 'my_home'
                      ? Translate('Home')
                      : Translate('Work')}
                    {/* {home ? Translate('Home') : Translate('Work')} */}
                  </TinyText>
                </View>
              </TouchableOpacity>

              <View
                style={{
                  paddingHorizontal: width(3),
                  width: width(44),
                }}>
                {Platform.OS == 'ios' ? (
                  <SmallText>{item.s_address}</SmallText>
                ) : (
                  <RegularText>{item.s_address}</RegularText>
                )}
                {Platform.OS == 'ios' ? (
                  <SmallText>{item.s_city + ', ' + item.s_country}</SmallText>
                ) : (
                  <RegularText>
                    {item.s_city + ', ' + item.s_country}
                  </RegularText>
                )}
              </View>
            </View>
          </View>
        </View>
        {/* <TouchableOpacity onPress={() => delAddress(item.profile_id)}>
          <MaterialCommunityIcons
            name={'close-circle'}
            size={width(7)}
            color={'#2863A4'}
          />
        </TouchableOpacity> */}
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.snow}]}>
          <ScrollView>
            <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
            <HeaderSimple
              onBackPress={() => navigation.pop()}
              onPress={() => navigation.navigate('Cart')}
            />
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin
              }
            />

            <KeyboardAwareScrollView>
              <View style={styles.bottomContainer}>
                <Spacer height={sizes.baseMargin} />

                <ImageProfile
                  source={
                    avatarSource
                      ? {
                          uri: avatarSource + '?' + new Date().getTime(),
                        }
                      : appImages.dummy
                  }
                  containerStyle={{alignSelf: 'center'}}
                  onPress={() => setModalVisible(true)}
                  isLoading={profileImageloading}
                />

                <Spacer height={sizes.baseMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('First Name')}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('First Name')}
                  </RegularText>
                )}
                <Spacer height={sizes.smallMargin} />
                <TextInputColored
                  containerStyle={styles.inputfieldStyle}
                  fieldStyle={styles.fieldStyle}
                  placeholder={Translate('First Name')}
                  value={FirstName.value}
                  onChangeText={FirstName.onChange}
                  LTR
                />
                <Spacer height={sizes.baseMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Last Name')}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Last Name')}
                  </RegularText>
                )}
                <Spacer height={sizes.smallMargin} />
                <TextInputColored
                  containerStyle={styles.inputfieldStyle}
                  fieldStyle={styles.fieldStyle}
                  placeholder={Translate('Last Name')}
                  value={LastName.value}
                  onChangeText={LastName.onChange}
                  LTR
                />
                {/* <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    color: colors.textGrey,
                  }}>
                  Password
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    color: colors.textGrey,
                  }}>
                  Password
                </RegularText>
              )}
              <Spacer height={sizes.smallMargin} />
              <TextInputColored
                containerStyle={styles.inputfieldStyle}
                fieldStyle={styles.fieldStyle}
                secureTextEntry
                placeholder={'12345678'}
              />
              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    color: colors.textGrey,
                  }}>
                  Confirm Password
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    color: colors.textGrey,
                  }}>
                  Confirm Password
                </RegularText>
              )}
              <Spacer height={sizes.smallMargin} />
              <TextInputColored
                containerStyle={styles.inputfieldStyle}
                fieldStyle={styles.fieldStyle}
                secureTextEntry
                placeholder={'12345678'}
              /> */}
                <Spacer height={sizes.baseMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Email')}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Email')}
                  </RegularText>
                )}
                <Spacer height={sizes.smallMargin} />
                <TextInputColored
                  containerStyle={styles.inputfieldStyle}
                  fieldStyle={styles.fieldStyle}
                  placeholder={Translate('Email')}
                  value={Email.value}
                  LTR
                />
                <Spacer height={sizes.baseMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Mobile Number')}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('Mobile Number')}
                  </RegularText>
                )}
                <Spacer height={sizes.smallMargin} />
                <TextInputColored
                  containerStyle={styles.inputfieldStyle}
                  fieldStyle={styles.fieldStyle}
                  placeholder={'+971-58-654-2520'}
                  value={MobileNumber.value}
                  onChangeText={MobileNumber.onChange}
                  LTR
                />

                <Spacer height={sizes.baseMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                   {Translate('My Address')}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      textAlign: LTR ? 'left' : 'right',
                    }}>
                    {Translate('My Address')}
                  </RegularText>
                )}
                <Spacer height={sizes.smallMargin} />
                <FlatList
                  data={address}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}
                  keyExtractor={item => item.id}
                  renderItem={({index, item}) => (
                    <ProductCard
                      key={index}
                      item={item}
                      home
                      onPress={() => {}}
                    />
                  )}
                />

                <TouchableOpacity
                  style={styles.addProduct}
                  onPress={() => navigation.navigate(routes.addAddress)}>
                  {Platform.OS == 'ios' ? (
                    <TinyText>
                      {Translate('Add Address')}
                    </TinyText>
                  ) : (
                    <Text style={{color: 'gray'}}>
                      {Translate('Add Address')}
                    </Text>
                  )}
                  <MaterialCommunityIcons
                    name={'plus-circle'}
                    size={width(7)}
                    color={'#2863A4'}
                  />
                </TouchableOpacity>

                <Spacer
                  height={
                    Platform.OS == 'ios'
                      ? sizes.baseMargin * 1.5
                      : sizes.baseMargin * 1.8
                  }
                />
                <ButtonColored
                  isLoading={dataLoading}
                  text={Translate('UPDATE')}
                  onPress={handleUpdateProfile}
                  // onPress={() => navigation.pop()}
                  buttonStyle={{
                    height: Platform.OS == 'ios' ? height(6) : height(7),
                    width: width(92),
                    alignSelf: 'center',
                    borderRadius: 5,
                  }}
                />
                <Spacer height={sizes.baseMargin} />
              </View>
            </KeyboardAwareScrollView>
            <CameraModal
              isVisible={modalVisible}
              onClose={() => setModalVisible(false)}
              imageFromCamera={() => SelectCamera()}
              imageFromGallery={() => SelectImage()}
            />
          </ScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
