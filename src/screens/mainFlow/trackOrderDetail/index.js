import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  Alert,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import Toast from 'react-native-simple-toast';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  MediumText,
  LineHorizontal,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
  storageConst,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import {cancelOrder} from '../../../services/api/cart.api';
import {store} from '../../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';

export default function TrackOrderDetail({navigation, route}) {
  const [loading, setLoading] = useState(false);

  const [accountList, setAccountList] = useState([
    // {
    //   title: 'Prescription #258963',
    //   date: '06/29/2020, 18:12',
    //   status: 'Shipped',
    //   collapsed: false,
    //   items: 2,
    //   text1: 'Candes Eclat Fresh Lightening Face Tonic 200 mL',
    //   text2: 'DW-NAD-P01',
    //   price: 'AED 69.00',
    // },
  ]);

  const [paramData, setParamData] = useState(route.params.item);
  const [response, setResponse] = useState(null);

  useEffect(() => {
    console.log('order detail: ', route.params.item);
    if (route.params.item.products) {
      setAccountList(route.params.item.products);
      setResponse(route.params.item);
    }
  }, [route.params.item]);

  const handleCancelOrder = async () => {
    Alert.alert('Cancel', 'Are you sure you want to Cancel Order?', [
      {
        text: 'Cancel',
        onPress: () => {
          console.log('Cancel Pressed');
        },
        style: 'cancel',
      },
      {
        text: 'Confirm',
        onPress: async () => {
          setLoading(true);
          let language = await AsyncStorage.getItem(storageConst.language);
          if (language == null || language == undefined) language = 'en';

          let orderData = {
            order_id: route.params.item.order_id,
            lang_code: language,
            currency_code: 'AED',
            scope: 'cancel_order',
          };
          cancelOrder(
            route.params.item.order_id,
            orderData,
            onGetResponse,
            setLoading,
          );
        },
      },
    ]);
  };

  const onGetResponse = async res => {
    console.log('delete order API: ', res);
    Toast.show(res.message);
    if (res.success) {
      setLoading(false);
      navigation.pop();
    }
    setLoading(false);
  };

  const ProductCard = ({item, onCollapsablePress}) => {
    return (
      <View>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={{paddingHorizontal: width(3)}}>
            {Platform.OS == 'ios' ? (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product}
              </RegularText>
            ) : (
              <MediumText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product}
              </MediumText>
            )}
            <Spacer height={sizes.TinyMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {item.product_code}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {item.product_code}
              </RegularText>
            )}
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <RegularText>{item.subtotal_format}</RegularText>
              ) : (
                <MediumText>{item.subtotal_format}</MediumText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText style={appStyles.blueText}>
                  {item.amount + Translate('Items')}
                </SmallText>
              ) : (
                <RegularText style={appStyles.blueText}>
                  {item.amount + Translate('Items')}
                </RegularText>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLight}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                styles.row,
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                },
              ]}>
              <View>
                <MediumText
                  style={[
                    appStyles.whiteText,
                    {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                  ]}>
                  {Translate('Order')} #{route.params.item.order_id}
                </MediumText>
                {/* <SmallText
                style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                1 item in your order
              </SmallText> */}
              </View>
            </View>
          </LinearGradient>

          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              <FlatList
                data={accountList}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                numColumns={1}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <ProductCard key={index} item={item} />
                )}
              />

              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Details')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Details')}
                </MediumText>
              )}

              <View
                style={[
                  styles.productCard,
                  Platform.OS == 'ios' && appStyles.shadowSmall,
                ]}>
                <View style={{paddingHorizontal: width(3)}}>
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Shipping Charge')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Shipping Charge')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>{paramData.shipping_cost}</SmallText>
                    ) : (
                      <RegularText>{paramData.shipping_cost}</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Item Total')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Item Total')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>{paramData.subtotal_format} </SmallText>
                    ) : (
                      <RegularText>{paramData.subtotal_format}</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Coupon Discount')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Coupon Discount')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>00.00</SmallText>
                    ) : (
                      <RegularText>00.00</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin * 1.5} />
                  <LineHorizontal height={1} />
                  <Spacer height={sizes.TinyMargin * 1.5} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {paramData.total_format}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {paramData.total_format}
                      </RegularText>
                    )}
                  </View>
                </View>
              </View>
              <Spacer height={sizes.baseMargin} />
              {(response && response.status == 'A') ||
              (response && response.status == 'P') ||
              (response && response.status == 'E') ||
              (response && response.status == 'C') ? (
                <>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Track Order')}
                    </RegularText>
                  ) : (
                    <MediumText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Track Order')}
                    </MediumText>
                  )}

                  <View
                    style={[
                      styles.productCard,
                      Platform.OS == 'ios' && appStyles.shadowSmall,
                    ]}>
                    <View
                      style={[
                        {
                          flexDirection: 'row',
                          paddingHorizontal: width(8),
                          paddingVertical: height(2),
                        },
                      ]}>
                      <View style={{alignItems: 'center'}}>
                        {/* <Spacer
                    height={
                      Platform.OS == 'ios'
                        ? sizes.TinyMargin * 1.8
                        : sizes.smallMargin * 1.6
                    }
                  /> */}
                        <View style={styles.circle} />
                        <View
                          style={[
                            styles.line,
                            {
                              backgroundColor:
                                response && response.status == 'P'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                        <View
                          style={[
                            styles.circle,
                            {
                              backgroundColor:
                                response && response.status == 'P'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                        <View
                          style={[
                            styles.line,
                            {
                              backgroundColor:
                                response && response.status == 'E'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                        <View
                          style={[
                            styles.circle,
                            {
                              backgroundColor:
                                response && response.status == 'E'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                        <View
                          style={[
                            styles.line,
                            {
                              backgroundColor:
                                response && response.status == 'C'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                        <View
                          style={[
                            styles.circle,
                            {
                              backgroundColor:
                                response && response.status == 'C'
                                  ? colors.statusBarColor
                                  : colors.textGrey1,
                            },
                          ]}
                        />
                      </View>

                      <View style={{marginLeft: width(5)}}>
                        <View>
                          {Platform.OS == 'ios' ? (
                            <SmallText>
                              {Translate('Order Confirmed')}
                            </SmallText>
                          ) : (
                            <RegularText>
                              {Translate('Order Confirmed')}
                            </RegularText>
                          )}
                          {/* {Platform.OS == 'ios' ? (
                      <TinyText style={appStyles.textGray}>
                        {paramData.timestamp}
                      </TinyText>
                    ) : (
                      <SmallText style={appStyles.textGray}>
                        {paramData.timestamp}
                      </SmallText>
                    )} */}
                        </View>

                        <Spacer
                          height={
                            Platform.OS == 'ios'
                              ? sizes.baseMargin * 2.1
                              : sizes.baseMargin * 1.3
                          }
                        />
                        <View>
                          {Platform.OS == 'ios' ? (
                            <SmallText>{Translate('Processed')}</SmallText>
                          ) : (
                            <RegularText>{Translate('Processed')}</RegularText>
                          )}

                          {/* {Platform.OS == 'ios' ? (
                      <TinyText style={appStyles.textGray}>N/A</TinyText>
                    ) : (
                      <SmallText style={appStyles.textGray}>N/A</SmallText>
                    )} */}
                        </View>
                        <Spacer
                          height={
                            Platform.OS == 'ios'
                              ? sizes.baseMargin * 2.1
                              : sizes.baseMargin * 1.3
                          }
                        />
                        <View>
                          {Platform.OS == 'ios' ? (
                            <SmallText>{Translate('Shipped')}</SmallText>
                          ) : (
                            <RegularText>{Translate('Shipped')}</RegularText>
                          )}

                          {/* {Platform.OS == 'ios' ? (
                      <TinyText style={appStyles.textGray}>N/A</TinyText>
                    ) : (
                      <SmallText style={appStyles.textGray}>N/A</SmallText>
                    )} */}
                        </View>

                        <Spacer
                          height={
                            Platform.OS == 'ios'
                              ? sizes.baseMargin * 2.1
                              : sizes.baseMargin * 1.3
                          }
                        />
                        <View>
                          {Platform.OS == 'ios' ? (
                            <SmallText style={appStyles.textRed}>
                              {Translate('Completed')}
                            </SmallText>
                          ) : (
                            <RegularText>{Translate('Completed')}</RegularText>
                          )}

                          {/* {Platform.OS == 'ios' ? (
                      <TinyText style={appStyles.textGray}>N/A</TinyText>
                    ) : (
                      <SmallText style={appStyles.textGray}>N/A</SmallText>
                    )} */}
                        </View>
                      </View>
                    </View>
                  </View>
                </>
              ) : null}

              <Spacer height={sizes.doubleBaseMargin} />
              {route.params.item && route.params.item.allow_to_cancel ? (
                <>
                  <ButtonColored
                    onPress={handleCancelOrder}
                    text={Translate('CANCEL ORDER')}
                    buttonStyle={{
                      height: Platform.OS == 'ios' ? height(6) : height(7),
                      width: width(92),
                      alignSelf: 'center',
                      borderRadius: 5,
                    }}
                  />
                  <Spacer height={sizes.smallMargin} />
                </>
              ) : null}
              <ButtonColored
                onPress={() => navigation.navigate('Home')}
                text={Translate('CONTINUE SHOPPING')}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(92),
                  alignSelf: 'center',
                  borderRadius: 5,
                }}
              />
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
