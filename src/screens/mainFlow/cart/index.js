import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  FlatList,
  ActivityIndicator,
  Alert,
  Linking,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  TouchableCustomIcon,
  TinyText,
  MediumText,
  LineHorizontal,
  CustomIcon,
  LargeText,
  LoadingCard,
  VersionModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
/* Api Calls */
import {getCart, checkoutCart} from '../../../services/api/cart.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {
  fetchCartCheckoutData,
  fetchCartData,
} from '../../../Redux/Actions/index';
import {fetchToken, fetchLanguage} from '../../../Redux/Actions';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToWishListMethod,
  ATCIncrementDecrementProductsMethod,
  RemoveProductFromCartMethod,
  ApplyCouponCode,
  Translate,
} from '../../../services/helpingMethods';
import AsyncStorage from '@react-native-community/async-storage';
import RNRestart from 'react-native-restart';
import {getHomeData} from '../../../services/api/home.api';

export default function Cart({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);
  const [couponLoading, setCouponLoading] = useState(false);

  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [response, setResponse] = useState(null);
  const [couponCode, setCouponCode] = useState('');
  const [cartProducts, setCartProducts] = useState(null);
  const [guest, setGuest] = useState(false);
  const [versionVisible, setVersionVisible] = useState(false);
  const [LTR, setLTR] = useState(true);
  const dispatch = useDispatch();

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    console.log('user: ', user);
    getHomeData(user.user_id, getHomeResponse, setLoading);
    await AsyncStorage.getItem(storageConst.guest).then(async value => {
      if (value == '1') {
        setGuest(true);
      } else {
        setGuest(false);
      }
    });
    // getCart('978830047', onGetResponse, setLoading);
    getCart(user.user_id, onGetResponse, setLoading);
    setCouponCode('');
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      console.log('user: ', user);
      await AsyncStorage.getItem(storageConst.guest).then(async value => {
        if (value == '1') {
          setGuest(true);
        } else {
          setGuest(false);
        }
      });
      getHomeData(user.user_id, getHomeResponse, setLoading);
      // getCart('978830047', onGetResponse, setLoading);
      getCart(user.user_id, onGetResponse, setLoading);
      setCouponCode('');
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  //Get Home API response
  const getHomeResponse = async res => {
    if (res) {
      if (res.api_version && sizes.current_version) {
        if (res.api_version != sizes.current_version) {
          console.log(res.api_version);
          setVersionVisible(true);
        } else {
          setVersionVisible(false);
        }
      }
    }
  };

  //Get Cart API Respnse
  const onGetResponse = async res => {
    console.log('GET Cart API: ', res);
    // setLoading(false);
    if (res.success) {
      setResponse(res);
      if (res.cart && res.cart.cart_products) {
        setCartProducts(res.cart.cart_products);
      } else if (res.total_products == 0) {
        setCartProducts([]);
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );

      navigation.navigate('Cart');
      setLoading(false);
    }
    setLoading(false);
  };

  //Handle Add Quantity (Plus Press)
  const handlePlus = async index => {
    // let tempCartProducts = cartProducts;
    let amount = cartProducts[index].amount;
    amount += 1;
    await ATCIncrementDecrementProductsMethod({
      product_id: cartProducts[index].product_id,
      item_id: cartProducts[index].item_id,
      amount,
      user,
      getCartResponse,
    });
  };

  //Handle Decrease Quantity (Minus Press)
  const handleMinus = async index => {
    let amount = cartProducts[index].amount;
    if (amount > 1) {
      amount -= 1;
      await ATCIncrementDecrementProductsMethod({
        product_id: cartProducts[index].product_id,
        item_id: cartProducts[index].item_id,
        amount,
        user,
        getCartResponse,
      });
    } else if (amount == 1) {
      Alert.alert('DWAAE', 'Delete this product from cart?', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            DeleteProduct(index);
          },
        },
      ]);
    }
  };

  //Handle Delete Product (Cross Press)
  const handleDelete = async index => {
    Alert.alert('DWAAE', 'Delete this product from cart?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Delete',
        onPress: () => {
          DeleteProduct(index);
        },
      },
    ]);
  };

  //Handle Delete Product
  const DeleteProduct = async index => {
    await RemoveProductFromCartMethod({
      product_id: cartProducts[index].product_id,
      item_id: cartProducts[index].item_id,
      user,
      getCartResponse,
    });
  };

  //Get Cart API Reponse
  const getCartResponse = async res => {
    console.log('add/delete product API: ', res);
    if (res) {
      console.log('Product edit response: ', res);
      Toast.show(res.message);
      getCart(user.user_id, onGetResponse, setLoading);

      //Updating store
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );

      if (res.cart && res.cart.cart_products) {
        setCartProducts(res.cart.cart_products);
        setCartProducts(prev => [...prev]);
      } else {
        setCartProducts([]);
        setCartProducts(prev => [...prev]);
      }
    }
  };

  //Get Coupon Response API
  const getCouponResponse = async res => {
    console.log('coupon code API: ', res);
    setCouponLoading(false);
    if (res.success) {
      // console.log('Product edit response: '.res);
      Toast.show(res.message);
      getCart(user.user_id, onGetResponse, setLoading);
      //Updating store
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      if (res.cart && res.cart.cart_products) {
        setCartProducts(res.cart.cart_products);
        setCartProducts(prev => [...prev]);
      } else {
        setCartProducts([]);
        setCartProducts(prev => [...prev]);
      }
    } else {
      Toast.show('Invalid coupon code');
    }
  };

  //Get CheckOut Button Press
  const handleCheckout = async () => {
    setDataLoading(true);
    await checkoutCart(user.user_id, getCheckoutResponse, setDataLoading);
  };

  //Get Checkout API Response
  const getCheckoutResponse = async res => {
    console.log('checkout API: ', res);
    if (res) {
      // await store.dispatch(
      //   fetchCartCheckoutData({
      //     data: res,
      //   }),
      // );
      // await store.dispatch(
      //   fetchCartData({
      //     data: 0,
      //   }),
      // );
      navigation.navigate(routes.checkOut);
      setDataLoading(false);
    }
    setDataLoading(false);
  };

  //Get Wish List API Response
  const getWishResponse = async res => {
    // console.log('Wishlist API: ', res);
    if (res) {
      Toast.show(res.message);
    }
  };

  //Product Card Component
  const ProductCard = ({item, image, onPlus, onMinus, onDelete}) => {
    return (
      <View style={[styles.onlyRow, {flex: 1}]}>
        <View
          style={[
            styles.productCardNew,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={styles.onlyRow}>
            <View>
              <CustomIcon
                icon={
                  item.image_url != ''
                    ? {uri: item.image_url}
                    : appImages.placeHolderProduct
                }
                size={totalSize(10)}
              />
            </View>
            <View style={{flex: 1, paddingHorizontal: width(3)}}>
              {Platform.OS == 'ios' ? (
                <SmallText>{item.product}</SmallText>
              ) : (
                <RegularText>{item.product}</RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText>{item.company_name}</SmallText>
              ) : (
                <RegularText>{item.company_name}</RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <TinyText style={{color: colors.textGrey2}}>
                  {item.product_code}
                </TinyText>
              ) : (
                <SmallText style={{color: colors.textGrey2}}>
                  {item.product_code}
                </SmallText>
              )}

              <Spacer height={sizes.TinyMargin} />
              <View style={[styles.onlyRow, {alignItems: 'flex-end'}]}>
                {Platform.OS == 'ios' ? (
                  <SmallText>AED </SmallText>
                ) : (
                  <RegularText>AED </RegularText>
                )}
                {Platform.OS == 'ios' ? (
                  <MediumText>{item.price}</MediumText>
                ) : (
                  <LargeText>{item.price}</LargeText>
                )}
              </View>
              <View
                style={[
                  styles.onlyRow,
                  {
                    marginTop: height(1),
                    justifyContent: 'space-between',
                  },
                ]}>
                <View style={styles.onlyRow}>
                  <TouchableOpacity style={styles.blueButton} onPress={onMinus}>
                    {Platform.OS == 'ios' ? (
                      <RegularText style={appStyles.whiteText}>-</RegularText>
                    ) : (
                      <MediumText style={appStyles.whiteText}>-</MediumText>
                    )}
                  </TouchableOpacity>
                  {Platform.OS == 'ios' ? (
                    <SmallText style={{marginRight: width(2)}}>
                      {item.amount}
                    </SmallText>
                  ) : (
                    <RegularText style={{marginRight: width(2)}}>
                      {item.amount}
                    </RegularText>
                  )}
                  <TouchableOpacity style={styles.blueButton} onPress={onPlus}>
                    {Platform.OS == 'ios' ? (
                      <RegularText style={appStyles.whiteText}>+</RegularText>
                    ) : (
                      <MediumText style={appStyles.whiteText}>+</MediumText>
                    )}
                  </TouchableOpacity>
                </View>
                <TouchableOpacity
                  onPress={async () => {
                    await AddToWishListMethod({
                      item,
                      user,
                      getWishResponse,
                    });
                  }}
                  style={styles.blueButtonLight}>
                  {Platform.OS == 'ios' ? (
                    <TinyText style={{color: colors.gradient1}}>
                      {Translate('Save for later')}
                    </TinyText>
                  ) : (
                    <SmallText style={{color: colors.gradient1}}>
                      {Translate('Save for later')}
                    </SmallText>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <TouchableCustomIcon
          icon={appImages.greyCross}
          size={totalSize(4)}
          style={{marginRight: width(4)}}
          onPress={onDelete}
        />
      </View>
    );
  };

  //Shipping Card Component
  const ShippingCard = ({}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        {LTR ? (
          <View style={{paddingHorizontal: width(3)}}>
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Shipping Charge')} </SmallText>
              ) : (
                <RegularText>{Translate('Shipping Charge')}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>
                  {response && response.cart
                    ? response.cart.shipping_cost
                    : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  {response && response.cart
                    ? response.cart.shipping_cost
                    : '0'}
                </RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Item Total')}</SmallText>
              ) : (
                <RegularText>{Translate('Item Total')}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>
                  AED {response && response.cart ? response.cart.subtotal : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  AED {response && response.cart ? response.cart.subtotal : '0'}
                </RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Tax Total')}</SmallText>
              ) : (
                <RegularText>{Translate('Tax Total')}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>
                  {response && response.cart ? response.cart.tax_subtotal : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  {response && response.cart ? response.cart.tax_subtotal : '0'}
                </RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText style={{color: colors.gradient1}}>
                  {Translate('Coupon Discount')}
                </SmallText>
              ) : (
                <RegularText style={{color: colors.gradient1}}>
                  {Translate('Coupon Discount')}
                </RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText style={{color: colors.gradient1}}>
                  {response &&
                  response.cart &&
                  response.cart.formated_discount_value
                    ? response.cart.formated_discount_value
                    : 'AED 0'}
                </SmallText>
              ) : (
                <RegularText style={{color: colors.gradient1}}>
                  {response &&
                  response.cart &&
                  response.cart.formated_discount_value
                    ? response.cart.formated_discount_value
                    : 'AED 0'}
                </RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin * 1.5} />
            <LineHorizontal height={1} />
            <Spacer height={sizes.TinyMargin * 1.5} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                  {Translate('Total Amount')}
                </RegularText>
              ) : (
                <MediumText style={{fontFamily: fontFamily.appTextBold}}>
                  {Translate('Total Amount')}
                </MediumText>
              )}
              {Platform.OS == 'ios' ? (
                <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                  AED {response && response.cart ? response.cart.total : '0'}
                </RegularText>
              ) : (
                <MediumText style={{fontFamily: fontFamily.appTextBold}}>
                  AED {response && response.cart ? response.cart.total : '0'}
                </MediumText>
              )}
            </View>
          </View>
        ) : (
          <View style={{paddingHorizontal: width(3)}}>
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>
                  {response && response.cart
                    ? response.cart.shipping_cost
                    : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  {response && response.cart
                    ? response.cart.shipping_cost
                    : '0'}
                </RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Shipping Charge')} </SmallText>
              ) : (
                <RegularText>{Translate('Shipping Charge')}</RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>
                  AED {response && response.cart ? response.cart.subtotal : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  AED {response && response.cart ? response.cart.subtotal : '0'}
                </RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Item Total')}</SmallText>
              ) : (
                <RegularText>{Translate('Item Total')}</RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText>
                  {response && response.cart ? response.cart.tax_subtotal : '0'}
                </SmallText>
              ) : (
                <RegularText>
                  {response && response.cart ? response.cart.tax_subtotal : '0'}
                </RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{Translate('Tax Total')}</SmallText>
              ) : (
                <RegularText>{Translate('Tax Total')}</RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <SmallText style={{color: colors.gradient1}}>
                  {response &&
                  response.cart &&
                  response.cart.formated_discount_value
                    ? response.cart.formated_discount_value
                    : 'AED 0'}
                </SmallText>
              ) : (
                <RegularText style={{color: colors.gradient1}}>
                  {response &&
                  response.cart &&
                  response.cart.formated_discount_value
                    ? response.cart.formated_discount_value
                    : 'AED 0'}
                </RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText style={{color: colors.gradient1}}>
                  {Translate('Coupon Discount')}
                </SmallText>
              ) : (
                <RegularText style={{color: colors.gradient1}}>
                  {Translate('Coupon Discount')}
                </RegularText>
              )}
            </View>
            <Spacer height={sizes.TinyMargin * 1.5} />
            <LineHorizontal height={1} />
            <Spacer height={sizes.TinyMargin * 1.5} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                  AED {response && response.cart ? response.cart.total : '0'}
                </RegularText>
              ) : (
                <MediumText style={{fontFamily: fontFamily.appTextBold}}>
                  AED {response && response.cart ? response.cart.total : '0'}
                </MediumText>
              )}
              {Platform.OS == 'ios' ? (
                <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                  {Translate('Total Amount')}
                </RegularText>
              ) : (
                <MediumText style={{fontFamily: fontFamily.appTextBold}}>
                  {Translate('Total Amount')}
                </MediumText>
              )}
            </View>
          </View>
        )}
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          {/* <HeaderSimple
            dontShowBack
            onPress={() => navigation.navigate('Cart')}
          /> */}
          {/* <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                styles.row,
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                },
              ]}>
              <View>
                <MediumText
                  style={[
                    appStyles.whiteText,
                    {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                  ]}>
                  {Translate('Cart')}
                </MediumText>
                <SmallText
                  style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                  {store.getState().cart.data && store.getState().cart.data.data
                    ? store.getState().cart.data.data
                    : 0}{' '}
                  {Translate('Items in your cart')}
                </SmallText>
              </View>
            </View>
          </LinearGradient> */}

          <View
            style={[
              styles.row,
              appStyles.shadowSmall,
              {
                backgroundColor: colors.snow,
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
                justifyContent: LTR ? 'flex-start' : 'flex-end',
                paddingHorizontal: width(4),
              },
            ]}>
            <View style={{alignItems: LTR ? 'flex-start' : 'flex-end'}}>
              <MediumText
                style={[
                  {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                ]}>
                {Translate('Cart')}
              </MediumText>
              <SmallText style={[{marginLeft: width(4)}]}>
                {store.getState().cart.data && store.getState().cart.data.data
                  ? store.getState().cart.data.data
                  : 0}{' '}
                {Translate('Items in your cart')}
              </SmallText>
            </View>
          </View>

          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              <Spacer height={sizes.smallMargin} />
              {/* {user && user.user_id && (
                <SmallText style={{marginLeft: width(5)}}>
                  {user.user_id}
                </SmallText>
              )} */}

              {cartProducts && cartProducts.length ? (
                <FlatList
                  data={cartProducts}
                  extraData={cartProducts}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({index, item}) => (
                    <ProductCard
                      key={index}
                      item={item}
                      image={appImages.product1}
                      onPlus={() => handlePlus(index)}
                      onMinus={() => handleMinus(index)}
                      onDelete={() => handleDelete(index)}
                    />
                  )}
                />
              ) : (
                <RegularText style={{alignSelf: 'center'}}>
                  {Translate('No items in cart')}
                </RegularText>
              )}
              <Spacer height={sizes.baseMargin} />
              <ImageBackground
                source={appImages.coupenBg}
                style={{
                  width: width(94),
                  height: Platform.OS == 'ios' ? height(20) : height(26),
                  alignSelf: 'center',
                }}
                imageStyle={{borderRadius: 5}}>
                <View
                  style={{
                    paddingHorizontal: width(5),
                    paddingVertical: width(4),
                  }}>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={[appStyles.whiteText, appStyles.textBold]}>
                      {Translate('Gift Coupen / Promo Code')}
                    </RegularText>
                  ) : (
                    <MediumText
                      style={[appStyles.whiteText, appStyles.textBold]}>
                      {Translate('Gift Coupen / Promo Code')}
                    </MediumText>
                  )}
                  <Spacer height={sizes.baseMargin} />
                  <View>
                    <TextInput
                      style={styles.textInput}
                      onChangeText={setCouponCode}
                      value={couponCode}
                      placeholderTextColor={colors.textGrey}
                      placeholder={'123'}
                    />
                  </View>
                  <Spacer height={sizes.smallMargin} />

                  <TouchableOpacity
                    onPress={async () => {
                      if (couponCode != '') {
                        setCouponLoading(true);
                        await ApplyCouponCode({
                          coupon_code: couponCode,
                          user,
                          getCartResponse: getCouponResponse,
                        });
                      } else {
                        Toast.show('Enter coupon code first');
                      }
                    }}
                    style={[
                      styles.blueButtonLight,
                      styles.blueButtonRemaining,
                    ]}>
                    {couponLoading ? (
                      <ActivityIndicator color={colors.snow} />
                    ) : Platform.OS == 'ios' ? (
                      <SmallText
                        style={[appStyles.whiteText, appStyles.textBold]}>
                        {Translate('Apply')}
                      </SmallText>
                    ) : (
                      <SmallText
                        style={[appStyles.whiteText, appStyles.textBold]}>
                        {Translate('Apply')}
                      </SmallText>
                    )}
                  </TouchableOpacity>
                  <Spacer height={sizes.TinyMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={[appStyles.whiteText, appStyles.textBold]}>
                      {Translate('Type code')} ...
                    </RegularText>
                  ) : (
                    <MediumText
                      style={[appStyles.whiteText, appStyles.textBold]}>
                      {Translate('Type code')} ...
                    </MediumText>
                  )}
                  {Platform.OS == 'ios' ? (
                    <RegularText style={[appStyles.whiteText]}>
                      {Translate('To get discounts')}
                    </RegularText>
                  ) : (
                    <MediumText style={[appStyles.whiteText]}>
                      {Translate('To get discounts')}
                    </MediumText>
                  )}
                </View>
              </ImageBackground>

              <Spacer height={sizes.smallMargin} />
              <ShippingCard />

              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={[appStyles.textGray, {marginHorizontal: width(5)}]}>
                  {Translate('At') +
                    ' ' +
                    'HOMEDEVO.com' +
                    ' ' +
                    Translate(
                      'the price and availability of items are subject to change',
                    ) +
                    '. ' +
                    Translate(
                      "The Cart is a temporary place to store a list of your items and reflects each item's most recent price",
                    )}
                </RegularText>
              ) : (
                <MediumText
                  style={[appStyles.textGray, {marginHorizontal: width(5)}]}>
                  {Translate('At') +
                    ' ' +
                    'HOMEDEVO.com' +
                    ' ' +
                    Translate(
                      'the price and availability of items are subject to change',
                    ) +
                    '. ' +
                    Translate(
                      "The Cart is a temporary place to store a list of your items and reflects each item's most recent price",
                    )}
                </MediumText>
              )}

              <Spacer height={sizes.baseMargin} />
              {cartProducts && cartProducts.length ? (
                <ButtonColored
                  isLoading={dataLoading}
                  text={Translate('CHECKOUT')}
                  // onPress={() => navigation.navigate(routes.checkOut)}
                  onPress={() => {
                    if (guest) {
                      Alert.alert(
                        Translate('Sign In'),
                        Translate('Sign In as a user to complete order'),
                        [
                          {
                            text: Translate('Cancel'),
                            onPress: () => {
                              console.log('Cancel Pressed');
                            },
                            style: 'cancel',
                          },
                          {
                            text: Translate('Sign In'),
                            onPress: () => {
                              let language = {
                                language: true,
                              };
                              dispatch(fetchLanguage(language));
                              let token = {
                                token: false,
                              };
                              dispatch(fetchToken(token));
                            },
                          },
                        ],
                      );
                    } else {
                      handleCheckout();
                    }
                  }}
                  buttonStyle={{
                    height: Platform.OS == 'ios' ? height(6) : height(7),
                    width: width(92),
                    alignSelf: 'center',
                    borderRadius: sizes.loginRadius,
                  }}
                />
              ) : null}
              {/* {guest ? (
                <ButtonColored
                  isLoading={dataLoading}
                  text={'LOGIN TO VIEW CART'}
                  onPress={async () => {
                    await AsyncStorage.removeItem(storageConst.userData).then(
                      async () => {
                        await AsyncStorage.removeItem(storageConst.userId).then(
                          () => {
                            // RNRestart.Restart();
                            navigation.navigate(routes.signin);
                          },
                        );
                      },
                    );
                  }}
                  // onPress={() => handleCheckout()}
                  buttonStyle={{
                    height: Platform.OS == 'ios' ? height(6) : height(7),
                    width: width(92),
                    alignSelf: 'center',
                    borderRadius: 5,
                  }}
                />
              ) : null} */}
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
      <VersionModal
        isVisible={versionVisible}
        onPress={() => {
          Platform.OS == 'ios'
            ? Linking.openURL(
                'https://apps.apple.com/pk/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328',
              )
            : Linking.openURL(
                'https://play.google.com/store/apps/details?id=com.dwaae.smartapp',
              );
        }}
      />
    </Fragment>
  );
}
