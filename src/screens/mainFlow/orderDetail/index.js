import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  Linking,
  Alert,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
  MediumText,
  LoadingCard,
  LineHorizontal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  sizes,
  Translate,
  storageConst,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast';
import {cancelOrder} from '../../../services/api/cart.api';
import {store} from '../../../Redux/configureStore';
import AsyncStorage from '@react-native-community/async-storage';

export default function OrderDetail({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const [accountList, setAccountList] = useState([]);
  const [paramData, setParamData] = useState(route.params.item);
  const [LTR, setLTR] = useState(true);

  useEffect(async () => {
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    console.log(route.params.item);
    if (route.params.item.products) {
      setAccountList(route.params.item.products);
    }
  }, [route.params.item]);

  //Handle Cancel Order
  const handleCancelOrder = async () => {
    Alert.alert('Cancel', 'Are you sure you want to Cancel Order?', [
      {
        text: 'Cancel',
        onPress: () => {
          console.log('Cancel Pressed');
        },
        style: 'cancel',
      },
      {
        text: 'Confirm',
        onPress: async () => {
          setLoading(true);
          let language = await AsyncStorage.getItem(storageConst.language);
          if (language == null || language == undefined) language = 'en';

          let orderData = {
            order_id: route.params.item.order_id,
            lang_code: language,
            currency_code: 'AED',
            scope: 'cancel_order',
          };
          cancelOrder(
            route.params.item.order_id,
            orderData,
            onGetResponse,
            setLoading,
          );
        },
      },
    ]);
  };

  //On Delete Order API Response
  const onGetResponse = async res => {
    console.log('delete order API: ', res);
    Toast.show(res.message);
    if (res.success) {
      setLoading(false);
      navigation.pop();
    }
    setLoading(false);
  };

  //Product Card Response
  const ProductCard = ({item, onCollapsablePress}) => {
    return (
      <View>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={{paddingHorizontal: width(3)}}>
            {Platform.OS == 'ios' ? (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product}
              </RegularText>
            ) : (
              <MediumText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product}
              </MediumText>
            )}
            <Spacer height={sizes.TinyMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {item.product_code}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {item.product_code}
              </RegularText>
            )}
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <RegularText>{item.subtotal_format}</RegularText>
              ) : (
                <MediumText>{item.subtotal_format}</MediumText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText style={appStyles.blueText}>
                  {item.amount + Translate('Items')}
                </SmallText>
              ) : (
                <RegularText style={appStyles.blueText}>
                  {item.amount + Translate('Items')}
                </RegularText>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  };

  //Comparison Card Response
  const ComparisonCard = ({leftIcon, rightIcon, leftText, rightText}) => {
    return (
      <View
        style={[
          styles.productCardNew,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <TouchableOpacity
          onPress={() => {
            if (LTR) {
              Linking.openURL(
                'whatsapp://send?text=' +
                  'Hello HomeDevo team' +
                  '&phone=+971506333105',
              )
                .then(data => {
                  console.log('WhatsApp Opened successfully ' + data); //<---Success
                })
                .catch(() => {
                  // alert('Make sure WhatsApp installed on your device'); //<---Error
                  Linking.openURL(
                    `sms:&addresses=+971506333105&body=Hello HomeDevo team`,
                  );
                });
            } else {
              Linking.openURL(
                'whatsapp://send?text=' +
                  'HomeDevo مرحبا فريق' +
                  '&phone=+971506333105',
              )
                .then(data => {
                  console.log('WhatsApp Opened successfully ' + data); //<---Success
                })
                .catch(() => {
                  // alert('Make sure WhatsApp installed on your device'); //<---Error
                  Linking.openURL(
                    `sms:&addresses=+971506333105&body=HomeDevo مرحبا فريق`,
                  );
                });
            }
            // Linking.openURL(
            //   'whatsapp://send?text=' + LTR
            //     ? 'Hello Dwaae team.'
            //     : 'DWAAE مرحبا فريق' + '&phone=+971506333105',
            // )
            //   .then(data => {
            //     console.log('WhatsApp Opened successfully ' + data); //<---Success
            //   })
            //   .catch(() => {
            //     // alert('Make sure WhatsApp installed on your device'); //<---Error
            //     Linking.openURL(
            //       LTR
            //         ? `sms:&addresses=+971506333105&body=Hello Dwaae team`
            //         : `sms:&addresses=+971506333105&body=DWAAE مرحبا فريق`,
            //     );
            //   });
          }}
          style={[
            styles.textContainer,
            {
              borderRightWidth: 1,
              borderColor: colors.borderLightColor,
            },
          ]}>
          <TouchableCustomIcon
            icon={leftIcon}
            size={totalSize(2)}
            style={{marginRight: width(2)}}
          />
          {Platform.OS == 'ios' ? (
            <RegularText>{leftText}</RegularText>
          ) : (
            <MediumText>{leftText}</MediumText>
          )}
        </TouchableOpacity>
        <TouchableOpacity style={styles.textContainer}>
          <TouchableCustomIcon
            icon={rightIcon}
            size={totalSize(2)}
            style={{marginRight: width(2)}}
          />
          {Platform.OS == 'ios' ? (
            <RegularText>{rightText}</RegularText>
          ) : (
            <MediumText>{rightText}</MediumText>
          )}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />

      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLight}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                styles.row,
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                },
              ]}>
              <View>
                <MediumText
                  style={[
                    appStyles.whiteText,
                    {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                  ]}>
                  {Translate('Order')} #{paramData.order_id}
                </MediumText>
                <SmallText
                  style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                  {paramData.timestamp}
                </SmallText>
              </View>
              <View style={[styles.onlyRow, {marginRight: width(4)}]}>
                {/* <TouchableCustomIcon
                icon={appImages.mail}
                size={totalSize(2.5)}
              />
              <TouchableCustomIcon
                icon={appImages.printWhite}
                size={totalSize(2.5)}
                style={{marginLeft: width(3.5)}}
              /> */}

                {/* <TouchableCustomIcon
                  icon={appImages.pdf}
                  size={totalSize(2.5)}
                  style={{marginLeft: width(3.5)}}
                /> */}
                <View />
              </View>
            </View>
          </LinearGradient>
          <View
            style={[
              appStyles.center,
              {
                width: width(100),
                paddingVertical: height(0.7),
                backgroundColor: colors.bgDarkGrey,
              },
            ]}>
            <SmallText style={[{color: colors.statusBarColor}]}>
              {Translate('Status')}: {paramData.status_description}
            </SmallText>
          </View>
          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              <FlatList
                data={accountList}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                numColumns={1}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <ProductCard key={index} item={item} />
                )}
              />

              {/* <ComparisonCard
                leftText={Translate('Need Help?')}
                rightText={Translate('View Prescription')}
                leftIcon={appImages.needHelp}
                rightIcon={appImages.uploadWhite}
              /> */}

              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Details')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Details')}
                </MediumText>
              )}

              <View
                style={[
                  styles.productCard,
                  Platform.OS == 'ios' && appStyles.shadowSmall,
                ]}>
                <View style={{paddingHorizontal: width(3)}}>
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Shipping Charge')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Shipping Charge')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>{paramData.shipping_cost}</SmallText>
                    ) : (
                      <RegularText>{paramData.shipping_cost}</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Item Total')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Item Total')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>{paramData.subtotal_format} </SmallText>
                    ) : (
                      <RegularText>{paramData.subtotal_format}</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText>{Translate('Coupon Discount')}</SmallText>
                    ) : (
                      <RegularText>{Translate('Coupon Discount')}</RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText>00.00</SmallText>
                    ) : (
                      <RegularText>00.00</RegularText>
                    )}
                  </View>
                  <Spacer height={sizes.TinyMargin * 1.5} />
                  <LineHorizontal height={1} />
                  <Spacer height={sizes.TinyMargin * 1.5} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {paramData.total_format}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {paramData.total_format}
                      </RegularText>
                    )}
                  </View>
                </View>
              </View>
              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Shipping Address')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Shipping Address')}
                </MediumText>
              )}

              <View
                style={[
                  styles.productCard,
                  Platform.OS == 'ios' && appStyles.shadowSmall,
                ]}>
                <View style={[styles.row, {paddingHorizontal: width(3)}]}>
                  <View style={{flex: 1}}>
                    {Platform.OS == 'ios' ? (
                      <SmallText style={appStyles.textGray}>
                        {paramData.s_address}
                      </SmallText>
                    ) : (
                      <RegularText style={appStyles.textGray}>
                        {paramData.s_address}
                      </RegularText>
                    )}
                    <Spacer height={sizes.TinyMargin} />
                    {Platform.OS == 'ios' ? (
                      <SmallText style={appStyles.textGray}>
                        {paramData.s_city}
                      </SmallText>
                    ) : (
                      <RegularText style={appStyles.textGray}>
                        {paramData.s_city}
                      </RegularText>
                    )}
                    {/* <Spacer height={sizes.TinyMargin} />
                  {Platform.OS == 'ios' ? (
                    <SmallText style={appStyles.textGray}>
                      P.O.Box 26291
                    </SmallText>
                  ) : (
                    <RegularText style={appStyles.textGray}>
                      P.O.Box 26291
                    </RegularText>
                  )} */}
                    <Spacer height={sizes.TinyMargin} />
                    {Platform.OS == 'ios' ? (
                      <SmallText style={appStyles.textGray}>
                        {paramData.s_country_descr}
                      </SmallText>
                    ) : (
                      <RegularText style={appStyles.textGray}>
                        {paramData.s_country_descr}
                      </RegularText>
                    )}
                    <Spacer height={sizes.TinyMargin} />
                  </View>

                  <CustomIcon icon={appImages.suitcase} size={totalSize(4)} />
                </View>
              </View>
              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  Payment Method
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  Payment Method
                </MediumText>
              )}

              <View
                style={[
                  styles.productCard,
                  Platform.OS == 'ios' && appStyles.shadowSmall,
                ]}>
                <View
                  style={[
                    styles.onlyRow,
                    {paddingHorizontal: width(3), paddingVertical: height(0.5)},
                  ]}>
                  <CustomIcon icon={appImages.product} size={totalSize(2.5)} />
                  {Platform.OS == 'ios' ? (
                    <SmallText
                      style={[appStyles.textGray, {marginLeft: width(3)}]}>
                      {Translate('Cash On Delivery')}
                    </SmallText>
                  ) : (
                    <RegularText
                      style={[appStyles.textGray, {marginLeft: width(3)}]}>
                      {Translate('Cash On Delivery')}
                    </RegularText>
                  )}
                </View>
              </View>

              <Spacer height={sizes.doubleBaseMargin} />
              {route.params.item && route.params.item.allow_to_cancel ? (
                <>
                  <ButtonColored
                    onPress={handleCancelOrder}
                    text={Translate('CANCEL ORDER')}
                    buttonStyle={{
                      height: Platform.OS == 'ios' ? height(6) : height(7),
                      width: width(92),
                      alignSelf: 'center',
                      borderRadius: 5,
                    }}
                  />
                  <Spacer height={sizes.smallMargin} />
                </>
              ) : null}
              <ButtonColored
                onPress={() => navigation.navigate('Home')}
                text={Translate('CONTINUE SHOPPING')}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(92),
                  alignSelf: 'center',
                  borderRadius: 5,
                }}
              />
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
