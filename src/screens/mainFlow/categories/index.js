import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  FlatList,
  Linking,
} from 'react-native';
import styles from './styles';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  CategoriesLeftImage,
  ImageSideCategoriesRight,
  RegularText,
  LoadingCard,
  VersionModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  storageConst,
  sizes,
  Translate,
} from '../../../services';

/* Api Calls */
import {getHomeData, getMainCategories} from '../../../services/api/home.api';
import {addToCart} from '../../../services/api/cart.api';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchCategoryId, fetchFilterData} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

export default function Categories({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [categoryData, setCategoryData] = useState([
    // {img: appImages.c1},
    // {img: appImages.c2},
    // {img: appImages.c3},
    // {img: appImages.c4},
    // {img: appImages.c5},
    // {img: appImages.c6},
    // {img: appImages.c7},
    // {img: appImages.c8},
  ]);
  const [categoryData1, setCategoryData1] = useState([
    // {img: appImages.c9},
  ]);
  const [brandData, setBrandData] = useState([
    {img: appImages.p1},
    {img: appImages.p2},
    {img: appImages.p3},
    {img: appImages.p4},
  ]);
  const [listData, setListData] = useState([
    {
      name: 'Oral Care',
    },
    {
      name: 'Lips Care',
    },
    {
      name: 'Eye Care',
    },
    {
      name: 'Nail Care',
    },
    {
      name: 'Men Care',
    },
    {
      name: 'Women Care',
    },
  ]);
  const [response, setResponse] = useState([]);
  const [searchText, setSearchText] = useState('');
  const dispatch = useDispatch();
  const [LTR, setLTR] = useState(true);
  const [versionVisible, setVersionVisible] = useState(false);
  const user = store.getState().user.user.user;

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getMainCategories(getHomeResponse, setLoading);
    getHomeData(user.user_id, getVersionHomeResponse, setLoading);
  }, [user]);

  const getVersionHomeResponse = async res => {
    if (res) {
      if (res.api_version && sizes.current_version) {
        if (res.api_version != sizes.current_version) {
          console.log(res.api_version);
          setVersionVisible(true);
        } else {
          setVersionVisible(false);
        }
      }
    }
  };

  const getHomeResponse = async res => {
    console.log('Categories Main API: ', res);
    // setLoading(false);
    if (res) {
      setResponse(res);
      setCategoryData(res.categories);
      if (
        res.categories &&
        res.categories.length &&
        res.categories[0].subcategories
      )
        setCategoryData1(res.categories[0].subcategories);
      setLoading(false);
    }
    setLoading(false);
  };

  const SearchFilterFunction = text => {
    // categoryData, categoryData1
    if (text != '') {
      //setCategoryData
      const CategoryFilterData = categoryData.filter(function (item) {
        const itemData = item.category
          ? item.category.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setCategoryData(CategoryFilterData);

      //setCategoryData1
      const productMedication = categoryData1.filter(function (item) {
        const itemData = item.category
          ? item.category.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setCategoryData1(productMedication);
    } else {
      removeFilter();
    }
  };

  const removeFilter = () => {
    let res = response;
    // setResponse(res);
    setCategoryData(res.categories);
    if (
      res.categories &&
      res.categories.length &&
      res.categories[0].subcategories
    )
      setCategoryData1(res.categories[0].subcategories);
    // setLoading(false);
    // }
  };

  const EmptyPlaceholderText = ({text}) => {
    return (
      // <View style={{backgroundColor: 'red'}}>
      <RegularText
        style={{alignSelf: 'center', marginVertical: sizes.doubleBaseMargin}}>
        {text ? text : 'No data found'}
      </RegularText>
      // </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          {/* <KeyboardAwareScrollView> */}
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.doubleBaseMargin
            }
          />
          {LTR ? (
            <View
              style={[
                styles.mainViewContainer,
                {width: width(100), flexDirection: 'row'},
              ]}>
              <View
                style={{
                  flex: 2.5,
                }}>
                <FlatList
                  data={categoryData}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({item}) => (
                    <CategoriesLeftImage
                      onPress={() => {
                        if (item.subcategories) {
                          setCategoryData1([]);
                          setTimeout(() => {
                            let tempSubCategories = item.subcategories;
                            setCategoryData1(tempSubCategories);
                          }, 300);

                          // setCategoryData1(item.subcategories);
                          // setCategoryData1(prev => [...prev]);
                        } else {
                          setCategoryData1([]);
                        }
                      }}
                      source={
                        item.image_url
                          ? {uri: item.image_url}
                          : appImages.placeHolderProduct
                      }
                      item={item}
                      animation={'fadeInLeft'}
                    />
                  )}
                />
              </View>
              <View
                style={{
                  flex: 7.5,
                  // paddingRight: width(5),
                }}>
                <View>
                  {categoryData1 ? (
                    <FlatList
                      data={categoryData1}
                      contentContainerStyle={{
                        justifyContent: 'space-between',
                      }}
                      numColumns={2}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={(contact, index) => String(index)}
                      renderItem={({index, item}) => (
                        <ImageSideCategoriesRight
                          onPress={async () => {
                            // store.dispatch(
                            //   fetchCategoryId({
                            //     categoryId: item.category_id,
                            //   }),
                            // );
                            let categoryId = {
                              categoryId: item.category_id,
                            };
                            dispatch(fetchCategoryId(categoryId));
                            await store.dispatch(
                              fetchFilterData({
                                data: {},
                              }),
                            );
                            navigation.navigate(routes.subCategories);
                          }}
                          source={
                            item.image_url
                              ? {uri: item.image_url}
                              : appImages.placeHolderProduct
                          }
                          item={item}
                          animation={'fadeInLeft'}
                        />
                      )}
                    />
                  ) : (
                    <EmptyPlaceholderText />
                  )}
                </View>
                <Spacer
                  height={
                    Platform.OS == 'ios'
                      ? sizes.baseMargin * 2
                      : sizes.baseMargin * 2
                  }
                />
              </View>
            </View>
          ) : (
            <View
              style={[
                styles.mainViewContainer,
                {width: width(100), flexDirection: 'row'},
              ]}>
              <View
                style={{
                  flex: 7.5,
                  // paddingRight: width(5),
                }}>
                <View>
                  {categoryData1 ? (
                    <FlatList
                      data={categoryData1}
                      contentContainerStyle={{
                        justifyContent: 'space-between',
                      }}
                      numColumns={2}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={(contact, index) => String(index)}
                      renderItem={({index, item}) => (
                        <ImageSideCategoriesRight
                          onPress={async () => {
                            // store.dispatch(
                            //   fetchCategoryId({
                            //     categoryId: item.category_id,
                            //   }),
                            // );
                            // navigation.navigate(routes.subCategories);
                            let categoryId = {
                              categoryId: item.category_id,
                            };
                            dispatch(fetchCategoryId(categoryId));
                            await store.dispatch(
                              fetchFilterData({
                                data: {},
                              }),
                            );
                            navigation.navigate(routes.subCategories);
                          }}
                          source={
                            item.image_url
                              ? {uri: item.image_url}
                              : appImages.placeHolderProduct
                          }
                          item={item}
                          animation={'fadeInLeft'}
                        />
                      )}
                    />
                  ) : (
                    <EmptyPlaceholderText />
                  )}
                </View>
                <Spacer
                  height={
                    Platform.OS == 'ios'
                      ? sizes.baseMargin * 2
                      : sizes.baseMargin * 2
                  }
                />
              </View>

              <View
                style={{
                  flex: 2.5,
                }}>
                <FlatList
                  data={categoryData}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({item}) => (
                    <CategoriesLeftImage
                      onPress={() => {
                        if (item.subcategories) {
                          setCategoryData1([]);
                          setTimeout(() => {
                            let tempSubCategories = item.subcategories;
                            setCategoryData1(tempSubCategories);
                          }, 300);

                          // setCategoryData1(item.subcategories);
                          // setCategoryData1(prev => [...prev]);
                        } else {
                          setCategoryData1([]);
                        }
                      }}
                      source={
                        item.image_url
                          ? {uri: item.image_url}
                          : appImages.placeHolderProduct
                      }
                      item={item}
                      animation={'fadeInLeft'}
                    />
                  )}
                />
              </View>
            </View>
          )}

          <Spacer height={sizes.doubleBaseMargin} />
          {/* </KeyboardAwareScrollView> */}
        </SafeAreaView>
      )}
      <VersionModal
        isVisible={versionVisible}
        onPress={() => {
          Platform.OS == 'ios'
            ? Linking.openURL(
                'https://apps.apple.com/pk/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328',
              )
            : Linking.openURL(
                'https://play.google.com/store/apps/details?id=com.dwaae.smartapp',
              );
        }}
      />
    </Fragment>
  );
}
