import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  HomeItemCard,
  Wrapper,
  TinierTitle,
  TinyText,
  HeaderSimple,
  FilterProductsModal,
  SliderImages,
  TinyTitle,
  RegularText,
  LineHorizontal,
  MediumText,
  LargeText,
  CustomIcon,
  LoadingCard,
  BottomTabBar,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {getProductDetail} from '../../../services/api/home.api';
import {storageConst} from '../../../services';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchCartData, fetchUser} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  ATCMultipleProductsMethod,
  Translate,
} from '../../../services/helpingMethods';
import {
  AddToCartMethod,
  AddToWishListMethod,
  RemoveProductFromWishListMethod,
  RemoveProductFromWLMethod,
} from '../../../services/helpingMethods';
import {Icon} from 'react-native-elements';
import {getWishList} from '../../../services/api/cart.api';

export default function ProductDetail({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);

  const [wisheAdded, setWisheAdded] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [cartLoading, setCartLoading] = useState(false);
  const [productId, setProductsid] = useState(
    route.params.productId ? route.params.productId : '',
  );
  const [categoryData1, setCategoryData1] = useState([
    {img: appImages.c9},
    {img: appImages.c10},
    {img: appImages.c11},
  ]);

  // console.log('Route : ', route.params.productId);
  const [listData, setListData] = useState([
    {
      chart: appImages.chart,
      like: appImages.like,
      titl1: 'Candes Eclat Fresh',
      title2: 'Lightening Face Tonic',
      quantity: '200 mL',
      price: '69.00',
      img: appImages.product1,
    },
  ]);
  const [productData, setProductData] = useState([
    // {
    //   chart: appImages.chart,
    //   like: appImages.like,
    //   titl1: 'Candes Eclat Fresh',
    //   title2: 'Lightening Face Tonic',
    //   quantity: '200 mL',
    //   price: '69.00',
    //   img: appImages.product1,
    // },
  ]);

  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const homeData = useSelector(state => state.data);
  const [response, setResponse] = useState(null);
  const [product, setProduct] = useState(null);
  const [wishListData, setWishListData] = useState([]);
  const [wishLoading, setwishLoading] = useState(true);
  const [pQuantity, setPQuantity] = useState(0);
  const [LTR, setLTR] = useState(true);

  useEffect(async () => {
    console.log('product ID in productdetail: ', productId, user.user_id);
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getProductDetail(productId, user.user_id, onGetResponse, setLoading);
    // getProductDetail('48825', user.user_id, onGetResponse, setLoading);
    getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
    setPQuantity(
      store.getState().cart.data && store.getState().cart.data.data
        ? store.getState().cart.data.data
        : 0,
    );
  }, [user, productId]);

  useEffect(() => {
    computeLikes();
  }, [productData, wishListData]);

  useEffect(() => {
    checkLike();
  }, [product, wishListData]);

  //Get Wish List API Response
  const onGetWishlistResponse = async res => {
    console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) setWishListData(res.products);
      setwishLoading(false);
    }
    setwishLoading(false);
  };

  //Product Detail API Response
  const onGetResponse = async res => {
    console.log('Product Detail API: ', res);

    if (res) {
      setProduct(res.product);

      let likeList = res.related_products;
      if (likeList)
        likeList.forEach(element => {
          element.liked = false;
        });

      setProductData(likeList);
      // setProductData(res.related_products);
      setLoading(false);
    }
    setLoading(false);
  };

  //Compute Likes according to Wish List
  const computeLikes = async () => {
    if (wishListData.length) {
      if (productData.length) {
        let newProductMedicationData = productData;
        for (let i = 0; i < newProductMedicationData.length; i++) {
          for (let j = 0; j < wishListData.length; j++) {
            if (
              newProductMedicationData[i].product_id ==
              wishListData[j].product_id
            ) {
              newProductMedicationData[i].liked = true;
            }
          }
        }
        setProductData(newProductMedicationData);
      }
    }
  };

  //Check Likes
  const checkLike = async () => {
    if (wishListData.length && product != null) {
      for (let i = 0; i < wishListData.length; i++) {
        if (wishListData[i].product_id == productId) {
          let newProduct = product;
          newProduct.liked = true;
          setProduct(newProduct);
          break;
        }
      }
    }
  };

  //Handle Add To Cart Press
  const handleAddToCart = async () => {
    setDataLoading(true);
    console.log(product);
    // if (user.user_id == '0') {
    //   navigation.navigate(routes.signin);
    // } else {
    await ATCMultipleProductsMethod({
      product_id: product.product_id,
      item_id: product.item_id,
      amount: quantity,
      user,
      getCartResponse,
    });
    // }
  };

  //Add Product To Cart API Response
  const getCartResponse = async res => {
    console.log('add product API: ', res);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        if (user.user_id == '0') {
          let updatedUser = user;
          user.user_id = res.guest_id;
          console.log('Previous user: ', user);
          console.log('Updated user: ', updatedUser);
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      setPQuantity(res.total_products);
      setDataLoading(false);
    }

    setDataLoading(false);
  };

  //Handle Add To Cart Response
  const getAddToCartResponse = async res => {
    // console.log('Cart API: ', res);
    // setLoading(false);
    setCartLoading(false);
    // Toast.show(res.message);
    if (res) {
      Toast.show(res.message);
      if (res.success && user.user_id == '0') {
        let updatedUser = user;
        user.user_id = res.guest_id;
        await store.dispatch(
          fetchUser({
            user: updatedUser,
          }),
        );
        await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
        await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
        await AsyncStorage.setItem(
          storageConst.userData,
          JSON.stringify(updatedUser),
        );
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      setPQuantity(res.total_products);
    }
  };

  //Get Wish List API Response
  const getWishResponse = async res => {
    // console.log('Wishlist API: ', res);
    if (res) {
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  //Get Wish List Product Detail Response
  const getWishResponseProductDetail = async res => {
    console.log('Wishlist API: ', res);
    if (res) {
      // if (
      //   wisheAdded &&
      //   res.message != 'This product is already in the wish list'
      // )
      // setWisheAdded(!wisheAdded);
      setWisheAdded(true);
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  //Handle Quantity
  const HandleQuantity = val => {
    if (val) {
      setQuantity(quantity + 1);
      return;
    } else {
      if (quantity > 1) {
        setQuantity(quantity - 1);
      }
    }
  };

  //Heading Container Component
  const HeadingContainer = props => {
    return (
      <View style={styles.titleRowContainer}>
        {Platform.OS == 'ios' ? (
          <TinierTitle style={props.style}>{props.title}</TinierTitle>
        ) : (
          <TinyTitle style={[props.style, {fontWeight: 'bold'}]}>
            Categories
          </TinyTitle>
        )}

        {props.viewAll ? null : (
          // <TouchableOpacity
          //   style={styles.viewAllButton}
          //   // onPress={() => navigation.navigate(props.route)}
          // >
          //   {Platform.OS == 'ios' ? (
          //     <SmallText>View All</SmallText>
          //   ) : (
          //     <RegularText>View All</RegularText>
          //   )}
          // </TouchableOpacity>
          <View style={styles.simpleRow}>
            <TouchableOpacity onPress={UpButtonHandler}>
              <CustomIcon
                icon={appImages.arrowLeft}
                size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[{marginLeft: totalSize(1.5)}]}
              onPress={DownButtonHandler}>
              <CustomIcon
                icon={appImages.arrowRight}
                size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          <HeaderSimple
            showSearch
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
            quantity={pQuantity}
          />
          <KeyboardAwareScrollView>
            <Wrapper style={[styles.productContainer, appStyles.shadow]}>
              <SliderImages
                images={
                  product.image_url != ''
                    ? [product.image_url]
                    : [appImages.placeHolderProductDetail]
                }
              />
              <View
                style={{position: 'absolute', right: width(4), top: height(2)}}>
                <Icon
                  name={'heart'}
                  type={'font-awesome'}
                  size={totalSize(3)}
                  onPress={async () => {
                    console.log(product);
                    // await AddToWishListMethod({
                    //   item: product,
                    //   user,
                    //   getWishResponse: getWishResponseProductDetail,
                    // });

                    let newList = product;
                    if (!newList.liked) {
                      await AddToWishListMethod({
                        item: product,
                        user,
                        getWishResponse: getWishResponseProductDetail,
                      });
                      newList.liked = true;
                      // setProduct(newList);
                      // setProduct(prev => [...prev]);
                    } else {
                      // await RemoveProductFromWishListMethod({
                      //   item_id: product.product_id,
                      //   getWishResponse: getWishResponseProductDetail,
                      // });
                      await RemoveProductFromWLMethod({
                        user_id: user.user_id,
                        product_id: product.product_id,
                        getWishResponse,
                      });
                      newList.liked = false;
                      // setProduct(newList);
                    }
                    setProduct(newList);
                    setProduct({...newList});
                  }}
                  color={product.liked ? 'red' : null}
                />
              </View>
            </Wrapper>
            <Spacer height={sizes.baseMargin * 1.3} />
            <View
              style={[
                styles.mainViewContainer,
                appStyles.spaceBetween,
                {
                  width: width(100),
                  paddingHorizontal: width(4),
                  alignItems: LTR ? 'flex-start' : 'flex-end',
                },
              ]}>
              <TinyTitle>{product != null ? product.product : ''}</TinyTitle>
              {/* <TinyTitle>Tonic 200 mL</TinyTitle> */}
              <Spacer height={sizes.TinyMargin} />
              <SmallText style={{color: colors.activeBottomIcon}}>
                {'By ' + product != null ? product.company_name : ''}
              </SmallText>
              <Spacer height={sizes.TinyMargin} />
              {/* <View style={[styles.row]}> */}
              <View
                style={[
                  styles.onlyRow,
                  {
                    alignItems: LTR ? 'flex-start' : 'flex-end',
                  },
                ]}>
                {/* <RegularText>AED</RegularText> */}
                <TinyTitle style={{marginLeft: width(1.2)}}>
                  {product != null ? product.format_price : 'AED 00.00'}
                </TinyTitle>
                {product.list_price > product.price ? (
                  <SmallText
                    style={{
                      color: colors.textGrey1,
                      textDecorationLine: 'line-through',
                      marginLeft: width(1.2),
                    }}>
                    {product.price}
                  </SmallText>
                ) : null}
              </View>
              {/* {product.list_price > product.price ? (
                  <View style={styles.discountTag}>
                    <SmallText>50% OFF</SmallText>
                  </View>
                ) : null} */}
              {/* </View> */}
            </View>
            <Spacer height={sizes.smallMargin} />
            <LineHorizontal />
            <Spacer height={sizes.smallMargin} />
            <View
              style={[
                styles.mainViewContainer,
                appStyles.spaceBetween,
                {width: width(100), paddingHorizontal: width(4)},
              ]}>
              <View style={{alignItems: LTR ? 'flex-start' : 'flex-end'}}>
                <TinyTitle>{Translate('Details')}</TinyTitle>
                <Spacer height={sizes.TinyMargin} />
                <RegularText>{product.otc_description}</RegularText>
              </View>

              <Spacer height={sizes.TinyMargin} />
              <View style={{flex: 1}}>
                {product.header_features && product.header_features.length
                  ? product.header_features.map((item, index) => {
                      return (
                        <View
                          style={[
                            styles.horizontalContainer,
                            {
                              borderBottomWidth:
                                index == product.header_features.length - 1
                                  ? 0
                                  : 1,
                            },
                          ]}>
                          {LTR ? (
                            <View
                              style={{
                                flexDirection: 'row',
                              }}>
                              <View style={{flex: 4}}>
                                <RegularText
                                  style={{
                                    fontWeight: 'bold',
                                    fontFamily: fontFamily.appTextMedium,
                                  }}>
                                  {item.name}{' '}
                                </RegularText>
                              </View>
                              <View style={{flex: 6}}>
                                <RegularText> {item.value}</RegularText>
                              </View>
                            </View>
                          ) : (
                            <View
                              style={{
                                // alignItems: 'center',
                                flexDirection: 'row',
                              }}>
                              <View style={{flex: 6}}>
                                <RegularText> {item.value}</RegularText>
                              </View>
                              <View style={{flex: 4}}>
                                <RegularText
                                  style={{
                                    fontWeight: 'bold',
                                    fontFamily: fontFamily.appTextMedium,
                                    alignSelf: 'flex-end',
                                  }}>
                                  {item.name}{' '}
                                </RegularText>
                              </View>
                            </View>
                          )}
                        </View>
                      );
                    })
                  : null}
              </View>
              <Spacer height={sizes.smallMargin} />
              <LineHorizontal />
              <Spacer height={sizes.smallMargin} />
              <View style={{alignItems: LTR ? 'flex-start' : 'flex-end'}}>
                <TinyTitle>{Translate('Features')}</TinyTitle>
              </View>
              <View style={{flex: 1}}>
                {product.product_features && product.product_features.length
                  ? product.product_features.map((item, index) => {
                      return (
                        <View
                          style={[
                            styles.horizontalContainer,
                            {
                              borderBottomWidth:
                                index == product.product_features.length - 1
                                  ? 0
                                  : 1,
                            },
                          ]}
                          key={index}>
                          {LTR ? (
                            <View
                              style={{
                                flexDirection: 'row',
                              }}>
                              <View style={{flex: 4}}>
                                <RegularText
                                  style={{
                                    marginLeft: width(2),
                                    marginRight: width(1),
                                  }}>
                                  {item.name}
                                </RegularText>
                              </View>
                              <View style={{flex: 6}}>
                                <SmallText>{item.value}</SmallText>
                              </View>
                            </View>
                          ) : (
                            <View
                              style={{
                                flexDirection: 'row',
                              }}>
                              <View style={{flex: 6}}>
                                <SmallText>{item.value}</SmallText>
                              </View>
                              <View style={{flex: 4}}>
                                <RegularText
                                  style={{
                                    marginLeft: width(2),
                                    marginRight: width(1),
                                    alignSelf: 'flex-end',
                                  }}>
                                  {item.name}
                                </RegularText>
                              </View>
                            </View>
                          )}
                        </View>
                      );
                    })
                  : null}
              </View>
            </View>
            <Spacer height={sizes.smallMargin} />
            <LineHorizontal />
            <Spacer height={sizes.smallMargin} />
            <View
              style={[
                styles.onlyRow,
                {
                  paddingHorizontal: width(4),
                  justifyContent: LTR ? 'flex-start' : 'flex-end',
                },
              ]}>
              <RegularText
                style={{
                  fontWeight: 'bold',
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {Translate('Stock')}
                {': '}
              </RegularText>
              <RegularText
                style={{
                  color:
                    product.amount != 0
                      ? colors.activeBottomIcon
                      : colors.error,
                }}>
                {' '}
                {product.amount != 0
                  ? Translate('Available')
                  : Translate('Not Available')}
              </RegularText>
            </View>
            <Spacer height={sizes.smallMargin} />
            <View
              style={[
                styles.row,
                {
                  width: width(100),
                  alignSelf: 'center',
                  paddingHorizontal: width(4),
                },
              ]}>
              <View
                style={[
                  appStyles.center,
                  styles.row,
                  styles.leftBlue,
                  {paddingHorizontal: width(3)},
                ]}>
                <TinyText style={{color: colors.activeBottomIcon}}>
                  QTY:{' '}
                </TinyText>
                <TouchableOpacity onPress={() => HandleQuantity(false)}>
                  <LargeText style={{color: colors.textGrey}}>-</LargeText>
                </TouchableOpacity>
                <SmallText style={{color: colors.textGrey}}>
                  {quantity}
                </SmallText>
                <TouchableOpacity onPress={() => HandleQuantity(true)}>
                  <LargeText style={{color: colors.textGrey}}>+</LargeText>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => handleAddToCart()}
                style={[appStyles.center, styles.rightBlue]}>
                {dataLoading ? (
                  <ActivityIndicator size={'small'} color={colors.snow} />
                ) : (
                  <MediumText
                    style={[
                      appStyles.whiteText,
                      {fontFamily: fontFamily.appTextMedium},
                    ]}>
                    {Translate('ADD TO CART')}
                  </MediumText>
                )}
              </TouchableOpacity>
            </View>
            <Spacer height={sizes.smallMargin} />
            <LineHorizontal />
            <Spacer height={sizes.baseMargin} />
            <View style={[styles.container, styles.center]}>
              <View style={styles.onlyRow}>
                <CustomIcon icon={appImages.medicine} size={totalSize(2.5)} />
                <SmallText style={{marginLeft: width(2)}}>
                  {Translate('See other seller items also')}
                </SmallText>
              </View>
            </View>
            <Spacer height={sizes.baseMargin} />
            <LineHorizontal />
            <Spacer height={sizes.smallMargin} />

            <View
              style={[
                styles.parentColorContainer,
                {
                  backgroundColor: colors.greyBg,
                  borderWidth: 0,
                },
              ]}>
              <View style={[styles.categoryMainContainer]}>
                <Spacer height={sizes.baseMargin} />
                <View style={{alignItems: LTR ? 'flex-start' : 'flex-end'}}>
                  <HeadingContainer
                    title={Translate('Frequently Bought Together')}
                    viewAll
                  />
                </View>
                <Spacer height={sizes.smallMargin} />
                <FlatList
                  data={productData}
                  horizontal
                  keyExtractor={(contact, index) => String(index)}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({index, item}) => (
                    <HomeItemCard
                      item={item}
                      LTR={LTR}
                      onPress={async () => {
                        setLoading(true);
                        getProductDetail(
                          item.product_id,
                          user.user_id,
                          onGetResponse,
                          setLoading,
                        );
                        // navigation.navigate(routes.productDetail, {
                        //   productId: item.product_id,
                        // })
                      }}
                      addToCartPress={async () => {
                        // if (user.user_id == '0') {
                        //   navigation.navigate(routes.signin);
                        // } else {
                        if (!cartLoading) {
                          setCartLoading(true);
                          await AddToCartMethod({
                            item,
                            user,
                            getCartResponse: getAddToCartResponse,
                          });
                        }
                        // }
                      }}
                      onWishPress={async () => {
                        let newList = productData;
                        if (!newList[index].liked) {
                          await AddToWishListMethod({
                            item,
                            user,
                            getWishResponse,
                          });
                          newList[index].liked = true;
                          setProductData(newList);
                          setProductData(prev => [...prev]);
                        } else {
                          await RemoveProductFromWLMethod({
                            user_id: user.user_id,
                            product_id: item.product_id,
                            getWishResponse,
                          });
                          newList[index].liked = false;
                          setProductData(newList);
                          setProductData(prev => [...prev]);
                        }
                      }}
                    />
                  )}
                />
                <Spacer
                  height={
                    Platform.OS == 'ios'
                      ? sizes.doubleBaseMargin * 1.5
                      : sizes.doubleBaseMargin * 2.3
                  }
                />
              </View>
            </View>

            <FilterProductsModal
              isVisible={isVisible}
              toggleModal={() => setIsVisible(false)}
            />
          </KeyboardAwareScrollView>
          {cartLoading ? (
            <View
              style={{
                position: 'absolute',
                bottom: height(30),
                borderRadius: 5,
                width: totalSize(5),
                height: totalSize(5),
                backgroundColor: 'rgba(255,255,255,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
          <BottomTabBar
            quantity={
              store.getState().cart.data && store.getState().cart.data.data
                ? store.getState().cart.data.data
                : 0
            }
            onPress={index => {
              if (index == 1) {
                navigation.navigate('Home');
              } else if (index == 2) {
                navigation.navigate('Categories');
              } else if (index == 3) {
                navigation.navigate('Services');
              } else if (index == 4) {
                navigation.navigate('Account');
              } else if (index == 5) {
                navigation.navigate('Cart');
              }
            }}
          />
        </SafeAreaView>
      )}
    </Fragment>
  );
}
