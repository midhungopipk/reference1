import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SearchBarHome,
  CustomIcon,
  SmallText,
  TextInputColored,
  RegularText,
  CheckBoxPrimary,
  TouchableCustomIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function MapVipTestHome({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [check, setCheck] = useState(true);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [lastLat, setLastLat] = useState('51.5347');
  const [lastLong, setLastLong] = useState('0.1246');
  const [test, setTest] = useState(1);
  const mapRef = useRef(null);

  useEffect(() => {
    // GetCurrentPosition();
  }, []);

  const GetCurrentPosition = async () => {
    let newPosition = {
      latitude: 51.5347,
      longitude: 0.1246,
    };
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                console.log(error.message);
                // Alert.alert(error.message);
              }
              break;
            default:
              console.log(error.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      console.log(e.message || '');
      // alert(e.message || '');
    }
  };
  const SetRegion = region => {
    setRegion(region);
    setLastLat(region.latitude);
    setLastLong(region.longitude);

    Animate();
  };
  const Animate = async () => {
    setTimeout(() => {
      if (
        region.latitude != 0 &&
        // this.refs.map.animateToRegion(
        mapRef.current.animateToRegion(
          {
            latitude: region.latitude,
            longitude: region.longitude,
            latitudeDelta: 0.005, //0.07
            longitudeDelta: 0.005, //0.0481
          },
          3000,
        )
      );
    }, 15);
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <View style={styles.map}>
          <MapView
            ref={mapRef}
            provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            // initialRegion={{
            //   latitude: 37.78825,
            //   longitude: -122.4324,
            //   latitudeDelta: 0.0922,
            //   longitudeDelta: 0.0421,
            // }}
            // region={region}
            initialRegion={region}>
            <Marker
              coordinate={{
                latitude: Number(region.latitude),
                longitude: Number(region.longitude),
              }}
              draggable
              // title={
              // }
              // onCalloutPress={() =>
              //   this.handleOpenProfile(item, key)
              // }
            >
              <CustomIcon icon={appImages.mapPin} size={45} />
            </Marker>
          </MapView>
        </View>

        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <TouchableCustomIcon
          onPress={GetCurrentPosition}
          icon={appImages.location}
          size={totalSize(3)}
          color={colors.statusBarBlueColor}
          style={{
            alignSelf: 'flex-start',
            marginLeft: width(4),
            marginTop: height(2),
          }}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />
        <View style={{width: width(100)}}>
          <SearchBarHome
            placeholder="Search your location"
            containerStyle={appStyles.shadowSmall}
            customIconLeft={appImages.placeMarker}
          />
        </View>
        <KeyboardAwareScrollView
          style={{marginTop: Platform.OS == 'ios' ? height(29) : height(26)}}>
          <View style={styles.bottomContainer}>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin * 1.5 : sizes.baseMargin
              }
            />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Destination
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Destination
              </RegularText>
            )}
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
              }
            />
            <View style={[styles.onlyRow, {justifyContent: 'space-around'}]}>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(1)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(1)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 1 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.homeIconTest}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>Home</SmallText>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(2)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(2)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 2 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.suitcase}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>Work</SmallText>
              </TouchableOpacity>
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Address
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Address
              </RegularText>
            )}

            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={[
                styles.fieldStyle,
                {
                  height: Platform.OS == 'ios' ? height(10) : height(12),
                  textAlignVertical: 'top',
                },
              ]}
              multiline
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Street / Area
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Street / Area
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'Hamdan Bin Muhammad st, Abu Dhabi'}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                City
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                City
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'Abu Dhabi'}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Country
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Country
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'United Arab Emirates'}
            />
            <Spacer height={sizes.baseMargin * 1.5} />
            <View style={styles.onlyRow}>
              <CheckBoxPrimary
                checked={check}
                onPress={() => setCheck(!check)}
              />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Keep it default location
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Keep it default location
                </RegularText>
              )}
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            <ButtonColored
              text={'CONTINUE'}
              onPress={() => navigation.navigate(routes.bookingSummary)}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
          </View>
          <Spacer height={sizes.baseMargin} />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
