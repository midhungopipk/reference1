import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  ProductListingCategories,
  ImageCategoryBanner,
  SmallText,
  HomeItemCard,
  Wrapper,
  TinierTitle,
  TouchableCustomIcon,
  TinyText,
  HeaderSimple,
  FilterProductsModal,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
} from '../../../services';

/* Api Calls */
import {
  getProductListingParentCategories,
  getProductListingParentCategoriesFilters,
} from '../../../services/api/home.api';
import {getWishList} from '../../../services/api/cart.api';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {
  fetchCategoryId,
  fetchCartData,
  fetchUser,
  fetchFilterData,
} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToCartMethod,
  AddToWishListMethod,
  RemoveProductFromWishListMethod,
  RemoveProductFromWLMethod,
  Translate,
} from '../../../services/helpingMethods';
import AsyncStorage from '@react-native-community/async-storage';

export default function ProductListing({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([]);
  const [listData, setListData] = useState([]);
  const dispatch = useDispatch();
  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [mainBanner, setMainBanner] = useState('');
  const [response, setResponse] = useState(null);
  const [categoryName, setCategoryName] = useState('');
  const [moreProductsLoading, setMoreProductsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [quantity, setQuantity] = useState(0);
  const [bannerImage, setBannerImage] = useState(null);
  const [LTR, setLTR] = useState(true);
  const [hashUrl, setHashUrl] = useState('');
  const [
    onEndReachedCalledDuringMomentum,
    setonEndReachedCalledDuringMomentum,
  ] = useState(false);
  const [selectedSort, setSelectedSort] = useState({
    sort_by: 'popularity',
    sort_order: 'desc',
  });
  const [cartLoading, setCartLoading] = useState(false);
  const ID = useSelector(state => state.category);
  const {categoryId} = ID;
  console.log('Category id redux: ', categoryId);

  // const [categoryId, setCategoryId] = useState(
  //   store.getState().category.categoryId,
  // );

  const [wishListData, setWishListData] = useState([]);
  const [wishLoading, setwishLoading] = useState(true);

  useEffect(async () => {
    let language = await AsyncStorage.getItem(storageConst.language);

    if (language == 'ar') {
      setLTR(false);
    }
    setLoading(true);
    getProductListingParentCategories(
      user.user_id,
      categoryId,
      page,
      selectedSort,
      onGetResponse,
      setLoading,
    );
    getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
    setQuantity(
      store.getState().cart.data && store.getState().cart.data.data
        ? store.getState().cart.data.data
        : 0,
    );
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      // console.log(store.getState().filter.data);
      setMoreProductsLoading(false);
      if (store.getState().filter.data && store.getState().filter.data.length) {
        setLoading(true);
        const data = store.getState().filter.data;
        //apply some logic here
        //features_hash={{filter_id}}-{{variant_value}}_{{filter_id}}-{{variant_value}}-{{variant_value}}
        let newUrl = 'features_hash=';
        for (let i = 0; i < data.length; i++) {
          let addOn = false;
          data[i].variants.forEach(element => {
            if (element.selected) addOn = true;
          });
          if (addOn) {
            if (newUrl == 'features_hash=') {
              newUrl = newUrl + data[i].filter_id + '-';
              // newUrl = newUrl + '{{' + data[i].filter_id + '}}-';
            } else if (newUrl.charAt(newUrl.length - 1) == '-') {
              newUrl = newUrl.slice(0, -1);
              newUrl = newUrl + '_' + data[i].filter_id + '-';
              // newUrl = newUrl + '_{{' + data[i].filter_id + '}}-';
            }
          }

          for (let j = 0; j < data[i].variants.length; j++) {
            if (data[i].variants[j].selected) {
              console.log('selected variants: ', data[i].variants[j].selected);
              newUrl = newUrl + data[i].variants[j].variant_id + '-';
              // newUrl = newUrl + '{{' + data[i].variants[j].variant_id + '}}-';
            }
          }
          if (data[i].description == 'Price' && !data[i].variants.length) {
            if (newUrl.charAt(newUrl.length - 1) == '-') {
              newUrl = newUrl.slice(0, -1);
              // newUrl = newUrl + '_{{' + data[i].filter_id + '}}-';
            }
            newUrl =
              newUrl +
              `_${data[i].filter_id}-${data[i].selectedRange.min}-${data[i].selectedRange.max}-AED_`;
            // newUrl +
            // `_{{${data[i].filter_id}}}-{{${data[i].selectedRange.min}}}-{{${data[i].selectedRange.max}}}-AED_`;
          }
          //features_hash={{1}}-{{1.82}}-{{6.82}}-AED
        }

        // console.log('lastChar: ', newUrl.charAt(newUrl.length - 1));
        if (
          newUrl.charAt(newUrl.length - 1) == '-' ||
          newUrl.charAt(newUrl.length - 1) == '_'
        ) {
          newUrl = newUrl.slice(0, -1);
        }
        console.log(newUrl);
        if (newUrl != 'features_hash=') {
          setHashUrl(newUrl);
          console.log('filter applied ');
          getProductListingParentCategoriesFilters(
            newUrl,
            user.user_id,
            categoryId,
            page,
            selectedSort,
            onGetResponse,
            setLoading,
          );
        } else {
          setLoading(false);
        }
      }
      getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
      setQuantity(
        store.getState().cart.data && store.getState().cart.data.data
          ? store.getState().cart.data.data
          : 0,
      );
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    computeLikes();
  }, [listData, wishListData]);

  // Get Wish List API Response
  const onGetWishlistResponse = async res => {
    console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) setWishListData(res.products);
      setwishLoading(false);
    }
    setwishLoading(false);
  };

  // Get Sub Category Api Response
  const onGetResponse = async res => {
    console.log('Sub category API: ', res);

    // console.log('Sub category: ', res.subcategories);
    if (res) {
      setResponse(res);
      // if (res.parent_category_id == '0') {
      setBannerImage(res.category_image);
      // }
      setCategoryName(res.category_name);
      setCategoryData1(res.subcategories);

      let likeList = res.products;
      // if (likeList.length)
      likeList.forEach(element => {
        element.liked = false;
      });
      setListData(likeList);

      setLoading(false);
      // fetchMore();
      computeLikes();
    }
    setLoading(false);
  };

  // Compute Likes
  const computeLikes = async () => {
    // console.log('In compute likes: ', wishListData);
    if (wishListData.length) {
      if (listData.length) {
        let newProductMedicationData = listData;
        for (let i = 0; i < newProductMedicationData.length; i++) {
          for (let j = 0; j < wishListData.length; j++) {
            if (
              newProductMedicationData[i].product_id ==
              wishListData[j].product_id
            ) {
              newProductMedicationData[i].liked = true;
            }
          }
        }
        // setListData([]);
        // console.log('compute likes called again');
        setListData(newProductMedicationData);
        // setListData(prev => [...prev, newProductMedicationData]);
      }
    }
  };

  // Apply Sort
  const applySort = async () => {
    let pageNumber = 0;
    setPage(pageNumber);
    setIsVisible(false);
    console.log(selectedSort);
    setLoading(true);
    getProductListingParentCategories(
      user.user_id,
      categoryId,
      pageNumber,
      selectedSort,
      onGetResponseFromSort,
      setLoading,
    );
  };

  // Get Sort Response
  const onGetResponseFromSort = async res => {
    console.log('response from sort: ', res);

    if (res) {
      setResponse(res);
      setListData(res.products);
      setListData(prev => [...prev]);
      setLoading(false);
    }
    setLoading(false);
  };

  // Handle Fetch More Products
  const fetchMore = () => {
    if (onEndReachedCalledDuringMomentum) {
      let pageNumber = page + 1;
      setPage(pageNumber);
      console.log('Fetch More called with page number ', page);
      setMoreProductsLoading(true);
      if (hashUrl == '') {
        getProductListingParentCategories(
          user.user_id,
          categoryId,
          pageNumber,
          selectedSort,
          fetchMoreResponse,
          setMoreProductsLoading,
        );
      } else {
        getProductListingParentCategoriesFilters(
          hashUrl,
          user.user_id,
          categoryId,
          page,
          selectedSort,
          fetchMoreResponse,
          setLoading,
        );
      }
      setonEndReachedCalledDuringMomentum(false);
    }
  };

  // Fetch More API Response
  const fetchMoreResponse = async res => {
    console.log('fetchMoreResponse Sub category API: ', res.products);
    setMoreProductsLoading(false);
    if (res.success) {
      if (res.products && res.products.length) {
        {
          res.products.forEach(element => {
            setListData(prev => [...prev, element]);
          });
          setListData(prev => [...prev]);
        }
      }
    }
  };

  // Get Cart API Response
  const getCartResponse = async res => {
    console.log('Cart API: ', res);
    setLoading(false);
    setCartLoading(false);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        console.log('came in success', user.user_id);
        if (user.user_id == '0') {
          console.log('came in success', user.user_id);
          let updatedUser = user;
          console.log('user', user);
          user.user_id = res.guest_id;
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
        await store.dispatch(
          fetchCartData({
            // data: res,
            data: res.total_products,
          }),
        );
        setQuantity(res.total_products);

        setLoading(false);
      }
    }
    setLoading(false);
  };

  // Get Wish List API Response
  const getWishResponse = async res => {
    // console.log('Wishlist API: ', res);
    if (res) {
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          <HeaderSimple
            showSearch
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
            quantity={quantity}
          />
          {/* <View
            style={[
              appStyles.spaceBetween,
              {width: width(100), flexDirection: 'row'},
            ]}>
            <ImageCategoryBanner
              animation={'fadeInRight'}
              source={
                bannerImage ? {uri: bannerImage} : appImages.placeHolderProduct
              }
              imageStyle={styles.bigBanner}
              text={response.category_name ? response.category_name : ''}
              subText={
                response.subcategories
                  ? response.subcategories.length + ' sub-categories found'
                  : '0 sub-categories found'
              }
            />
          </View> */}

          <View style={{flex: 1}}>
            {categoryData1 && categoryData1.length ? (
              <FlatList
                data={categoryData1}
                // contentContainerStyle={{
                //   alignItems: 'center',
                // }}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(contact, index) => String(index)}
                renderItem={({item}) => (
                  <ProductListingCategories
                    source={item.image ? {uri: item.image} : null}
                    onPress={async () => {
                      // store.dispatch(
                      //   fetchCategoryId({
                      //     categoryId: item.category_id,
                      //   }),
                      // );
                      let categoryId = {
                        categoryId: item.category_id,
                      };
                      dispatch(fetchCategoryId(categoryId));
                      await store.dispatch(
                        fetchFilterData({
                          data: {},
                        }),
                      );
                      navigation.navigate(routes.subCategories);
                    }}
                    item={item}
                  />
                )}
              />
            ) : null}
            <Spacer height={sizes.smallMargin} />
            <Wrapper
              animation={'fadeIn'}
              style={[styles.filterContainer, appStyles.shadowSmall]}>
              <View>
                <TinierTitle
                  style={{
                    fontFamily:
                      Platform.OS == 'ios'
                        ? fontFamily.appTextMedium
                        : fontFamily.appTextBold,
                  }}>
                  {categoryName != '' ? categoryName : 'Skin Care'}
                </TinierTitle>
                {Platform.OS == 'ios' ? (
                  <TinyText style={{color: colors.textGrey1}}>
                    {listData.length + ' products found'}
                  </TinyText>
                ) : (
                  <SmallText style={{color: colors.textGrey1}}>
                    {listData.length + ' products found'}
                  </SmallText>
                )}
              </View>
              <View style={styles.onlyRow}>
                <TouchableOpacity
                  onPress={() => {
                    if (listData && listData.length) {
                      navigation.navigate(routes.filters);
                    } else {
                      Toast.show(Translate('Not available'));
                    }
                  }}
                  style={[
                    styles.onlyRow,
                    {borderRightWidth: 1, borderColor: colors.borderColor},
                  ]}>
                  <SmallText>{Translate('Filter')}</SmallText>
                  <TouchableCustomIcon
                    onPress={() => {
                      if (listData && listData.length) {
                        navigation.navigate(routes.filters);
                      } else {
                        Toast.show(Translate('Not available'));
                      }
                    }}
                    icon={appImages.filter}
                    size={Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)}
                    style={{marginLeft: width(2), marginRight: width(5)}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    if (listData && listData.length) {
                      setIsVisible(true);
                    } else {
                      Toast.show(Translate('Not available'));
                    }
                  }}
                  style={styles.onlyRow}>
                  <SmallText style={{marginLeft: width(5)}}>
                    {Translate('Sort')}
                  </SmallText>
                  <TouchableCustomIcon
                    onPress={() => {
                      if (listData && listData.length) {
                        setIsVisible(true);
                      } else {
                        Toast.show(Translate('Not available'));
                      }
                    }}
                    icon={appImages.sort}
                    size={Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)}
                    style={{marginLeft: width(2)}}
                  />
                </TouchableOpacity>
              </View>
            </Wrapper>
            {listData && listData.length ? (
              <FlatList
                data={listData}
                extraData={listData}
                // contentContainerStyle={{
                //   alignItems: 'center',
                // }}
                numColumns={2}
                keyExtractor={(contact, index) => String(index)}
                showsHorizontalScrollIndicator={false}
                onEndReached={fetchMore}
                onEndReachedThreshold={0.5}
                onMomentumScrollBegin={() => {
                  setonEndReachedCalledDuringMomentum(true);
                }}
                renderItem={({index, item}) => (
                  <HomeItemCard
                    LTR={LTR}
                    item={item}
                    style={{
                      borderColor: colors.borderLightColor,
                      borderRadius: 5,
                      width: width(43),
                      marginLeft: width(2),
                    }}
                    onPress={() =>
                      navigation.navigate(routes.productDetail, {
                        productId: item.product_id,
                      })
                    }
                    addToCartPress={async () => {
                      // if (user.user_id == '0') {
                      //   navigation.navigate(routes.signin);
                      // } else {
                      if (!cartLoading) {
                        setCartLoading(true);
                        await AddToCartMethod({
                          item,
                          user,
                          getCartResponse,
                        });
                      }
                      // }
                    }}
                    onWishPress={async () => {
                      let newList = listData;
                      if (!newList[index].liked) {
                        await AddToWishListMethod({
                          item,
                          user,
                          getWishResponse,
                        });
                        newList[index].liked = true;
                        setListData(newList);
                        setListData(prev => [...prev]);
                      } else {
                        await RemoveProductFromWLMethod({
                          user_id: user.user_id,
                          product_id: item.product_id,
                          getWishResponse,
                        });

                        newList[index].liked = false;
                        setListData(newList);
                        setListData(prev => [...prev]);
                      }
                    }}
                  />
                )}
              />
            ) : null}
          </View>
          {moreProductsLoading ? (
            <View style={styles.loaderBg}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />

          {cartLoading ? (
            <View
              style={{
                position: 'absolute',
                bottom: height(30),
                borderRadius: 5,
                width: totalSize(5),
                height: totalSize(5),
                backgroundColor: 'rgba(255,255,255,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}

          <FilterProductsModal
            isVisible={isVisible}
            toggleModal={() => setIsVisible(false)}
            onPressButton={() => applySort()}
            selectedValue={value => setSelectedSort(value)}
          />
        </SafeAreaView>
      )}
    </Fragment>
  );
}
