import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SearchBarHome,
  CustomIcon,
  TouchableCustomIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function MapTest({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [lastLat, setLastLat] = useState('51.5347');
  const [lastLong, setLastLong] = useState('0.1246');

  const mapRef = useRef(null);

  useEffect(() => {
    GetCurrentPosition();
  }, []);

  const GetCurrentPosition = async () => {
    let newPosition = {
      latitude: 51.5347,
      longitude: 0.1246,
    };
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                console.log(error.message);
                // Alert.alert(error.message);
              }
              break;
            default:
              console.log(e.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      console.log(e.message || '');
      // alert(e.message || '');
    }
  };
  const SetRegion = region => {
    setRegion(region);
    setLastLat(region.latitude);
    setLastLong(region.longitude);

    Animate();
  };
  const Animate = async () => {
    setTimeout(() => {
      if (
        region.latitude != 0 &&
        // this.refs.map.animateToRegion(
        mapRef.current.animateToRegion(
          {
            latitude: region.latitude,
            longitude: region.longitude,
            latitudeDelta: 0.005, //0.07
            longitudeDelta: 0.005, //0.0481
          },
          3000,
        )
      );
    }, 15);
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <View style={styles.map}>
          <MapView
            ref={mapRef}
            provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            // initialRegion={{
            //   latitude: 37.78825,
            //   longitude: -122.4324,
            //   latitudeDelta: 0.0922,
            //   longitudeDelta: 0.0421,
            // }}
            // region={region}
            initialRegion={region}>
            <Marker
              coordinate={{
                latitude: Number(region.latitude),
                longitude: Number(region.longitude),
              }}
              draggable
              // title={
              // }
              // onCalloutPress={() =>
              //   this.handleOpenProfile(item, key)
              // }
            >
              <CustomIcon icon={appImages.mapPin} size={45} />
            </Marker>
          </MapView>
        </View>
        {/* <KeyboardAwareScrollView> */}
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <TouchableCustomIcon
          onPress={GetCurrentPosition}
          icon={appImages.location}
          size={totalSize(3)}
          color={colors.statusBarBlueColor}
          style={{
            alignSelf: 'flex-start',
            marginLeft: width(4),
            marginTop: height(2),
          }}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />
        {/* <View style={{width: width(100)}}>
          <SearchBarHome
            placeholder="Search your location"
            containerStyle={appStyles.shadowSmall}
            customIconLeft={appImages.placeMarker}
          />
        </View> */}
        {/* </KeyboardAwareScrollView> */}
        <View
          style={{width: width(100), position: 'absolute', bottom: height(4)}}>
          <ButtonColored
            text={'CONTINUE'}
            onPress={() => navigation.navigate(routes.bookingSummary)}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: 5,
            }}
          />
        </View>
        <Spacer height={sizes.baseMargin} />
      </SafeAreaView>
    </Fragment>
  );
}
