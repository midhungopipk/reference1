import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {useInputValue} from '../../../services/hooks/useInputValue';

/* Api Calls */
import {UpdateExistingAddress} from '../../../services/api/user.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SearchBarHome,
  GooglePlacesInput,
  CustomIcon,
  SmallText,
  TextInputColored,
  RegularText,
  CheckBoxPrimary,
  TouchableCustomIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {GetAddressFromCoords} from '../../../services/helpingMethods';

export default function CheckoutChangeAddress({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);
  const [check, setCheck] = useState(true);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [marker, setMarker] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
  });
  const [markerLat, setMarkerLat] = useState('51.5347');
  const [markerLong, setMarkerLong] = useState('0.1246');
  const [lastLat, setLastLat] = useState('51.5347');
  const [lastLong, setLastLong] = useState('0.1246');
  const [test, setTest] = useState(1);

  const user = store.getState().user.user.user;
  const address = useInputValue('');
  const streetArea = useInputValue('');
  const city = useInputValue('');
  const country = useInputValue('');

  const [paramData, setParamData] = useState(
    route.params.item ? route.params.item : null,
  );

  const mapRef = useRef(null);
  console.log('Route Params: ', route.params.item);

  useEffect(() => {
    // GetCurrentPosition();
    if (
      route.params.item.ec_lat &&
      route.params.item.ec_lat != '' &&
      route.params.item.ec_lng &&
      route.params.item.ec_lng != ''
    ) {
      let regionObject = {
        latitude: Number(route.params.item.ec_lat),
        longitude: Number(route.params.item.ec_lng),
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      };
      SetRegion(regionObject);
      let markerObj = {
        latitude: Number(route.params.item.ec_lat),
        longitude: Number(route.params.item.ec_lng),
      };
      setMarker(markerObj);
      address.onChange(route.params.item.s_address);
      streetArea.onChange(route.params.item.s_address_2);
      city.onChange(route.params.item.s_city);
      country.onChange(route.params.item.s_country);
    } else {
      GetCurrentPosition();
    }
  }, [route.params.item]);

  const getGeocode = res => {
    // console.log('response: ', res);
    address.onChange(res.results[0].formatted_address);
    streetArea.onChange(
      res.results[0].address_components[0].long_name +
        ', ' +
        res.results[0].address_components[1].long_name,
    );
    const words = res.plus_code.compound_code.split(' ');
    let cityWord = words[1].split(',');
    city.onChange(cityWord[0]);
    // console.log('words split: ', words);
    if (words.length) {
      country.onChange(words[words.length - 1]);
    }
  };

  const handleSaveAdress = async () => {
    setDataLoading(true);
    let addressObj = {
      user_id: user.user_id,
      profile_data: {
        profile_id: route.params.item.profile_id,
        ec_lat: marker.latitude,
        ec_lng: marker.longitude,
        is_default: 'Y',
        profile_name: test == 1 ? 'my_home' : 'my_work',
        s_address: address.value,
        s_address_2: streetArea.value,
        s_address_type: 'residential',
        s_city: city.value,
        s_country: country.value,
        s_firstname: user.firstname,
        s_phone: user.phone,
        s_state: city.value,
        s_zipcode: '',
      },
    };
    UpdateExistingAddress(
      user.user_id,
      addressObj,
      getAddAdressApiResponse,
      setDataLoading,
    );
  };

  const getAddAdressApiResponse = res => {
    console.log('update address api: ', res);
    if (res.success) {
      setDataLoading(false);
      Toast.show('You address has been updated successfully!');
      navigation.pop();
    }
    setDataLoading(false);
  };

  const GetCurrentPosition = async () => {
    // let newPosition = {
    //   latitude: 51.5347,
    //   longitude: 0.1246,
    // };
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
          let markerObj = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
          setMarker(markerObj);
          GetAddressFromCoords(
            position.coords.latitude,
            position.coords.longitude,
            getGeocode,
          );
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                console.log(error.message);
                // Alert.alert(error.message);
              }
              break;
            default:
              console.log(error.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      console.log(e.message || '');
      // alert(e.message || '');
    }
  };
  const SetRegion = region => {
    setRegion(region);
    setLastLat(region.latitude);
    setLastLong(region.longitude);

    Animate(region);
  };
  const Animate = async region => {
    setTimeout(() => {
      if (
        region.latitude != 0 &&
        // this.refs.map.animateToRegion(
        mapRef.current.animateToRegion(
          {
            latitude: region.latitude,
            longitude: region.longitude,
            latitudeDelta: 0.005, //0.07
            longitudeDelta: 0.005, //0.0481
          },
          3000,
        )
      );
    }, 15);
  };

  //   const initialize = () => {

  //     var map = new google.maps.Map(document.getElementById('map-canvas'), {
  //         center: new google.maps.LatLng(0, 0),
  //         zoom: 15
  //     });

  //     var service = new google.maps.places.PlacesService(map);

  //     service.getDetails({
  //         placeId: 'ChIJneQ1fBO5t4kRf8mTw4ieb4Q'
  //     }, function(place, status) {
  //         if (status === google.maps.places.PlacesServiceStatus.OK) {

  //             // Create marker
  //             var marker = new google.maps.Marker({
  //                 map: map,
  //                 position: place.geometry.location
  //             });

  //             // Center map on place location
  //             map.setCenter(place.geometry.location);
  //         }
  //     });
  // }

  // const handleAddressChange = async () => {
  //   const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GoogleAPIKey}&input=${this.state.address}`;
  //   try {
  //     const result = await fetch(url);
  //     const json = await result.json();
  //     this.setState({addressPredictions: json.predictions});
  //   } catch (err) {
  //     console.error(err);
  //   }
  // };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <View style={styles.map}>
          <MapView
            ref={mapRef}
            provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            // initialRegion={{
            //   latitude: 37.78825,
            //   longitude: -122.4324,
            //   latitudeDelta: 0.0922,
            //   longitudeDelta: 0.0421,
            // }}
            // region={region}
            initialRegion={region}>
            <Marker
              coordinate={marker}
              draggable
              onDragEnd={e => {
                setMarker(e.nativeEvent.coordinate);
                // console.log('Position from map = ', e.nativeEvent.coordinate);
                GetAddressFromCoords(
                  e.nativeEvent.coordinate.latitude,
                  e.nativeEvent.coordinate.longitude,
                  getGeocode,
                );
              }}
              position={marker}
              // centerOffset={{x: -18, y: -60}}
              // anchor={{x: 0.69, y: 1}}
              // title={
              // }
              // onCalloutPress={() =>
              //   this.handleOpenProfile(item, key)
              // }
            >
              <CustomIcon icon={appImages.mapPin} size={45} />
            </Marker>
          </MapView>
        </View>

        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <TouchableCustomIcon
          onPress={GetCurrentPosition}
          icon={appImages.location}
          size={totalSize(3)}
          color={colors.statusBarBlueColor}
          style={{
            alignSelf: 'flex-start',
            marginLeft: width(4),
            marginTop: height(2),
          }}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />
        <View style={{width: width(100)}}>
          {/* <SearchBarHome
            placeholder="Search your location"
            containerStyle={appStyles.shadowSmall}
            customIconLeft={appImages.placeMarker}
          /> */}
          {/* <GooglePlacesInput /> */}
        </View>
        <KeyboardAwareScrollView
          style={{marginTop: Platform.OS == 'ios' ? height(32) : height(28)}}>
          <View style={styles.bottomContainer}>
            <Spacer
              height={Platform.OS == 'ios' ? sizes.baseMargin * 1.5 : 0}
            />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Destination
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Destination
              </RegularText>
            )}
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
              }
            />
            <View style={[styles.onlyRow, {justifyContent: 'space-around'}]}>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(1)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(1)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 1 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.homeIconTest}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>Home</SmallText>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(2)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(2)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 2 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.suitcase}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>Work</SmallText>
              </TouchableOpacity>
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Address
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Address
              </RegularText>
            )}

            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={[
                styles.fieldStyle,
                {
                  height: Platform.OS == 'ios' ? height(10) : height(12),
                  textAlignVertical: 'top',
                },
              ]}
              multiline
              value={address.value}
              onChangeText={address.onChange}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Street / Area
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Street / Area
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            {/* <GooglePlacesInput
              onPress={(data, details) => {
                console.log(data.place_id, details.place_id);
              }}
            /> */}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'Hamdan Bin Muhammad st, Abu Dhabi'}
              value={streetArea.value}
              onChangeText={streetArea.onChange}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                City
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                City
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'Abu Dhabi'}
              value={city.value}
              onChangeText={city.onChange}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Country
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Country
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'United Arab Emirates'}
              value={country.value}
              onChangeText={country.onChange}
            />
            <Spacer height={sizes.baseMargin * 1.5} />
            <View style={styles.onlyRow}>
              <CheckBoxPrimary
                checked={check}
                onPress={() => setCheck(!check)}
              />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Keep it default location
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Keep it default location
                </RegularText>
              )}
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            <ButtonColored
              isLoading={dataLoading}
              text={'CONTINUE'}
              onPress={handleSaveAdress}
              // onPress={() => navigation.pop()}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
            <Spacer height={sizes.baseMargin} />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
