import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  TouchableCustomIcon,
  HeaderSimple,
  TinyText,
  CustomIcon,
  RegularText,
  MediumText,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';
import {AirbnbRating} from 'react-native-elements';
/* Api Calls */
import {getVendorDetail} from '../../../services/api/vendor.api';
import {store} from '../../../Redux/configureStore';

export default function PharmacyPage({navigation, route}) {
  const [loading, setLoading] = useState(true);

  const [collapsable1, setCollapsable1] = useState(false);
  const [collapsable2, setCollapsable2] = useState(true);
  const [collapsable3, setCollapsable3] = useState(true);
  const [content1, setContent1] = useState({
    title: Translate('About'),
    text: 'HOMEDEVO.com is an eCommerce portal. HOMEDEVO.com is designed to be a one stop-shop offering premium healthcare and wellness products. HOMEDEVO.com becomes the first pharmacy in the UAE to go online -- one of the many firsts in the healthcare and wellness space. HOMEDEVO.com was created because of our constant endeavor to improve ourselves and the services we bring to you. HOMEDEVO.com is a robust eCommerce healthcare platform that brings to your fingertips thousands of products being offered through various categories. The products range from curative and nutritive products, baby care, lifestyle, wellness and rehabilitation to FMCG products, cosmetics, personal and home care products in addition to medical devices and equipment. The platform lets you compare products by way of price, brands and offers -- therefore helping you make informed decisions before purchasing them and getting them delivered to your doorstep. These are just some of many benefits we bring to you by introducing HOMEDEVO.com',
  });
  const [content2, setContent2] = useState({
    title: Translate('Contact Information'),
    text: 'HOMEDEVO.com is an eCommerce portal. HOMEDEVO.com is designed to be a one stop-shop offering premium healthcare and wellness products. HOMEDEVO.com becomes the first pharmacy in the UAE to go online -- one of the many firsts in the healthcare and wellness space. HOMEDEVO.com was created because of our constant endeavor to improve ourselves and the services we bring to you. HOMEDEVO.com is a robust eCommerce healthcare platform that brings to your fingertips thousands of products being offered through various categories. The products range from curative and nutritive products, baby care, lifestyle, wellness and rehabilitation to FMCG products, cosmetics, personal and home care products in addition to medical devices and equipment. The platform lets you compare products by way of price, brands and offers -- therefore helping you make informed decisions before purchasing them and getting them delivered to your doorstep. These are just some of many benefits we bring to you by introducing HOMEDEVO.com',
  });
  const [content3, setContent3] = useState({
    title: Translate('Shipping Address'),
    text: 'HOMEDEVO.com is an eCommerce portal. HOMEDEVO.com is designed to be a one stop-shop offering premium healthcare and wellness products. HOMEDEVO.com becomes the first pharmacy in the UAE to go online -- one of the many firsts in the healthcare and wellness space. HOMEDEVO.com was created because of our constant endeavor to improve ourselves and the services we bring to you. HOMEDEVO.com is a robust eCommerce healthcare platform that brings to your fingertips thousands of products being offered through various categories. The products range from curative and nutritive products, baby care, lifestyle, wellness and rehabilitation to FMCG products, cosmetics, personal and home care products in addition to medical devices and equipment. The platform lets you compare products by way of price, brands and offers -- therefore helping you make informed decisions before purchasing them and getting them delivered to your doorstep. These are just some of many benefits we bring to you by introducing HOMEDEVO.com',
  });
  const [listData, setListData] = useState([]);
  const [response, setResponse] = useState([]);
  const user = store.getState().user.user.user;
  const [paramData, setParamData] = useState(route.params.item);

  useEffect(() => {
    setLoading(true);
    getVendorDetail(
      user.user_id,
      // paramData.company_id,
      route.params.item,
      getVendorResponse,
      setLoading,
    );
  }, [user]);

  //Get Vendor API Response
  const getVendorResponse = async res => {
    console.log('Vendor Detail API: ', res);
    if (res.success) {
      setResponse(res.vendor);
      let newList1 = content1;
      newList1.text = res.vendor.company;
      setContent1(newList1);

      let newList = content2;
      newList.text = res.vendor.email;
      setContent2(newList);
      setContent3(newList);
      if (res.categories && res.categories.length) {
        setListData(res.categories);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Product Card Component
  const ProductCard = ({onPress, onPress1, onPress2}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View
          style={[
            appStyles.center,
            {
              flex: 1,
              borderRightWidth: 0.5,
              borderColor: colors.borderLightColor,
              //   paddingVertical: height(1),
            },
          ]}>
          <CustomIcon
            icon={
              response && response.image_url != ''
                ? {uri: response.image_url}
                : appImages.logoFull
            }
            size={Platform.OS == 'ios' ? totalSize(13) : totalSize(15)}
          />
        </View>
        <View style={{flex: 1, paddingLeft: width(4)}}>
          {Platform.OS == 'ios' ? (
            <RegularText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </RegularText>
          ) : (
            <MediumText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </MediumText>
          )}
          {Platform.OS == 'ios' ? (
            <TinyText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </TinyText>
          ) : (
            <SmallText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </SmallText>
          )}
          <Spacer height={sizes.baseMargin} />
          <TouchableOpacity
            onPress={onPress1}
            style={[
              styles.buttonStyle,
              {
                backgroundColor: colors.activeBottomIcon,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyText style={[appStyles.whiteText, {alignSelf: 'center'}]}>
                {Translate('Pharmacy Products')}
              </TinyText>
            ) : (
              <SmallText style={[appStyles.whiteText, {alignSelf: 'center'}]}>
                {Translate('Pharmacy Products')}
              </SmallText>
            )}
          </TouchableOpacity>
          <Spacer height={sizes.TinyMargin * 1.5} />
          <View style={{alignSelf: 'flex-start'}}>
            <AirbnbRating
              count={5}
              isDisabled={true}
              defaultRating={
                response && response.average_rating
                  ? response.average_rating
                  : 0
              }
              size={15}
              showRating={false}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  //Contetn Card Component
  const ContentCard = ({onPress, collapsable, title, content}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
          {
            flexDirection: 'column',
            alignItems: 'flex-start',
            paddingHorizontal: width(5),
          },
        ]}>
        <Spacer height={sizes.baseMargin} />
        <View style={[styles.onlyRow]}>
          <TouchableCustomIcon
            onPress={onPress}
            icon={collapsable ? appImages.addIcon : appImages.removeIcon}
            size={Platform.OS == 'ios' ? totalSize(3) : totalSize(4)}
          />
          {Platform.OS == 'ios' ? (
            <RegularText
              style={{
                fontFamily: fontFamily.appTextMedium,
                marginLeft: width(5),
              }}>
              {title}
            </RegularText>
          ) : (
            <MediumText
              style={{
                fontFamily: fontFamily.appTextMedium,
                marginLeft: width(5),
              }}>
              {title}
            </MediumText>
          )}
        </View>
        <Spacer height={sizes.baseMargin} />
        {collapsable ? null : (
          <View>
            {Platform.OS == 'ios' ? (
              <SmallText style={{color: colors.textGrey1}}>{content}</SmallText>
            ) : (
              <RegularText style={{color: colors.textGrey1}}>
                {content}
              </RegularText>
            )}
            <Spacer height={sizes.baseMargin} />
          </View>
        )}
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          <KeyboardAwareScrollView>
            <HeaderSimple
              onBackPress={() => navigation.pop()}
              onPress={() => navigation.navigate('Cart')}
            />
            <Spacer height={sizes.TinyMargin} />
            <View
              style={[
                styles.mainViewContainer,
                {backgroundColor: colors.bgLight},
              ]}>
              <View style={styles.bottomContainer}>
                <ProductCard
                  onPress1={() =>
                    navigation.navigate(routes.pharmacyProducts, {
                      item: paramData,
                    })
                  }
                />
                <ContentCard
                  onPress={() => setCollapsable1(!collapsable1)}
                  collapsable={collapsable1}
                  title={content1.title}
                  content={content1.text}
                />
                <ContentCard
                  onPress={() => setCollapsable2(!collapsable2)}
                  collapsable={collapsable2}
                  title={content2.title}
                  content={content2.text}
                />
                <ContentCard
                  onPress={() => setCollapsable3(!collapsable3)}
                  collapsable={collapsable3}
                  title={content3.title}
                  content={content3.text}
                />
              </View>
              <Spacer height={sizes.baseMargin} />
            </View>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin
              }
            />
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
