import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {
  GetAddressFromCoords,
  Translate,
} from '../../../services/helpingMethods';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SearchBarHome,
  CustomIcon,
  SmallText,
  TextInputColored,
  RegularText,
  CheckBoxPrimary,
  GooglePlacesInput,
  TouchableCustomIcon,
  DropdownPickerModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {useInputValue} from '../../../services/hooks/useInputValue';

/* Api Calls */
import {
  AddNewAddress,
  GetCountryStateData,
} from '../../../services/api/user.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';

export default function AddAddress({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);
  const [check, setCheck] = useState(true);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [marker, setMarker] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
  });
  const [lastLat, setLastLat] = useState('51.5347');
  const [lastLong, setLastLong] = useState('0.1246');
  const [test, setTest] = useState(1);
  const mapRef = useRef(null);

  const user = store.getState().user.user.user;
  const address = useInputValue('');
  const streetArea = useInputValue('');
  const city = useInputValue('');
  const country = useInputValue('');
  const [listStates, setListStates] = useState([
    // {label: '01', value: '1'},
  ]);
  const [openCountry, setOpenCountry] = useState(false);
  const [valueCountry, setValueCountry] = useState('');
  const [itemsCountry, setItemsCountry] = useState([
    // {label: '01', value: '1'},
  ]);
  const [openState, setOpenState] = useState(false);
  const [valueState, setValueState] = useState('');
  const [itemsStae, setItemsState] = useState([
    // {label: '01', value: '1'},
  ]);

  useEffect(() => {
    GetCurrentPosition();
  }, []);
  useEffect(() => {
    console.log('user: ', user);
    GetCurrentPosition();
    GetCountryStateData(user.user_id, countryDataResponse);
  }, [user]);

  useEffect(() => {
    console.log('country selected: ', valueCountry);
    // listStates.forEach(element => {
    //   console.log(element);
    // });
    // console.log(listStates.);
    getPropByString(listStates, valueCountry);
  }, [valueCountry]);

  function getPropByString(obj, propString) {
    if (!propString) return obj;

    var prop,
      props = propString.split('.');

    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];

      var candidate = obj[prop];
      if (candidate !== undefined) {
        obj = candidate;
      } else {
        break;
      }
    }
    if (obj[props[i]] != undefined) {
      obj[props[i]].forEach(element => {
        element.label = element.state;
        element.value = element.code;
      });
      console.log('inside getter function: ', obj[props[i]]);
      setItemsState(obj[props[i]]);
    } else {
      setItemsState([]);
    }
    return obj[props[i]];
  }

  const countryDataResponse = res => {
    console.log('get country state api: ', res);
    if (res.success) {
      let countries = res.countries;
      countries.forEach(element => {
        element.label = element.name;
        element.value = element.code;
      });
      console.log('after customization: ', countries);
      setItemsCountry(countries);
      setListStates(res.states);
    }
    setDataLoading(false);
  };

  const getGeocode = res => {
    address.onChange(res.results[0].formatted_address);
    streetArea.onChange(
      res.results[0].address_components[0].long_name +
        ', ' +
        res.results[0].address_components[1].long_name,
    );
    const words = res.plus_code.compound_code.split(' ');
    let cityWord = words[1].split(',');
    city.onChange(cityWord[0]);
    // console.log('words split: ', words);
    if (words.length) {
      country.onChange(words[words.length - 1]);
    }
  };

  const handleSaveAdress = async () => {
    if (user.user_id == '0') {
      navigation.navigate(routes.signin);
    } else {
      if (address.value == '') {
        Toast.show('Enter data in Address field first');
      } else if (streetArea.value == '') {
        Toast.show('Enter data in Street Address field first');
      } else if (city.value == '') {
        Toast.show('Enter data in City field first');
      } else if (country.value == '') {
        Toast.show('Enter data in Country field first');
      } else {
        console.log('Final latitude and longitude: ', marker);
        setDataLoading(true);
        let addressObj = {
          user_id: user.user_id,
          profile_data: {
            ec_lat: marker.latitude,
            ec_lng: marker.longitude,
            is_default: 'Y',
            profile_name: test == 1 ? 'my_home' : 'my_work',
            s_address: address.value,
            s_address_2: streetArea.value,
            s_address_type: 'residential',
            s_city: city.value,
            // s_country: country.value,
            s_country: valueCountry,
            s_firstname: user.firstname,
            s_phone: user.phone,
            s_state: valueState,
            s_zipcode: '',
          },
        };
        AddNewAddress(addressObj, getAddAdressApiResponse, setDataLoading);
      }
    }
  };

  const getAddAdressApiResponse = res => {
    console.log('add address api: ', res);
    if (res.success) {
      setDataLoading(false);
      Toast.show('You address has been added successfully!');
      navigation.pop();
    }
    setDataLoading(false);
  };

  const GetCurrentPosition = async () => {
    let newPosition = {
      latitude: 51.5347,
      longitude: 0.1246,
    };
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
          GetAddressFromCoords(
            position.coords.latitude,
            position.coords.longitude,
            getGeocode,
          );
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                Alert.alert(error.message);
              }
              break;
            default:
              console.log(error.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      // alert(e.message || '');
      console.log(e.message || '');
    }
  };
  const SetRegion = region => {
    setRegion(region);
    // setLastLat(region.latitude);
    // setLastLong(region.longitude);

    Animate(region);
  };
  const Animate = async region => {
    setTimeout(() => {
      // if (
      //   region.latitude != 0 &&
      // this.refs.map.animateToRegion(
      mapRef.current.animateToRegion(
        {
          latitude: region.latitude,
          longitude: region.longitude,
          latitudeDelta: 0.005, //0.07
          longitudeDelta: 0.005, //0.0481
        },
        3000,
      );
      // );
    }, 15);
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <View style={styles.map}>
          <MapView
            ref={mapRef}
            provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            initialRegion={region}
            onPress={e => console.log(e)}
            // region={region}
          >
            <Marker
              coordinate={{
                latitude: region.latitude,
                longitude: region.longitude,
              }}
              draggable
              onDragEnd={e => {
                setMarker(e.nativeEvent.coordinate);
                // console.log('Position from map = ', e.nativeEvent.coordinate);
                GetAddressFromCoords(
                  e.nativeEvent.coordinate.latitude,
                  e.nativeEvent.coordinate.longitude,
                  getGeocode,
                );
              }}
              position={marker}
              // title={
              // }
              // onCalloutPress={() =>
              //   this.handleOpenProfile(item, key)
              // }
            >
              <CustomIcon icon={appImages.mapPin} size={45} />
            </Marker>
          </MapView>
        </View>

        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <TouchableCustomIcon
          onPress={GetCurrentPosition}
          icon={appImages.location}
          size={totalSize(3)}
          color={colors.statusBarBlueColor}
          style={{
            alignSelf: 'flex-start',
            marginLeft: width(4),
            marginTop: height(2),
          }}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />
        {/* <View style={{width: width(100)}}>
          <SearchBarHome
            placeholder="Search your location"
            containerStyle={appStyles.shadowSmall}
            customIconLeft={appImages.placeMarker}
          />
        </View> */}
        <KeyboardAwareScrollView
          style={{marginTop: Platform.OS == 'ios' ? height(28) : height(30)}}>
          <View style={styles.bottomContainer}>
            <Spacer
              height={Platform.OS == 'ios' ? sizes.baseMargin * 1.2 : 0}
            />

            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Destination')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Destination')}
              </RegularText>
            )}
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.2
              }
            />
            <View style={[styles.onlyRow, {justifyContent: 'space-around'}]}>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(1)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(1)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 1 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.homeIconTest}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>
                  {Translate('Home')}
                </SmallText>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.onlyRow}
                onPress={() => setTest(2)}>
                <TouchableOpacity
                  style={[styles.radioButton, {marginRight: width(3)}]}
                  onPress={() => setTest(2)}>
                  <View
                    style={[
                      styles.innerRadioButton,
                      {
                        backgroundColor:
                          test == 2 ? colors.statusBarColor : 'transparent',
                      },
                    ]}
                  />
                </TouchableOpacity>
                <CustomIcon
                  icon={appImages.suitcase}
                  size={Platform.OS == 'ios' ? totalSize(2.3) : totalSize(2.8)}
                />
                <SmallText style={{marginLeft: width(3)}}>
                  {Translate('Work')}
                </SmallText>
              </TouchableOpacity>
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            {/* <GooglePlacesInput
              onPress={(data, details) => {
                console.log(data.place_id, details.place_id);
              }}
            /> */}
            {/* <Spacer
              height={
                 sizes.baseMargin
              }
            /> */}
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Address')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Address')}
              </RegularText>
            )}

            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={[
                styles.fieldStyle,
                {
                  height: Platform.OS == 'ios' ? height(8) : height(10),
                  textAlignVertical: 'top',
                },
              ]}
              multiline
              value={address.value}
              onChangeText={address.onChange}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Street / Area')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Street / Area')}
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              // placeholder={'Hamdan Bin Muhammad st, Abu Dhabi'}
              value={streetArea.value}
              onChangeText={streetArea.onChange}
            />
            {/* <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('City')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('City')}
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              // placeholder={'Abu Dhabi'}
              value={city.value}
              onChangeText={city.onChange}
            /> */}

            <Spacer height={sizes.baseMargin * 1.5} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Country')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('Country')}
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <DropdownPickerModal
              open={openCountry}
              value={valueCountry}
              items={itemsCountry}
              setOpen={setOpenCountry}
              setValue={setValueCountry}
              setItems={setItemsCountry}
              containerStyle={{
                marginLeft: width(4),
                width: width(80),
              }}
            />
            <Spacer height={sizes.doubleBaseMargin * 1.3} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('State')}
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                {Translate('State')}
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <DropdownPickerModal
              open={openState}
              value={valueState}
              items={itemsStae}
              setOpen={setOpenState}
              setValue={setValueState}
              setItems={setItemsState}
              containerStyle={{
                marginLeft: width(4),
                width: width(80),
              }}
            />

            {/* <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              // placeholder={'United Arab Emirates'}
              value={country.value}
              onChangeText={country.onChange}
            /> */}
            <Spacer height={sizes.baseMargin * 1.5} />
            <View style={styles.onlyRow}>
              <CheckBoxPrimary
                checked={check}
                onPress={() => setCheck(!check)}
              />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  {Translate('Keep it default location')}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  {Translate('Keep it default location')}
                </RegularText>
              )}
            </View>
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
            <ButtonColored
              isLoading={dataLoading}
              text={Translate('CONTINUE')}
              // onPress={() => navigation.pop()}
              onPress={handleSaveAdress}
              // onPress={() => navigation.navigate(routes.downloadReport)}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
            <Spacer height={sizes.baseMargin} />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
