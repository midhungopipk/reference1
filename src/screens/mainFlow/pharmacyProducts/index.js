import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  HomeItemCard,
  Wrapper,
  TinierTitle,
  HeaderSimple,
  TinyText,
  CustomIcon,
  RegularText,
  MediumText,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {AirbnbRating} from 'react-native-elements';
import {
  fetchHomeData,
  fetchCategoryId,
  fetchCartData,
  fetchUser,
} from '../../../Redux/Actions/index';
/* Api Calls */
import {
  AddToCartMethod,
  AddToWishListMethod,
  RemoveProductFromWishListMethod,
  RemoveProductFromWLMethod,
  Translate,
} from '../../../services/helpingMethods';
import {storageConst} from '../../../services';
import {getVendorDetail} from '../../../services/api/vendor.api';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import {getWishList} from '../../../services/api/cart.api';

export default function PharmacyProducts({navigation, route}) {
  const [loading, setLoading] = useState(true);

  const [listData, setListData] = useState([
    // {
    //   chart: appImages.chart,
    //   like: appImages.like,
    //   titl1: 'Candes Eclat Fresh',
    //   title2: 'Lightening Face Tonic',
    //   quantity: '200 mL',
    //   price: '69.00',
    //   img: appImages.product1,
    // },
  ]);
  const [cartLoading, setCartLoading] = useState(false);
  const [response, setResponse] = useState([]);
  const user = store.getState().user.user.user;
  const [paramData, setParamData] = useState(route.params.item);

  const [wishListData, setWishListData] = useState([]);
  const [wishLoading, setwishLoading] = useState(true);
  const [LTR, setLTR] = useState(true);
  const [quantity, setQuantity] = useState(0);

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getVendorDetail(
      user.user_id,
      // paramData.company_id,
      route.params.item,
      getVendorResponse,
      setLoading,
    );
    getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
    setQuantity(
      store.getState().cart.data && store.getState().cart.data.data
        ? store.getState().cart.data.data
        : 0,
    );
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      setQuantity(
        store.getState().cart.data && store.getState().cart.data.data
          ? store.getState().cart.data.data
          : 0,
      );
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    computeLikes();
  }, [listData, wishListData]);

  //Get Response of Wish List API
  const onGetWishlistResponse = async res => {
    // console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) setWishListData(res.products);
      setwishLoading(false);
    }
    setwishLoading(false);
  };

  //Get Response of Vendor API
  const getVendorResponse = async res => {
    console.log('Vendor Detail API: ', res);
    if (res) {
      setResponse(res.vendor);
      if (res.products && res.products.length) {
        let likeList = res.products;
        likeList.forEach(element => {
          element.liked = false;
        });
        setListData(res.products);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Compute Likes according to Wish List
  const computeLikes = async () => {
    if (wishListData.length) {
      if (listData.length) {
        let newProductMedicationData = listData;
        for (let i = 0; i < newProductMedicationData.length; i++) {
          for (let j = 0; j < wishListData.length; j++) {
            if (
              newProductMedicationData[i].product_id ==
              wishListData[j].product_id
            ) {
              newProductMedicationData[i].liked = true;
            }
          }
        }
        setListData(newProductMedicationData);
      }
    }
  };

  //Get Cart API response
  const getCartResponse = async res => {
    console.log('Cart API: ', res);
    setCartLoading(false);
    // Toast.show(res.message);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        if (user.user_id == '0') {
          let updatedUser = user;
          user.user_id = res.guest_id;
          console.log('Previous user: ', user);
          console.log('Updated user: ', updatedUser);
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      setQuantity(res.total_products);

      // setLoading(false);
    }

    // setLoading(false);
  };

  //Get Wish List Api Response
  const getWishResponse = async res => {
    if (res) {
      console.log('Wishlist API: ', res);
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  //Product Card Component
  const ProductCard = ({onPress, onPress1, onPress2}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View
          style={[
            appStyles.center,
            {
              flex: 1,
              borderRightWidth: 0.5,
              borderColor: colors.borderLightColor,
              //   paddingVertical: height(1),
            },
          ]}>
          <CustomIcon
            icon={
              response && response.image_url != ''
                ? {uri: response.image_url}
                : appImages.logoFull
            }
            size={Platform.OS == 'ios' ? totalSize(13) : totalSize(15)}
          />
        </View>
        <View style={{flex: 1, paddingLeft: width(4)}}>
          {Platform.OS == 'ios' ? (
            <RegularText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </RegularText>
          ) : (
            <MediumText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </MediumText>
          )}
          {Platform.OS == 'ios' ? (
            <TinyText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </TinyText>
          ) : (
            <SmallText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </SmallText>
          )}
          <Spacer height={sizes.baseMargin} />
          <TouchableOpacity
            onPress={onPress1}
            style={[
              styles.buttonStyle,
              {
                backgroundColor: colors.activeBottomIcon,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyText style={[appStyles.whiteText, {alignSelf: 'center'}]}>
                {Translate('About Pharmacy')}
              </TinyText>
            ) : (
              <SmallText style={[appStyles.whiteText, {alignSelf: 'center'}]}>
                {Translate('About Pharmacy')}
              </SmallText>
            )}
          </TouchableOpacity>
          <Spacer height={sizes.TinyMargin * 1.5} />
          <View style={{alignSelf: 'flex-start'}}>
            <AirbnbRating
              count={5}
              isDisabled={true}
              defaultRating={
                response && response.average_rating
                  ? response.average_rating
                  : 0
              }
              size={15}
              showRating={false}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          <HeaderSimple
            showSearch
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
            quantity={quantity}
          />
          <KeyboardAwareScrollView>
            {/* <HeaderSimple
              onBackPress={() => navigation.pop()}
              onPress={() => navigation.navigate('Cart')}
            /> */}
            <Spacer height={sizes.TinyMargin} />
            <View
              style={[
                styles.mainViewContainer,
                {backgroundColor: colors.bgLight},
              ]}>
              <View style={styles.bottomContainer}>
                <ProductCard
                  onPress1={() =>
                    navigation.navigate(routes.pharmacyPage, {
                      item: route.params.item,
                    })
                  }
                />
              </View>
              <Spacer height={sizes.smallMargin} />
              <Wrapper
                animation={'fadeIn'}
                style={[styles.filterContainer, appStyles.shadowSmall]}>
                <View>
                  <TinierTitle
                    style={{
                      fontFamily:
                        Platform.OS == 'ios'
                          ? fontFamily.appTextMedium
                          : fontFamily.appTextBold,
                    }}>
                    {response && response.company ? response.company : ''}{' '}
                    Products
                  </TinierTitle>
                  {Platform.OS == 'ios' ? (
                    <TinyText style={{color: colors.textGrey1}}>
                      {listData.length} {Translate('items found')}
                    </TinyText>
                  ) : (
                    <SmallText style={{color: colors.textGrey1}}>
                      {listData.length} {Translate('items found')}
                    </SmallText>
                  )}
                </View>
                {/* <View style={styles.onlyRow}>
                  <View
                    style={[
                      styles.onlyRow,
                      {borderRightWidth: 1, borderColor: colors.borderColor},
                    ]}>
                    <SmallText>Filter</SmallText>
                    <TouchableCustomIcon
                      icon={appImages.filter}
                      size={
                        Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)
                      }
                      style={{marginLeft: width(2), marginRight: width(5)}}
                    />
                  </View>
                  <View style={styles.onlyRow}>
                    <SmallText style={{marginLeft: width(5)}}>Sort</SmallText>
                    <TouchableCustomIcon
                      icon={appImages.sort}
                      size={
                        Platform.OS == 'ios' ? totalSize(1.6) : totalSize(2)
                      }
                      style={{marginLeft: width(2)}}
                    />
                  </View>
                </View> */}
              </Wrapper>
              <Spacer height={sizes.TinyMargin} />
              <FlatList
                data={listData}
                extraData={listData}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                numColumns={2}
                keyExtractor={(contact, index) => String(index)}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <HomeItemCard
                    item={item}
                    LTR={LTR}
                    style={{
                      borderColor: colors.borderLightColor,
                      borderRadius: 5,
                      width: width(43),
                      marginHorizontal: width(1),
                    }}
                    // onPress={() => navigation.navigate(routes.productDetail)}
                    onPress={() =>
                      navigation.navigate(routes.productDetail, {
                        productId: item.product_id,
                      })
                    }
                    addToCartPress={async () => {
                      // if (user.user_id == '0') {
                      //   navigation.navigate(routes.signin);
                      // } else {
                      if (!cartLoading) {
                        setCartLoading(true);
                        await AddToCartMethod({
                          item,
                          user,
                          getCartResponse,
                        });
                      }
                      // }
                    }}
                    onWishPress={async () => {
                      let newList = listData;
                      // console.log('liked = ', newList[index].liked);
                      if (!newList[index].liked) {
                        await AddToWishListMethod({
                          item,
                          user,
                          getWishResponse,
                        });

                        newList[index].liked = true;
                        setListData(newList);
                        // setListData(prev => [...prev]);
                      } else {
                        await RemoveProductFromWLMethod({
                          user_id: user.user_id,
                          product_id: item.product_id,
                          getWishResponse,
                        });
                        newList[index].liked = false;
                        setListData(newList);
                        // setListData(prev => [...prev]);
                      }
                      console.log('liked = ', newList[index].liked);
                      setListData(prev => [...prev]);
                    }}
                  />
                )}
              />
            </View>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin
              }
            />
          </KeyboardAwareScrollView>
          {cartLoading ? (
            <View
              style={{
                position: 'absolute',
                bottom: height(30),
                borderRadius: 5,
                width: totalSize(5),
                height: totalSize(5),
                backgroundColor: 'rgba(255,255,255,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
        </SafeAreaView>
      )}
    </Fragment>
  );
}
