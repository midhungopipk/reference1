import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SearchBarHome,
  CustomIcon,
  SmallText,
  TextInputColored,
  RegularText,
  LoadingCard,
  MediumText,
  TinyText,
  TouchableCustomIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';

/* Api Calls */
import {getAllNearestVendors} from '../../../services/api/vendor.api';
import {store} from '../../../Redux/configureStore';

export default function Pharmacies({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(true);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const markers = useState([
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1246,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1247,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1244,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1245,
      },
    },
  ]);
  const [response, setResponse] = useState([]);
  const [vendorList, setVendorList] = useState([]);
  const mapRef = useRef(null);
  const user = store.getState().user.user.user;
  const [quantity, setQuantity] = useState(0);

  useEffect(() => {
    setLoading(true);
    getAllNearestVendors(user.user_id, getVendorResponse, setLoading);
    setQuantity(
      store.getState().cart.data && store.getState().cart.data.data
        ? store.getState().cart.data.data
        : 0,
    );
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      setQuantity(
        store.getState().cart.data && store.getState().cart.data.data
          ? store.getState().cart.data.data
          : 0,
      );
    });
    return unsubscribe;
  }, [navigation]);

  //Get Vendor API response
  const getVendorResponse = async res => {
    console.log('Vendor nearest API: ', res);
    if (res) {
      setResponse(res);
      if (res.vendors && res.vendors.length) {
        setVendorList(res.vendors);
        let region = {
          latitude: Number(res.vendors[0].latitude),
          longitude: Number(res.vendors[0].longitude),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        };
        SetRegion(region);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Get Current Position from map
  const GetCurrentPosition = async () => {
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                console.log(error.message);
                // Alert.alert(error.message);
              }
              break;
            default:
              console.log(error.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      console.log(e.message || '');
      // alert(e.message || '');
    }
  };

  //Set Region after getting Position
  const SetRegion = region => {
    setRegion(region);
    // setTimeout(() => {
    //   // if (
    //   mapRef.current.animateToRegion(
    //     {
    //       latitude: region.latitude,
    //       longitude: region.longitude,
    //       latitudeDelta: 0.005, //0.07
    //       longitudeDelta: 0.005, //0.0481
    //     },
    //     3000,
    //   );
    //   // );
    // }, 15);
    // Animate();
  };

  //Animate Map to the Region and Position
  const Animate = async () => {
    setTimeout(() => {
      mapRef.current.animateToRegion(
        {
          latitude: region.latitude,
          longitude: region.longitude,
          latitudeDelta: 0.005, //0.07
          longitudeDelta: 0.005, //0.0481
        },
        3000,
      );
    }, 15);
  };

  //Product card component
  const ProductCard = ({item, onPress, onPress1, onPress2}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View
          style={[
            appStyles.center,
            {
              flex: 1,
              borderRightWidth: 0.5,
              borderColor: colors.borderLightColor,
              // paddingVertical: height(2),
            },
          ]}>
          <CustomIcon
            icon={
              item.image_url
                ? {uri: item.image_url}
                : appImages.placeHolderProduct
            }
            size={Platform.OS == 'ios' ? totalSize(13) : totalSize(15)}
          />
        </View>
        <View style={{flex: 1, paddingLeft: width(4)}}>
          {Platform.OS == 'ios' ? (
            <RegularText style={{fontFamily: fontFamily.appTextMedium}}>
              {item.company}
            </RegularText>
          ) : (
            <MediumText style={{fontFamily: fontFamily.appTextMedium}}>
              {item.company}
            </MediumText>
          )}
          {Platform.OS == 'ios' ? (
            <TinyText style={{color: colors.textGrey2}}>
              {item.company_subtitle}
            </TinyText>
          ) : (
            <SmallText style={{color: colors.textGrey2}}>
              {item.company_subtitle}
            </SmallText>
          )}
          <Spacer height={sizes.smallMargin} />
          <TouchableOpacity
            onPress={onPress1}
            style={[
              styles.buttonStyle,
              {
                backgroundColor: colors.activeBottomIcon,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyText style={appStyles.whiteText}>
                {Translate('Pharmacy Products')}
              </TinyText>
            ) : (
              <SmallText style={appStyles.whiteText}>
                {Translate('Pharmacy Products')}
              </SmallText>
            )}
          </TouchableOpacity>
          <Spacer height={sizes.TinyMargin * 1.5} />
          <TouchableOpacity
            onPress={onPress}
            style={[
              styles.buttonStyle,
              {
                backgroundColor: colors.greenLighter,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyText style={{color: colors.buttonLightText}}>
                {/* Show All Branches */}
                {Translate('Pharmacy Detail')}
              </TinyText>
            ) : (
              <SmallText style={{color: colors.buttonLightText}}>
                {Translate('Pharmacy Detail')}
              </SmallText>
            )}
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLight}]}>
          {/* <View style={styles.map}>
            <MapView
              ref={mapRef}
              provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              zoomEnabled
              // region={region}
              initialRegion={region}>
              {vendorList.map((item, index) => {
                return (
                  <Marker
                    key={index}
                    coordinate={{
                      latitude: Number(item.latitude),
                      longitude: Number(item.longitude),
                    }}
                    title={item.company}
                    // onCalloutPress={() =>
                    //   this.handleOpenProfile(item, key)
                    // }
                  >
                    <CustomIcon icon={appImages.dwaaeMarker} size={50} />
                  </Marker>
                );
              })}
            </MapView>
          </View> */}

          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            showSearch
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
            quantity={quantity}
          />
          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />

          <KeyboardAwareScrollView>
            {/* <KeyboardAwareScrollView
            style={{marginTop: Platform.OS == 'ios' ? height(33) : height(28)}}> */}
            <View
              style={[
                appStyles.shadowSmall,
                styles.row,
                {
                  width: width(100),
                  paddingVertical: height(2),
                  borderWidth: 0.5,
                  borderColor: colors.borderLightColor,
                  backgroundColor: colors.snow,
                  paddingHorizontal: width(4),
                  // marginTop: Platform.OS == 'ios' ? height(30) : height(28),
                },
              ]}>
              <View>
                {Platform.OS == 'ios' ? (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('24/7 Pharmacies')}
                  </MediumText>
                ) : (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('24/7 Pharmacies')}
                  </MediumText>
                )}
                {Platform.OS == 'ios' ? (
                  <TinyText
                    style={{
                      fontFamily: fontFamily.appTextLight,
                      color: colors.textGrey2,
                    }}>
                    {vendorList.length} {Translate('Pharmacies Open Now')}
                  </TinyText>
                ) : (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextLight,
                      color: colors.textGrey2,
                    }}>
                    {vendorList.length} {Translate('Pharmacies Open Now')}
                  </SmallText>
                )}
              </View>
              {/* <View style={styles.onlyRow}>
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.textGrey,
                      marginRight: width(2),
                    }}>
                    Sort
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      marginRight: width(2),
                    }}>
                    Sort
                  </RegularText>
                )}
                <TouchableCustomIcon
                  icon={appImages.sort}
                  size={totalSize(2)}
                />
              </View> */}
            </View>
            <View style={styles.bottomContainer}>
              <FlatList
                data={vendorList}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                numColumns={1}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(contact, index) => String(index)}
                renderItem={({index, item}) => (
                  <ProductCard
                    item={item}
                    onPress={() =>
                      navigation.navigate(routes.pharmacyDetail, {
                        item: item.company_id,
                      })
                    }
                    onPress1={() =>
                      navigation.navigate(routes.pharmacyProducts, {
                        item: item.company_id,
                      })
                    }
                    // onPress2={() => navigation.navigate(routes.allBranches)}
                  />
                )}
              />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
