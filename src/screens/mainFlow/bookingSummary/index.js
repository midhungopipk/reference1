import React, {Fragment, useState, useEffect, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform, FlatList} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  TinyTitle,
  HeaderSimple,
  CustomIcon,
  SmallText,
  TinierTitle,
  ButtonColored,
  RegularText,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function BookingSummary({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [testDetails, setTestDetails] = useState([
    {text: 'Test Type:  Blood Test'},
    {text: 'Patient Name:  Ali Mohammad Ahmed'},
    {text: 'Emirates ID Number:  785-1990-125545-8'},
    {text: 'Test Date:  22/02/2021'},
    {text: 'Preferable Test Time: 08:00 AM - 12:00 PM'},
    {text: 'Laboratory:  Accuracy Plus Medical Laboratory'},
  ]);

  const TextLine = props => {
    return (
      <View>
        {Platform.OS == 'ios' ? (
          <SmallText style={styles.textStyle}>{props.text}</SmallText>
        ) : (
          <RegularText style={styles.textStyle}>{props.text}</RegularText>
        )}
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />

          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          <View style={styles.row}>
            {Platform.OS == 'ios' ? (
              <TinierTitle style={{marginLeft: width(4)}}>
                Booking Summary
              </TinierTitle>
            ) : (
              <TinyTitle style={{marginLeft: width(4)}}>
                Booking Summary
              </TinyTitle>
            )}
            <View style={[styles.onlyRow, {marginRight: width(4)}]}>
              <SmallText
                style={{color: colors.statusBarColor, marginRight: width(2)}}>
                Download Summary
              </SmallText>
              <CustomIcon
                icon={appImages.downloadPrescription}
                size={Platform.OS == 'ios' ? totalSize(3.5) : totalSize(4)}
              />
            </View>
          </View>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />

          <View style={[appStyles.shadowSmall, styles.bookingSummaryContainer]}>
            <FlatList
              data={testDetails}
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <TextLine key={index} text={item.text} />
              )}
            />
          </View>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>Test Price</TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>Test Price</TinyTitle>
          )}
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin * 1.3
            }
          />

          <View
            style={[
              appStyles.shadowSmall,
              {
                width: width(92),
                // borderRadius: 10,
                borderWidth: 0.5,
                borderColor: colors.borderLightColor,
                alignSelf: 'center',
              },
            ]}>
            <View
              style={[
                styles.row,
                styles.innerText,
                {
                  backgroundColor: colors.greenLighter,
                },
              ]}>
              {Platform.OS == 'ios' ? (
                <SmallText>Service</SmallText>
              ) : (
                <RegularText>Service</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>Blood Test</SmallText>
              ) : (
                <RegularText>Blood Test</RegularText>
              )}
            </View>
            <View style={[styles.row, styles.innerText]}>
              {Platform.OS == 'ios' ? (
                <SmallText>Blood Test</SmallText>
              ) : (
                <RegularText>Blood Test</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>AED 370</SmallText>
              ) : (
                <RegularText>AED 370</RegularText>
              )}
            </View>
            <View style={[styles.row, styles.innerText]}>
              <View />
              {Platform.OS == 'ios' ? (
                <SmallText>Sub Total AED 370</SmallText>
              ) : (
                <RegularText>Sub Total AED 370</RegularText>
              )}
            </View>
            <View style={[styles.row, styles.innerText]}>
              <View />
              {Platform.OS == 'ios' ? (
                <SmallText>Total AED 370</SmallText>
              ) : (
                <RegularText>Total AED 370</RegularText>
              )}
            </View>
            <View
              style={[
                styles.row,
                {
                  paddingHorizontal: width(2),
                  paddingVertical: height(1),
                },
              ]}>
              <View />
              {Platform.OS == 'ios' ? (
                <SmallText>Advance AED 200</SmallText>
              ) : (
                <RegularText>Advance AED 200</RegularText>
              )}
            </View>
          </View>

          <Spacer
            height={
              Platform.OS == 'ios'
                ? sizes.doubleBaseMargin
                : sizes.baseMargin * 1.5
            }
          />
          <ButtonColored
            text={'PAY ADVANCE (AED 200)'}
            // onPress={() => navigation.navigate(routes.selectTimeNDate)}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              borderRadius: 5,
            }}
          />
          <Spacer height={sizes.doubleBaseMargin} />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
