import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  TouchableOpacity,
  Platform,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import {AirbnbRating} from 'react-native-elements';

let LATITUDE = 51.4724;
let LONGITUDE = 0.4505;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = 0.005;

import {
  Spacer,
  HeaderSimple,
  CustomIcon,
  SmallText,
  RegularText,
  MediumText,
  TinyText,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';
/* Api Calls */
import {getVendorDetail} from '../../../services/api/vendor.api';
import {store} from '../../../Redux/configureStore';

export default function PharmacyDetail({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [region, setRegion] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [cooards, setCooards] = useState({
    latitude: 51.5347,
    longitude: 0.1246,
  });
  const markers = useState([
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5347,
        longitude: 0.1246,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1247,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1244,
      },
    },
    {
      title: 'London kings cross rank 1',
      description: 'waiting time: 10 mins',
      cooards: {
        latitude: 51.5456,
        longitude: 0.1245,
      },
    },
  ]);
  const mapRef = useRef(null);
  const [response, setResponse] = useState([]);
  const user = store.getState().user.user.user;
  const [paramData, setParamData] = useState(route.params.item);

  useEffect(() => {
    setLoading(true);
    getVendorDetail(
      user.user_id,
      // paramData.company_id,
      route.params.item,
      getVendorResponse,
      setLoading,
    );
  }, [user]);

  // useEffect(() => {
  //   setCooards({
  //     latitude: Number(route.params.item.latitude),
  //     longitude: Number(route.params.item.longitude),
  //   });
  // }, [route.params.item]);

  //Get Vendors API Response
  const getVendorResponse = async res => {
    console.log('Vendor Detail API: ', res);
    if (res) {
      setResponse(res.vendor);
      setParamData(res.vendor);
      setCooards({
        latitude: Number(res.vendor.latitude),
        longitude: Number(res.vendor.longitude),
      });
      let region = {
        latitude: Number(res.vendor.latitude),
        longitude: Number(res.vendor.longitude),
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      };
      SetRegion(region);
      setLoading(false);
    }
    setLoading(false);
  };

  //Get Current User Position
  const GetCurrentPosition = async () => {
    let newPosition = {
      latitude: 51.5347,
      longitude: 0.1246,
    };
    try {
      Geolocation.getCurrentPosition(
        async position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: 0.005,
          };
          SetRegion(region);
        },
        error => {
          console.log(error.message);
          switch (error.code) {
            case 1:
              if (Platform.OS === 'ios') {
                // Alert.alert(error.message);
              } else {
                console.log(error.message);
                // Alert.alert(error.message);
              }
              break;
            default:
              console.log(error.message);
            // Alert.alert(error.message);
          }
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (e) {
      console.log(e.message || '');
      // alert(e.message || '');
    }
  };

  //Set Region of Current Position
  const SetRegion = region => {
    setRegion(region);
    setTimeout(() => {
      // if (
      mapRef.current.animateToRegion(
        {
          latitude: Number(region.latitude),
          longitude: Number(region.longitude),
          latitudeDelta: 0.005, //0.07
          longitudeDelta: 0.005, //0.0481
        },
        3000,
      );
      // );
    }, 15);
    // Animate();
  };

  //Animate to Position
  const Animate = async () => {
    setTimeout(() => {
      mapRef.current.animateToRegion(
        {
          latitude: region.latitude,
          longitude: region.longitude,
          latitudeDelta: 0.005, //0.07
          longitudeDelta: 0.005, //0.0481
        },
        3000,
      );
    }, 15);
  };

  //Product Card Component
  const ProductCard = ({onPress, onPress1, onPress2}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View
          style={[
            appStyles.center,
            {
              flex: 1,
              borderRightWidth: 0.5,
              borderColor: colors.borderLightColor,
              //   paddingVertical: height(1),
            },
          ]}>
          <CustomIcon
            icon={
              response && response.image_url != ''
                ? {uri: response.image_url}
                : appImages.logoFull
            }
            size={Platform.OS == 'ios' ? totalSize(13) : totalSize(15)}
          />
        </View>
        <View style={{flex: 1, paddingLeft: width(4)}}>
          {Platform.OS == 'ios' ? (
            <RegularText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </RegularText>
          ) : (
            <MediumText style={{fontFamily: fontFamily.appTextMedium}}>
              {response && response.company ? response.company : ''}
            </MediumText>
          )}
          {Platform.OS == 'ios' ? (
            <TinyText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </TinyText>
          ) : (
            <SmallText style={{color: colors.textGrey2}}>
              {response && response.company_subtitle
                ? response.company_subtitle
                : ''}
            </SmallText>
          )}
          <Spacer height={sizes.smallMargin} />
          <TouchableOpacity
            onPress={onPress1}
            style={[
              styles.buttonStyle,
              {
                backgroundColor: colors.activeBottomIcon,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <TinyText style={appStyles.whiteText}>
                {Translate('Pharmacy Products')}
              </TinyText>
            ) : (
              <SmallText style={appStyles.whiteText}>
                {Translate('Pharmacy Products')}
              </SmallText>
            )}
          </TouchableOpacity>
          <Spacer height={sizes.TinyMargin * 1.5} />
          <View style={{alignSelf: 'flex-start'}}>
            <AirbnbRating
              count={5}
              isDisabled={true}
              defaultRating={
                response && response.average_rating
                  ? response.average_rating
                  : 0
              }
              size={15}
              showRating={false}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLight}]}>
          <View style={styles.map}>
            <MapView
              ref={mapRef}
              provider={Platform.OS == 'ios' ? null : PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              zoomEnabled
              // region={region}
              initialRegion={region}>
              {/* {markers.map((item, key) => {
                return ( */}
              <Marker
                // key={key}
                coordinate={cooards}
                title={response && response.company ? response.company : ''}
                // onCalloutPress={() =>
                //   this.handleOpenProfile(item, key)
                // }
              >
                <CustomIcon icon={appImages.dwaaeMarker} size={50} />
              </Marker>
              {/* );
              })} */}
            </MapView>
          </View>

          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />
          {/* <View style={{width: width(100)}}>
          <SearchBarHome
            placeholder="Search your location"
            containerStyle={appStyles.shadowSmall}
            customIconLeft={appImages.placeMarker}
          />
        </View> */}
          <KeyboardAwareScrollView
            style={{marginTop: Platform.OS == 'ios' ? height(32) : height(28)}}>
            <View
              style={[
                appStyles.shadowSmall,
                styles.row,
                {
                  width: width(100),
                  paddingVertical: height(2),
                  borderWidth: 0.5,
                  borderColor: colors.borderLightColor,
                  backgroundColor: colors.snow,
                  paddingHorizontal: width(4),
                },
              ]}>
              <View>
                {Platform.OS == 'ios' ? (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {response && response.company ? response.company : ''}
                  </MediumText>
                ) : (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {response && response.company ? response.company : ''}
                  </MediumText>
                )}
                <Spacer height={sizes.TinyMargin} />
                <View style={[styles.onlyRow]}>
                  <CustomIcon icon={appImages.cursor} size={totalSize(1.3)} />
                  {Platform.OS == 'ios' ? (
                    <TinyText
                      style={{
                        fontFamily: fontFamily.appTextLight,
                        color: colors.textGrey2,
                        marginLeft: width(1),
                      }}>
                      {response && response.distance ? response.distance : 0} KM
                      {Translate('away from your location')}
                    </TinyText>
                  ) : (
                    <SmallText
                      style={{
                        fontFamily: fontFamily.appTextLight,
                        color: colors.textGrey2,
                        marginLeft: width(1),
                      }}>
                      {response && response.distance ? response.distance : 0} KM
                      {Translate('away from your location')}
                    </SmallText>
                  )}
                </View>
              </View>
            </View>
            <View style={styles.bottomContainer}>
              <ProductCard
                onPress1={() =>
                  navigation.navigate(routes.pharmacyProducts, {
                    item: route.params.item,
                  })
                }
              />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
