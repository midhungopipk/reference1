import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TinierTitle,
  TinyTitle,
  TextInputColored,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {TouchableOpacity} from 'react-native';

export default function QuotationRequest({navigation, route}) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {}, []);

  const ProductCard = props => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={[appStyles.center, {flex: 0.15}]}>
          <TouchableCustomIcon icon={appImages.addIcon} size={totalSize(3)} />
        </View>
        <View style={{flex: 0.7}}>
          {Platform.OS == 'ios' ? (
            <SmallText style={{color: colors.textGrey}}>{props.text}</SmallText>
          ) : (
            <RegularText style={{color: colors.textGrey}}>
              {props.text}
            </RegularText>
          )}
        </View>
        <View style={[appStyles.center, {flex: 0.15}]}>
          <TouchableCustomIcon icon={appImages.crossIcon} size={totalSize(3)} />
        </View>
      </View>
    );
  };
  const AddProductCard = props => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
          {
            flexDirection: 'column',
            paddingHorizontal: width(4),
            alignItems: 'flex-start',
          },
        ]}>
        {Platform.OS == 'ios' ? (
          <SmallText
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            Product Name
          </SmallText>
        ) : (
          <RegularText
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            Product Name
          </RegularText>
        )}
        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={styles.fieldStyle}
          placeholder={'Candes Eclat Face Tonic'}
        />
        <Spacer height={sizes.baseMargin} />
        <View style={[styles.row]}>
          <View style={{flex: 1, marginRight: width(1.5)}}>
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Quantity
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Quantity
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'200'}
            />
          </View>
          <View style={{flex: 1, marginLeft: width(1.5)}}>
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Unit
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Unit
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
              placeholder={'Milimeters'}
              customRightIcon={appImages.arrowBottom}
            />
          </View>
        </View>
        <Spacer height={sizes.baseMargin} />
        {Platform.OS == 'ios' ? (
          <SmallText
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            Product Description
          </SmallText>
        ) : (
          <RegularText
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            Product Description
          </RegularText>
        )}

        <Spacer height={sizes.smallMargin} />
        <TextInputColored
          containerStyle={styles.inputfieldStyle}
          fieldStyle={[
            styles.fieldStyle,
            {
              height: Platform.OS == 'ios' ? height(10) : height(12),
              textAlignVertical: 'top',
            },
          ]}
          multiline
          placeholder={'Type any extra details'}
        />
        <Spacer height={sizes.baseMargin} />
        <TouchableOpacity style={styles.saveButton}>
          <SmallText style={appStyles.whiteText}>Save</SmallText>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />

        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <TinierTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Tell us what you need
              </TinierTitle>
            ) : (
              <TinyTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Tell us what you need
              </TinyTitle>
            )}
            <Spacer height={sizes.smallMargin} />

            <ProductCard
              text={'Candes Eclat Fresh Lightening Face Tonic 200 Millilitre'}
            />
            <ProductCard
              text={'Candes Eclat Fresh Lightening Face Tonic 200 Millilitre'}
            />
            <AddProductCard />
            <ProductCard
              text={'Candes Eclat Fresh Lightening Face Tonic 200 Millilitre'}
            />
            <ProductCard
              text={'Candes Eclat Fresh Lightening Face Tonic 200 Millilitre'}
            />
            <Spacer height={sizes.baseMargin} />
            <TouchableOpacity style={styles.addProduct}>
              <SmallText style={[appStyles.whiteText, {marginRight: width(1)}]}>
                Add Product
              </SmallText>
              <CustomIcon icon={appImages.plusCircle} size={totalSize(1.5)} />
            </TouchableOpacity>
            <Spacer height={sizes.doubleBaseMargin} />
            <ButtonColored
              text={'CONTINUE'}
              onPress={() =>
                navigation.navigate(routes.quotationRequestDelivery)
              }
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
