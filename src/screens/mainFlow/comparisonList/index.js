import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  ComparisonItemCard,
  HeaderSimple,
  RegularText,
  MediumText,
  TinierTitle,
  TinyTitle,
  ButtonColored,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';

export default function ComparisonList({navigation, route}) {
  const [loading, setLoading] = useState(true);

  const ProductCard = ({onPress, onPress1, onPress2}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View
          style={[
            {
              flex: 1,
              borderRightWidth: 1,
              borderColor: colors.borderLightColor,
            },
          ]}>
          <ComparisonItemCard
            item={{
              chart: appImages.chart,
              like: appImages.like,
              titl1: 'Candes Eclat Fresh',
              title2: 'Lightening Face Tonic',
              quantity: '200 mL',
              price: '69.00',
              img: appImages.product1,
            }}
          />
        </View>
        <View style={{flex: 1}}>
          <ComparisonItemCard
            item={{
              chart: appImages.chartFill,
              like: appImages.likeFill,
              titl1: 'Candes Eclat Fresh',
              title2: 'Lightening Face Tonic',
              quantity: '200 mL',
              price: '69.00',
              img: appImages.product2,
            }}
          />
        </View>
      </View>
    );
  };
  const ComparisonCard = ({header, leftText, rightText}) => {
    return (
      <View>
        {Platform.OS == 'ios' ? (
          <TinierTitle
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            {header}
          </TinierTitle>
        ) : (
          <TinyTitle
            style={{
              fontFamily: fontFamily.appTextBold,
            }}>
            {header}
          </TinyTitle>
        )}
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View
            style={[
              styles.textContainer,
              {
                borderRightWidth: 1,
                borderColor: colors.borderLightColor,
              },
            ]}>
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>{leftText}</SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>{leftText}</RegularText>
            )}
          </View>
          <View style={styles.textContainer}>
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>{rightText}</SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>{rightText}</RegularText>
            )}
          </View>
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container]}>
        <KeyboardAwareScrollView>
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                },
              ]}>
              <MediumText
                style={[
                  appStyles.whiteText,
                  {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                ]}>
                Comparison List
              </MediumText>

              <SmallText
                style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                Compare between two products features
              </SmallText>
            </View>
          </LinearGradient>
          <Spacer height={sizes.TinyMargin} />
          <View
            style={[
              styles.mainViewContainer,
              {backgroundColor: colors.bgLight},
            ]}>
            <View style={styles.bottomContainer}>
              <ProductCard />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Manufacturer'}
                leftText={'Galderma'}
                rightText={'Acm Laboratories'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Net Volume'}
                leftText={'225 G'}
                rightText={'250 ML'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Age Group'}
                leftText={'Adults'}
                rightText={'Adults'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Gender'}
                leftText={'Unisex'}
                rightText={'Unisex'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Product Form'}
                leftText={'Lotion'}
                rightText={'Lotion'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Storage Room'}
                leftText={'Room Temperature'}
                rightText={'Room Temperature'}
              />
              <Spacer height={sizes.baseMargin} />
              <ComparisonCard
                header={'Brands'}
                leftText={'Cetaphil'}
                rightText={'Acm'}
              />
            </View>
            <Spacer height={sizes.doubleBaseMargin} />
            <ButtonColored
              onPress={() => navigation.navigate('Home')}
              text={'CONTINUE SHOPPING'}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
          </View>
          <Spacer height={sizes.doubleBaseMargin} />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
