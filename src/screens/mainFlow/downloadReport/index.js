import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TinierTitle,
  TinyTitle,
  TextInputColored,
  RegularText,
} from '../../../components';
import {fontFamily, routes, sizes} from '../../../services';

export default function DownloadReport({navigation, route}) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {}, []);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />

        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <TinierTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Covid PCR Result
              </TinierTitle>
            ) : (
              <TinyTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Covid PCR Result
              </TinyTitle>
            )}
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Emirates ID Number / Passport Number
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Emirates ID Number / Passport Number
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
            />
            <Spacer height={sizes.baseMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Mobile Number
              </SmallText>
            ) : (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Mobile Number
              </RegularText>
            )}
            <Spacer height={sizes.smallMargin} />
            <TextInputColored
              containerStyle={styles.inputfieldStyle}
              fieldStyle={styles.fieldStyle}
            />
            <Spacer
              height={
                Platform.OS == 'ios'
                  ? sizes.baseMargin * 1.5
                  : sizes.baseMargin * 1.8
              }
            />
          </View>
        </KeyboardAwareScrollView>
        <View
          style={{width: width(100), position: 'absolute', bottom: height(4)}}>
          <ButtonColored
            text={'SHOW REPORT'}
            // onPress={() => navigation.navigate(routes.addAddress)}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: 5,
            }}
          />
        </View>
      </SafeAreaView>
    </Fragment>
  );
}
