import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  FlatList,
  View,
  Platform,
  TouchableOpacity,
  ImageBackground,
  I18nManager,
  Alert,
  Linking,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
  TinyText,
  MediumText,
  LoadingCard,
  BackIcon,
  Wrapper,
  LineHorizontal,
  ImageProfile,
  VersionModal,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  storageConst,
  sizes,
} from '../../../services';
import AsyncStorage from '@react-native-community/async-storage';
import {store} from '../../../Redux/configureStore';
import {Translate} from '../../../services/helpingMethods';
import Toast from 'react-native-simple-toast';
import RNRestart from 'react-native-restart';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import {fetchToken, fetchLanguage} from '../../../Redux/Actions';
import {useDispatch} from 'react-redux';
import {GetUserData} from '../../../services/api/user.api';
import {fetchUser} from '../../../Redux/Actions/index';
import FastImage from 'react-native-fast-image';
import {getHomeData} from '../../../services/api/home.api';

const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  ar: () => require('../../../assets/translation/ar.json'),
  en: () => require('../../../assets/translation/en.json'),
};

export default function Account({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);
  const [accountList, setAccountList] = useState([
    {
      title: Translate('My Orders'),
      image: appImages.myOrders,
      route: routes.myOrders,
    },
    {
      title: Translate('Track Order'),
      image: appImages.trackOrder,
      route: routes.trackOrder,
    },
    {
      title: Translate('Quotations'),
      image: appImages.prescriptionOrders,
      route: routes.prescriptions,
    },
    {
      title: Translate('Wish List'),
      image: appImages.wishList,
      route: routes.wishList,
    },
    // {
    //   title: Translate('My Points'),
    //   image: appImages.myPoints,
    //   route: routes.myPoints,
    // },
    // {
    //   title: Translate('Quotations'),
    //   image: appImages.quotations,
    //   route: routes.quotationsProfile,
    // },
    // {
    //   title: Translate('Request For Quotation'),
    //   image: appImages.requestForQuotations,
    //   route: routes.quotationRequest,
    // },
    // {
    //   title: Translate('Upload Prescription'),
    //   image: appImages.uploadPrescriptionProfile,
    //   route: routes.uploadPrescription,
    // },
    // {
    //   title: Translate('Comparison List'),
    //   image: appImages.comparisonList,
    //   route: routes.comparisonList,
    // },
  ]);
  const [guest, setGuest] = useState(false);
  const [userData, setUserData] = useState(null);
  const [language, setLanguage] = useState('English');
  const [versionVisible, setVersionVisible] = useState(false);
  const [LTR, setLTR] = useState(true);
  const dispatch = useDispatch();

  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  console.log('user: ', user);

  useEffect(async () => {
    setLoading(true);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    getHomeData(user.user_id, getHomeResponse, setLoading);
    await AsyncStorage.getItem(storageConst.guest).then(async value => {
      if (value == '1') {
        console.log('Logged in as Guest');
        setGuest(true);
      } else {
        console.log('Logged in as User');
        setGuest(false);
        GetUserData(user.user_id, onGetUserDataResponse);
      }
    });
    await AsyncStorage.getItem(storageConst.language).then(async value => {
      console.log(value);
      if (!value) {
        AsyncStorage.setItem(storageConst.language, 'English');
      } else {
        if (value === 'en') {
          setLanguage('English');
        } else {
          setLanguage('Arabic');
        }
      }
    });
  }, [user]);

  const onGetUserDataResponse = async res => {
    if (res.success) {
      console.log('User Data api response: ', res.profile);
      setUserData(res.profile);
      setUserData({...res.profile});
      await AsyncStorage.getItem(storageConst.userData).then(async data => {
        if (data) {
          let oldData = JSON.parse(data);
          oldData.firstname = res.profile.firstname;
          oldData.lastname = res.profile.lastname;
          oldData.email = res.profile.email;
          oldData.phone = res.profile.phone;
          oldData.url = res.profile.image_profile;
          await store.dispatch(
            fetchUser({
              user: oldData,
            }),
          );
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(oldData),
          );
        }
      });
      setDataLoading(false);
    } else {
      setDataLoading(false);
    }
    setLoading(false);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      await AsyncStorage.getItem(storageConst.language).then(async value => {
        console.log(value);
        if (!value) {
          AsyncStorage.setItem(storageConst.language, 'English');
        } else {
          if (value === 'en') {
            setLanguage('English');
          } else {
            setLanguage('Arabic');
          }
        }
      });
      getHomeData(user.user_id, getHomeResponse, setLoading);
      setLoading(true);
      GetUserData(user.user_id, onGetUserDataResponse);
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation, user]);

  const getHomeResponse = async res => {
    if (res) {
      if (res.api_version && sizes.current_version) {
        if (res.api_version != sizes.current_version) {
          console.log(res.api_version);
          setVersionVisible(true);
        } else {
          setVersionVisible(false);
        }
      }
    }
  };

  const onSaveLanguage = async () => {
    if (language == 'Arabic') {
      setI18nConfig('en');
      Toast.show('Language has been changed to English');
      await AsyncStorage.setItem(storageConst.language, 'en');
      RNRestart.Restart();
    } else if (language == 'English') {
      // console.warn('Language changed to germarn');
      setI18nConfig('ar');
      Toast.show('تم تغيير اللغة إلى العربية');
      await AsyncStorage.setItem(storageConst.language, 'ar');
      RNRestart.Restart();
    }
  };

  const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key),
  );

  const setI18nConfig = lang => {
    // fallback if no available language fits
    const fallback = {languageTag: lang == 'en' ? 'en' : 'ar'};

    const {languageTag, isRTL} =
      RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
      fallback;

    // clear translation cache
    translate.cache.clear();
    // update layout direction
    I18nManager.forceRTL(isRTL);
    // set i18n-js config
    i18n.translations = {[languageTag]: translationGetters[languageTag]()};
    i18n.locale = languageTag;
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <View style={{backgroundColor: colors.gradient1}}>
          <KeyboardAwareScrollView>
            <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
            <ImageBackground
              source={appImages.profileBgCard}
              imageStyle={styles.imageStyleBg}
              style={{alignItems: 'center'}}>
              <Spacer
                height={
                  Platform.OS == 'ios'
                    ? sizes.doubleBaseMargin * 1.5
                    : sizes.doubleBaseMargin
                }
              />
              <TouchableOpacity
                style={appStyles.center}
                onPress={
                  () => {
                    if (!guest) {
                      navigation.navigate(routes.editProfile);
                    }
                  }
                  // guest ? null : navigation.navigate(routes.editProfile)
                }>
                {/* <View style={styles.profilePictureContainer}>
                  {guest ? (
                    <CustomIcon
                      icon={appImages.userProfile}
                      size={totalSize(5)}
                    />
                  ) : userData &&
                    userData.image_profile &&
                    userData.image_profile != '' ? (
                    <CustomIcon
                      icon={{
                        uri:
                          userData.image_profile + '?' + new Date().getTime(),
                      }}
                      size={totalSize(5)}
                    />
                  ) : (
                    <CustomIcon
                      icon={appImages.userProfile}
                      size={totalSize(5)}
                    />
                  )}
                </View> */}

                <ImageProfile
                  source={
                    guest
                      ? appImages.dummy
                      : userData &&
                        userData.image_profile &&
                        userData.image_profile != ''
                      ? {
                          uri:
                            userData.image_profile + '?' + new Date().getTime(),
                        }
                      : appImages.dummy
                  }
                  containerStyle={{alignSelf: 'center'}}
                  onPress={() => {
                    if (!guest) {
                      navigation.navigate(routes.editProfile);
                    }
                  }}
                  imageStyle={{
                    width: totalSize(9),
                    height: totalSize(9),
                  }}
                  // isLoading={profileImageloading}
                />

                <Spacer height={sizes.smallMargin} />
                {Platform.OS == 'ios' ? (
                  <RegularText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.snow,
                    }}>
                    {userData && userData.firstname
                      ? userData.firstname + ' ' + userData.lastname
                      : 'Guest User'}
                  </RegularText>
                ) : (
                  <MediumText
                    style={{
                      fontFamily: fontFamily.appTextBold,
                      color: colors.snow,
                    }}>
                    {userData && userData.firstname
                      ? userData.firstname + ' ' + userData.lastname
                      : 'Guest User'}
                  </MediumText>
                )}
                <Spacer height={sizes.TinyMargin} />
                {Platform.OS == 'ios' ? (
                  <SmallText
                    style={{
                      color: colors.bgLight,
                    }}>
                    {userData && userData.email ? userData.email : ''}
                  </SmallText>
                ) : (
                  <RegularText
                    style={{
                      color: colors.bgLight,
                    }}>
                    {userData && userData.email ? userData.email : ''}
                  </RegularText>
                )}
              </TouchableOpacity>

              <Spacer height={sizes.smallMargin} />
            </ImageBackground>

            <View
              style={[
                styles.bottomContainer,
                {
                  borderTopLeftRadius: totalSize(5),
                  borderTopRightRadius: totalSize(5),
                },
              ]}>
              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    marginHorizontal: width(4),
                    alignSelf: LTR ? 'flex-start' : 'flex-end',
                  }}>
                  {Translate('My Account')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    marginHorizontal: width(4),
                    alignSelf: LTR ? 'flex-start' : 'flex-end',
                  }}>
                  {Translate('My Account')}
                </MediumText>
              )}
              <Spacer height={sizes.baseMargin} />
              <FlatList
                data={accountList}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                keyExtractor={(contact, index) => String(index)}
                numColumns={3}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <Wrapper key={index}>
                    <TouchableOpacity
                      style={[appStyles.shadowSmall, styles.box]}
                      onPress={() => navigation.navigate(item.route)}>
                      <CustomIcon icon={item.image} size={totalSize(4)} />
                      <Spacer height={sizes.TinyMargin * 1.5} />
                      {Platform.OS == 'ios' ? (
                        <TinyText>{item.title}</TinyText>
                      ) : (
                        <TinyText>{item.title}</TinyText>
                      )}
                    </TouchableOpacity>
                  </Wrapper>
                )}
              />
              <Spacer height={sizes.baseMargin} />

              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    marginHorizontal: width(4),
                    alignSelf: LTR ? 'flex-start' : 'flex-end',
                  }}>
                  {Translate('Settings')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextBold,
                    marginHorizontal: width(4),
                    alignSelf: LTR ? 'flex-start' : 'flex-end',
                  }}>
                  {Translate('Settings')}
                </MediumText>
              )}
              <Spacer height={sizes.baseMargin} />
              {/* <LineHorizontal height={0.8} /> */}
              {LTR ? (
                <View>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => {
                      onSaveLanguage();
                    }}
                    style={[styles.row, styles.horizontalContainer]}>
                    <View style={styles.onlyRow}>
                      <CustomIcon
                        icon={appImages.language}
                        size={totalSize(2)}
                      />
                      {Platform.OS == 'ios' ? (
                        <SmallText style={styles.textStyle}>
                          {Translate('Language')}
                        </SmallText>
                      ) : (
                        <RegularText style={styles.textStyle}>
                          {Translate('Language')}
                        </RegularText>
                      )}
                    </View>

                    <View style={styles.onlyRow}>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={{
                            fontFamily: fontFamily.appTextBold,
                            marginRight: width(2),
                          }}>
                          {language == 'Arabic' ? 'English' : 'عربى'}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={{
                            fontFamily: fontFamily.appTextBold,
                            marginRight: width(2),
                          }}>
                          {language == 'Arabic' ? 'عربى' : 'English'}
                        </RegularText>
                      )}

                      <View style={{marginLeft: width(2)}}>
                        <TouchableCustomIcon
                          icon={appImages.arrowRight}
                          size={totalSize(4)}
                          onPress={() => {
                            onSaveLanguage();
                          }}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => navigation.navigate(routes.editProfile)}
                    style={[styles.row, styles.horizontalContainer]}>
                    <View style={styles.onlyRow}>
                      <CustomIcon icon={appImages.about} size={totalSize(2)} />
                      {Platform.OS == 'ios' ? (
                        <SmallText style={styles.textStyle}>
                          {Translate('UpdateProfile')}
                        </SmallText>
                      ) : (
                        <RegularText style={styles.textStyle}>
                          {Translate('UpdateProfile')}
                        </RegularText>
                      )}
                    </View>
                    <TouchableCustomIcon
                      icon={appImages.arrowRight}
                      size={totalSize(4)}
                      onPress={() => navigation.navigate(routes.editProfile)}
                    />
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => navigation.navigate(routes.about)}
                    style={[styles.row, styles.horizontalContainer]}>
                    <View style={styles.onlyRow}>
                      <CustomIcon icon={appImages.about} size={totalSize(2)} />
                      {Platform.OS == 'ios' ? (
                        <SmallText style={styles.textStyle}>
                          {Translate('About')}
                        </SmallText>
                      ) : (
                        <RegularText style={styles.textStyle}>
                          {Translate('About')}
                        </RegularText>
                      )}
                    </View>
                    <TouchableCustomIcon
                      icon={appImages.arrowRight}
                      size={totalSize(4)}
                      onPress={() => navigation.navigate(routes.about)}
                    />
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={async () => {
                      if (!guest) {
                        Alert.alert(
                          Translate('LOG OUT'),
                          Translate('Are you sure you want to Log out?'),
                          [
                            {
                              text: Translate('Cancel'),
                              onPress: () => {
                                console.log('Cancel Pressed');
                              },
                              style: 'cancel',
                            },
                            {
                              text: Translate('Confirm'),
                              onPress: async () => {
                                setLoading(true);
                                await AsyncStorage.removeItem(
                                  storageConst.userData,
                                ).then(async () => {
                                  await AsyncStorage.removeItem(
                                    storageConst.userId,
                                  ).then(async () => {
                                    await AsyncStorage.removeItem(
                                      storageConst.guest,
                                    ).then(async () => {
                                      setLoading(false);
                                      Toast.show('You have been Logged out!');
                                      let language = {
                                        language: true,
                                      };
                                      dispatch(fetchLanguage(language));
                                      let token = {
                                        token: false,
                                      };
                                      dispatch(fetchToken(token));
                                    });
                                  });
                                });
                              },
                            },
                          ],
                        );
                      } else {
                        setLoading(true);
                        let language = {
                          language: true,
                        };
                        dispatch(fetchLanguage(language));
                        let token = {
                          token: false,
                        };
                        dispatch(fetchToken(token));
                      }
                    }}
                    style={[styles.row, styles.horizontalContainer]}>
                    <View style={styles.onlyRow}>
                      <CustomIcon icon={appImages.lock} size={totalSize(2)} />
                      {Platform.OS == 'ios' ? (
                        <SmallText style={styles.textStyle}>
                          {guest ? Translate('Sign In') : Translate('Log Out')}
                        </SmallText>
                      ) : (
                        <RegularText style={styles.textStyle}>
                          {guest ? Translate('Sign In') : Translate('Log Out')}
                        </RegularText>
                      )}
                    </View>
                    <TouchableCustomIcon
                      icon={appImages.arrowRight}
                      size={totalSize(4)}
                      onPress={async () => {
                        if (!guest) {
                          Alert.alert(
                            Translate('LOG OUT'),
                            Translate('Are you sure you want to Log out?'),
                            [
                              {
                                text: Translate('Cancel'),
                                onPress: () => {
                                  console.log('Cancel Pressed');
                                },
                                style: 'cancel',
                              },
                              {
                                text: Translate('Confirm'),
                                onPress: async () => {
                                  setLoading(true);
                                  await AsyncStorage.removeItem(
                                    storageConst.userData,
                                  ).then(async () => {
                                    await AsyncStorage.removeItem(
                                      storageConst.userId,
                                    ).then(async () => {
                                      await AsyncStorage.removeItem(
                                        storageConst.guest,
                                      ).then(async () => {
                                        setLoading(false);
                                        Toast.show('You have been Logged out!');
                                        let language = {
                                          language: true,
                                        };
                                        dispatch(fetchLanguage(language));
                                        let token = {
                                          token: false,
                                        };
                                        dispatch(fetchToken(token));
                                      });
                                    });
                                  });
                                },
                              },
                            ],
                          );
                        } else {
                          setLoading(true);
                          let language = {
                            language: true,
                          };
                          dispatch(fetchLanguage(language));
                          let token = {
                            token: false,
                          };
                          dispatch(fetchToken(token));
                        }
                      }}
                    />
                  </TouchableOpacity>
                </View>
              ) : (
                <View>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => {
                      onSaveLanguage();
                    }}
                    style={[styles.row, styles.horizontalContainer]}>
                    <View style={styles.onlyRow}>
                      <View
                      // style={{marginLeft: width(2)}}
                      >
                        <TouchableCustomIcon
                          icon={appImages.arrowLeft}
                          size={totalSize(4)}
                          onPress={() => {
                            onSaveLanguage();
                          }}
                        />
                      </View>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={{
                            fontFamily: fontFamily.appTextBold,
                            marginRight: width(2),
                          }}>
                          {language == 'Arabic' ? 'English' : 'عربى'}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={{
                            fontFamily: fontFamily.appTextBold,
                            marginRight: width(2),
                          }}>
                          {language == 'Arabic' ? 'عربى' : 'English'}
                        </RegularText>
                      )}
                    </View>

                    <View style={styles.onlyRow}>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('Language')}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('Language')}
                        </RegularText>
                      )}
                      <CustomIcon
                        icon={appImages.language}
                        size={totalSize(2)}
                      />
                    </View>
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => navigation.navigate(routes.editProfile)}
                    style={[styles.row, styles.horizontalContainer]}>
                    <TouchableCustomIcon
                      icon={appImages.arrowLeft}
                      size={totalSize(4)}
                      onPress={() => navigation.navigate(routes.editProfile)}
                    />
                    <View style={styles.onlyRow}>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('UpdateProfile')}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('UpdateProfile')}
                        </RegularText>
                      )}
                      <CustomIcon icon={appImages.about} size={totalSize(2)} />
                    </View>
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={() => navigation.navigate(routes.about)}
                    style={[styles.row, styles.horizontalContainer]}>
                    <TouchableCustomIcon
                      icon={appImages.arrowLeft}
                      size={totalSize(4)}
                      onPress={() => navigation.navigate(routes.about)}
                    />
                    <View style={styles.onlyRow}>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('About')}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {Translate('About')}
                        </RegularText>
                      )}
                      <CustomIcon icon={appImages.about} size={totalSize(2)} />
                    </View>
                  </TouchableOpacity>
                  <LineHorizontal height={0.8} />
                  <TouchableOpacity
                    onPress={async () => {
                      if (!guest) {
                        Alert.alert(
                          Translate('LOG OUT'),
                          Translate('Are you sure you want to Log out?'),
                          [
                            {
                              text: Translate('Cancel'),
                              onPress: () => {
                                console.log('Cancel Pressed');
                              },
                              style: 'cancel',
                            },
                            {
                              text: Translate('Confirm'),
                              onPress: async () => {
                                setLoading(true);
                                await AsyncStorage.removeItem(
                                  storageConst.userData,
                                ).then(async () => {
                                  await AsyncStorage.removeItem(
                                    storageConst.userId,
                                  ).then(async () => {
                                    await AsyncStorage.removeItem(
                                      storageConst.guest,
                                    ).then(async () => {
                                      setLoading(false);
                                      Toast.show('You have been Logged out!');
                                      let language = {
                                        language: true,
                                      };
                                      dispatch(fetchLanguage(language));
                                      let token = {
                                        token: false,
                                      };
                                      dispatch(fetchToken(token));
                                    });
                                  });
                                });
                              },
                            },
                          ],
                        );
                      } else {
                        setLoading(true);
                        let language = {
                          language: true,
                        };
                        dispatch(fetchLanguage(language));
                        let token = {
                          token: false,
                        };
                        dispatch(fetchToken(token));
                      }
                    }}
                    style={[styles.row, styles.horizontalContainer]}>
                    <TouchableCustomIcon
                      icon={appImages.arrowLeft}
                      size={totalSize(4)}
                      onPress={async () => {
                        if (!guest) {
                          Alert.alert(
                            Translate('LOG OUT'),
                            Translate('Are you sure you want to Log out?'),
                            [
                              {
                                text: Translate('Cancel'),
                                onPress: () => {
                                  console.log('Cancel Pressed');
                                },
                                style: 'cancel',
                              },
                              {
                                text: Translate('Confirm'),
                                onPress: async () => {
                                  setLoading(true);
                                  await AsyncStorage.removeItem(
                                    storageConst.userData,
                                  ).then(async () => {
                                    await AsyncStorage.removeItem(
                                      storageConst.userId,
                                    ).then(async () => {
                                      await AsyncStorage.removeItem(
                                        storageConst.guest,
                                      ).then(async () => {
                                        setLoading(false);
                                        Toast.show('You have been Logged out!');
                                        let language = {
                                          language: true,
                                        };
                                        dispatch(fetchLanguage(language));
                                        let token = {
                                          token: false,
                                        };
                                        dispatch(fetchToken(token));
                                      });
                                    });
                                  });
                                },
                              },
                            ],
                          );
                        } else {
                          setLoading(true);
                          let language = {
                            language: true,
                          };
                          dispatch(fetchLanguage(language));
                          let token = {
                            token: false,
                          };
                          dispatch(fetchToken(token));
                        }
                      }}
                    />
                    <View style={styles.onlyRow}>
                      {Platform.OS == 'ios' ? (
                        <SmallText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {guest ? Translate('Sign In') : Translate('Log Out')}
                        </SmallText>
                      ) : (
                        <RegularText
                          style={[styles.textStyle, {marginRight: width(3)}]}>
                          {guest ? Translate('Sign In') : Translate('Log Out')}
                        </RegularText>
                      )}
                      <CustomIcon icon={appImages.lock} size={totalSize(2)} />
                    </View>
                  </TouchableOpacity>
                </View>
              )}
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </View>
      )}

      <VersionModal
        isVisible={versionVisible}
        onPress={() => {
          Platform.OS == 'ios'
            ? Linking.openURL(
                'https://apps.apple.com/pk/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328',
              )
            : Linking.openURL(
                'https://play.google.com/store/apps/details?id=com.dwaae.smartapp',
              );
        }}
      />
    </Fragment>
  );
}
