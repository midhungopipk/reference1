import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  tooltipView: {
    paddingHorizontal: 24,
    paddingVertical: 8,
  },
  tooltipText: {
    color: 'black',
    fontSize: 18,
  },
});

const makeTooltipContent = text => (
  <View style={styles.tooltipView}>
    <Text style={styles.tooltipText}>{text}</Text>
  </View>
);

export default [
  {
    id: 'change-categories',
    content: makeTooltipContent('Tap here to change Categories'),
  },
  {
    id: 'search-products',
    content: makeTooltipContent('Search products from here'),
  },
  {
    id: 'get-categories',
    content: makeTooltipContent('Easy approach to categories'),
  },
  {
    id: 'ask-pharmacy',
    content: makeTooltipContent('Contact directly to Dwaae team'),
  },
  //   {
  //     id: 'profile-name',
  //     content: makeTooltipContent("Here is the user's name"),
  //     placement: 'bottom',
  //     triggerEvent: 'profile-focus',
  //   },
];
