import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  TouchableCustomIcon,
  MediumText,
  Wrapper,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';

export default function QuotationsProfile({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [uploadIdVisible, setUploadIdVisible] = useState(false);

  const [accountList, setAccountList] = useState([
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: true,
    },
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: true,
    },
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: false,
    },
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: true,
    },
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: false,
    },
    {
      title: 'Quotations #258963',
      date: '06/29/2020, 18:12',
      status: false,
    },
  ]);

  useEffect(() => {}, []);

  const ProductCard = ({item}) => {
    return (
      <Wrapper animation={'fadeInLeft'}>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={styles.row}>
            <View>
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                  }}>
                  {item.title}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                  }}>
                  {item.title}
                </MediumText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {item.date}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {item.date}
                </RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    color: item.status
                      ? colors.statusBarColor
                      : colors.redLight,
                  }}>
                  {item.status ? 'Status: Approved' : 'Status: Rejected'}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    color: item.status
                      ? colors.statusBarColor
                      : colors.redLight,
                  }}>
                  {item.status ? 'Status: Approved' : 'Status: Rejected'}
                </RegularText>
              )}
            </View>
            <View style={[styles.onlyRow]}>
              {/* <TouchableCustomIcon
                icon={appImages.mail}
                size={totalSize(2.5)}
              />
              <TouchableCustomIcon
                icon={appImages.print}
                size={totalSize(2.5)}
                style={{marginLeft: width(3.5)}}
              /> */}
              <TouchableCustomIcon
                icon={appImages.pdf}
                size={totalSize(2.5)}
                style={{marginLeft: width(3.5)}}
              />
            </View>
          </View>
        </View>
      </Wrapper>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 1, y: 1}}
          colors={colors.authGradient}
          style={[
            // appStyles.center,
            {
              width: width(100),
              height: Platform.OS == 'ios' ? height(7) : height(9),
            },
          ]}>
          <View
            style={[
              {
                flex: 1,
                backgroundColor: 'transparent',
                justifyContent: 'center',
              },
            ]}>
            <MediumText
              style={[
                appStyles.whiteText,
                {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
              ]}>
              Quotations
            </MediumText>

            <SmallText style={[{color: colors.bgLight, marginLeft: width(4)}]}>
              5 Quotations
            </SmallText>
          </View>
        </LinearGradient>

        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <FlatList
              data={accountList}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              numColumns={1}
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <ProductCard key={index} item={item} />
              )}
            />

            <Spacer height={sizes.doubleBaseMargin} />
            <ButtonColored
              onPress={() => navigation.navigate('Home')}
              text={'CONTINUE SHOPPING'}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: 5,
              }}
            />
            <Spacer height={sizes.doubleBaseMargin} />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
