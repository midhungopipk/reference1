import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  TinyTitle,
  FilterProductsModal,
  CovidTestBanner,
  ImageCategoriesTextSimple,
  TinierTitle,
  TestCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';

export default function CovidTest({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [categoryData1, setCategoryData1] = useState([
    {img: appImages.bloodTest},
    {img: appImages.swabTest},
    {img: appImages.vaccine},
  ]);
  const [confirmedTests, setConfirmedTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Confirmed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Confirmed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
    },
  ]);
  const [historyTests, setHistoryTests] = useState([
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 52,
      time: '3:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 55,
      time: '1:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.covidTestIcon,
      name: 'Swab Test',
      status: 'Completed',
      tokenNumber: 24,
      time: '4:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
    {
      img: appImages.bloodTubeIcon,
      name: 'Blood Test',
      status: 'Completed',
      tokenNumber: 18,
      time: '12:00 PM',
      date: '22/06/2021',
      testResult: 'Negative',
    },
  ]);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container]}>
        <KeyboardAwareScrollView>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <CovidTestBanner
            source={appImages.CovidTestBanner}
            imageStyle={styles.bannerImage}
            onBackPress={() => navigation.pop()}
            text={'Inner health at your door step'}
            subText={'49 Clinics'}
          />
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin * 1.3
            }
          />
          <FlatList
            data={categoryData1}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            numColumns={3}
            showsHorizontalScrollIndicator={false}
            renderItem={({index, item}) => (
              <ImageCategoriesTextSimple
                source={item.img}
                animation={'fadeInLeft'}
                imageStyle={styles.smallBannerList}
                resizeMode={'stretch'}
                onPress={() =>
                  index == 0
                    ? navigation.navigate(routes.bloodTest)
                    : index == 1
                    ? navigation.navigate(routes.swabTest)
                    : null
                }
              />
            )}
          />
          <Spacer height={sizes.baseMargin * 1.3} />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>
              Booking Status
            </TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>Booking Status</TinyTitle>
          )}
          <FlatList
            data={confirmedTests}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            showsHorizontalScrollIndicator={false}
            renderItem={({index, item}) => <TestCard key={index} item={item} />}
          />

          <Spacer height={sizes.doubleBaseMargin} />
          {Platform.OS == 'ios' ? (
            <TinierTitle style={{marginLeft: width(4)}}>
              Test History
            </TinierTitle>
          ) : (
            <TinyTitle style={{marginLeft: width(4)}}>Test History</TinyTitle>
          )}

          <FlatList
            data={historyTests}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            showsHorizontalScrollIndicator={false}
            renderItem={({index, item}) => (
              <TestCard
                key={index}
                item={item}
                onPress={() => navigation.navigate(routes.downloadReport)}
              />
            )}
          />
          <Spacer height={sizes.doubleBaseMargin} />

          <FilterProductsModal
            isVisible={isVisible}
            toggleModal={() => setIsVisible(false)}
          />
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
