import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Switch} from 'react-native-elements';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  TouchableCustomIcon,
  MediumText,
  Wrapper,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
/* Api Calls */
import {
  getQuotationList,
  getPrescriptionList,
} from '../../../services/api/prescription.api';
/* Redux */
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';

export default function Prescriptions({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [uploadIdVisible, setUploadIdVisible] = useState(false);
  const [accountList, setAccountList] = useState([
    // {
    //   title: 'Prescription #258963',
    //   date: '06/29/2020, 18:12',
    //   status: true,
    // },
  ]);
  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;

  useEffect(() => {
    setLoading(true);
    getQuotationList(user.user_id, onGetResponse, setLoading);
  }, [user]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      setLoading(true);
      getQuotationList(user.user_id, onGetResponse, setLoading);
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation, user]);

  //Get Prescriptions API response
  const onGetResponse = async res => {
    console.log('Get prescriptions API: ', res);
    if (res) {
      if (res.quotations && res.quotations.length) {
        // res.orders.forEach(element => {
        //   element.collapsed = true;
        // });
        // setAccountListTemp(res.orders);
        setAccountList(res.quotations);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Product Card Component
  const ProductCard = ({item}) => {
    return (
      <Wrapper animation={'fadeInLeft'}>
        <TouchableOpacity
          onPress={() => navigation.navigate(routes.prescriptionDetail, {item})}
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={styles.row}>
            <View>
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                  }}>
                  {item.email}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                  }}>
                  {item.email}
                </RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {'Created at: '} {item.create_time}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {'Created at: '} {item.create_time}
                </RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {'Valid till: '} {item.valid_till}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    color: colors.textGrey2,
                  }}>
                  {'Valid till: '} {item.valid_till}
                </RegularText>
              )}
              <Spacer height={sizes.TinyMargin} />
              {Platform.OS == 'ios' ? (
                <SmallText
                  style={{
                    color: colors.statusBarColor,
                    // item.status
                    //   ? colors.statusBarColor
                    //   : colors.redLight,
                  }}>
                  {/* {Translate('Status')} */}
                  {Translate('Status') + ': '}
                  {item.status == 'A'
                    ? 'ACTIVE'
                    : item.status == 'P'
                    ? 'PROCESSED'
                    : item.status == 'C'
                    ? 'COMPLETED'
                    : item.status == 'R'
                    ? 'REJECTED'
                    : item.status == 'B'
                    ? 'PAYMENT_COMPLETE'
                    : 'EXPIRED'}
                </SmallText>
              ) : (
                <RegularText
                  style={{
                    color: colors.statusBarColor,
                    // item.status
                    //   ? colors.statusBarColor
                    //   : colors.redLight,
                  }}>
                  {Translate('Status') + ': '}
                  {item.status == 'A'
                    ? 'ACTIVE'
                    : item.status == 'P'
                    ? 'PROCESSED'
                    : item.status == 'C'
                    ? 'COMPLETED'
                    : item.status == 'R'
                    ? 'REJECTED'
                    : item.status == 'B'
                    ? 'PAYMENT_COMPLETE'
                    : 'EXPIRED'}
                </RegularText>
              )}
            </View>
            <View style={[styles.onlyRow]}>
              {/* <TouchableCustomIcon
                icon={appImages.mail}
                size={totalSize(2.5)}
              />
              <TouchableCustomIcon
                icon={appImages.print}
                size={totalSize(2.5)}
                style={{marginLeft: width(3.5)}}
              />
              <TouchableCustomIcon
                icon={appImages.downloadPrescription}
                size={totalSize(2.5)}
                style={{marginLeft: width(3.5)}}
              /> */}
              <TouchableCustomIcon
                icon={appImages.arrowRight}
                size={totalSize(4)}
              />
            </View>
          </View>
        </TouchableOpacity>
      </Wrapper>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          {/* <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              // appStyles.center,
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}> */}
          <View
            style={[
              appStyles.shadowSmall,
              {
                // flex: 1,
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
                backgroundColor: colors.snow,
                justifyContent: 'center',
              },
            ]}>
            <MediumText
              style={[
                // appStyles.whiteText,
                {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
              ]}>
              {Translate('Quotations')}
            </MediumText>

            <SmallText style={[{marginLeft: width(4)}]}>
              {accountList.length} {Translate('Quotations')}
            </SmallText>
          </View>
          {/* </LinearGradient> */}

          {/* <KeyboardAwareScrollView> */}
          <View style={styles.bottomContainer}>
            {accountList.length ? (
              <FlatList
                data={accountList}
                keyExtractor={(contact, index) => String(index)}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                numColumns={1}
                showsHorizontalScrollIndicator={false}
                renderItem={({index, item}) => (
                  <ProductCard key={index} item={item} />
                )}
              />
            ) : (
              <RegularText
                style={{alignSelf: 'center', marginTop: sizes.baseMargin}}>
                {Translate('No data found')}
              </RegularText>
            )}

            <Spacer height={sizes.doubleBaseMargin} />
            <ButtonColored
              onPress={() => navigation.navigate('Home')}
              text={Translate('CONTINUE SHOPPING')}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: sizes.loginRadius,
              }}
            />
            <Spacer height={sizes.doubleBaseMargin} />
          </View>
          {/* </KeyboardAwareScrollView> */}
        </SafeAreaView>
      )}
    </Fragment>
  );
}
