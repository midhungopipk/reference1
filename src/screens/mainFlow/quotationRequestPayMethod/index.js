import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TinierTitle,
  TinyTitle,
  TextInputColored,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
  TinyText,
  MediumText,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';

export default function QuotationRequestPayMethod({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [test1, setTest1] = useState(true);
  const [test2, setTest2] = useState(false);

  useEffect(() => {}, []);

  const ProductCard = ({cod, text1, onPress, selected, icon}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={[appStyles.center, styles.onlyRow, {flex: 0.15}]}>
          <TouchableOpacity style={[styles.radioButton]} onPress={onPress}>
            <View
              style={[
                styles.innerRadioButton,
                {
                  backgroundColor: selected
                    ? colors.statusBarColor
                    : 'transparent',
                },
              ]}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 0.65}}>
          {Platform.OS == 'ios' ? (
            <SmallText style={{fontFamily: fontFamily.appTextBold}}>
              {text1}
            </SmallText>
          ) : (
            <RegularText style={{fontFamily: fontFamily.appTextBold}}>
              {text1}
            </RegularText>
          )}
        </View>
        <View style={[{flex: 0.2}]}>
          <CustomIcon icon={icon} size={totalSize(5)} />
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />

        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <Spacer height={sizes.baseMargin} />

            {Platform.OS == 'ios' ? (
              <TinierTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Payment Method
              </TinierTitle>
            ) : (
              <TinyTitle
                style={{
                  fontFamily: fontFamily.appTextBold,
                }}>
                Payment Method
              </TinyTitle>
            )}
            <Spacer height={sizes.smallMargin} />

            <ProductCard
              text1={'Credit & Debit Card / Bank Transfer'}
              onPress={() => {
                setTest1(true), setTest2(false);
              }}
              selected={test1}
              icon={appImages.card}
            />
            <ProductCard
              text1={'Cash On Delivery'}
              onPress={() => {
                setTest1(false), setTest2(true);
              }}
              selected={test2}
              icon={appImages.cod}
            />

            <Spacer height={sizes.baseMargin} />
            <TouchableOpacity style={styles.addProduct}>
              {Platform.OS == 'ios' ? (
                <TinyText
                  style={[appStyles.whiteText, {marginRight: width(2)}]}>
                  Upload Attachments
                </TinyText>
              ) : (
                <SmallText
                  style={[appStyles.whiteText, {marginRight: width(2)}]}>
                  Upload Attachments
                </SmallText>
              )}
              <CustomIcon icon={appImages.prescription} size={totalSize(1.7)} />
            </TouchableOpacity>
            <Spacer height={sizes.doubleBaseMargin} />
          </View>
        </KeyboardAwareScrollView>
        <View
          style={[
            {width: width(100), position: 'absolute', bottom: height(4)},
          ]}>
          {Platform.OS == 'ios' ? (
            <RegularText
              style={{
                color: colors.textGrey3,
                marginLeft: width(8),
              }}>
              By Submitting this order, you agree to the
            </RegularText>
          ) : (
            <MediumText style={{color: colors.textGrey3, marginLeft: width(8)}}>
              By Submitting this order, you agree to the
            </MediumText>
          )}
          <TouchableOpacity>
            {Platform.OS == 'ios' ? (
              <RegularText
                style={{
                  color: colors.gradient1,
                  textDecorationLine: 'underline',
                  marginLeft: width(8),
                }}>
                Buyer Agreement
              </RegularText>
            ) : (
              <MediumText
                style={{
                  color: colors.gradient1,
                  textDecorationLine: 'underline',
                  marginLeft: width(8),
                }}>
                Buyer Agreement
              </MediumText>
            )}
          </TouchableOpacity>
          <Spacer height={sizes.baseMargin} />
          <ButtonColored
            text={'CONTINUE'}
            onPress={() =>
              navigation.navigate(routes.quotationRequestPayMethod)
            }
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: 5,
            }}
          />
          <Spacer height={sizes.baseMargin} />
        </View>
      </SafeAreaView>
    </Fragment>
  );
}
