import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  SmallText,
  HomeItemCard,
  Wrapper,
  TinierTitle,
  HeaderSimple,
  TinyText,
  RegularText,
  LoadingCard,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
} from '../../../services';
/* Api Calls */
import {getWishList} from '../../../services/api/cart.api';
import {getViewAllData} from '../../../services/api/home.api';
import AsyncStorage from '@react-native-community/async-storage';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {
  fetchHomeData,
  fetchCategoryId,
  fetchCartData,
  fetchUser,
} from '../../../Redux/Actions/index';

import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  AddToCartMethod,
  AddToWishListMethod,
  RemoveProductFromWishListMethod,
  RemoveProductFromWLMethod,
  Translate,
} from '../../../services/helpingMethods';

export default function DiscountProducts({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [quantity, setQuantity] = useState(0);
  const [listData, setListData] = useState([]);
  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [wishLoading, setwishLoading] = useState(true);
  const [wishListData, setWishListData] = useState([]);
  //   const [gridId, setGridId] = useState(route.params.id ? route.params.id : '1');
  const [gridId, setGridId] = useState('1');
  const [LTR, setLTR] = useState(true);
  const [cartLoading, setCartLoading] = useState(false);

  useEffect(async () => {
    // console.log(route.params.id);
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    setLoading(true);
    getViewAllData(user.user_id, gridId, getHomeResponse, setLoading);
    setQuantity(
      store.getState().cart.data && store.getState().cart.data.data
        ? store.getState().cart.data.data
        : 0,
    );
    getWishList(user.user_id, onGetWishlistResponse, setwishLoading);
  }, [user]);

  useEffect(() => {
    computeLikes();
  }, [listData, wishListData]);

  //Get Wish List API Response
  const onGetWishlistResponse = async res => {
    // console.log('Get wishlist API: ', res);
    if (res) {
      if (res.products && res.products.length) setWishListData(res.products);
      setwishLoading(false);
    }
    setwishLoading(false);
  };

  //Get Home Response
  const getHomeResponse = async res => {
    console.log('View all products API: ', res);
    if (res) {
      if (
        res.success &&
        res.products &&
        res.products.products &&
        res.products.products.length
      )
        setListData(res.products.products);
      setLoading(false);
    }
    setLoading(false);
  };

  //Compute Likes according to the wish list
  const computeLikes = async () => {
    if (wishListData.length) {
      if (listData.length) {
        let newProductMedicationData = listData;
        for (let i = 0; i < newProductMedicationData.length; i++) {
          for (let j = 0; j < wishListData.length; j++) {
            if (
              newProductMedicationData[i].product_id ==
              wishListData[j].product_id
            ) {
              newProductMedicationData[i].liked = true;
            }
          }
        }
        setListData(newProductMedicationData);
      }
    }
  };

  //Get Cart Api Response
  const getCartResponse = async res => {
    console.log('Cart API: ', res);
    setCartLoading(false);
    // Toast.show(res.message);
    if (res) {
      Toast.show(res.message);
      if (res.success) {
        if (user.user_id == '0') {
          let updatedUser = user;
          user.user_id = res.guest_id;
          console.log('Previous user: ', user);
          console.log('Updated user: ', updatedUser);
          await store.dispatch(
            fetchUser({
              user: updatedUser,
            }),
          );
          await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
          await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
          await AsyncStorage.setItem(
            storageConst.userData,
            JSON.stringify(updatedUser),
          );
        }
      }
      await store.dispatch(
        fetchCartData({
          data: res.total_products,
        }),
      );
      setQuantity(res.total_products);
    }
    i;
  };

  //Get Wish List Response
  const getWishResponse = async res => {
    if (res) {
      Toast.show(res.message);
    }
    if (res.success && user.user_id == '0') {
      let updatedUser = user;
      user.user_id = res.guest_id;
      await store.dispatch(
        fetchUser({
          user: updatedUser,
        }),
      );
      await AsyncStorage.setItem(storageConst.guestId, updatedUser.user_id);
      await AsyncStorage.setItem(storageConst.userId, updatedUser.user_id);
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(updatedUser),
      );
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView style={[styles.container]}>
          {/* <KeyboardAwareScrollView> */}
          <HeaderSimple
            showSearch
            onBackPress={() => navigation.navigate('Home')}
            onPress={() => navigation.navigate('Cart')}
            quantity={quantity}
          />

          <View
            style={[
              styles.mainViewContainer,
              {backgroundColor: colors.bgLightMedium},
            ]}>
            <Wrapper
              animation={'fadeIn'}
              style={[
                styles.filterContainer,
                appStyles.shadowSmall,
                {
                  justifyContent: LTR ? 'flex-start' : 'flex-end',
                },
              ]}>
              <View>
                <TinierTitle
                  style={{
                    fontFamily:
                      Platform.OS == 'ios'
                        ? fontFamily.appTextMedium
                        : fontFamily.appTextBold,
                  }}>
                  {Translate('Offers Sale')}
                </TinierTitle>
                {Platform.OS == 'ios' ? (
                  <TinyText style={{color: colors.textGrey1}}>
                    {listData.length ? listData.length : 0}{' '}
                    {Translate('items found')}
                  </TinyText>
                ) : (
                  <SmallText style={{color: colors.textGrey1}}>
                    {listData.length ? listData.length : 0}{' '}
                    {Translate('items found')}
                  </SmallText>
                )}
              </View>
              {/* <TouchableOpacity>
                  <RegularText
                    style={{
                      fontFamily:
                        Platform.OS == 'ios'
                          ? fontFamily.appTextMedium
                          : fontFamily.appTextBold,
                      color: colors.gradient1,
                    }}>
                    CLEAR ALL
                  </RegularText>
                </TouchableOpacity> */}
            </Wrapper>
            <Spacer height={sizes.smallMargin} />
            {/* <View style={{paddingHorizontal: width(2)}}> */}
            <FlatList
              data={listData}
              extraData={listData}
              keyExtractor={(contact, index) => String(index)}
              numColumns={2}
              showsHorizontalScrollIndicator={false}
              renderItem={({index, item}) => (
                <HomeItemCard
                  item={item}
                  LTR={LTR ? true : false}
                  style={{marginLeft: width(2), marginRight: 0}}
                  onPress={() =>
                    navigation.navigate(routes.productDetail, {
                      productId: item.product_id,
                    })
                  }
                  addToCartPress={async () => {
                    if (!cartLoading) {
                      setCartLoading(true);
                      await AddToCartMethod({
                        item,
                        user,
                        getCartResponse,
                      });
                    }
                    // }
                  }}
                  onWishPress={async () => {
                    console.log('status: ', listData[index].liked);
                    let newList = listData;
                    if (!newList[index].liked) {
                      await AddToWishListMethod({
                        item,
                        user,
                        getWishResponse,
                      });

                      newList[index].liked = true;
                      setListData(newList);
                      setListData(prev => [...prev]);
                    } else {
                      await RemoveProductFromWLMethod({
                        user_id: user.user_id,
                        product_id: item.product_id,
                        getWishResponse,
                      });
                      newList[index].liked = false;
                      setListData(newList);
                      setListData(prev => [...prev]);
                    }
                    // setListData(newList);
                    console.log('liked = ', newList[index].liked);
                    // setListData(prev => [...prev]);
                  }}
                />
              )}
            />
            {/* </View> */}
          </View>
          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />
          {cartLoading ? (
            <View
              style={{
                position: 'absolute',
                bottom: height(30),
                borderRadius: 5,
                width: totalSize(5),
                height: totalSize(5),
                backgroundColor: 'rgba(255,255,255,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size={'large'}
                color={colors.activeBottomIcon}
              />
            </View>
          ) : null}
          {/* </KeyboardAwareScrollView> */}
        </SafeAreaView>
      )}
    </Fragment>
  );
}
