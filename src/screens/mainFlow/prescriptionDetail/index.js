import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  PermissionsAndroid,
  Linking,
  Alert,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
  MediumText,
  LineHorizontal,
  LoadingCard,
  TinyText,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  sizes,
  Translate,
  storageConst,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
/* Api Calls */
import {
  getPrescriptionDetail,
  downloadPrescription,
  getQuotationDetail,
} from '../../../services/api/prescription.api';
/* Redux */
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

export default function PrescriptionDetail({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [downloading, setDownLoading] = useState(false);
  const [uploadIdVisible, setUploadIdVisible] = useState(false);

  const [accountList, setAccountList] = useState([]);
  const [response, setResponse] = useState(null);

  const [paramData, setParamData] = useState(route.params.item);
  const [LTR, setLTR] = useState(true);
  const user = store.getState().user.user.user;
  console.log('Param data: ', paramData.rfq_id);

  useEffect(async () => {
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == 'ar') {
      setLTR(false);
    }
    setLoading(true);
    console.log('Param data of prescription detail: ', route.params.item);
    // if (route.params.item.products) {
    //   setAccountList(route.params.item.products);
    // }
    getQuotationDetail(
      user.user_id,
      route.params.item.rfq_id,
      onGetResponse,
      setLoading,
    );
  }, [route.params.item, user]);

  //On Get Quotation API Response
  const onGetResponse = async res => {
    console.log('Get Quotation detail API: ', res);
    if (res.success) {
      setResponse(res);
      if (res.quotation.products) {
        setAccountList(res.quotation.products);
        // setAccountList(prev => [...prev]);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  //Handle Download Prescription Press
  const handleDownloadPrescription = async () => {
    console.log(response);
    // console.log(response.files);
    for (let i = 0; i < response.files.length; i++) {
      console.log(i);
      setTimeout(() => {
        downloadPrescription(
          route.params.item.req_id,
          response.files[i],
          // response.files[0],
          onDownloadResponse,
          setDownLoading,
        );
      }, 500);
    }
  };

  //Check Permission Response
  const checkPermission = async () => {
    // Function to check the platform
    // If Platform is Android then check for permissions.

    if (Platform.OS === 'ios') {
      handleDownloadPrescription();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Start downloading
          handleDownloadPrescription();
          console.log('Storage Permission Granted.');
        } else {
          // If permission denied then show alert
          Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('++++' + err);
      }
    }
  };

  //On Donwload API Response
  const onDownloadResponse = async res => {
    console.log('Download prescription detail API: ', res);
    setDownLoading(false);
  };

  //Product Card Component
  const ProductCard = ({item, onCollapsablePress}) => {
    return (
      <View>
        <View
          style={[
            styles.productCard,
            Platform.OS == 'ios' && appStyles.shadowSmall,
          ]}>
          <View style={{paddingHorizontal: width(3)}}>
            {Platform.OS == 'ios' ? (
              <RegularText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product_name}
              </RegularText>
            ) : (
              <MediumText
                style={{
                  fontFamily: fontFamily.appTextMedium,
                }}>
                {item.product_name}
              </MediumText>
            )}
            <Spacer height={sizes.TinyMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {'Price: '} {item.formated_price}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {'Price: '} {item.formated_price}
              </RegularText>
            )}
            <Spacer height={sizes.TinyMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {'Non-Tax Price: '} {item.formated_non_tax_price}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {'Non-Tax Price: '} {item.formated_non_tax_price}
              </RegularText>
            )}
            <Spacer height={sizes.TinyMargin} />
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {'Tax Price: '} {item.formated_tax_price_total}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {'Tax Price: '} {item.formated_tax_price_total}
              </RegularText>
            )}
            {Platform.OS == 'ios' ? (
              <SmallText style={appStyles.textGray}>
                {'Description: '} {item.description}
              </SmallText>
            ) : (
              <RegularText style={appStyles.textGray}>
                {'Description: '} {item.description}
              </RegularText>
            )}
            <Spacer height={sizes.TinyMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <RegularText>{item.formated_price}</RegularText>
              ) : (
                <MediumText>{item.formated_price}</MediumText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText style={appStyles.blueText}>
                  {item.quantity + ' ' + item.quantity_unit}
                </SmallText>
              ) : (
                <RegularText style={appStyles.blueText}>
                  {item.quantity + ' ' + item.quantity_unit}
                </RegularText>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  };

  //Comparison card component
  const ComparisonCard = ({leftIcon, rightIcon, leftText, rightText}) => {
    return (
      <View
        style={[
          styles.productCardNew,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <TouchableOpacity
          onPress={() => {
            if (LTR) {
              Linking.openURL(
                'whatsapp://send?text=' +
                  'Hello HomeDevo team' +
                  '&phone=+971506333105',
              )
                .then(data => {
                  console.log('WhatsApp Opened successfully ' + data); //<---Success
                })
                .catch(() => {
                  // alert('Make sure WhatsApp installed on your device'); //<---Error
                  Linking.openURL(
                    `sms:&addresses=+971506333105&body=Hello HomeDevo team`,
                  );
                });
            } else {
              Linking.openURL(
                'whatsapp://send?text=' +
                  'HomeDevo مرحبا فريق' +
                  '&phone=+971506333105',
              )
                .then(data => {
                  console.log('WhatsApp Opened successfully ' + data); //<---Success
                })
                .catch(() => {
                  // alert('Make sure WhatsApp installed on your device'); //<---Error
                  Linking.openURL(
                    `sms:&addresses=+971506333105&body=HomeDevo مرحبا فريق`,
                  );
                });
            }
            // Linking.openURL(
            //   'whatsapp://send?text=' + LTR
            //     ? 'Hello Dwaae team.'
            //     : 'DWAAE مرحبا فريق' + '&phone=+971506333105',
            // )
            //   .then(data => {
            //     console.log('WhatsApp Opened successfully ' + data); //<---Success
            //   })
            //   .catch(() => {
            //     // alert('Make sure WhatsApp installed on your device'); //<---Error
            //     Linking.openURL(
            //       LTR
            //         ? `sms:&addresses=+971506333105&body=Hello Dwaae team`
            //         : `sms:&addresses=+971506333105&body=DWAAE مرحبا فريق`,
            //     );
            //   });
          }}
          style={[
            styles.textContainer,
            {
              borderRightWidth: 1,
              borderColor: colors.borderLightColor,
            },
          ]}>
          <TouchableCustomIcon
            icon={leftIcon}
            size={totalSize(2)}
            style={{marginRight: width(2)}}
          />
          {Platform.OS == 'ios' ? (
            <RegularText>{leftText}</RegularText>
          ) : (
            <MediumText>{leftText}</MediumText>
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={checkPermission}
          style={styles.textContainer}>
          <TouchableCustomIcon
            onPress={checkPermission}
            icon={rightIcon}
            size={totalSize(2)}
            style={{marginRight: width(2)}}
          />
          {downloading ? (
            <ActivityIndicator size={'large'} />
          ) : Platform.OS == 'ios' ? (
            <RegularText>{rightText}</RegularText>
          ) : (
            <MediumText>{rightText}</MediumText>
          )}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          {/* <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradient}
            style={[
              {
                width: width(100),
                height: Platform.OS == 'ios' ? height(7) : height(9),
              },
            ]}>
            <View
              style={[
                styles.row,
                {
                  flex: 1,
                  backgroundColor: 'transparent',
                },
              ]}>
              <View>
                <SmallText
                  style={[
                    appStyles.whiteText,
                    {fontFamily: fontFamily.appTextBold, marginLeft: width(4)},
                  ]}>
                  {paramData.accessignatureqid}
                </SmallText>
                <SmallText
                  style={[{color: colors.bgLight, marginLeft: width(4)}]}>
                  {paramData.date}
                </SmallText>
              </View>
              <View style={[styles.onlyRow, {marginRight: width(4)}]}>
                <TouchableCustomIcon
                  icon={appImages.pdf}
                  size={totalSize(2.5)}
                  style={{marginLeft: width(3.5)}}
                  onPress={checkPermission}
                />
              </View>
            </View>
          </LinearGradient> */}
          <View
            style={[
              appStyles.center,
              {
                width: width(100),
                paddingVertical: height(0.7),
                backgroundColor: colors.bgDarkGrey,
              },
            ]}>
            <SmallText style={[{color: colors.statusBarColor}]}>
              {Translate('Status')}
              {': '}
              {paramData.status == 'A'
                ? 'ACTIVE'
                : paramData.status == 'P'
                ? 'PROCESSED'
                : paramData.status == 'C'
                ? 'COMPLETED'
                : paramData.status == 'R'
                ? 'REJECTED'
                : paramData.status == 'B'
                ? 'PAYMENT_COMPLETE'
                : 'EXPIRED'}
            </SmallText>
          </View>
          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              {response && response.quotation ? (
                <>
                  <Spacer height={sizes.baseMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      Products
                    </RegularText>
                  ) : (
                    <MediumText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      Products
                    </MediumText>
                  )}
                  <FlatList
                    data={accountList}
                    contentContainerStyle={{
                      alignItems: 'center',
                    }}
                    numColumns={1}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({index, item}) => (
                      <ProductCard key={index} item={item} />
                    )}
                  />

                  <Spacer height={sizes.baseMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      Summary
                    </RegularText>
                  ) : (
                    <MediumText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      Summary
                    </MediumText>
                  )}

                  <View
                    style={[
                      styles.productCard,
                      Platform.OS == 'ios' && appStyles.shadowSmall,
                    ]}>
                    <View style={{paddingHorizontal: width(3)}}>
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Created at</SmallText>
                        ) : (
                          <RegularText>Created at</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>
                            {response.quotation.create_time}
                          </SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.create_time}
                          </RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Valid till</SmallText>
                        ) : (
                          <RegularText>Valid till</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>{response.quotation.valid_till}</SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.valid_till}
                          </RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Company</SmallText>
                        ) : (
                          <RegularText>Company</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>{response.quotation.company}</SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.company}
                          </RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Name</SmallText>
                        ) : (
                          <RegularText>Name</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>
                            {response.quotation.firstname +
                              ' ' +
                              response.quotation.lastname}
                          </SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.firstname +
                              ' ' +
                              response.quotation.lastname}
                          </RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Email</SmallText>
                        ) : (
                          <RegularText>Email</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>{response.quotation.email}</SmallText>
                        ) : (
                          <RegularText>{response.quotation.email}</RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin * 1.5} />
                      {/* <LineHorizontal height={1} />
                  <Spacer height={sizes.TinyMargin * 1.5} />
                  <View style={styles.row}>
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {Translate('Total Amount')}
                      </RegularText>
                    )}
                    {Platform.OS == 'ios' ? (
                      <SmallText style={{fontFamily: fontFamily.appTextBold}}>
                        {response.format_total ? response.format_total : 0}
                      </SmallText>
                    ) : (
                      <RegularText style={{fontFamily: fontFamily.appTextBold}}>
                        {response.format_total ? response.format_total : 0}
                      </RegularText>
                    )}
                  </View> */}
                    </View>
                  </View>
                  <Spacer height={sizes.baseMargin} />
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Payment Details')}
                    </RegularText>
                  ) : (
                    <MediumText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Payment Details')}
                    </MediumText>
                  )}

                  <View
                    style={[
                      styles.productCard,
                      Platform.OS == 'ios' && appStyles.shadowSmall,
                    ]}>
                    <View style={{paddingHorizontal: width(3)}}>
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>Shipping Charges</SmallText>
                        ) : (
                          <RegularText>Shipping Charges</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>
                            {response.quotation.formated_shipping_charges}
                          </SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.formated_shipping_charges}
                          </RegularText>
                        )}
                      </View>
                      <Spacer height={sizes.TinyMargin} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText>VAT</SmallText>
                        ) : (
                          <RegularText>VAT</RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText>
                            {response.quotation.formated_total_tax}
                          </SmallText>
                        ) : (
                          <RegularText>
                            {response.quotation.formated_total_tax}
                          </RegularText>
                        )}
                      </View>

                      <Spacer height={sizes.TinyMargin * 1.5} />
                      <LineHorizontal height={1} />
                      <Spacer height={sizes.TinyMargin * 1.5} />
                      <View style={styles.row}>
                        {Platform.OS == 'ios' ? (
                          <SmallText
                            style={{fontFamily: fontFamily.appTextBold}}>
                            {Translate('Total Amount')}
                          </SmallText>
                        ) : (
                          <RegularText
                            style={{fontFamily: fontFamily.appTextBold}}>
                            {Translate('Total Amount')}
                          </RegularText>
                        )}
                        {Platform.OS == 'ios' ? (
                          <SmallText
                            style={{fontFamily: fontFamily.appTextBold}}>
                            {response.quotation.formated_total}
                          </SmallText>
                        ) : (
                          <RegularText
                            style={{fontFamily: fontFamily.appTextBold}}>
                            {response.quotation.formated_total}
                          </RegularText>
                        )}
                      </View>
                    </View>
                  </View>
                </>
              ) : null}
              <Spacer height={sizes.baseMargin} />
              {response && response.locations ? (
                <>
                  {Platform.OS == 'ios' ? (
                    <RegularText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Shipping Address')}
                    </RegularText>
                  ) : (
                    <MediumText
                      style={{
                        fontFamily: fontFamily.appTextMedium,
                        marginLeft: width(4),
                      }}>
                      {Translate('Shipping Address')}
                    </MediumText>
                  )}

                  <View
                    style={[
                      styles.productCard,
                      Platform.OS == 'ios' && appStyles.shadowSmall,
                    ]}>
                    <View style={[styles.row, {paddingHorizontal: width(3)}]}>
                      <View style={{flex: 1}}>
                        {Platform.OS == 'ios' ? (
                          <SmallText style={appStyles.textGray}>
                            {response.locations[0].address}
                          </SmallText>
                        ) : (
                          <RegularText style={appStyles.textGray}>
                            {response.locations[0].address}
                          </RegularText>
                        )}
                        <Spacer height={sizes.TinyMargin} />
                        {Platform.OS == 'ios' ? (
                          <SmallText style={appStyles.textGray}>
                            {response.locations[0].city}
                          </SmallText>
                        ) : (
                          <RegularText style={appStyles.textGray}>
                            {response.locations[0].city}
                          </RegularText>
                        )}
                        <Spacer height={sizes.TinyMargin} />
                        {Platform.OS == 'ios' ? (
                          <SmallText style={appStyles.textGray}>
                            {response.locations[0].country}
                          </SmallText>
                        ) : (
                          <RegularText style={appStyles.textGray}>
                            {response.locations[0].country}
                          </RegularText>
                        )}
                        <Spacer height={sizes.TinyMargin} />
                      </View>
                      {/* <View style={{flex: 0.2}}> */}
                      <CustomIcon
                        icon={appImages.suitcase}
                        size={totalSize(4)}
                      />
                      {/* </View> */}
                    </View>
                  </View>
                </>
              ) : null}
              <Spacer height={sizes.baseMargin} />
              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Method')}
                </RegularText>
              ) : (
                <MediumText
                  style={{
                    fontFamily: fontFamily.appTextMedium,
                    marginLeft: width(4),
                  }}>
                  {Translate('Payment Method')}
                </MediumText>
              )}

              <View
                style={[
                  styles.productCard,
                  Platform.OS == 'ios' && appStyles.shadowSmall,
                ]}>
                <View
                  style={[
                    styles.onlyRow,
                    {paddingHorizontal: width(3), paddingVertical: height(0.5)},
                  ]}>
                  <CustomIcon icon={appImages.product} size={totalSize(2.5)} />
                  {Platform.OS == 'ios' ? (
                    <SmallText
                      style={[appStyles.textGray, {marginLeft: width(3)}]}>
                      {Translate('Cash On Delivery')}
                    </SmallText>
                  ) : (
                    <RegularText
                      style={[appStyles.textGray, {marginLeft: width(3)}]}>
                      {Translate('Cash On Delivery')}
                    </RegularText>
                  )}
                </View>
              </View>

              <Spacer height={sizes.doubleBaseMargin} />
              <ButtonColored
                onPress={() => navigation.navigate('Home')}
                text={Translate('CONTINUE SHOPPING')}
                buttonStyle={{
                  height: Platform.OS == 'ios' ? height(6) : height(7),
                  width: width(92),
                  alignSelf: 'center',
                  borderRadius: sizes.loginRadius,
                }}
              />
              <Spacer height={sizes.doubleBaseMargin} />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
