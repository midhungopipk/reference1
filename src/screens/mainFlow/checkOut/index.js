import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  Image,
  Linking,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TinierTitle,
  TinyTitle,
  RegularText,
  CustomIcon,
  MediumText,
  TinyText,
  LoadingCard,
  SmallTitle,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  Translate,
  storageConst,
} from '../../../services';
import {TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {getUserAddresses} from '../../../services/api/user.api';
import {
  PlaceOrderCheckout,
  capturePaytabPayment,
  checkoutCart,
} from '../../../services/api/cart.api';
/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchCartData} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {
  RNPaymentSDKLibrary,
  PaymentSDKConfiguration,
  PaymentSDKBillingDetails,
  PaymentSDKTheme,
} from '@paytabs/react-native-paytabs';
import {WebView} from 'react-native-webview';

export default function CheckOut({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [dataLoading, setDataLoading] = useState(false);
  const [test1, setTest1] = useState(false);
  const [test2, setTest2] = useState(false);

  // const user = useSelector(state => state.user.user);
  const user = store.getState().user.user.user;
  const [response, setResponse] = useState(null);
  const [checkOutResponse, setCheckOutResponse] = useState(null);
  const [checkOutCart, setCheckOutCart] = useState(null);
  const [addresses, setAddresses] = useState(null);
  const [addressesTemp, setAddressesTemp] = useState(null);
  const [profileId, setProfileId] = useState(null); //Address Id as profile Id
  const [cartProducts, setCartProducts] = useState(null);
  const [shippingMethodId, setShippingMethodId] = useState('');
  const [paymentMethodId, setPaymentMethodId] = useState('');
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [shippingMethods, setShippingMethods] = useState([]);
  const [tempPaymentMethods, setTempPaymentMethods] = useState([]);

  useEffect(() => {
    console.log('user:::::::::: ', user);
    setLoading(true);
    getUserAddresses(user.user_id, onGetResponse, setLoading);
    console.log('data = ', store.getState().cart.data.data);
    if (store.getState().cart.data.data) {
      setResponse(store.getState().cart.data.data);
    }
    checkoutCart(user.user_id, getCheckoutResponse, setDataLoading);
    // setLoading(false);
  }, [user]);

  const getCheckoutResponse = async res => {
    console.log('checkout API: ', res);
    if (res) {
      setCheckOutResponse(res);
      setCheckOutCart(res.cart);
      if (res.shipping_methods && res.shipping_methods.length) {
        let custom_payment = res.payment_methods;
        custom_payment.forEach(element => {
          element.selected = false;
        });
        setShippingMethods(res.shipping_methods);
      }
      // setDataLoading(false);
      if (res.payment_methods && res.payment_methods.length) {
        let custom_payment = res.payment_methods;
        custom_payment.forEach(element => {
          element.selected = false;
        });
        setPaymentMethods(custom_payment);
        setTempPaymentMethods(custom_payment);

        // setPaymentMethods(res.payment_methods);
      }
      setLoading(false);
    }
    setLoading(false);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUserAddresses(user.user_id, onGetResponse, setLoading);
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const onGetResponse = async res => {
    console.log('Get user Address API: ', res);
    if (res) {
      if (res.profiles && res.profiles.length) {
        res.profiles.forEach(element => {
          element.selected = false;
        });
        setAddressesTemp(res.profiles);
        setAddresses(res.profiles);
      }
      // setLoading(false);
    }
    // setLoading(false);
  };

  const handlePlaceOrder = async () => {
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == null || language == undefined) language = 'en';

    if (profileId == null) {
      Toast.show(Translate('Select the shipping address first'));
    } else {
      if (paymentMethodId == '') {
        Toast.show(Translate('Select the payment method first'));
      } else if (shippingMethodId == '') {
        Toast.show(Translate('Select the shipping method first'));
      } else {
        if (test1) {
          // Toast.show('Issue found in payment sdk');
          console.log('ok that: ', checkOutCart.order_id);
          setDataLoading(true);
          let dataObject = {
            user_id: user.user_id,
            selected_payment_method: paymentMethodId,
            currency_code: 'AED',
            lang_code: language,
            notes: 'Instructions',
            gc_code: '',
            order_id: checkOutCart.order_id,
            profile_id: profileId,
            shipping_ids: shippingMethodId,
          };
          console.log('Data for API: ', dataObject);
          PlaceOrderCheckout(dataObject, onGerOredrIdResponse, setDataLoading);
        } else {
          setDataLoading(true);
          let dataObject = {
            user_id: user.user_id,
            selected_payment_method: paymentMethodId,
            currency_code: 'AED',
            lang_code: language,
            notes: 'Instructions',
            gc_code: '',
            order_id: checkOutResponse.cart.order_id,
            profile_id: profileId,
            shipping_ids: shippingMethodId,
          };
          console.log('Data for API: ', dataObject);
          PlaceOrderCheckout(dataObject, onPlaceOrderResponse, setDataLoading);
        }
      }
    }
  };
  const onGerOredrIdResponse = async res => {
    console.log('Get Order ID response: ', res);
    setTimeout(() => {
      handlePayment(res.order_id);
    }, 1000);
  };
  const handlePayment = async order_id => {
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == null || language == undefined) language = 'en';
    console.log('Get Order ID response: ', order_id);
    await AsyncStorage.setItem(storageConst.paymentDone, '0');
    setDataLoading(false);
    // Toast.show('Payment SDK issue found');
    //'48033';47217
    let configuration = new PaymentSDKConfiguration();
    configuration.profileID = '48033';
    configuration.serverKey = 'S2JNLKBZG2-JBTBLMJWZT-BDZ6NTZMHT';
    configuration.clientKey = 'CNKMDV-QRGD62-72BN6V-Q6GTDH';
    configuration.cartID = JSON.stringify(order_id); //123456
    configuration.currency = 'AED';
    configuration.cartDescription = 'Flowers';
    configuration.merchantCountryCode = 'ae';
    configuration.merchantName = 'Flowers Store';
    configuration.amount =
      checkOutResponse.cart && checkOutResponse.cart.total
        ? checkOutResponse.cart.total
        : 0;
    configuration.screenTitle = 'Pay with Card';

    let billingDetails = new PaymentSDKBillingDetails(
      (name = user.firstname + ' ' + user.lastname),
      (email = user.email),
      (phone = user.phone),
      (addressLine = 'NA'),
      (city = 'Abu Dhabi'),
      (state = 'Abu Dhabi'),
      (countryCode = 'AE'),
      (zip = '1234'),
    );
    configuration.billingDetails = billingDetails;
    let theme = new PaymentSDKTheme();
    // theme.backgroundColor = "a83297"
    configuration.theme = theme;

    RNPaymentSDKLibrary.startCardPayment(JSON.stringify(configuration)).then(
      result => {
        if (result['PaymentDetails'] != null) {
          let paymentDetails = result['PaymentDetails'];
          console.log('Payment log: ', paymentDetails);
          // paymentResult = {
          //  acquirerMessage: "00:Approved",
          //  acquirerRRN: "118009217908",
          //  responseCode: "635294",
          //  responseMessage: "Authorised", // this one
          //  responseStatus: "A", // this one
          //  transactionTime: "2021-06-29T09:44:10Z"
          //  }

          // if (paymentDetails.paymentResult.responseMessage == 'A') {
          if (paymentDetails.paymentResult.responseStatus == 'A') {
            //success
            let dataObject = {
              order_id: order_id,
              payment_success: 'Y',
              user_id: user.user_id,
              payment_result: {
                responseCode: paymentDetails.paymentResult.responseCode, //"635294",
                responseMessage: paymentDetails.paymentResult.responseMessage, //"Authorised",
                responseStatus: paymentDetails.paymentResult.responseStatus, //"A",
                acquireRrn: paymentDetails.paymentResult.acquirerRRN, //"1180056788"
              },
              lang_code: language,
              currency_code: 'AED',
            };

            console.log('The object i am sending on success: ', dataObject);
            capturePaytabPayment(
              dataObject,
              onCapturePaytabResponse,
              setDataLoading,
            );
          } else {
            //fail
            Toast.show(paymentDetails.paymentResult.responseMessage);
            let dataObject = {
              order_id: order_id,
              payment_success: 'N',
              user_id: user.user_id,
              payment_result: {
                responseCode: paymentDetails.paymentResult.responseCode, //"635294",
                responseMessage: paymentDetails.paymentResult.responseMessage, //"Authorised",
                responseStatus: paymentDetails.paymentResult.responseStatus, //"A",
                acquireRrn: paymentDetails.paymentResult.acquirerRRN, //"1180056788"
              },
              lang_code: language,
              currency_code: 'AED',
            };
            console.log('The object i am sending on fail: ', dataObject);
            capturePaytabPayment(
              dataObject,
              onCapturePaytabResponse,
              setDataLoading,
            );
          }
        } else if (result['Event'] == 'CancelPayment') {
          console.log('Cancel Payment Event');
          setDataLoading(false);
        }
      },
      function (error) {
        console.log(error);
      },
    );
  };
  const onCapturePaytabResponse = async res => {
    console.log('Capture paytab response: ', res);
    if (res.success) {
      await store.dispatch(
        fetchCartData({
          data: 0,
        }),
      );
      navigation.navigate(routes.orderConfirm);
      setDataLoading(false);
    }
    Toast.showWithGravity(res.message, Toast.LONG, Toast.CENTER);
    setDataLoading(false);
    // navigation.navigate(routes.orderConfirm);
  };
  const onPlaceOrderResponse = async res => {
    console.log('Place order API Cash on delivery response: ', res);
    if (res.success) {
      await store.dispatch(
        fetchCartData({
          data: 0,
        }),
      );
      navigation.navigate(routes.orderConfirm);
      setDataLoading(false);
    }
    Toast.showWithGravity(res.message, Toast.LONG, Toast.CENTER);
    setDataLoading(false);
    // navigation.navigate(routes.orderConfirm);
  };
  const handleSelectAddress = index => {
    let newData = addressesTemp;
    newData.forEach(element => {
      element.selected = false;
    });
    newData[index].selected = true;
    setAddresses(newData);
    setAddresses(prev => [...prev]);
    setProfileId(newData[index].profile_id);
  };
  const ProductCard = ({home, onPress, item}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <View style={[styles.row, {flex: 1, paddingHorizontal: width(4)}]}>
          <View style={styles.onlyRow}>
            <TouchableOpacity
              style={[appStyles.center, styles.onlyRow]}
              onPress={onPress}>
              <View style={appStyles.center}>
                <View
                  style={{
                    width: Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                    height: Platform.OS == 'ios' ? totalSize(6) : totalSize(7),
                    borderRadius:
                      Platform.OS == 'ios'
                        ? totalSize(6) / 2
                        : totalSize(7) / 2,
                    backgroundColor: colors.greyBg,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    borderColor: colors.borderLightColor,
                  }}>
                  <CustomIcon
                    icon={
                      item.profile_name == 'my_home'
                        ? appImages.building
                        : appImages.suitcase
                    }
                    size={Platform.OS == 'ios' ? totalSize(3) : totalSize(4)}
                  />
                </View>
                <TinyText
                  style={{
                    marginTop: height(1),
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  {item.profile_name == 'my_home'
                    ? Translate('Home')
                    : Translate('Work')}
                  {/* {home ? Translate('Home') : Translate('Work')} */}
                </TinyText>
              </View>
            </TouchableOpacity>

            <View
              style={{
                paddingHorizontal: width(3),
                width: width(44),
              }}>
              {Platform.OS == 'ios' ? (
                <SmallText>{item.s_address}</SmallText>
              ) : (
                <RegularText>{item.s_address}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{item.s_city + ', ' + item.s_country}</SmallText>
              ) : (
                <RegularText>{item.s_city + ', ' + item.s_country}</RegularText>
              )}
            </View>
          </View>

          <View style={appStyles.center}>
            {/* <View style={{backgroundColor: 'red'}}> */}
            {/* <TouchableOpacity
              style={styles.addProduct}
              onPress={() => {
                console.log('adress item: ', item);
                navigation.navigate(routes.checkOutChangeAddress, {item});
              }}>
              {Platform.OS == 'ios' ? (
                <TinyText style={[appStyles.whiteText]}>
                  {Translate('Change Address')}
                </TinyText>
              ) : (
                <SmallText style={[appStyles.whiteText]}>
                  {Translate('Change Address')}
                </SmallText>
              )}
            </TouchableOpacity> */}
            <Spacer height={sizes.baseMargin} />
            <TouchableOpacity style={[styles.radioButton]} onPress={onPress}>
              <View
                style={[
                  styles.innerRadioButton,
                  {
                    backgroundColor: item.selected
                      ? colors.statusBarColor
                      : 'transparent',
                  },
                ]}
              />
            </TouchableOpacity>
            {/* </View> */}
          </View>
        </View>
      </View>
    );
  };
  const PaymentMethodCard = ({cod, item, text1, onPress, selected}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
          {flexDirection: 'column'},
        ]}>
        <TouchableOpacity
          onPress={onPress}
          style={[styles.row, {flex: 1, paddingHorizontal: width(4)}]}>
          <View style={[appStyles.center, styles.onlyRow]}>
            {/* <CustomIcon
              icon={cod ? appImages.cod : appImages.card}
              size={totalSize(8)}
            /> */}
            <Image
              style={{width: width(10), height: height(7)}}
              resizeMode={'cover'}
              // source={cod ? appImages.cod : appImages.card}
              source={text1 == 'PayTab' ? appImages.card : appImages.cod}
            />
            <View style={{paddingHorizontal: width(2)}}>
              {Platform.OS == 'ios' ? (
                <SmallText>{text1}</SmallText>
              ) : (
                <RegularText>{text1}</RegularText>
              )}
            </View>
          </View>

          <TouchableOpacity
            style={[styles.radioButton, {marginRight: width(3)}]}
            onPress={onPress}>
            <View
              style={[
                styles.innerRadioButton,
                {
                  backgroundColor: selected
                    ? colors.statusBarColor
                    : 'transparent',
                },
              ]}
            />
          </TouchableOpacity>
        </TouchableOpacity>
        {item.instructions != '' && (
          <WebView
            originWhitelist={['*']}
            source={{html: item.instructions}}
            style={{width: width(80), height: height(15)}}
          />
        )}
      </View>
    );
  };
  const ShippingMethodCard = ({cod, item, text1, onPress, selected}) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <TouchableOpacity
          onPress={onPress}
          style={[styles.row, {flex: 1, paddingHorizontal: width(4)}]}>
          <View style={[appStyles.center, styles.onlyRow]}>
            {/* <CustomIcon
              icon={cod ? appImages.cod : appImages.card}
              size={totalSize(8)}
            /> */}
            {/* <Image
              style={{width: width(10), height: height(7)}}
              resizeMode={'cover'}
              // source={cod ? appImages.cod : appImages.card}
              source={text1 == 'PayTab' ? appImages.card : appImages.cod}
            /> */}
            <View style={{paddingHorizontal: width(2)}}>
              {Platform.OS == 'ios' ? (
                <TinierTitle>{item.shipping}</TinierTitle>
              ) : (
                <TinyTitle>{item.shipping}</TinyTitle>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{item.service_delivery_time}</SmallText>
              ) : (
                <RegularText>{item.service_delivery_time}</RegularText>
              )}
              {Platform.OS == 'ios' ? (
                <SmallText>{item.format_rate}</SmallText>
              ) : (
                <RegularText>{item.format_rate}</RegularText>
              )}
            </View>
          </View>

          <TouchableOpacity
            style={[styles.radioButton, {marginRight: width(3)}]}
            onPress={onPress}>
            <View
              style={[
                styles.innerRadioButton,
                {
                  backgroundColor: selected
                    ? colors.statusBarColor
                    : 'transparent',
                },
              ]}
            />
          </TouchableOpacity>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {loading ? (
        <LoadingCard />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.bgLightMedium}]}>
          <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
          <HeaderSimple
            onBackPress={() => navigation.pop()}
            onPress={() => navigation.navigate('Cart')}
          />
          <Spacer
            height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
          />

          <KeyboardAwareScrollView>
            <View style={styles.bottomContainer}>
              <Spacer height={sizes.baseMargin} />
              <View>
                {Platform.OS == 'ios' ? (
                  <TinierTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Shipping Address')}
                  </TinierTitle>
                ) : (
                  <TinyTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Shipping Address')}
                  </TinyTitle>
                )}
              </View>
              <Spacer height={sizes.smallMargin} />
              <View style={styles.row}>
                <View>
                  {Platform.OS == 'ios' ? (
                    <SmallText>
                      {addresses && addresses.length
                        ? ''
                        : Translate('No shipping address found')}
                    </SmallText>
                  ) : (
                    <RegularText>
                      {addresses && addresses.length
                        ? ''
                        : Translate('No shipping address found')}
                    </RegularText>
                  )}
                </View>
                <TouchableOpacity
                  style={styles.addProduct}
                  onPress={() => navigation.navigate(routes.addAddress)}>
                  {Platform.OS == 'ios' ? (
                    <TinyText style={[appStyles.whiteText]}>
                      {Translate('Add Address')}
                    </TinyText>
                  ) : (
                    <SmallText style={[appStyles.whiteText]}>
                      {Translate('Add Address')}
                    </SmallText>
                  )}
                </TouchableOpacity>
              </View>
              <FlatList
                data={addresses}
                contentContainerStyle={{
                  alignItems: 'center',
                }}
                keyExtractor={(contact, index) => String(index)}
                renderItem={({index, item}) => (
                  <ProductCard
                    key={index}
                    item={item}
                    home
                    onPress={() => {
                      handleSelectAddress(index);
                    }}
                  />
                )}
              />

              <Spacer height={sizes.baseMargin} />
              <View>
                {Platform.OS == 'ios' ? (
                  <TinierTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Payment Method')}
                  </TinierTitle>
                ) : (
                  <TinyTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Payment Method')}
                  </TinyTitle>
                )}
              </View>

              {paymentMethods.length ? (
                <FlatList
                  data={paymentMethods}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({index, item}) => (
                    <PaymentMethodCard
                      text1={item.payment}
                      item={item}
                      onPress={() => {
                        let new_payment = paymentMethods;
                        new_payment.forEach(element => {
                          element.selected = false;
                        });
                        new_payment[index].selected = true;
                        setPaymentMethodId(item.payment_id);
                        setPaymentMethods(new_payment);
                        setPaymentMethods(prev => [...prev]);
                        if (index == 1) {
                          setTest2(true);
                          setTest1(false);
                        } else {
                          setTest2(false);
                          setTest1(true);
                        }
                      }}
                      selected={item.selected}
                    />
                  )}
                />
              ) : (
                <SmallText>
                  {Translate('No payment method available')}
                </SmallText>
              )}
              <Spacer height={sizes.baseMargin} />
              <View>
                {Platform.OS == 'ios' ? (
                  <TinierTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Shipping Method')}
                  </TinierTitle>
                ) : (
                  <TinyTitle
                    style={{
                      fontFamily: fontFamily.appTextBold,
                    }}>
                    {Translate('Shipping Method')}
                  </TinyTitle>
                )}
              </View>

              {shippingMethods.length ? (
                <FlatList
                  data={shippingMethods}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}
                  keyExtractor={(contact, index) => String(index)}
                  renderItem={({index, item}) => (
                    <ShippingMethodCard
                      // text1={item.payment}
                      item={item}
                      onPress={() => {
                        let new_payment = shippingMethods;
                        new_payment.forEach(element => {
                          element.selected = false;
                        });
                        new_payment[index].selected = true;
                        setShippingMethodId(item.shipping_id);
                        setShippingMethods(new_payment);
                        setShippingMethods(prev => [...prev]);
                      }}
                      selected={item.selected}
                    />
                  )}
                />
              ) : (
                <SmallText>
                  {Translate('No shipping method available')}
                </SmallText>
              )}

              {/* <PaymentMethodCard
                text1={'Credit & Debit Card/ Bank Transfer'}
                onPress={() => {
                  setTest1(true), setTest2(false);
                }}
                selected={test1}
              />
              <PaymentMethodCard
                text1={Translate('Cash On Delivery')}
                cod
                onPress={() => {
                  setTest1(false), setTest2(true);
                }}
                selected={test2}
              /> */}
              <Spacer height={sizes.baseMargin * 1.5} />

              {Platform.OS == 'ios' ? (
                <RegularText
                  style={{
                    color: colors.textGrey3,
                    marginLeft: width(5),
                  }}>
                  {Translate('By Submitting this order you agree to the')}
                </RegularText>
              ) : (
                <MediumText
                  style={{color: colors.textGrey3, marginLeft: width(8)}}>
                  {Translate('By Submitting this order you agree to the')}
                </MediumText>
              )}
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    checkOutResponse && checkOutResponse.terms_link
                      ? checkOutResponse.terms_link
                      : '',
                  )
                }>
                {Platform.OS == 'ios' ? (
                  <RegularText
                    style={{
                      color: colors.gradient1,
                      textDecorationLine: 'underline',
                      marginLeft: width(5),
                    }}>
                    {Translate('Buyer Agreement')}
                  </RegularText>
                ) : (
                  <MediumText
                    style={{
                      color: colors.gradient1,
                      textDecorationLine: 'underline',
                      marginLeft: width(8),
                    }}>
                    {Translate('Buyer Agreement')}
                  </MediumText>
                )}
              </TouchableOpacity>
              <Spacer height={sizes.doubleBaseMargin * 2} />
            </View>
          </KeyboardAwareScrollView>
          <View
            style={{
              width: width(100),
              position: 'absolute',
              bottom: height(4),
            }}>
            <ButtonColored
              isLoading={dataLoading}
              text={
                Translate('PLACE ORDER') +
                ' ' +
                (checkOutResponse &&
                checkOutResponse.cart &&
                checkOutResponse.cart.total
                  ? checkOutResponse.cart.total
                  : '')
              }
              onPress={() => handlePlaceOrder()}
              buttonStyle={{
                height: Platform.OS == 'ios' ? height(6) : height(7),
                width: width(92),
                alignSelf: 'center',
                borderRadius: sizes.loginRadius,
              }}
            />
          </View>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
