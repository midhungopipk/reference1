import React, {Fragment, useState, useEffect, useRef, Component} from 'react';
import {StatusBar, SafeAreaView, View, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';

import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  SmallText,
  TinierTitle,
  TinyTitle,
  TextInputColored,
  RegularText,
  CustomIcon,
  TouchableCustomIcon,
  TinyText,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
} from '../../../services';
import {TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';

export default function QuotationRequestDelivery({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [test1, setTest1] = useState(true);
  const [test2, setTest2] = useState(false);

  useEffect(() => {}, []);

  const ProductCard = ({
    home,
    text1,
    text2,
    text3,
    text4,
    onPress,
    selected,
  }) => {
    return (
      <View
        style={[
          styles.productCard,
          Platform.OS == 'ios' && appStyles.shadowSmall,
        ]}>
        <TouchableOpacity
          style={[appStyles.center, styles.onlyRow, {flex: 0.3}]}
          onPress={onPress}>
          <TouchableOpacity
            style={[styles.radioButton, {marginRight: width(3)}]}
            onPress={onPress}>
            <View
              style={[
                styles.innerRadioButton,
                {
                  backgroundColor: selected
                    ? colors.statusBarColor
                    : 'transparent',
                },
              ]}
            />
          </TouchableOpacity>
          <View style={appStyles.center}>
            <CustomIcon
              icon={home ? appImages.homeIconTest : appImages.suitcase}
              size={totalSize(2.5)}
            />
            <TinyText
              style={{
                marginTop: height(1),
                fontFamily: fontFamily.appTextBold,
              }}>
              {home ? 'Home' : 'Work'}
            </TinyText>
          </View>
        </TouchableOpacity>
        <View style={{flex: 0.6, paddingHorizontal: width(4)}}>
          {Platform.OS == 'ios' ? (
            <SmallText>{text1}</SmallText>
          ) : (
            <RegularText>{text1}</RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText>{text2}</SmallText>
          ) : (
            <RegularText>{text2}</RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText>{text3}</SmallText>
          ) : (
            <RegularText>{text3}</RegularText>
          )}
          {Platform.OS == 'ios' ? (
            <SmallText>{text4}</SmallText>
          ) : (
            <RegularText>{text4}</RegularText>
          )}
        </View>
        <View style={[{flex: 0.1}]}>
          <Icon
            name={'dots-three-vertical'}
            type={'entypo'}
            size={Platform.OS == 'ios' ? totalSize(1.5) : totalSize(1.8)}
            style={{marginBottom: height(5)}}
          />
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <Spacer
          height={Platform.OS == 'ios' ? sizes.smallMargin : sizes.baseMargin}
        />

        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <Spacer height={sizes.baseMargin} />
            <View style={styles.row}>
              {Platform.OS == 'ios' ? (
                <TinierTitle
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Delivery Location
                </TinierTitle>
              ) : (
                <TinyTitle
                  style={{
                    fontFamily: fontFamily.appTextBold,
                  }}>
                  Delivery Location
                </TinyTitle>
              )}
              <TouchableOpacity
                style={styles.addProduct}
                onPress={() => navigation.navigate(routes.addAddress)}>
                {Platform.OS == 'ios' ? (
                  <TinyText
                    style={[appStyles.whiteText, {marginRight: width(1)}]}>
                    Add Address
                  </TinyText>
                ) : (
                  <SmallText
                    style={[appStyles.whiteText, {marginRight: width(1)}]}>
                    Add Address
                  </SmallText>
                )}
                <CustomIcon icon={appImages.plusCircle} size={totalSize(1.5)} />
              </TouchableOpacity>
            </View>
            <Spacer height={sizes.smallMargin} />

            <ProductCard
              text1={'AlFalah St, Plot C1,'}
              text2={'Mezanine Floor'}
              text3={'P.O.Box 26291'}
              text4={'Abu Dhabi, UAE'}
              home
              onPress={() => {
                setTest1(true), setTest2(false);
              }}
              selected={test1}
            />
            <ProductCard
              text1={'AlFalah St, Plot C1,'}
              text2={'Mezanine Floor'}
              text3={'P.O.Box 26291'}
              text4={'Abu Dhabi, UAE'}
              onPress={() => {
                setTest1(false), setTest2(true);
              }}
              selected={test2}
            />

            <Spacer height={sizes.doubleBaseMargin} />
          </View>
        </KeyboardAwareScrollView>
        <View
          style={{width: width(100), position: 'absolute', bottom: height(4)}}>
          <ButtonColored
            text={'CONTINUE'}
            onPress={() =>
              navigation.navigate(routes.quotationRequestPayMethod)
            }
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: 5,
            }}
          />
        </View>
        <Spacer height={sizes.baseMargin} />
      </SafeAreaView>
    </Fragment>
  );
}
