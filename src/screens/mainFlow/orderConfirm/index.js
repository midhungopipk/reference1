import React, {
  Fragment,
  useState,
  useEffect,
  useCallback,
  useRef,
  Component,
} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Platform,
  FlatList,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  HeaderSimple,
  ButtonColored,
  TouchableCustomIcon,
  CustomIcon,
} from '../../../components';
import {appImages, routes, sizes, Translate} from '../../../services';
import {useFocusEffect} from '@react-navigation/native';
export default function OrderConfirm({navigation, route}) {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // getCart(user.user_id, onGetResponse, setLoading);
      BackHandler.addEventListener(
        'hardwareBackPress',
        hardwareBackPressCustom,
      );
    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const hardwareBackPressCustom = useCallback(() => {
    console.log('called now');
    return true;
  }, []);

  // useFocusEffect(() => {
  //   BackHandler.addEventListener('hardwareBackPress', hardwareBackPressCustom);
  //   return () => {
  //     BackHandler.removeEventListener(
  //       'hardwareBackPress',
  //       hardwareBackPressCustom,
  //     );
  //   };
  // }, []);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.bgLight}]}>
        <Spacer height={Platform.OS == 'ios' ? 0 : sizes.smallMargin} />
        <HeaderSimple
          dontShowBack
          onBackPress={() => navigation.pop()}
          onPress={() => navigation.navigate('Cart')}
        />
        <Spacer height={sizes.baseMargin} />
        {/* <View style={[styles.row, {paddingHorizontal: width(4)}]}>
          <View />
          <View style={[styles.onlyRow]}>
            <TouchableCustomIcon icon={appImages.mail} size={totalSize(3)} />
            <TouchableCustomIcon
              icon={appImages.print}
              size={totalSize(3)}
              style={{marginLeft: width(3.5)}}
            />
            <TouchableCustomIcon
              icon={appImages.pdf}
              size={totalSize(3)}
              style={{marginLeft: width(3.5)}}
            />
          </View>
        </View> */}
        <KeyboardAwareScrollView>
          <View style={styles.bottomContainer}>
            <Spacer height={sizes.doubleBaseMargin * 2} />
            <View style={{alignSelf: 'center'}}>
              <CustomIcon icon={appImages.orderConfirm} size={totalSize(30)} />
            </View>
            <Spacer height={sizes.doubleBaseMargin} />
          </View>
        </KeyboardAwareScrollView>

        <View
          style={{width: width(100), position: 'absolute', bottom: height(4)}}>
          <ButtonColored
            text={Translate('TRACK ORDER')}
            onPress={() => navigation.navigate(routes.trackOrder)}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: sizes.loginRadius,
            }}
          />
          <Spacer height={sizes.baseMargin} />
          <ButtonColored
            text={Translate('CONTINUE SHOPPING')}
            onPress={() => navigation.navigate('Home')}
            buttonStyle={{
              height: Platform.OS == 'ios' ? height(6) : height(7),
              width: width(92),
              alignSelf: 'center',
              borderRadius: sizes.loginRadius,
            }}
          />
        </View>
        <Spacer height={sizes.baseMargin} />
      </SafeAreaView>
    </Fragment>
  );
}
