import React, {Fragment, Component} from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  ActivityIndicator,
  PERMISSIONS,
  request,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import {
  MainWrapper,
  XXLTitle,
  LogoMain,
  Spacer,
  LargeText,
  LargeTitle,
  RegularText,
  ButtonColored,
} from '../../../components';
import {
  appImages,
  appStyles,
  colors,
  sizes,
  Translate,
} from '../../../services';
import {height, totalSize, width} from 'react-native-dimension';
import Geolocation from 'react-native-geolocation-service';
import AsyncStorage from '@react-native-community/async-storage';
import {routes, storageConst} from '../../../services/constants';
import {store} from '../../../Redux/configureStore';
import {fetchCategoryId, fetchUser} from '../../../Redux/Actions/index';
import {useSelector, useDispatch} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import {ImageBackground} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fcm: '',
      loading: true,
    };
  }

  // componentDidMount = async () => {
  //   // AsyncStorage.removeItem(storageConst.userData);
  //   // AsyncStorage.removeItem(storageConst.userId);
  //   // this.checkPermission();
  //   this.checkTokens();
  //   this.requestLocationPermission();
  //   this.notificationSettings();
  // };
  checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    alert(enabled);
    if (enabled) {
      this.getFcmToken();
    } else {
      this.requestPermission();
    }
  };
  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      // this.setState({fcm: fcmToken});
    } else {
    }
  };
  requestPermission = async () => {
    try {
      await messaging().requestPermission();
      if (authorizationStatus) {
        console.log('Permission status:', authorizationStatus);
      }
      // User has authorised
    } catch (error) {
      // User has rejected permissions
    }
  };
  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      this.getFcmToken();
    }
  };

  notificationSettings = async () => {
    this.requestUserPermission();
    await messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage);
    });
    await messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log(remoteMessage);
      console.log('Message handled in the splash!', remoteMessage);
    });
    await messaging().onNotificationOpenedApp(async remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        // remoteMessage.notification,
        remoteMessage,
      );
      // navigation.navigate(remoteMessage.data.type);
      await AsyncStorage.getItem(storageConst.userId).then(async userId => {
        if (userId) {
          await AsyncStorage.getItem(storageConst.userData).then(
            async userData => {
              if (userData) {
                let user = JSON.parse(userData);
                store.dispatch(
                  fetchUser({
                    user,
                  }),
                );

                await AsyncStorage.getItem(storageConst.language).then(
                  async language => {
                    if (language == null || language == undefined) {
                      await AsyncStorage.setItem(storageConst.language, 'en');
                    } else {
                      setTimeout(() => {
                        this.setState({loading: false});
                        this.props.navigation.navigate(routes.home);
                      }, 300);
                    }
                  },
                );
              }

              if (remoteMessage.data.type == 'P') {
                this.props.navigation.navigate(routes.productDetail, {
                  productId: remoteMessage.data.type_data,
                });
              } else if (remoteMessage.data.type == 'V') {
                this.props.navigation.navigate(routes.pharmacyDetail, {
                  item: remoteMessage.data.type_data,
                });
              } else if (remoteMessage.data.type == 'C') {
                store.dispatch(
                  fetchCategoryId({
                    categoryId: remoteMessage.data.type_data,
                  }),
                );
                this.props.navigation.navigate(routes.productListing);
              }
            },
          );
        } else {
          await AsyncStorage.getItem(storageConst.guestId).then(async value => {
            let user_id = '0';
            if (value) {
              user_id = value;
            }
            let user = {
              api_key: '',
              birthday: '0',
              company: '',
              company_id: '0',
              ec_location_detail: '',
              email: '',
              emirate_id_number: '',
              fax: '',
              firstname: 'Guest',
              insurance_number: '',
              is_root: 'N',
              janrain_identifier: '',
              lang_code: 'en',
              last_login: '0',
              last_passwords: '',
              lastname: 'User',
              message: 'You have been successfully logged in.',
              password_change_timestamp: '1',
              phone: '',
              purchase_timestamp_from: '0',
              purchase_timestamp_to: '0',
              referer: '',
              responsible_email: '',
              status: 'A',
              success: true,
              tax_exempt: 'N',
              timestamp: '1622368819',
              total_products: 0,
              url: '',
              user_id: user_id,
              user_login: '0',
              user_type: 'C',
            };
            await store.dispatch(
              fetchUser({
                user: user,
              }),
            );
            await AsyncStorage.setItem(storageConst.guest, '1');
            await AsyncStorage.setItem(
              storageConst.userData,
              JSON.stringify(user),
            ).then(async () => {
              await AsyncStorage.setItem(storageConst.userId, '0').then(
                async () => {
                  if (remoteMessage.data.type == 'P') {
                    this.props.navigation.navigate(routes.productDetail, {
                      productId: remoteMessage.data.type_data,
                    });
                  } else if (remoteMessage.data.type == 'V') {
                    this.props.navigation.navigate(routes.pharmacyDetail, {
                      item: remoteMessage.data.type_data,
                    });
                  } else if (remoteMessage.data.type == 'C') {
                    store.dispatch(
                      fetchCategoryId({
                        categoryId: remoteMessage.data.type_data,
                      }),
                    );
                    this.props.navigation.navigate(routes.productListing);
                  }
                },
              );
            });
          });
        }
      });
    });

    // Check whether an initial notification is available
    await messaging()
      .getInitialNotification()
      .then(async remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
            remoteMessage.data,
          );

          await AsyncStorage.getItem(storageConst.userId).then(async userId => {
            if (userId) {
              await AsyncStorage.getItem(storageConst.userData).then(
                async userData => {
                  if (userData) {
                    let user = JSON.parse(userData);
                    store.dispatch(
                      fetchUser({
                        user,
                      }),
                    );

                    await AsyncStorage.getItem(storageConst.language).then(
                      async language => {
                        if (language == null || language == undefined) {
                          await AsyncStorage.setItem(
                            storageConst.language,
                            'en',
                          );
                        } else {
                          setTimeout(() => {
                            this.setState({loading: false});
                            this.props.navigation.navigate(routes.home);
                          }, 300);
                        }
                      },
                    );
                  }

                  if (remoteMessage.data.type == 'P') {
                    this.props.navigation.navigate(routes.productDetail, {
                      productId: remoteMessage.data.type_data,
                    });
                  } else if (remoteMessage.data.type == 'V') {
                    this.props.navigation.navigate(routes.pharmacyDetail, {
                      item: remoteMessage.data.type_data,
                    });
                  } else if (remoteMessage.data.type == 'C') {
                    store.dispatch(
                      fetchCategoryId({
                        categoryId: remoteMessage.data.type_data,
                      }),
                    );
                    this.props.navigation.navigate(routes.productListing);
                  }
                },
              );
            } else {
              await AsyncStorage.getItem(storageConst.guestId).then(
                async value => {
                  let user_id = '0';
                  if (value) {
                    user_id = value;
                  }
                  let user = {
                    api_key: '',
                    birthday: '0',
                    company: '',
                    company_id: '0',
                    ec_location_detail: '',
                    email: '',
                    emirate_id_number: '',
                    fax: '',
                    firstname: 'Guest',
                    insurance_number: '',
                    is_root: 'N',
                    janrain_identifier: '',
                    lang_code: 'en',
                    last_login: '0',
                    last_passwords: '',
                    lastname: 'User',
                    message: 'You have been successfully logged in.',
                    password_change_timestamp: '1',
                    phone: '',
                    purchase_timestamp_from: '0',
                    purchase_timestamp_to: '0',
                    referer: '',
                    responsible_email: '',
                    status: 'A',
                    success: true,
                    tax_exempt: 'N',
                    timestamp: '1622368819',
                    total_products: 0,
                    url: '',
                    user_id: user_id,
                    user_login: '0',
                    user_type: 'C',
                  };
                  await store.dispatch(
                    fetchUser({
                      user: user,
                    }),
                  );
                  await AsyncStorage.setItem(storageConst.guest, '1');
                  await AsyncStorage.setItem(
                    storageConst.userData,
                    JSON.stringify(user),
                  ).then(async () => {
                    await AsyncStorage.setItem(storageConst.userId, '0').then(
                      async () => {
                        if (remoteMessage.data.type == 'P') {
                          this.props.navigation.navigate(routes.productDetail, {
                            productId: remoteMessage.data.type_data,
                          });
                        } else if (remoteMessage.data.type == 'V') {
                          this.props.navigation.navigate(
                            routes.pharmacyDetail,
                            {
                              item: remoteMessage.data.type_data,
                            },
                          );
                        } else if (remoteMessage.data.type == 'C') {
                          store.dispatch(
                            fetchCategoryId({
                              categoryId: remoteMessage.data.type_data,
                            }),
                          );
                          this.props.navigation.navigate(routes.productListing);
                        }
                      },
                    );
                  });
                },
              );
            }
          });
        }
      });
  };

  async requestLocationPermission() {
    // if (Platform.OS == 'ios') Geolocation.requestAuthorization('always');
    try {
      if (Platform.OS == 'ios') {
        Geolocation.requestAuthorization('always');
      } else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Dwaae',
            message: 'Dwaae wants to access to your location ',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the location');
          // alert('You can use the location');
        } else {
          console.log('location permission denied');
          alert('Location permission denied');
        }
      }
    } catch (err) {
      console.warn(err);
    }
  }

  checkTokens = async () => {
    // console.log(this.props.navigation);
    await AsyncStorage.getItem(storageConst.userId).then(async userId => {
      if (userId) {
        await AsyncStorage.getItem(storageConst.userData).then(
          async userData => {
            if (userData) {
              let user = JSON.parse(userData);
              store.dispatch(
                fetchUser({
                  user,
                }),
              );

              await AsyncStorage.getItem(storageConst.language).then(
                async language => {
                  if (language == null || language == undefined) {
                    await AsyncStorage.setItem(storageConst.language, 'en');
                  } else {
                    setTimeout(() => {
                      this.setState({loading: false});
                      this.props.navigation.navigate(routes.home);
                    }, 300);
                  }
                },
              );
            }
          },
        );
      } else {
        this.setState({loading: false});
        await AsyncStorage.getItem(storageConst.firstTime).then(async value => {
          if (value == null || value == undefined) {
            setTimeout(() => {
              this.props.navigation.navigate(routes.languageSelection);
            }, 300);
          } else {
            setTimeout(() => {
              this.props.navigation.navigate(routes.signin);
            }, 300);
          }
        });
      }
    });
  };

  render() {
    const {loading} = this.state;
    return (
      <MainWrapper style={[appStyles.mainContainer, appStyles.center]}>
        <LogoMain size={totalSize(30)} />
      </MainWrapper>
    );
  }
}

export default Splash;
