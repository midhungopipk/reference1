import React, {Fragment, useState, useEffect, Component} from 'react';
import {StatusBar, SafeAreaView, View, Image, Platform} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  LogoMain,
  SmallText,
  LargeText,
  TextInputOTPColored,
  ButtonColored,
  Wrapper,
  TinyText,
  LogoMainImage,
} from '../../../components';
import {
  appImages,
  appStyles,
  routes,
  sizes,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import {useInputValue} from '../../../services/hooks/useInputValue';
import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {sendOTP} from '../../../services/api/user.api';
import Toast from 'react-native-simple-toast';

export default function OTPLogin({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const number = useInputValue('');
  const numberErr = useInputValue('');

  const CheckValidateFn = async () => {
    numberErr.onChange('');

    if (number.value == '') {
      numberErr.onChange('The Number cannot be empty!');
    }

    let reg1 = /^\d{8}$/;
    if (reg1.test(number.value) === false) {
      number.value === ''
        ? numberErr.onChange('The Number cannot be empty!')
        : number.value.length > 8
        ? numberErr.onChange('The Number is badly formatted!')
        : numberErr.onChange('The Number must be atleast 8 characters long!');

      return 1;
    }
    return 0;
  };

  const GotoCodeScreen = async () => {
    CheckValidateFn().then(async response => {
      if (response == 0) {
        setLoading(true);
        //07578769
        let mobileNumber = '+9715' + number.value;
        sendOTP(mobileNumber, onOTPReponse);
        // sendOTP('0557366185', onOTPReponse);
      }
      //0557366185
      //+971503389706
    });
  };

  const onOTPReponse = async res => {
    setLoading(false);
    console.log(res);
    Toast.show(res.message);

    if (res.success) {
      navigation.navigate(routes.otpCode, {response: res});
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Image source={appImages.homeVectorBg} style={styles.vectorBg} />
        <KeyboardAwareScrollView>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
            }
          />
          <View style={styles.selfCenter}>
            <LogoMainImage style={[{width: width(50), height: height(18)}]} />
          </View>
          <Spacer height={sizes.baseMargin} />

          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.activeBottomGradientDark}
            style={styles.borderGradient}>
            <LargeText
              style={[
                appStyles.whiteText,
                styles.selfCenter,
                {marginVertical: totalSize(2)},
              ]}>
              {Translate('Login with OTP')}
            </LargeText>

            <View style={[styles.whiteContainer]}>
              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <>
                  <LargeText style={styles.selfCenter}>
                    {Translate('Welcome back')}
                  </LargeText>
                  <SmallText
                    style={[styles.selfCenter, {color: colors.textGrey}]}>
                    {Translate('Sign in to continue')}
                  </SmallText>
                </>
              ) : (
                <>
                  <LargeText
                    style={[
                      styles.selfCenter,
                      {fontSize: sizes.fontSize.large},
                    ]}>
                    {Translate('Welcome back')}
                  </LargeText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {Translate('Sign in to continue')}
                  </SmallText>
                </>
              )}

              <Spacer
                height={
                  Platform.OS == 'ios'
                    ? sizes.doubleBaseMargin * 1.5
                    : sizes.doubleBaseMargin * 2
                }
              />
              <Wrapper animation={'fadeInLeft'}>
                <TextInputOTPColored
                  placeholder={Translate('Mobile Number')}
                  customVectorIconLeft={'mobile'}
                  iconType={'font-awesome'}
                  keyboardType={'number-pad'}
                  onChangeText={number.onChange}
                  value={number.value}
                  fieldStyle={{fontSize: 13, width: '100%'}}
                />
                {numberErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {numberErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Spacer height={sizes.doubleBaseMargin} />
              <ButtonColored
                isLoading={loading}
                // onPress={() => navigation.navigate(routes.otpCode)}
                onPress={GotoCodeScreen}
                text={Translate('REQUEST OTP')}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.activeBottomIcon},
                ]}
                textStyle={{color: colors.snow}}
              />
              <Spacer height={sizes.baseMargin} />
              <ButtonColored
                onPress={() => navigation.pop()}
                text={Translate('BACK')}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.activeBottomLight},
                ]}
                textStyle={{color: colors.activeBottomIcon}}
              />
              <Spacer height={sizes.doubleBaseMargin * 2} />
            </View>
          </LinearGradient>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
