import Splash from './splash';
import LanguageSelection from './languageSelection';
// import WalkThrough from './walkthrough';
import Login from './login';
import SignUp from './signUp';
import SignUpComplete from './signUpComplete';
import OTPLogin from './loginOtp';
import OTPCode from './otpCode';

export {
  Splash,
  LanguageSelection,
  // WalkThrough,
  Login,
  SignUp,
  SignUpComplete,
  OTPLogin,
  OTPCode,
};
