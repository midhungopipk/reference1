import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Image,
  Platform,
  TouchableOpacity,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {setScreenState} from '../../../Redux/Actions/Navigation';
import {Icon} from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {
  Spacer,
  LogoMain,
  SmallText,
  LargeText,
  ButtonColored,
  Wrapper,
  LogoMainImage,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {sendOTP, verifyOTP} from '../../../services/api/user.api';
import Toast from 'react-native-simple-toast';
import {fetchUser, fetchToken} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import messaging from '@react-native-firebase/messaging';
import {useInputValue} from '../../../services/hooks/useInputValue';

export default function OTPCode({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState('');
  const [response, setResponse] = useState(
    route.params.response ? route.params.response : {},
  );
  //03389706
  const FCMToken = useInputValue('');
  const dispatch = useDispatch();

  useEffect(() => {
    // console.log('response params: ', response, ;
    // add fcm over here
    checkPermission();
  }, [route.params]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      checkPermission();
    });
    return unsubscribe;
  }, [navigation]);

  const checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      getFcmToken();
    } else {
      requestPermission();
    }
  };

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      FCMToken.onChange(fcmToken);
      // this.setState({fcm: fcmToken});
    } else {
    }
  };
  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      // User has authorised
    } catch (error) {
      // User has rejected permissions
    }
  };

  const reSendOtp = async () => {
    setLoading(true);
    // let mobileNumber = response.to;
    sendOTP(response.to, onOTPReponse);
    // sendOTP('0557366185', onOTPReponse);
  };

  const onOTPReponse = async res => {
    setLoading(false);
    console.log(res);
    Toast.show(res.message);
    if (res.success) {
      setResponse(res);
    }
  };

  const handleverifyOtp = async () => {
    let language = await AsyncStorage.getItem(storageConst.language);
    if (language == null || language == undefined) language = 'en';

    setLoading(true);
    if (code.length != 6) {
      Toast.show('Please enter complete OTP');
      setLoading(false);
    } else {
      // if (code == response.otp) {
      verifyOTP(
        {
          to: response.to, //'0557366185', //+9715 //response.to +9715()
          otp: code,
          lang_code: language,
          currency_code: 'AED',
          fcm_token: FCMToken.value,
        },
        onVerificationReponse,
      );
      // } else {
      //   Toast.show("Your OTP doesn't match. Try again!");
      //   setLoading(false);
      // }
    }
  };

  const onVerificationReponse = async res => {
    setLoading(false);
    console.log(res);
    Toast.show(res.message);
    if (res.success) {
      await AsyncStorage.getItem(storageConst.guestId).then(async value => {
        if (value) {
          res.user_id = value;
          await AsyncStorage.removeItem(storageConst.guest);
          await AsyncStorage.removeItem(storageConst.guestId);
        } else {
          await AsyncStorage.removeItem(storageConst.guest);
          await AsyncStorage.removeItem(storageConst.guestId);
        }
        await store.dispatch(
          fetchUser({
            user: res,
          }),
        );
        await AsyncStorage.setItem(storageConst.userId, res.user_id);
        await AsyncStorage.setItem(storageConst.userData, JSON.stringify(res));
        // navigation.navigate(routes.home);
        let token = {
          token: true,
        };
        dispatch(fetchToken(token));
      });
      // Sending Data to redux store
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Image source={appImages.homeVectorBg} style={styles.vectorBg} />
        <KeyboardAwareScrollView>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
            }
          />
          <View style={styles.selfCenter}>
            <LogoMainImage style={[{width: width(50), height: height(18)}]} />
          </View>
          <Spacer height={sizes.baseMargin} />
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.activeBottomGradientDark}
            style={styles.borderGradient}>
            <LargeText
              style={[
                appStyles.whiteText,
                styles.selfCenter,
                {marginVertical: totalSize(2)},
              ]}>
              Login With OTP
            </LargeText>

            <View style={[styles.whiteContainer]}>
              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <>
                  <LargeText style={styles.selfCenter}>
                    {Translate('Welcome back')}
                  </LargeText>
                  <SmallText
                    style={[styles.selfCenter, {color: colors.textGrey}]}>
                    {Translate('Sign in to continue')}
                  </SmallText>
                </>
              ) : (
                <>
                  <LargeText
                    style={[
                      styles.selfCenter,
                      {fontSize: sizes.fontSize.large},
                    ]}>
                    {Translate('Welcome back')}
                  </LargeText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {Translate('Sign in to continue')}
                  </SmallText>
                </>
              )}

              <Spacer
                height={
                  Platform.OS == 'ios'
                    ? sizes.doubleBaseMargin
                    : sizes.doubleBaseMargin * 1.5
                }
              />
              <View
                style={[
                  styles.simpleRow,
                  styles.selfCenter,
                  {
                    flexWrap: 'wrap',
                    marginHorizontal: width(12),
                  },
                ]}>
                <SmallText style={[{fontSize: sizes.fontSize.small}]}>
                  {Translate('We have sent code to your Mobile Number')}
                </SmallText>
              </View>
              <Spacer
                height={
                  Platform.OS == 'ios' ? sizes.baseMargin : sizes.baseMargin
                }
              />
              <Wrapper
                animation={'flipInY'}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                }}>
                <OTPInputView
                  style={styles.otpContainer}
                  pinCount={6}
                  code={code}
                  secureTextEntry
                  onCodeChanged={code => setCode(code)}
                  codeInputFieldStyle={styles.underlineStyleBase}
                  codeInputHighlightStyle={styles.underlineStyleHighLighted}
                />
              </Wrapper>
              <Spacer height={sizes.TinyMargin * 1.3} />
              {/* {Platform.OS == 'ios' ? (
                <View style={[styles.simpleRow, styles.selfCenter]}>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontFamily: fontFamily.appTextRegular,
                      },
                    ]}>
                    Code expires in:
                  </SmallText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        fontFamily: fontFamily.appTextRegular,
                        color: colors.redLight,
                      },
                    ]}>
                    {' '}
                    01:14
                  </SmallText>
                </View>
              ) : (
                <View style={[styles.simpleRow, styles.selfCenter]}>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    Code expires in:
                  </SmallText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {' '}
                    01:14
                  </SmallText>
                </View>
              )}
              <Spacer height={sizes.baseMargin * 1.3} /> */}
              <View
                style={[
                  styles.simpleRow,
                  styles.selfCenter,
                  {
                    flexWrap: 'wrap',
                    marginHorizontal: width(12),
                  },
                ]}>
                <SmallText
                  style={[
                    {fontSize: sizes.fontSize.small, color: colors.textGrey},
                  ]}>
                  {Translate('Not received OTP?')}
                </SmallText>
                <TouchableOpacity onPress={reSendOtp}>
                  <SmallText
                    style={[
                      {
                        color: colors.activeBottomIcon,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {' '}
                    {Translate('Resend OTP')}
                  </SmallText>
                </TouchableOpacity>
              </View>
              <Spacer height={sizes.doubleBaseMargin} />
              <ButtonColored
                isLoading={loading}
                onPress={handleverifyOtp}
                // onPress={() => navigation.navigate(routes.home)}
                text={Translate('LOGIN')}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.activeBottomIcon},
                ]}
                textStyle={{color: colors.snow}}
              />
              <Spacer height={sizes.baseMargin} />
              <ButtonColored
                onPress={() => navigation.pop()}
                text={Translate('BACK')}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.activeBottomLight},
                ]}
                textStyle={{color: colors.activeBottomIcon}}
              />
              <Spacer height={sizes.doubleBaseMargin * 2} />
            </View>
          </LinearGradient>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
