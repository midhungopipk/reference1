import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {setScreenState} from '../../../Redux/Actions/Navigation';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  LogoMain,
  SmallText,
  LargeText,
  TextInputColored,
  ButtonColored,
  Wrapper,
} from '../../../components';
import {appImages, appStyles, routes, sizes} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';

export default function SignUpComplete({navigation, route}) {
  const [loading, setLoading] = useState(true);

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView
        style={[styles.container, {backgroundColor: colors.greenLighter}]}>
        <KeyboardAwareScrollView>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
            }
          />
          <View style={styles.selfCenter}>
            <LogoMain
              size={Platform.OS == 'ios' ? totalSize(14) : totalSize(16)}
            />
          </View>
          <Spacer height={sizes.doubleBaseMargin} />

          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.authGradientLight}
            style={styles.borderGradient}>
            <Wrapper
              style={{
                flexDirection: 'row',
                position: 'absolute',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate(routes.signin)}
                style={[
                  styles.container,
                  {paddingTop: totalSize(2), backgroundColor: 'transparent'},
                ]}>
                <LargeText style={appStyles.whiteText}>Login</LargeText>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.container, {backgroundColor: 'transparent'}]}>
                <LinearGradient
                  start={{x: 0, y: 1}}
                  end={{x: 1, y: 1}}
                  colors={colors.authGradient}
                  style={[
                    appStyles.center,
                    {
                      borderTopLeftRadius: totalSize(5),
                      width: '100%',
                      paddingTop: totalSize(2),
                      paddingBottom: totalSize(8),
                    },
                  ]}>
                  <LargeText style={appStyles.whiteText}>Sign Up</LargeText>
                </LinearGradient>
              </TouchableOpacity>
            </Wrapper>

            <View style={[styles.whiteContainer, {marginTop: totalSize(6)}]}>
              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <>
                  <LargeText style={styles.selfCenter}>
                    Create Account
                  </LargeText>
                  <SmallText
                    style={[styles.selfCenter, {color: colors.textGrey}]}>
                    Sign up to get started!
                  </SmallText>
                </>
              ) : (
                <>
                  <LargeText
                    style={[
                      styles.selfCenter,
                      {fontSize: sizes.fontSize.large},
                    ]}>
                    Create Account
                  </LargeText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    Sign up to get started!
                  </SmallText>
                </>
              )}

              <Spacer
                height={
                  Platform.OS == 'ios'
                    ? sizes.doubleBaseMargin * 1.3
                    : sizes.doubleBaseMargin * 2
                }
              />
              <Wrapper animation={'fadeInLeft'}>
                <TextInputColored
                  placeholder={'Password'}
                  customRightIcon={appImages.eye}
                  customIconLeft={appImages.lock}
                />
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInRight'}>
                <TextInputColored
                  placeholder={'Re-type Password'}
                  customIconLeft={appImages.lock}
                  customRightIcon={appImages.eye}
                  secureTextEntry
                />
              </Wrapper>
              <Spacer height={sizes.doubleBaseMargin * 1.5} />
              <View
                style={[
                  styles.simpleRow,
                  styles.selfCenter,
                  {
                    flexWrap: 'wrap',
                    marginHorizontal: width(12),
                  },
                ]}>
                <SmallText
                  style={[
                    {fontSize: sizes.fontSize.small, color: colors.textGrey},
                  ]}>
                  By pressing "Sign Up" you agree to our
                </SmallText>
                <TouchableOpacity>
                  <SmallText
                    style={[
                      {
                        color: colors.blueText,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    terms & conditions
                  </SmallText>
                </TouchableOpacity>
              </View>
              <Spacer height={sizes.baseMargin * 1.5} />
              <ButtonColored
                onPress={() => navigation.navigate(routes.home)}
                text={'SIGN UP'}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.statusBarColor},
                ]}
                textStyle={{color: colors.snow}}
              />
              <Spacer height={sizes.doubleBaseMargin * 2} />
            </View>
          </LinearGradient>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
