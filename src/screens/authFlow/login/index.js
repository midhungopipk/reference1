import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  Alert,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  FlatList,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';

/* Api Calls */
import {loginUser, forgotPassword} from '../../../services/api/user.api';
import {parseJwt} from '../../../services/api/utilities';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchUser, fetchToken} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {useInputValue} from '../../../services/hooks/useInputValue';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {
  Spacer,
  LogoMain,
  SmallText,
  LargeText,
  TextInputColored,
  ButtonColored,
  Wrapper,
  TinyText,
  LoadingCardCustom,
  NoInternetCard,
  LogoMainImage,
} from '../../../components';
import {
  appImages,
  appStyles,
  fontFamily,
  routes,
  sizes,
  storageConst,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';
import messaging from '@react-native-firebase/messaging';

export default function Login({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const [passwordLoading, setPasswordLoading] = useState(false);
  const [guestLoading, setGuestLoading] = useState(false);
  const [otpLoading, setOtpLoading] = useState(false);
  const [eye, setEye] = useState(true);
  const dispatch = useDispatch();

  const FCMToken = useInputValue('');
  const Email = useInputValue('');
  const Password = useInputValue('');
  const EmailErr = useInputValue('');
  const PasswordErr = useInputValue('');

  useEffect(() => {
    checkPermission();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      checkPermission();
    });
    return unsubscribe;
  }, [navigation]);

  //Check Permissions
  const checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      getFcmToken();
    } else {
      requestPermission();
    }
  };

  //Get FCM Token
  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      FCMToken.onChange(fcmToken);
      // this.setState({fcm: fcmToken});
    } else {
    }
  };

  //Request Permission
  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      // User has authorised
    } catch (error) {
      // User has rejected permissions
    }
  };

  //Check Email Validation
  const EmailCheckValidateFn = async () => {
    EmailErr.onChange('');
    // PasswordErr.onChange('');

    if (Email.value == '') {
      EmailErr.onChange('The Email cannot be empty!');
    }
    const reg2 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg2.test(Email.value) === false) {
      Email.value !== undefined && Email.value !== ''
        ? EmailErr.onChange(
            'The email is badly formatted, must include @ and . in the end',
          )
        : EmailErr.onChange('The email cannot be empty');
      return 1;
    }
    return 0;
  };

  //Handle Reset Password
  const handleResetPassword = async () => {
    EmailCheckValidateFn().then(async response => {
      if (response == 0) {
        let language = await AsyncStorage.getItem(storageConst.language);
        if (language == null || language == undefined) language = 'en';
        forgotPassword(
          {
            forget_password: true,
            email: Email.value,
            lang_code: language,
            currency_code: 'AED',
          },
          onResetPasswordReponse,
        );
      }
    });
  };

  //On Reset Password API Response
  const onResetPasswordReponse = async res => {
    console.log('reset password api: ', res);
    if (res.success) {
      setPasswordLoading(true);
      setTimeout(() => {
        // Toast.show(res.message);
        setPasswordLoading(false);
      }, 4000);
    } else {
      Toast.show(res.message);
    }
  };

  //Check Validation Function
  const CheckValidateFn = async () => {
    EmailErr.onChange('');
    PasswordErr.onChange('');

    if (Password.value == '' && Email.value == '') {
      EmailErr.onChange('The Email cannot be empty!');
      PasswordErr.onChange('The password cannot be empty!');
    }

    const reg2 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg2.test(Email.value) === false) {
      Email.value !== undefined && Email.value !== ''
        ? EmailErr.onChange(
            'The email is badly formatted, must include @ and . in the end',
          )
        : EmailErr.onChange('The email cannot be empty');
      return 1;
    }

    let reg1 = /^[\w\d@$!%*#?&]{4,30}$/;
    if (reg1.test(Password.value) === false) {
      Password.value === ''
        ? PasswordErr.onChange('The password cannot be empty!')
        : Password.value.length > 4
        ? PasswordErr.onChange('The password is badly formatted!')
        : PasswordErr.onChange(
            'The password must be atleast 6 characters long!',
          );

      return 1;
    }
    return 0;
  };

  //Navigate to Home Screen
  const GoToHome = async () => {
    console.log('Login called');
    CheckValidateFn().then(async response => {
      if (response == 0) {
        setLoading(true);
        await AsyncStorage.getItem(storageConst.guestId).then(async value => {
          if (value) {
            console.log('guest id:', value);
            console.log('fcm:', FCMToken.value);
            loginUser(
              {
                gu_id: value,
                user_login: Email.value,
                password: Password.value,
                fcm_token: FCMToken.value,
              },
              onLoginReponse,
            );
            await AsyncStorage.removeItem(storageConst.guest);
            await AsyncStorage.removeItem(storageConst.guestId);
          } else {
            await AsyncStorage.removeItem(storageConst.guest);
            await AsyncStorage.removeItem(storageConst.guestId);
            console.log('fcm:', FCMToken.value);
            loginUser(
              {
                user_login: Email.value,
                password: Password.value,
                fcm_token: FCMToken.value,
                gu_id: '',
              },
              onLoginReponse,
            );
          }
        });
      }
    });
  };

  //On Login API Response
  const onLoginReponse = async res => {
    setLoading(false);
    console.log(res);
    Toast.show(res.message);
    if (res.success) {
      await messaging()
        .subscribeToTopic('HomedevoApp')
        .then(() => console.log('Subscribed to topic!'));
      await store.dispatch(
        fetchUser({
          user: res,
        }),
      );
      await AsyncStorage.setItem(storageConst.userId, res.user_id);
      await AsyncStorage.setItem(storageConst.userData, JSON.stringify(res));
      let token = {
        token: true,
      };
      dispatch(fetchToken(token));
      // navigation.navigate(routes.homeLanding);
    }
  };

  //Handle Continue As Guest Press
  const continueAsGuest = async () => {
    setGuestLoading(true);
    await AsyncStorage.getItem(storageConst.guestId).then(async value => {
      console.log('value of user id in guest: ', value);
      let user_id = '0';
      if (value) {
        user_id = value;
      }
      let user = {
        api_key: '',
        birthday: '0',
        company: '',
        company_id: '0',
        ec_location_detail: '',
        email: '',
        emirate_id_number: '',
        fax: '',
        firstname: 'Guest',
        insurance_number: '',
        is_root: 'N',
        janrain_identifier: '',
        lang_code: 'en',
        last_login: '0',
        last_passwords: '',
        lastname: 'User',
        message: 'You have been successfully logged in.',
        password_change_timestamp: '1',
        phone: '',
        purchase_timestamp_from: '0',
        purchase_timestamp_to: '0',
        referer: '',
        responsible_email: '',
        status: 'A',
        success: true,
        tax_exempt: 'N',
        timestamp: '1622368819',
        total_products: 0,
        url: '',
        user_id: user_id,
        user_login: '0',
        user_type: 'C',
      };
      await store.dispatch(
        fetchUser({
          user: user,
        }),
      );
      await AsyncStorage.setItem(storageConst.guest, '1');
      await AsyncStorage.setItem(
        storageConst.userData,
        JSON.stringify(user),
      ).then(async () => {
        await AsyncStorage.setItem(storageConst.userId, '0').then(async () => {
          console.log('fine till here');
          setGuestLoading(false);
          let token = {
            token: true,
          };
          dispatch(fetchToken(token));
          // store.dispatch(
          //   fetchToken({
          //     token: true,
          //   }),
          // );
          // navigation.navigate(routes.home);
        });
      });
      setGuestLoading(false);
    });
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      {/* <Image source={appImages.homeVectorBg} style={styles.vectorBg} /> */}
      {passwordLoading ? (
        <LoadingCardCustom
          gif={appImages.passwordGif}
          text={Translate(
            'Password recovery instructions has been sent to your email',
          )}
        />
      ) : (
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.snow}]}>
          <Image source={appImages.homeVectorBg} style={styles.vectorBg} />
          <KeyboardAwareScrollView>
            <Spacer
              height={
                Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
              }
            />
            <View style={styles.selfCenter}>
              <LogoMainImage style={[{width: width(50), height: height(18)}]} />
            </View>
            <Spacer height={sizes.baseMargin} />
            <LinearGradient
              start={{x: 0, y: 1}}
              end={{x: 1, y: 1}}
              colors={colors.activeBottomGradientLight}
              style={styles.borderGradient}>
              <Wrapper
                style={{
                  flexDirection: 'row',
                  position: 'absolute',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity
                  style={[styles.container, {backgroundColor: 'transparent'}]}>
                  <LinearGradient
                    start={{x: 0, y: 1}}
                    end={{x: 1, y: 1}}
                    colors={colors.activeBottomGradientDark}
                    style={[
                      appStyles.center,
                      {
                        borderTopLeftRadius: totalSize(5),
                        borderTopRightRadius: totalSize(8),
                        width: '100%',
                        paddingTop: totalSize(2),
                        paddingBottom: totalSize(8),
                      },
                    ]}>
                    <LargeText style={appStyles.whiteText}>
                      {Translate('Login')}
                    </LargeText>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate(routes.signup)}
                  style={[
                    styles.container,
                    {paddingTop: totalSize(2), backgroundColor: 'transparent'},
                  ]}>
                  <LargeText style={{color: colors.activeBottomIcon}}>
                    {Translate('Sign Up')}
                  </LargeText>
                </TouchableOpacity>
              </Wrapper>

              <View style={[styles.whiteContainer, {marginTop: totalSize(6)}]}>
                <Spacer height={sizes.doubleBaseMargin} />
                {Platform.OS == 'ios' ? (
                  <>
                    <LargeText style={styles.selfCenter}>
                      {Translate('Welcome back')}
                    </LargeText>
                    <SmallText
                      style={[styles.selfCenter, {color: colors.textGrey}]}>
                      {Translate('Sign in to continue')}
                    </SmallText>
                  </>
                ) : (
                  <>
                    <LargeText
                      style={[
                        styles.selfCenter,
                        {fontSize: sizes.fontSize.large},
                      ]}>
                      {Translate('Welcome back')}
                    </LargeText>
                    <SmallText
                      style={[
                        styles.selfCenter,
                        {
                          color: colors.textGrey,
                          fontSize: sizes.fontSize.small,
                        },
                      ]}>
                      {Translate('Sign in to continue')}
                    </SmallText>
                  </>
                )}

                <Spacer height={sizes.doubleBaseMargin} />
                <Wrapper animation={'fadeInLeft'}>
                  <TextInputColored
                    placeholder={Translate('Email')}
                    customIconLeft={appImages.envelope}
                    value={Email.value}
                    onChangeText={Email.onChange}
                    autoCapitalize="none"
                  />
                  {EmailErr.value != '' ? (
                    <TinyText
                      style={[appStyles.textRed, appStyles.marginLeft5]}>
                      {EmailErr.value}
                    </TinyText>
                  ) : null}
                </Wrapper>
                <Spacer height={sizes.baseMargin * 1.3} />
                <Wrapper animation={'fadeInRight'}>
                  <TextInputColored
                    placeholder={Translate('Password')}
                    customIconLeft={appImages.lock}
                    customRightIcon={appImages.eye}
                    secureTextEntry={eye}
                    value={Password.value}
                    onChangeText={Password.onChange}
                    autoCapitalize="none"
                    onEyePress={() => setEye(!eye)}
                  />
                  {PasswordErr.value != '' ? (
                    <TinyText
                      style={[appStyles.textRed, appStyles.marginLeft5]}>
                      {PasswordErr.value}
                    </TinyText>
                  ) : null}
                </Wrapper>
                <Spacer height={sizes.baseMargin * 1.2} />
                <View
                  style={[
                    styles.row,
                    styles.selfCenter,
                    {paddingHorizontal: width(13)},
                  ]}>
                  <TouchableOpacity onPress={handleResetPassword}>
                    <SmallText
                      style={[
                        {
                          // color: colors.blueText,
                          fontSize: sizes.fontSize.small,
                          fontFamily: fontFamily.appTextBold,
                        },
                      ]}>
                      {Translate('Forgot Password?')}
                    </SmallText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => navigation.navigate(routes.otpLogin)}>
                    <SmallText
                      style={[
                        {
                          color: colors.activeBottomIcon,
                          fontSize: sizes.fontSize.small,
                          fontFamily: fontFamily.appTextBold,
                        },
                      ]}>
                      {Translate('Login with OTP')}
                    </SmallText>
                  </TouchableOpacity>
                </View>
                <Spacer height={sizes.doubleBaseMargin} />
                <ButtonColored
                  isLoading={loading}
                  onPress={() => {
                    GoToHome();
                  }}
                  text={Translate('LOGIN')}
                  buttonStyle={[
                    styles.customButtonStyle,
                    {backgroundColor: colors.activeBottomIcon},
                  ]}
                  textStyle={{color: colors.snow}}
                />
                <Spacer height={sizes.baseMargin} />
                {/* <ButtonColored
                  onPress={() => navigation.navigate(routes.otpLogin)}
                  text={'LOGIN WITH OTP'}
                  buttonStyle={[
                    styles.customButtonStyle,
                    {backgroundColor: colors.gradient1},
                  ]}
                  textStyle={{color: colors.snow}}
                />
                <Spacer height={sizes.baseMargin} /> */}
                {/* <ButtonColored
                  isLoading={guestLoading}
                  onPress={continueAsGuest}
                  text={Translate('CONTINUE AS GUEST')}
                  buttonStyle={[
                    styles.customButtonStyle,
                    {backgroundColor: colors.activeBottomLight},
                  ]}
                  textStyle={{color: colors.activeBottomIcon}}
                /> */}
                <Spacer height={sizes.doubleBaseMargin * 2} />
              </View>
            </LinearGradient>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )}
    </Fragment>
  );
}
