import React, {Fragment, Component, useState} from 'react';
import {View, Text, Image, Platform} from 'react-native';
import {
  LogoMain,
  Spacer,
  LanguageButton,
  TinyTitle,
  LogoMainImage,
} from '../../../components';
import {
  appImages,
  appStyles,
  colors,
  sizes,
  Translate,
} from '../../../services';
import {height, totalSize, width} from 'react-native-dimension';
import AsyncStorage from '@react-native-community/async-storage';
import {routes, storageConst} from '../../../services/constants';
import {fetchLanguage} from '../../../Redux/Actions/index';
import {useSelector, useDispatch} from 'react-redux';
import {ImageBackground} from 'react-native';
import RNRestart from 'react-native-restart';

export default function LanguageSelection({navigation, route}) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  return (
    <Fragment>
      <ImageBackground
        style={{
          width: width(100),
          height: height(100),
          backgroundColor: 'rgba(40, 100, 165, 0.6)',
        }}
        source={appImages.bg}>
        {/* <KeyboardAwareScrollView> */}
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spacer height={sizes.doubleBaseMargin} />
          <Image
            source={appImages.vector}
            style={{
              position: 'absolute',
              top: Platform.OS == 'ios' ? 0 : height(3),
              right: 0,
              width: width(90),
              height: height(40),
              alignSelf: 'flex-end',
              resizeMode: 'stretch',
            }}
          />
          {/* <LogoMain size={totalSize(15)} animation="fadeIn" duration={500} /> */}
          <Spacer
            height={
              Platform.OS == 'ios'
                ? sizes.doubleBaseMargin * 5
                : sizes.doubleBaseMargin * 3
            }
          />

          <LogoMainImage
            logo={appImages.logoWhite}
            style={{width: width(70), height: height(25)}}
          />
          <Spacer height={sizes.baseMargin} />
        </View>
        <View
          style={[
            appStyles.center,
            {
              width: width(100),
              borderTopLeftRadius: totalSize(5),
              borderTopRightRadius: totalSize(5),
              backgroundColor: colors.activeBottomIcon,
              borderTopWidth: 1,
              borderLeftWidth: 1,
              borderRightWidth: 1,
              borderColor: colors.snow,
            },
          ]}>
          <Spacer height={sizes.baseMargin} />
          <TinyTitle style={appStyles.textWhite}>Select Language</TinyTitle>
          <Spacer height={sizes.baseMargin} />
          <LanguageButton
            onPress={async () => {
              await AsyncStorage.setItem(storageConst.firstTime, '1');
              await AsyncStorage.setItem(storageConst.language, 'en');
              setTimeout(() => {
                RNRestart.Restart();
              }, 100);
            }}
            isLoading={loading}
            text={'ENGLISH'}
            buttonStyle={[
              {
                backgroundColor: colors.snow,
                borderRadius: totalSize(10),
                marginHorizontal: width(12),
                height: Platform.OS == 'ios' ? height(6.5) : height(8),
                width: width(70),
              },
            ]}
            textStyle={{color: colors.activeBottomIcon}}
            customIcon={appImages.englishFlag}
          />
          <Spacer height={sizes.baseMargin} />
          <LanguageButton
            onPress={async () => {
              await AsyncStorage.setItem(storageConst.firstTime, '1');
              await AsyncStorage.setItem(storageConst.language, 'ar');
              setTimeout(() => {
                RNRestart.Restart();
              }, 100);
              // let language = {
              //   language: true,
              // };
              // dispatch(fetchLanguage(language));
              // navigation.navigate(routes.walkthrough);
            }}
            isLoading={loading}
            text={'عربى'}
            buttonStyle={[
              {
                backgroundColor: colors.activeBottomIcon,
                borderRadius: totalSize(10),
                marginHorizontal: width(12),
                height: Platform.OS == 'ios' ? height(6.5) : height(8),
                width: width(70),
                borderWidth: 2,
                borderColor: colors.snow,
              },
            ]}
            textStyle={{color: colors.snow}}
            customIcon={appImages.arabicFlag}
          />
          <Spacer height={sizes.doubleBaseMargin} />
        </View>
      </ImageBackground>
    </Fragment>
  );
}
