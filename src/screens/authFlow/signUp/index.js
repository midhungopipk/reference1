import React, {Fragment, useState, useEffect, Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';
import styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors} from '../../../services/utilities/colors/index';
import {width, height, totalSize} from 'react-native-dimension';
import {setScreenState} from '../../../Redux/Actions/Navigation';
import {Icon} from 'react-native-elements';
import {
  Spacer,
  LogoMain,
  SmallText,
  LargeText,
  TextInputColored,
  ButtonColored,
  Wrapper,
  TinyText,
  LogoMainImage,
} from '../../../components';
import {
  appImages,
  appStyles,
  routes,
  sizes,
  storageConst,
  Translate,
} from '../../../services';
import LinearGradient from 'react-native-linear-gradient';

import AsyncStorage from '@react-native-community/async-storage';
/* Api Calls */
import {loginUser, createUser} from '../../../services/api/user.api';
import {parseJwt} from '../../../services/api/utilities';

/* Redux */
import {useSelector, useDispatch} from 'react-redux';
import {fetchUser, fetchToken} from '../../../Redux/Actions/index';
import {store} from '../../../Redux/configureStore';
import Toast from 'react-native-simple-toast';
import {useInputValue} from '../../../services/hooks/useInputValue';
import messaging from '@react-native-firebase/messaging';
import {Linking} from 'react-native';

export default function SignUp({navigation, route}) {
  const [loading, setLoading] = useState(false);
  const [eye1, setEye1] = useState(true);
  const [eye2, setEye2] = useState(true);

  const FCMToken = useInputValue('');
  const FirstName = useInputValue('');
  const LastName = useInputValue('');
  const MobileNumber = useInputValue('');
  const Email = useInputValue('');
  const Password = useInputValue('');
  const RPassword = useInputValue('');
  const FirstNameErr = useInputValue('');
  const LastNameErr = useInputValue('');
  const MobileNumberErr = useInputValue('');
  const EmailErr = useInputValue('');
  const PasswordErr = useInputValue('');
  const RPasswordErr = useInputValue('');

  const dispatch = useDispatch();

  useEffect(() => {
    checkPermission();
  }, []);

  //Check Permissions
  const checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      getFcmToken();
    } else {
      requestPermission();
    }
  };

  //Get FCM Token
  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      FCMToken.onChange(fcmToken);
      // this.setState({fcm: fcmToken});
    } else {
    }
  };

  //Request Permissions
  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      // User has authorised
    } catch (error) {
      // User has rejected permissions
    }
  };

  //Check Validate Function
  const CheckValidateFn = async () => {
    FirstNameErr.onChange('');
    LastNameErr.onChange('');
    MobileNumberErr.onChange('');
    EmailErr.onChange('');
    PasswordErr.onChange('');
    RPasswordErr.onChange('');

    let reg5 = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
    if (reg5.test(FirstName.value) === false) {
      FirstName.value === ''
        ? FirstNameErr.onChange('The First name cannot be empty!')
        : FirstName.value.length > 3
        ? FirstNameErr.onChange('The First name is badly formatted!')
        : FirstNameErr.onChange(
            'The first name should be atleast 3 characters!',
          );
      return 1;
    }

    let reg6 = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
    if (reg6.test(LastName.value) === false) {
      LastName.value === ''
        ? LastNameErr.onChange('The Last name cannot be empty!')
        : FirstName.value.length > 2
        ? LastNameErr.onChange('The Last name is badly formatted!')
        : LastNameErr.onChange('The Last name should be atleast 3 characters!');
      return 1;
    }

    // const reg3 = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    // if (reg3.test(MobileNumber.value) === false) {
    //   MobileNumber.value !== undefined && MobileNumber.value !== ''
    //     ? MobileNumberErr.onChange('The Mobile number is badly formatted!')
    //     : MobileNumberErr.onChange('The Mobile number cannot be empty!');
    //   return 1;
    // }

    const reg2 =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg2.test(Email.value) === false) {
      Email.value !== undefined && Email.value !== ''
        ? EmailErr.onChange(
            'The email is badly formatted, must include @ and . in the end',
          )
        : EmailErr.onChange('The email cannot be empty');
      return 1;
    }

    // let reg1 = /^[\w\d@$!%*#?&]{6,30}$/;
    let reg1 = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    if (reg1.test(Password.value) === false) {
      Password.value === ''
        ? PasswordErr.onChange('The password cannot be empty!')
        : Password.value.length > 6
        ? PasswordErr.onChange(
            'The password must contain 1 letter, 1 number and 1 special character',
          )
        : PasswordErr.onChange(
            'The password must be atleast 8 characters long!',
          );
      return 1;
    }

    // let reg4 = /^[\w\d@$!%*#?&]{6,30}$/;
    if (Password.value !== RPassword.value) {
      RPassword.value === ''
        ? RPasswordErr.onChange('The Confirm password cannot be empty!')
        : Password.value.length > 6
        ? RPasswordErr.onChange(
            'The password and repeat password does not match!',
          )
        : RPasswordErr.onChange('The password is badly formatted!');
      return 1;
    }
    return 0;
  };

  //Handle Sign Up Press
  const HandleSignUp = async () => {
    CheckValidateFn().then(async response => {
      if (response == 0) {
        setLoading(true);
        await AsyncStorage.getItem(storageConst.guestId).then(async value => {
          if (value) {
            createUser(
              {
                gu_id: value,
                firstname: FirstName.value,
                lastname: LastName.value,
                phone: MobileNumber.value,
                email: Email.value,
                password1: Password.value,
                password2: RPassword.value,
                fcm_token: FCMToken,
              },
              onSignupReponse,
            );
            await AsyncStorage.removeItem(storageConst.guest);
            await AsyncStorage.removeItem(storageConst.guestId);
          } else {
            await AsyncStorage.removeItem(storageConst.guest);
            await AsyncStorage.removeItem(storageConst.guestId);
            createUser(
              {
                firstname: FirstName.value,
                lastname: LastName.value,
                phone: MobileNumber.value,
                email: Email.value,
                password1: Password.value,
                password2: RPassword.value,
              },
              onSignupReponse,
            );
          }
        });
      }
    });
  };

  //Get Sign Up API Response
  const onSignupReponse = async res => {
    setLoading(false);
    console.log(res);
    if (res.success) {
      await messaging()
        .subscribeToTopic('HomedevoApp')
        .then(() => console.log('Subscribed to topic!'));
      await store.dispatch(
        fetchUser({
          user: res,
        }),
      );
      await AsyncStorage.setItem(storageConst.userId, res.user_id);
      await AsyncStorage.setItem(storageConst.userData, JSON.stringify(res));
      Toast.show('You have been successfully Signed Up!');
      // navigation.navigate(routes.home);
      let token = {
        token: true,
      };
      dispatch(fetchToken(token));
    } else {
      Toast.show(res.message);
    }
  };

  return (
    <Fragment>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={colors.activeBottomIcon}
      />
      <SafeAreaView style={[styles.container, {backgroundColor: colors.snow}]}>
        <Image source={appImages.homeVectorBg} style={styles.vectorBg} />
        <KeyboardAwareScrollView>
          <Spacer
            height={
              Platform.OS == 'ios' ? sizes.baseMargin : sizes.doubleBaseMargin
            }
          />
          <View style={styles.selfCenter}>
            <LogoMainImage style={[{width: width(50), height: height(18)}]} />
          </View>
          <Spacer height={sizes.baseMargin} />

          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 1, y: 1}}
            colors={colors.activeBottomGradientLight}
            style={styles.borderGradient}>
            <Wrapper
              style={{
                flexDirection: 'row',
                position: 'absolute',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate(routes.signin)}
                style={[
                  styles.container,
                  {paddingTop: totalSize(2), backgroundColor: 'transparent'},
                ]}>
                <LargeText style={{color: colors.activeBottomIcon}}>
                  {Translate('Login')}
                </LargeText>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.container, {backgroundColor: 'transparent'}]}>
                <LinearGradient
                  start={{x: 0, y: 1}}
                  end={{x: 1, y: 1}}
                  colors={colors.activeBottomGradientDark}
                  style={[
                    appStyles.center,
                    {
                      borderTopLeftRadius: totalSize(8),
                      width: '100%',
                      paddingTop: totalSize(2),
                      paddingBottom: totalSize(8),
                    },
                  ]}>
                  <LargeText style={appStyles.whiteText}>
                    {Translate('Sign Up')}
                  </LargeText>
                </LinearGradient>
              </TouchableOpacity>
            </Wrapper>

            <View style={[styles.whiteContainer, {marginTop: totalSize(6)}]}>
              <Spacer height={sizes.doubleBaseMargin} />
              {Platform.OS == 'ios' ? (
                <>
                  <LargeText style={styles.selfCenter}>
                    {Translate('Create Account')}
                  </LargeText>
                  <SmallText
                    style={[styles.selfCenter, {color: colors.textGrey}]}>
                    {Translate('Sign up to get started')}
                  </SmallText>
                </>
              ) : (
                <>
                  <LargeText
                    style={[
                      styles.selfCenter,
                      {fontSize: sizes.fontSize.large},
                    ]}>
                    {Translate('Create Account')}
                  </LargeText>
                  <SmallText
                    style={[
                      styles.selfCenter,
                      {
                        color: colors.textGrey,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {Translate('Sign up to get started')}
                  </SmallText>
                </>
              )}

              <Spacer height={sizes.doubleBaseMargin * 1.3} />
              <Wrapper animation={'fadeInLeft'}>
                <TextInputColored
                  placeholder={Translate('First Name')}
                  customIconLeft={appImages.user}
                  value={FirstName.value}
                  onChangeText={FirstName.onChange}
                  // autoCapitalize="none"
                />

                {FirstNameErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {FirstNameErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>

              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInRight'}>
                <TextInputColored
                  placeholder={Translate('Last Name')}
                  customIconLeft={appImages.user}
                  value={LastName.value}
                  onChangeText={LastName.onChange}
                  // autoCapitalize="none"
                />
                {LastNameErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {LastNameErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInLeft'}>
                <TextInputColored
                  placeholder={Translate('Mobile Number')}
                  customIconLeft={appImages.mobile}
                  keyboardType={'phone-pad'}
                  value={MobileNumber.value}
                  onChangeText={MobileNumber.onChange}
                />
                {MobileNumberErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {MobileNumberErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInRight'}>
                <TextInputColored
                  placeholder={Translate('Email')}
                  customIconLeft={appImages.message}
                  keyboardType={'email-address'}
                  value={Email.value}
                  onChangeText={Email.onChange}
                  autoCapitalize="none"
                />
                {EmailErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {EmailErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInLeft'}>
                <TextInputColored
                  placeholder={Translate('Password')}
                  customRightIcon={appImages.eye}
                  customIconLeft={appImages.lock}
                  secureTextEntry={eye1}
                  value={Password.value}
                  onChangeText={Password.onChange}
                  autoCapitalize="none"
                  onEyePress={() => setEye1(!eye1)}
                />
                {PasswordErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {PasswordErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.3} />
              <Wrapper animation={'fadeInRight'}>
                <TextInputColored
                  placeholder={Translate('Confirm Password')}
                  customIconLeft={appImages.lock}
                  customRightIcon={appImages.eye}
                  secureTextEntry={eye2}
                  value={RPassword.value}
                  onChangeText={RPassword.onChange}
                  autoCapitalize="none"
                  onEyePress={() => setEye2(!eye2)}
                />
                {RPasswordErr.value != '' ? (
                  <TinyText style={[appStyles.textRed, appStyles.marginLeft5]}>
                    {RPasswordErr.value}
                  </TinyText>
                ) : null}
              </Wrapper>
              <Spacer height={sizes.baseMargin * 1.5} />
              <View
                style={[
                  styles.simpleRow,
                  styles.selfCenter,
                  {
                    flexWrap: 'wrap',
                    marginHorizontal: width(12),
                  },
                ]}>
                <SmallText
                  style={[
                    {fontSize: sizes.fontSize.small, color: colors.textGrey},
                  ]}>
                  {Translate('By pressing Sign Up you agree to our')}
                </SmallText>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('https://homedevo.com/terms-and-conditions')
                  }>
                  <SmallText
                    style={[
                      {
                        color: colors.activeBottomIcon,
                        fontSize: sizes.fontSize.small,
                      },
                    ]}>
                    {Translate('terms and conditions')}
                  </SmallText>
                </TouchableOpacity>
              </View>
              <Spacer height={sizes.baseMargin * 1.5} />
              <ButtonColored
                isLoading={loading}
                onPress={() => {
                  // navigation.navigate(routes.signin)
                  HandleSignUp();
                }}
                text={'SIGN UP'}
                buttonStyle={[
                  styles.customButtonStyle,
                  {backgroundColor: colors.activeBottomIcon},
                ]}
                textStyle={{color: colors.snow}}
              />
              <Spacer height={sizes.doubleBaseMargin * 2} />
            </View>
          </LinearGradient>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </Fragment>
  );
}
